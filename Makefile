ISO_OUTPUT=power.iso
SYSTEM_ROOT=$$SYSROOT

AS=nasm
CC=gcc
LD=ld

ifeq ($(CC),clang)
CC+=-target x86_64-pc-unknown-elf
endif

ASFLAGS=-g -F dwarf -f elf64
CFLAGS=-g -mgeneral-regs-only -c -ffreestanding -fno-stack-protector -nostdlib \
	-fno-stack-check -fno-lto -fno-stack-protector -m64 \
	-march=x86-64 -mabi=sysv -mno-80387 -mno-red-zone -Wno-unused-command-line-argument \
	-Wno-address-of-packed-member -Wall -Wextra -std=gnu17 -fvisibility=default \
	-Wall -Wextra -D_POWER_SOURCE

LDFLAGS=-nostdlib -Wl,-m -Wl,elf_x86_64 -Wl,-z -Wl,max-page-size=0x1000

all: iso/$(ISO_OUTPUT)

clean:
	rm -rf build/
	mkdir build

MODULES=kernel drivers auxfs
$(foreach m,$(MODULES),$(eval include $(m)/recipes.mk))

# Files which should be copied over to
# SYSTEM_ROOT.
SRT_CPYFILES=$(KERNEL) limine/limine-bios.sys   limine/limine-bios-cd.bin limine.cfg limine/limine-uefi-cd.bin \
		$(KERNEL_OUTPUT) $(AUXFS_PATH)

install-into-sysroot: $(KERNEL_OUTPUT) $(AUXFS_PATH)
	cp $(SRT_CPYFILES) $(SYSTEM_ROOT)

iso/root:
	mkdir -p iso/root/EFI/BOOT

# Build an ISO image based on what
# is inside SYSTEM_ROOT.
iso/$(ISO_OUTPUT): install-into-sysroot iso/root $(KERNEL_OUTPUT) $(AUXFS_PATH) FORCE
	cp -v limine/BOOTX64.EFI $(SYSTEM_ROOT)/EFI/BOOT/
	xorriso -as mkisofs -b limine-bios-cd.bin \
			-no-emul-boot -boot-load-size 4 -boot-info-table \
			 --protective-msdos-label \
			 --efi-boot limine-uefi-cd.bin \
			 -efi-boot-part --efi-boot-image \
			iso/root/ -o iso/$(ISO_OUTPUT)
	./limine/limine bios-install iso/$(ISO_OUTPUT)

# Quickly test the ISO image inside QEMU.
qemu: iso/$(ISO_OUTPUT)
	qemu-system-x86_64 \
		-m 256  -no-reboot -drive format=raw,media=disk,file=disk/power_ext2.img \
		-smp 1 -usb -vga std -d guest_errors -no-reboot -no-shutdown \
		-boot d -debugcon stdio -accel tcg -S -s

disk: install-into-sysroot $(KERNEL_OUTPUT) $(AUXFS_PATH)
	./tools/diskcreat.sh

FORCE:

