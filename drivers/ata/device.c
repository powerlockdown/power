#include "ata_def.h"
#include "ata_read.h"
#include "ata_scsi.h"
#include "power/driver.h"
#include <power/error.h>

#include <power/memory/heap.h>
#include <power/tty.h>
#include <power/ata.h>
#include <power/scsi.h>
#include <power/memory/memset.h>
#include <power/endian.h>

static int getAtaIdentify(struct ata_device* dev,
			  uint16_t* restrict buffer,
			  int flags);
static int initDeviceData(struct ata_device*, int flags);
static int attachAta(struct ata_device* dev);

int ataAttachDevice(struct ata_device* dev, int dmabsize, int flags)
{
  int error;
  if ((error = initDeviceData(dev, flags)) < 0) {
    return error;
  }

  if (dev->Flags & ATA_DEVICE_ATAPI_BIT) {
    if ((error = ataAttachScsi(dev) < 0))
      return error;
  } else {
    if ((error = attachAta(dev) < 0))
      return error;
  }

  dev->DmaLimit = dmabsize;
  return 0;
}

/** Checks whether we should retry using
    IDENTIFY PACKET or not.  */
static inline
int shouldRequestAtapi(const struct ata_taskfile* tf)
{
  if (!(tf->Error & ATA_ERR_ABRT))
    return 0;

  /* This only detects one type of ATAPI!
     TODO: make it detect the other. */
  return tf->LbaLow == 0x01 &&
    tf->LbaHigh == 0xEB &&
    tf->LbaMid == 0x14;
}

/** Submits IDENTIFY command, retrying for ATAPI
    if necessary. */
int getAtaIdentify(struct ata_device* dev,
		   uint16_t* restrict buffer,
		   int flags)
{
  int error;
  int command;

  command = 0xEC;
  if (flags & ATA_ATTACH_HINT_ATAPI) {
  atapi:
    command = 0xA1;
  }
  
  struct ata_taskfile tf;
  memset(&tf, 0, sizeof(tf));
  tf.Command = command;
  
  if ((error = dev->Ops.submitCommand(dev, &tf)) < 0) {  
    goto error;
  }

  if (tf.Status & ATA_STATUS_ERR) {
    /* Did we attempt ATAPI already?
       If so, nothing we can do anymore.*/
    if (command == 0xA1) {
      error = -ERROR_IO;
      goto error;
    }
    
    if (shouldRequestAtapi(&tf))
      goto atapi;
  }
  
  dev->Ops.recv(dev, ATA_TRANSFER_PIO, buffer, 512);
  return 0;
 error:
  trmLogfn("Could not submit IDENTIFY%s command: %i",
	     command == 0xA1 ? " PACKET" : "",  error);
    return error;
}

static inline
void fillSectorData(struct ata_device* dev, const uint16_t* data)
{
  uint16_t w0, w1;
  unsigned long ualba;
  unsigned long* reinterpret;
  
  w0 = data[60];
  w1 = data[61];
  ualba = (w1 << 16) | w0;

  /* Sector count cannot fit LBA28?
     Use the quad-word version instead.*/
  if (ualba >= ATA_LBA28_MAX) {
    reinterpret = (unsigned long*)(data + 100);
    ualba = *reinterpret;
  }

  dev->SectorCount = ualba;

  w0 = data[116];
  if ((w0 & (1 << 14)) && (w0 & (1 << 15))) {
    w0 = data[117];
    w1 = data[118];
    
    dev->SectorSize = ((unsigned int)w1 << 16) | w0;
    return;
  }

  /* ACS-3 about sector size
     is really obtuse in the way
     it is displayed to consumers
     so we just presume if everything
     fails. */
  dev->SectorSize = 512;
  if (dev->Flags & ATA_DEVICE_ATAPI_BIT)
    dev->SectorSize = 2048;
}

/** Parse and fill the device with appropriate flags
    based off IDENTIFY {PACKET} buffer. */
static inline
int fillDeviceData(struct ata_device* dev, const uint16_t* data)
{
  /* I think I should add macros to these bits... */

  uint16_t w0 = data[0];
  
  /* Data is incomplete. Tecnically I would have to do
     some PUIS stuff in order to get drive going OK. */
  if (w0 & (1 << 2))
    return -ERROR_NOT_SUPPORTED;
  
  uint16_t w49;
  if (w0 & (1 << 15)) {
    dev->Flags |= ATA_DEVICE_ATAPI_BIT;
  }

  w49 = data[49];
  if (unlikely(!(w49 & (1 << 9))))
    dev->Flags |= ATA_DEVICE_CHS_ONLY_BIT;
  if (likely(w49 & (1 << 8)))
    dev->Flags |= ATA_DEVICE_DMA_BIT;

  fillSectorData(dev, data);
  return 0;
}

int initDeviceData(struct ata_device* dev, int flags)
{
  int error;
  uint16_t* idspace;
  idspace = khsAlloc(512);

  if (unlikely(!idspace))
    return -ERROR_NO_MEMORY;

  if ((error = getAtaIdentify(dev, idspace, flags)) < 0) {
    khsFree(idspace);
    return error;
  }

  error = fillDeviceData(dev, idspace);
  khsFree(idspace);
   
  return error;
}

/** Plug ATA callbacks and attribs onto interface
    fields. Only for non-ATAPI devices. */
int attachAta(struct ata_device* dev)
{
  struct driver_disk_device_interface* inf;
  inf = (struct driver_disk_device_interface*)dev;
  inf->SectorCount = dev->SectorCount;
  inf->SectorSize = dev->SectorSize;
  inf->readSector = ataReadSector;
  
  return 0;
}
