#include "power/scsi.h"
#include "ata_def.h"
#include "ata_scsi.h"
#include "power/assert.h"
#include "power/ata.h"
#include <string.h>

/** Initialize SCSI library for the supplied device. */
int ataAttachScsi(struct ata_device* dev)
{
  int error;

  PANIC_IF(!(dev->Flags & ATA_DEVICE_ATAPI_BIT));
  dev->Base.sendCommand = ataSubmitScsiCommand;
  if ((error = scsiAttachDevice(&dev->Base, dev->SectorSize, 0)) < 0) {
    return error;
  }
  
  return 0;
}

int ataSubmitScsiCommand(struct scsi_device* sc, struct scsi_packet* pack)
{
  struct ata_device* ata = (struct ata_device*)sc;

  int error, transfer;
  
  struct ata_taskfile tf;
  memset(&tf, 0, sizeof(tf));
  
  tf.Command = 0xA0;
  
  if (likely(ata->Flags & ATA_DEVICE_DMA_BIT)) {
    transfer = ATA_TRANSFER_DMA;
    tf.Features = 1;
    if (!(pack->Flags & SCSI_CMD_WRITE_BIT)
	&& ata->Flags & ATA_PACKET_DMADIR_BIT) {
      tf.Features |= (1 << 2);
    }
  } else if (pack->TransferBuffer) {
    transfer = ATA_TRANSFER_PIO;
    tf.LbaMid = pack->TransferLength & 0xFF;
    tf.LbaHigh = (pack->TransferLength >> 8) & 0xFF;
  }

  error = ata->Ops.atapiCommand(ata, pack->CommandData, pack->CommandLength);
  if (error < 0)
    return error;

  if (pack->Flags & SCSI_CMD_WRITE_BIT) {
    error = ata->Ops.send(ata, transfer,
			  pack->TransferBuffer,
			  pack->TransferLength);
  }
  
  if ((error = ata->Ops.submitCommand(ata, &tf)) < 0)
    return error;

  /* TODO: If an CRC error occurs, we could retry the command
     using PIO */

  if (tf.Status & ATA_STATUS_SENSE) {
    pack->Flags |= SCSI_CMD_ERROR_BIT;
    return 0;
  }

  if (tf.Status & ATA_STATUS_ERR) {
    return -ERROR_IO;
  }

  if (pack->Flags & SCSI_CMD_WRITE_BIT)
    return 0;

  ata->Ops.recv(ata, transfer, pack->TransferBuffer, pack->TransferLength);
  return 0;
}

int ataFillAtapiInformation(struct ata_device* dev, const uint16_t* idspace)
{
  
}
