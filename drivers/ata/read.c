#include "ata_read.h"
#include "power/error.h"

#include <power/ata.h>
#include <power/memory/memset.h>

/* Maximum LBA represented by LBA28. */
#define LBA28_MAX 0xFFFFFFF

static inline
int getReadCommand(struct ata_device* dev, lba_t lba);

static inline
int setBlockRegisters(struct ata_device* dev,
		      struct ata_taskfile* restrict tf, lba_t lba);
static inline
int readSectorLoop(struct ata_device* dev, lba_t lba,
		   unsigned long length, uint8_t* restrict buffer)
{ 
  int error, transfer;

  transfer = ATA_TRANSFER_PIO;
  if (likely(dev->Flags & ATA_DEVICE_DMA_BIT))
    transfer = ATA_TRANSFER_DMA;
  
  struct ata_taskfile tf;
  memset(&tf, 0, sizeof(tf));

  if ((error = setBlockRegisters(dev, &tf, lba)) < 0) {
    return error;
  }

  tf.Command = getReadCommand(dev, lba);
  tf.SectorCount = length / dev->SectorSize;
  if (tf.SectorCount == 256)
    tf.SectorCount = 0;

  if (transfer == ATA_TRANSFER_DMA) {
    tf.Features = (1);
  }

  if ((error = dev->Ops.submitCommand(dev, &tf)) < 0)
    return error;

  dev->Ops.recv(dev, transfer, buffer, length);
  return 0;
}

int ataReadSector(struct driver_disk_device_interface* dri, lba_t lba,
		  unsigned long length, uint8_t* restrict buffer)
{
  struct ata_device* dev;
  dev = (struct ata_device*)dri;

  int error;
  int iters, sectors;
  unsigned long l;
  
  if (likely(length <= dev->DmaLimit
	     || !(dev->Flags & ATA_DEVICE_DMA_BIT))) {
    return readSectorLoop(dev, lba, length, buffer);
  }

  /* This entire logic is needed to mask out the
     limit of DMA space to the caller and pretend
     it is infinite/does not exist. */
  sectors = dev->DmaLimit / dev->SectorSize;
  iters = length / dev->DmaLimit;
  
  for (int i = 0; i < iters; i++) {
    l = min(dev->DmaLimit, length);
    if ((error = readSectorLoop(dev, lba + (i * sectors), l,
				buffer + (i * sectors)) < 0)) {
      return error;
    }
  }

  return 0;
}

int getReadCommand(struct ata_device* dev, lba_t lba)
{
  int cmd = 0x20; /* READ SECTOR(S) */ 
  if (likely(dev->Flags & ATA_DEVICE_DMA_BIT)) {
    cmd = 0xC8; /* READ DMA */
    if (lba > LBA28_MAX) {
      cmd = 0x25; /* READ DMA EXT */
    }

    return cmd;
  }

  if (cmd > LBA28_MAX)
    cmd = 0x24; /* READ SECTOR(S) EXT */

  return cmd;
}

int setBlockRegisters(struct ata_device* dev,
		      struct ata_taskfile* restrict tf, lba_t lba)
{
  if (unlikely(dev->Flags & ATA_DEVICE_CHS_ONLY_BIT)) {
    /* Handle CHS separately. */
    return -ERROR_NOT_SUPPORTED;
  }

  tf->DriveHead = (1 << 6);
  if (lba > LBA28_MAX) {
    tf->LbaLow = lba & 0xFFFF;
    tf->LbaMid = (lba >> 16) & 0xFFFF;
    tf->LbaHigh = (lba >> 32) & 0xFFFF;

    return 0;
  }

  return 0;
}
