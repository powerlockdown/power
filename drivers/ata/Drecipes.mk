ATA_BUILD=build/drivers/ata
ATA_SOURCE=drivers/ata
ATA_OUTPUT=$(ATA_BUILD)/libata.a

ATA_CFILES=device.c scsi.c read.c
ATA_CSOURCES:=$(addprefix $(ATA_SOURCE)/,$(ATA_CFILES))
ATA_CBUILD:=$(addprefix $(ATA_BUILD)/,$(addsuffix .o,$(ATA_CFILES)))

AUXFS_FILES+=$(ATA_OUTPUT)

ATA_ARFLAGS=rcso

ata: $(ATA_OUTPUT)

$(ATA_OUTPUT): $(ATA_CBUILD) $(KERNEL_STUB) $(SCSI_OUTPUT)
	@mkdir -p $(ATA_BUILD)/$(dir $(subst $(ATA_SOURCE)/,,$<))

	@echo AR $(SCSI_OUTPUT) $(ATA_OUTPUT)
	@$(AR) $(ATA_ARFLAGS) $(ATA_OUTPUT) $(SCSI_CBUILD) $(ATA_CBUILD)

$(ATA_BUILD)/%.c.o: $(ATA_SOURCE)/%.c
	@mkdir -p $(ATA_BUILD)/$(dir $(subst $(ATA_SOURCE)/,,$<))

	@echo CC $<
	@$(CC) $(CFLAGS) $(DRIVER_CFLAGS) -c $< -o $@

$(eval $(call driver_easy_std_tasks,ata,ATA))

