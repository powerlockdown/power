/* ata_def.h
   Purpose: ATA definitions */
#pragma once

/* Source for such definitions: ACS-8 draft.

   Things under quotes means that I'm not sure
   what it does used and am just
   copying what the spec says verbatim. */

/* ATA status */
/** Error occurred. */
#define ATA_STATUS_ERR (1 << 0)

/** Data Request.
    Usually indicates ready to send/receive PIO data
    but "transport dependent" in the spec. */
#define ATA_STATUS_DRQ (1 << 4)

/** Device fault. */
#define ATA_STATUS_DF (1 << 5)

/** Device Ready.
    Usually indicates ready to have commands
    submitted to it but "transport dependent" by the spec. */
#define ATA_STATUS_DRDY (1 << 6)

/** Busy. */
#define ATA_STATUS_BSY (1 << 7)

/** Service. Presence of a queued command.  */
#define ATA_STATUS_SERV (1 << 4)

/** Sense data available. Only when the
    issued command is PACKET. */
#define ATA_STATUS_SENSE (1 << 1)

/** Stream error.
    
    Only when the issued command is participates in
    the streaming feature set.*/
#define ATA_STATUS_SE (1 << 5)

/* ATA errors */
/** Command timed out. */
#define ATA_ERR_CCTO (1 << 0)

/** End of media. "ATAPI-specific" */
#define ATA_ERR_EOM  (1 << 1)

/** Command aborted. */
#define ATA_ERR_ABRT (1 << 2)

/** ID not found. (User address not found) */
#define ATA_ERR_IDNF  (1 << 3)

/** "Uncorrectable data" */
#define ATA_ERR_UNC (1 << 6)

/** "Illegal Length Indicator". ATAPI-specific */
#define ATA_ERR_ILI (1 << 0)

/** Interface CRC error. UDMA/Multiword DMA-specific */
#define ATA_ERR_ICRC (1 << 7)

/** "Media error" */
#define ATA_ERR_MED (1 << 0)

/** SCSI sense key. ATAPI-specific. */
#define ATA_SENSE_SHIFT 4
#define ATA_SENSE_BITS  (15 << ATA_SENSE_SHIFT)

/* These three are irrelevant for us, I guess. */

/** "Insufficient NV Cache Space".
    Only when the issued command is "ADD LBA(S) TO NV CACHE PINNED SET". */
#define ATA_ERR_INCS (1 << 0)

/** "Attempted Partial Range Removal".
    Only when issued command is "REMOVE LBA(S) FROM NV CACHE PINNED SET". */
#define ATA_ERR_APRRR (1 << 0)

/** "Insufficient LBA Rnage Entries Remaining".
    Only when issued command is "ADD LBA(S) TO NV CACHE PINNED SET". */
#define ATA_ERR_ILRER (1 << 1)
