/* scsi_cmd.h
   Purpose: definitions of SCSI commands
            and their response */
#pragma once

#include <power/config.h>

/* Keep in mind: SCSI parameters are in BIG ENDIAN! */

struct __packed scsi_read_cap10_res {
    uint32_t MaximumLba;
    uint32_t BlockSize;
};
