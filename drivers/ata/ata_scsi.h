/* ata_scsi.h
   Purpose: Handling of PACKET commands. */
#pragma once

#include "power/ata.h"
#include <power/scsi.h>

int ataAttachScsi(struct ata_device* dev);
int ataSubmitScsiCommand(struct scsi_device* sc, struct scsi_packet* pack);
int ataFillAtapiInformation(struct ata_device* dev, const uint16_t* idspace);
