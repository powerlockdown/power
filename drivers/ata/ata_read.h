/* ata_read.h
   Purpose: Read/write a sector */
#pragma once

#include <power/driver.h>

int ataReadSector(struct driver_disk_device_interface* dri, lba_t lba,
                  unsigned long length, uint8_t* restrict buffer);

