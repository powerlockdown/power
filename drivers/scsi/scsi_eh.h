/* scsi_eh.h
   Purpose: SCSI error handling */
#pragma once

#include <power/scsi.h>

struct scsi_sense {
  uint8_t Response;
};

/* Request a sense and return an appropriate
   error code. */
int scsiHandleSense(struct scsi_device* dev);


