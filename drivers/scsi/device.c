#include "power/memory/heap.h"
#include <power/scsi.h>
#include <power/error.h>

#include <power/endian.h>
#include <power/device.h>
#include "scsi_cmd.h"

int scsiReadCapacity(struct scsi_device* dev, struct scsi_read_cap10_res* result);

int scsiAttachDevice(struct scsi_device* dev, 
                     int secSize, int flags)
{
    if (!dev->sendCommand) {
        return -ERROR_INVALID_ARGUMENT;
    }

    dev->SectorSize = secSize;
    dev->Base.readSector = scsiReadSector;
    dev->SectorSize = secSize;
    dev->Flags = flags | SCSI_DEVICE_ATTACHED_BIT | SCSI_DEVICE_INSERTED_BIT;

    struct scsi_read_cap10_res* data = khsAlloc(sizeof(*data));
    if (!data)
      return -ERROR_NO_MEMORY;
    
    int error = scsiReadCapacity(dev, data);
    if (error == -ERROR_NO_DEVICE) {
        dev->Flags &= ~SCSI_DEVICE_INSERTED_BIT;
        goto ret;
    } else if (error < 0) {
      khsFree(data);
      return error;
    }

    if (!dev->SectorSize) {
        dev->SectorSize = swapbe32cpu(data->BlockSize);
    }

    dev->SectorCount = swapbe32cpu(data->MaximumLba);
    dev->Base.SectorSize = dev->SectorSize;
    dev->Base.SectorCount = dev->SectorCount;

ret:
    khsFree(data);
    return 0;
}

int scsiReadSector(struct driver_disk_device_interface* device,
                   uint64_t lba, size_t length, uint8_t* restrict buffer)
{
    struct scsi_device* scsi = (void*)device;
    if (!(scsi->Flags & SCSI_DEVICE_INSERTED_BIT)) {
        return -ERROR_NO_DEVICE;
    }

    /* clang-format off */
    volatile uint8_t cmd[12] = { 0xA8, 0, /* READ (12) */
                                 ((uint8_t)(lba >> 0x18) & 0xFF),
                                 ((uint8_t)(lba >> 0x10) & 0xFF),
                                 ((uint8_t)(lba >> 0x08) & 0xFF),
                                 ((uint8_t)(lba) & 0xFF),
                                 0, 0, 0, 1,
                                 0, 0 };
    /* clang-format on */

    int status;
    struct scsi_packet* pk = khsAlloc(sizeof(*pk));
    if (!pk)
      return -ERROR_NO_MEMORY;
    
    pk->CommandData = (void*)cmd;
    pk->CommandLength = sizeof(cmd);
    pk->TransferLength = length;
    pk->TransferBuffer = (char*)buffer;
    
    status = scsi->sendCommand(scsi, pk);
    
    khsFree(pk);
    return status;
}

int scsiReadCapacity(struct scsi_device* dev, struct scsi_read_cap10_res* result)
{
    static uint8_t commandBuffer[10] = {
        0x25, 0,
        0, 0, 0, 0,
        0, 0,
        0, 0
    };

    struct scsi_packet pack;
        
    pack.CommandData = (const char*)commandBuffer;
    pack.CommandLength = sizeof(commandBuffer);
    pack.TransferBuffer = (void*)result;
    pack.TransferLength = 8;
    
    int status = dev->sendCommand(dev, &pack);
    return status;
}
