/* cmd_def.h
   Purpose: Definition for SCSI commands. */
#pragma once

enum { /*< READ command opcodes */
  SCSI_READ_6 = 0x08,
  SCSI_READ_10 = 0x28,
  SCSI_READ_12 = 0xA8,
  SCSI_READ_32 = 0x7F
};
