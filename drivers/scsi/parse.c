/* parse.c
   Purpose: routines that parse CDBs. */
#include <power/scsi.h>
#include <power/endian.h>
#include <power/error.h>

#include "cmd_def.h"

uint32_t scsiCmdGetTransferLength(uint8_t* cdb)
{
  uint8_t op = *cdb;
  switch (op) {
  case SCSI_READ_6:
    return cdb[4];
  case SCSI_READ_10:
    return swaple16cpu(*(uint16_t*)&cdb[7]);
  case SCSI_READ_12:
    return (cdb[6] << 24) | (cdb[7] << 16) | (cdb[8] << 8) | cdb[9];
  case SCSI_READ_32:
    return swaple32cpu(*(uint32_t*)&cdb[28]);
  }

  return -ERROR_INVALID_ARGUMENT;
}

