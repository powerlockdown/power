SCSI_BUILD=build/drivers/scsi
SCSI_SOURCE=drivers/scsi
SCSI_OUTPUT=$(SCSI_BUILD)/libscsi.a

SCSI_CFILES=device.c parse.c
SCSI_CSOURCES:=$(addprefix $(SCSI_SOURCE)/,$(SCSI_CFILES))
SCSI_CBUILD:=$(addprefix $(SCSI_BUILD)/,$(addsuffix .o,$(SCSI_CFILES)))

AUXFS_FILES+=$(SCSI_OUTPUT)

SCSI_ARFLAGS=rcso

scsi: $(SCSI_OUTPUT)

$(SCSI_OUTPUT): $(SCSI_CBUILD) $(KERNEL_STUB)
	@mkdir -p $(SCSI_BUILD)/$(dir $(subst $(SCSI_SOURCE)/,,$<))

	@echo AR $(SCSI_OUTPUT)
	@$(AR) $(SCSI_ARFLAGS) $(SCSI_OUTPUT) $(SCSI_CBUILD)

$(SCSI_BUILD)/%.c.o: $(SCSI_SOURCE)/%.c
	@mkdir -p $(SCSI_BUILD)/$(dir $(subst $(SCSI_SOURCE)/,,$<))

	@echo CC $<
	@$(CC) $(CFLAGS) $(DRIVER_CFLAGS) -c $< -o $@

$(eval $(call driver_easy_std_tasks,scsi,SCSI))
