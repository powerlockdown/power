#include <power/driver.h>
#include <power/memory/heap.h>

#include "mount.h"
#include "power/filestats.h"
#include "read.h"

struct driver_fs_interface* ext2CreateInterface()
{
    struct driver_fs_interface* interface = khsAlloc(sizeof(*interface));
    if (!interface)
      return NULL;
    
    interface->Name = "ext2";
    interface->mount = ext2Mount;
    return interface;
}

static void driverMain(struct kdriver_manager*);

static struct driver_info info = { .Name = "ext2",
                                   .Role = DRIVER_ROLE_FILESYSTEM,
                                   .ConditionalLoading = NULL,
                                   .EntryPoint = driverMain,
                                   .Interface = NULL };

struct driver_info* driverQuery()
{
  return &info;
}

void driverMain(struct kdriver_manager* driver)
{
    __unused(driver);
    info.Interface = ext2CreateInterface();
}

static void createFile(struct driver_mounted_fs_interface* fs, const char* name,
                       struct fs_file* outFile)
{
    return ext2CreateFile((struct ext2_mounted*)fs, name, outFile);
}

static int readFile(struct driver_mounted_fs_interface* fs, uint8_t* buffer,
                    size_t size, struct fs_file* file)
{
    return ext2ReadFile((struct ext2_mounted*)fs, buffer, size, file);
}

static int writeFile(struct driver_mounted_fs_interface* fs, const uint8_t* buffer,
		     size_t size, struct fs_file* file)
{
  return ext2WriteFile((struct ext2_mounted*)fs, buffer, size, file);
}

static void closeFile(struct driver_mounted_fs_interface* fs,
                      struct fs_file* file)
{
  ext2CloseFile((struct ext2_mounted*)fs, file);
}

static int getFileStats(struct driver_mounted_fs_interface* fs,
			struct fs_file* file,
			struct __filestats* stat)
{
  return ext2GetFileStatistics((struct ext2_mounted*)fs, file, stat);
}

void ext2CreateMountedInterface(struct driver_mounted_fs_interface* in)
{
    in->createFile = createFile;
    in->read = readFile;
    in->write = writeFile;
    in->closeFile = closeFile;
    in->stat = getFileStats;
}
