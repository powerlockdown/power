/* block.h
   Purpose: */
#pragma once

#include "mount.h"

struct ext2_block_group_desc {
    uint32_t BlockBitmap;
    uint32_t InodeBitmap;
    uint32_t InodeTable;
    uint16_t FreeBlockCount;
    uint16_t FreeInodeCount;
    uint16_t DirCount;
    uint16_t Padding;
    uint8_t Reserved[12];
};

int ext2AllocateBlock(struct ext2_mounted*,
		      struct ext2_block_group_desc* block);
void ext2GetBlockDescriptor(struct ext2_mounted* mt);
int ext2WriteBlock(struct ext2_mounted*, int block,
		   const uint8_t* buffer,
		   size_t offset, size_t size);
