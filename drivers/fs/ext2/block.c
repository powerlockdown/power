#include "block.h"
#include <power/memory/heap.h>
#include <power/memory/pfa.h>

#include "mount.h"
#include "power/error.h"
#include "power/vector.h"
#include "superblock.h"
#include "inode.h"

#include <power/tty.h>
#include <power/memory/memset.h>
#include <power/bit.h>

struct mm_cache* blockCache;
struct mm_cache* groupCache;

void ext2GetBlockDescriptor(struct ext2_mounted* mt)
{
    if (!blockCache) {
        blockCache = khCreateCache("ext2_block", mt->BlockSize);
    }

    if (!groupCache) {
        groupCache = khCreateCache("ext2_group",
				   sizeof(struct ext2_block_group_desc));
    }

    int groupc = mt->Superblock->TotalBlockCount / mt->Superblock->BlockGroupBlockCount;
    mt->GroupDescriptors = vectorCreate(groupc);
    void* block = khAlloc(blockCache);

    int disp;
    int offset = 0;

    struct ext2_block_group_desc* bgc;
    
    /* The block descriptor is located exactly
       one block after the SB. */
    int cblk = (1024 / mt->BlockSize) + 1;   
    int perblock = mt->BlockSize / sizeof(*bgc);
    int lba = sbOffsetToLba(mt, cblk);
    sbReadBlock(mt, lba, block);

    for (int i = 0; i < groupc; i++) {
        if (i >= perblock - 1) {
	    cblk++;
	    lba = sbOffsetToLba(mt, cblk);
	    sbReadBlock(mt, lba, block);
	    
	    offset = i;
        }

	disp = (i - offset) * sizeof(*bgc);
	
        bgc = khAlloc(groupCache);
        memcpy(bgc, block + disp, sizeof(*bgc));
        vectorInsert(&mt->GroupDescriptors, bgc);
    }

    khFree(blockCache, block);
}

/* Attempts to allocate a block
   respecting bg boundaries. */
int ext2AllocateBlock(struct ext2_mounted* mt,
                      struct ext2_block_group_desc* bg)
{
  int bit;
  uint8_t* bitmap;
  ptrdiff_t gindex;
  
  gindex = bg - (typeof(bg))mt->GroupDescriptors.Data;
  long block = sbOffsetToLba(mt, bg->BlockBitmap);

  bitmap = khAlloc(blockCache);
  sbReadBlock(mt, block, bitmap);

  /* No block found */
  if ((bit = ffzm((const char*)bitmap, mt->BlockSize)) == 65) {
    return -1;
  }
  
  int byte = bit - (bit % 8);
  int bybi = bit - byte;
  
  bitmap[byte] |= (1 << bybi);
  sbWriteBlock(mt, block, bitmap);
  return bit + (gindex * mt->Superblock->BlockGroupBlockCount);
}

int ext2WriteBlock(struct ext2_mounted* mt, int block,
		   const uint8_t* buffer,
		   size_t offset, size_t size)
{
  int error = 0;
  void* datablock = khAlloc(blockCache);
  if (!datablock)
    return -ERROR_NO_MEMORY;

  if (offset + size > (size_t)mt->BlockSize) {
    error = -ERROR_TOO_BIG;
    goto tb;
  }
  
  lba_t lba = sbOffsetToLba(mt, block);
  memcpy(datablock + offset, buffer, size);
  
  sbWriteBlock(mt, lba, buffer);
  
tb:
  khFree(blockCache, datablock);
  return error;
}

int ext2Write(struct ext2_mounted* mt, int block,
              const uint8_t* buffer, size_t size,
	      struct ext2_ino* ino)
{
  struct ext2_block_group_desc* bg;
  struct ext2_inode* inode;
  
  uint32_t* inoblock;
  int end = block + (size / mt->BlockSize);
  int group = (ino->Number - 1) / mt->Superblock->BlockGroupInodeCount;

  bg = mt->GroupDescriptors.Data[group];
  inode = ino->Inode;
  
  for (int i = block; i < end; i++) {
    if (i < 12) {
      inoblock = &inode->DirectBlockPtr[i];
    }

    /* TODO: And the file is not sparse. */
    if (!*inoblock) {
      (*inoblock) = ext2AllocateBlock(mt, bg);
    }

    ext2WriteBlock(mt, *inoblock, buffer, 0, 1);
  }
}

#define ARRAYBP_SING_READ (1 << 0)
#define ARRAYBP_DOUB_READ (1 << 1)
#define ARRAYBP_TRIP_READ (1 << 2)

int ext2BlockEnsureCreated(struct ext2_mounted* mt,
			   struct ext2_ino* no,
			   int offset, int begin)
{
  struct ext2_inode* ino = no->Inode;
  struct ext2_block_group_desc* bg;

  bg = ext2GetInodeBlockDesc(mt, no->Number);

  int inpib;
  int ratio = mt->BlockSize / sizeof(uint32_t);
  /* IS THIS CORRECT? */
  long di = p2pow(ratio, 2);
  long ti = p2pow(ratio, 3);
  
  int df = begin - offset;
  int offblk = offset / mt->BlockSize;
  int blocks = offblk + (df / mt->BlockSize);

  int arraybp = 0;

  uint32_t* array;
  uint32_t* dblarray;
  uint32_t* tplarray;
  
  array = khAlloc(blockCache);
  if (!array)
    return -ERROR_NO_MEMORY;
  
  uint32_t* blkno;
  for (int i = offblk; i < blocks; i++) {
    if (i < 12) {
      blkno = &ino->DirectBlockPtr[i];
    } else if (i >= 13 && i < ratio) {
      /* Single indirect block  */
      inpib = i - 13;
      if (!(arraybp & ARRAYBP_SING_READ)) {
	arraybp |= ARRAYBP_SING_READ;
	sbReadBlock(mt, sbOffsetToLba(mt, ino->SngIndirectBlk),
		    array);
      }

      blkno = &array[inpib];
    } else if (i >= ratio && i < di) {
      inpib -= ratio;
      
    }

    if (!*blkno) {
      (*blkno) = ext2AllocateBlock(mt, bg);
    }
  }
  
  return 0;
}
