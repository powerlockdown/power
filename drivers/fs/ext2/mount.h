/* mount.h
   Purpose: interface requirements */
#pragma once

#include <power/driver.h>
#include "superblock.h"
#include <power/vector.h>
#include <power/vfs.h>

#define EXT2MT_MOUNTEDRO_BIT (1 << 0)

struct ext2_mounted {
    struct driver_mounted_fs_interface Base;

    struct ext2_superblock* Superblock;
    struct driver_disk_device_interface* Device;

    lba_t PartitionBegin;
    int BlockSize;
    int InodeSize;
    int Flags;
  
    struct vector GroupDescriptors;

};

/* Convert an offset from the SB to an LBA.
   The returned LBA is relative. */
static inline lba_t sbOffsetToLba(struct ext2_mounted* mt, uint64_t blkoffset)
{
    /* TODO: this will not work if sectorsize > 1024. */
    int sectorsize = mt->Device->SectorSize;
    return (blkoffset * (mt->BlockSize / sectorsize));
}

static inline int sbReadBlock(struct ext2_mounted* mt, lba_t block,
                              void* restrict buffer)
{
    int error;
    error = mt->Device->readSector(mt->Device, mt->PartitionBegin + block,
				   mt->BlockSize, buffer);

    return error;
}

static inline int sbWriteBlock(struct ext2_mounted* mt, lba_t block,
                               const void* buffer)
{
    int error;
    error = mt->Device->writeSector(mt->Device, mt->PartitionBegin + block,
				    mt->BlockSize, buffer);
    return error;
}


int ext2FindSuperblock(struct driver_disk_device_interface* dev,
                       lba_t initialLba, struct ext2_superblock* sp);

struct driver_mounted_fs_interface*
ext2Mount(struct driver_fs_interface*, struct driver_disk_device_interface* dev,
          lba_t beginLba, struct fs_mountpoint*);

void ext2CreateMountedInterface(struct driver_mounted_fs_interface* in);
