EXT2_BUILD=build/drivers/fs/ext2
EXT2_SOURCE=drivers/fs/ext2
EXT2_OUTPUT=$(EXT2_BUILD)/ext2.kd

EXT2_CFILES=main.c mount.c block.c inode.c read.c
EXT2_CSOURCES:=$(addprefix $(EXT2_SOURCE)/,$(EXT2_CFILES))
EXT2_CBUILD:=$(addprefix $(EXT2_BUILD)/,$(addsuffix .o,$(EXT2_CFILES)))

AUXFS_FILES+=$(EXT2_OUTPUT)

$(EXT2_OUTPUT): $(EXT2_CBUILD) $(KERNEL_STUB)
	$(CC) $(LDFLAGS) $(DRIVER_LDFLAGS) $(EXT2_CBUILD) -o $@

$(eval $(call driver_easy_c,$(EXT2_BUILD),$(EXT2_SOURCE)))

$(eval $(call driver_easy_std_tasks,ext2,EXT2))
