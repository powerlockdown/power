#include "inode.h"

#include <power/error.h>

#include "block.h"
#include <power/config.h>
#include <power/memory/memset.h>

#include <power/elf.h>
#include "mount.h"
#include <power/tty.h>

#include <power/memory/heap.h>
#include <power/string.h>

extern struct mm_cache* blockCache;

static int readIndirectDataBlock(struct ext2_mounted* mt, uint32_t sector,
                                 size_t size, void* buffer);

/* Finds inode structure located at index `no`
   inside the inode table and writes it in `ino` pointer. */
void ext2FindInode(struct ext2_mounted* mt, int no, struct ext2_inode* ino)
{
    void* bg;
    struct ext2_inode* buffer;
    struct ext2_block_group_desc* desc;
    struct vector* groups = &mt->GroupDescriptors;

    int group = (no - 1) / mt->Superblock->BlockGroupInodeCount;
    int local = (no - 1) % mt->Superblock->BlockGroupInodeCount;

    int c;
    int b = local * mt->InodeSize;
    int d = (b % mt->BlockSize);
    
    b = b - d;
    c = b / mt->BlockSize;
    
    desc = groups->Data[group];
    buffer = khAlloc(blockCache);
    bg = buffer;

    lba_t lba = sbOffsetToLba(mt, desc->InodeTable + c);
    sbReadBlock(mt, lba, buffer);

    memcpy(ino, bg + d, mt->InodeSize);
    khFree(blockCache, bg);
}

/* Finds the block descriptor which
   is closest to this inode. */
struct ext2_block_group_desc*
ext2GetInodeBlockDesc(struct ext2_mounted* mt, int inode)
{
  int group;
  struct vector* groups;

  groups = &mt->GroupDescriptors;
  group = (inode - 1) / mt->Superblock->BlockGroupInodeCount;
  return groups->Data[group];
}

/* Finds inode `name` inside directory `parent` and if found, 
   writes the inode index in the `inoResult` pointer.
   
   May return `ERROR_NOT_FOUND` if no such inode exists there. */
int ext2GetInodeFromDir(struct ext2_mounted* mt, struct ext2_inode* parent,
                        const char* name, size_t namel, uint32_t* inoResult)
{
    lba_t lba;
    void* begin;
    int result = 0;
    size_t consumed = 0;
    size_t total = mt->BlockSize;

    lba = sbOffsetToLba(mt, parent->DirectBlockPtr[0]);
    struct ext2_dirent* buffer = khAlloc(blockCache);

    begin = buffer;
    sbReadBlock(mt, lba, buffer);

    while (consumed < total) {
        if (!strncmp(&buffer->Name, name, namel)) {
            (*inoResult) = buffer->Inode;
            goto free;
        }

        consumed += buffer->RecordLength;
        buffer = direntNext(buffer);
    }

    result = -ERROR_NOT_FOUND;
free:
    khFree(blockCache, begin);
    return result;
}

/* Read the indirect blocks (double, triple...) of inode `ino`. */
int ext2ReadInodeIndirect(struct ext2_mounted* mt, struct ext2_inode* ino,
                          uint8_t* buffer, size_t size)
{
    int read;
    size_t total, offset = 0;
    read = readIndirectDataBlock(mt, ino->SngIndirectBlk, size, buffer);
    offset = read;
    size -= read;
    if (size <= 0) {
        goto ret;
    }

    if (!ino->DblIndirectBlk)
        goto nosec;

    /* DOUBLE BLOCK */
    total = mt->BlockSize / sizeof(uint32_t);
    lba_t lba = sbOffsetToLba(mt, ino->DblIndirectBlk);
    void* block = khAlloc(blockCache);
    sbReadBlock(mt, lba, block);

    uint32_t* array = block;
    for (size_t i = 0; i < total; i++) {
        uint32_t sing = array[i];
        if (!sing || size <= 0) {
            break;
        }

        read = readIndirectDataBlock(mt, sing, size, buffer + offset);
        offset += read;
        size -= read;
    }

    if (!ino->TplIndirectBlk)
        goto ret;

    /* TRIPLE BLOCK */
    void* secondary = khAlloc(blockCache);
    void* tertiary = khAlloc(blockCache);
    lba = sbOffsetToLba(mt, ino->TplIndirectBlk);
    sbReadBlock(mt, lba, block);
    array = block;

    for (size_t i = 0; i < total; i++) {
        uint32_t dbl = array[i];
        lba = sbOffsetToLba(mt, dbl);
        sbReadBlock(mt, lba, secondary);
        uint32_t* secarray = secondary;

        if (size <= 0) {
            break;
        }

        for (size_t j = 0; j < total; j++) {
            uint32_t sing = secarray[j];
            if (size <= 0) {
                break;
            }

            read = readIndirectDataBlock(mt, sing, size, buffer + offset);
            offset += read;
            size -= read;
        }
    }

    khFree(blockCache, tertiary);
    khFree(blockCache, secondary);
ret:
    khFree(blockCache, block);
nosec:
    return offset;
}

/* Reads the single indirect block of an inode. 

   It does not have to be an actual single indirect block,
   it just have to pretend to be. */
int readIndirectDataBlock(struct ext2_mounted* mt, uint32_t sector, size_t size,
                          void* buffer)
{
    size_t read = 0;
    lba_t lba = sbOffsetToLba(mt, sector);
    void* block = khAlloc(blockCache);
    if (!block) {
      return -ERROR_NO_MEMORY;
    }

    uint32_t* array = khsAlloc(mt->BlockSize);
    if (!array) {
      khFree(blockCache, block);
      return -ERROR_NO_MEMORY;
    }

    sbReadBlock(mt, lba, array);
    size_t total = mt->BlockSize;
    for (size_t i = 0; i < total / sizeof(uint32_t); i++) {
        if (read >= size)
            break;

        uint32_t b = array[i];
        if (!b)
            break;

        lba = sbOffsetToLba(mt, b);
        sbReadBlock(mt, lba, block);
        memcpy(buffer + (mt->BlockSize * i), block, mt->BlockSize);
        read += mt->BlockSize;
    }

    khsFree(array);
    khFree(blockCache, block);
    return read;
}

/* Submit an inode back to the disk. */
int ext2FlushInode(struct ext2_mounted* mt,
		   struct ext2_ino* ino)
{
  void* bd;
  unsigned toffset;
  
  lba_t ito;
  lba_t no;
  lba_t blk;
  lba_t lba;

  bd = khAlloc(blockCache);
  if (!bd)
    return -ERROR_NO_MEMORY;
  
  struct ext2_block_group_desc* bg;
  bg = ext2GetInodeBlockDesc(mt, ino->Number);
  
  no = ino->Number % mt->Superblock->BlockGroupInodeCount;
  ito = (no * mt->InodeSize) / mt->BlockSize;
  toffset = (no * mt->InodeSize) % mt->BlockSize;
  
  blk = ito + bg->InodeTable;
  lba = sbOffsetToLba(mt, blk);
  
  sbReadBlock(mt, lba, bd);
  memcpy(bd + toffset, ino->Inode, mt->InodeSize);
  sbWriteBlock(mt, lba, bd);
  return 0;
}
