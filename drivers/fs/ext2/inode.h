/* inode.h
   Purpose: */
#pragma once

#include <power/config.h>
#include <power/memory/pfa.h>
#include "mount.h"
#include <stdint.h>

/* file type */
#define EXT2_S_IFSOCK 0xC000
#define EXT2_S_IFLNK 0xA000
#define EXT2_S_IFREG 0x8000
#define EXT2_S_IFBLK 0x6000
#define EXT2_S_IFDIR 0x4000
#define EXT2_S_IFCHR 0x2000
#define EXT2_S_IFIFO 0x1000

/* User/group override */
#define EXT2_S_ISUID 0x0800
#define EXT2_S_ISGID 0x0400
#define EXT2_S_ISVTX 0x0200

/* Access rights */
#define EXT2_S_IRUSR 0x0100
#define EXT2_S_IWUSR 0x0080
#define EXT2_S_IXUSR 0x0040
#define EXT2_S_IRGRP 0x0020
#define EXT2_S_IWGRP 0x0010
#define EXT2_S_IROTH 0x0004
#define EXT2_S_IXOTH 0x0001

struct ext2_inode {
    uint16_t Mode;
    uint16_t Uid;
    uint32_t Size;

    uint32_t AccessTime;
    uint32_t CreatTime;
    uint32_t ModTime;
    uint32_t DelTime;

    uint16_t Gid;
    uint16_t LinkCount;
    uint32_t Blocks;
    uint32_t Flags;
    uint32_t OsDep1;

    uint32_t DirectBlockPtr[12];
    uint32_t SngIndirectBlk; /*< Singly indirect block. */
    uint32_t DblIndirectBlk; /*< Double indirect block. */
    uint32_t TplIndirectBlk; /*< Triply indirect block. */

    uint32_t Generation;
    uint32_t FileAcl;
    uint32_t DirectoryAcl;
    uint32_t FragmentAddr; /*< Unused. */
    uint8_t OsDep2[12];
};

#define EXT2_FT_UNKNOWN 0
#define EXT2_FT_REG_FILE 1
#define EXT2_FT_DIR 2
#define EXT_FT_CHRDEV 3
#define EXT2_FT_BLKDEV 4
#define EXT2_FT_FIFO 5
#define EXT2_FT_SOCK 6
#define EXT2_FT_SYMLINK 7

struct ext2_dirent {
    uint32_t Inode;
    uint16_t RecordLength;
    uint8_t Length;
    uint8_t FileType;

    char Name; /*< ptr to first char */
};

__used static inline void* direntNext(struct ext2_dirent* ne)
{
    int nalen = ne->RecordLength;
    return PaAdd(ne, nalen);
}

struct ext2_ino;

struct ext2_block_group_desc* ext2GetInodeBlockDesc(struct ext2_mounted* mt,
						    int inode);
void ext2FindInode(struct ext2_mounted* mt, int no, struct ext2_inode* ino);
int ext2GetInodeFromDir(struct ext2_mounted* mt, struct ext2_inode* parent,
                        const char* name, size_t namel, uint32_t* inoResult);
int ext2ReadInodeIndirect(struct ext2_mounted* mt, struct ext2_inode* ino,
                          uint8_t* buffer, size_t size);
int ext2FlushInode(struct ext2_mounted* mt,
		   struct ext2_ino* ino);

struct ext2_ino { /*< Inode + fields useful for us. */
  uint32_t Number;
  struct ext2_inode* Inode;
};
