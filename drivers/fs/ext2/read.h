/* read.h
   Purpose: ReadFile */
#pragma once

#include <power/driver.h>
#include "mount.h"

void ext2CreateFile(struct ext2_mounted*, const char* path,
                    struct fs_file* file);
int ext2ReadFile(struct ext2_mounted*, uint8_t* buffer, size_t size,
                 struct fs_file* file);
int ext2WriteFile(struct ext2_mounted* mt, const uint8_t* buffer, size_t size,
		  struct fs_file* file);
int ext2CloseFile(struct ext2_mounted* mt, struct fs_file* file);
int ext2GetFileStatistics(struct ext2_mounted* mt,
			  struct fs_file* file,
			  struct __filestats* stat);
