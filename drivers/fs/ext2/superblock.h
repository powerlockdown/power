/* superblock.h
   Purpose: ext2 superblock definition */
#pragma once 

#include <stdint.h>

struct __attribute__((packed))
ext2_ext_superblock { /*< Extended superblock (only available if Major >= 1) */
    uint32_t FirstFreeInode;
    uint16_t InodeStructureSize;
    uint16_t Group; /*< Group this SB participates in. */
    uint32_t RecommendedFeatures;
    uint32_t RequiredFeatures;
    uint32_t WriteRequiredFeatures;
    char FileSystemId[16];
    char VolumeName[16];
    char LastMountPoint[64]; /*< N/A to Lockdown. */
    uint32_t CompressionAlg; /*< Used compression algorithms. */
    uint8_t FileBlockPreallocation;
    uint8_t DirBlockPreallocation;
    uint16_t Unused;
    char JournalId[16];
    uint32_t JournalInode;
    uint32_t JournalDevice;
    uint32_t OrphanInodeList;
};

struct __attribute__((packed)) 
ext2_superblock {
    uint32_t TotalInodeCount;
    uint32_t TotalBlockCount;
    uint32_t SuperuserBlockCount;
    uint32_t UnallocatedBlocks;
    uint32_t UnallocatedInodes;
    uint32_t SuperblockBlock;
    uint32_t UnshiftedBlockSize; /*< Shift 1024 to the left. */
    uint32_t UnshiftedFragSize; /*< Shift 1024 to the left */
    uint32_t BlockGroupBlockCount;
    uint32_t BlockGroupFragCount;
    uint32_t BlockGroupInodeCount;
    uint32_t LastMount; /*< Seconds since POSIX epoch */
    uint32_t LastWrite; /*< Seconds since POSIX epoch */
    uint16_t MountsSinceLastConsistency;
    uint16_t RemainingMountsConsistency; /*< Number of mounts allowed before 
                                            a consistency check must be done. 
                                            We currently don't have an FSCK equivalent
                                            in Lockdown */
    uint16_t Signature;
    uint16_t FileSystemState;
    uint16_t ErrorRecoveryStrategy;
    uint16_t MinorVersion;
    uint32_t LastConsistency; /*< Seconds since POSIX epoch. */
    uint32_t ForcedConsistCheckInterval;
    uint32_t CreatorId;
    uint32_t MajorVersion;
    uint16_t SuperUserId;
    uint16_t SuperUserGroupId;

    struct ext2_ext_superblock Extended;
};
