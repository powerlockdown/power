#include "mount.h"

#include "block.h"
#include <power/driver.h>

#include <power/memory/heap.h>
#include <power/tty.h>
#include <power/vector.h>

#include <power/vfs.h>
#include <power/memory/pfa.h>
#include <power/memory/memset.h>

struct driver_mounted_fs_interface*
ext2Mount(struct driver_fs_interface* it,
          struct driver_disk_device_interface* dev, lba_t beginLba,
          struct fs_mountpoint* mp)
{
    __unused(it);
    __unused(mp);

    struct ext2_superblock* super = khsAlloc(sizeof(*super));
    if (!super)
      return NULL;
    
    if (ext2FindSuperblock(dev, beginLba, super) < 0) {
        trmLogfn("failed to mount: could not find superblock");
        return NULL;
    }
    
    struct ext2_mounted* mnt = khsAlloc(sizeof(*mnt));
    if (!mnt)
      return NULL;
    
    mnt->Superblock = super;
    mnt->BlockSize = 1024 << super->UnshiftedBlockSize;
    mnt->Device = dev;
    mnt->GroupDescriptors = vectorCreate(5);
    mnt->PartitionBegin = beginLba;
    mnt->InodeSize = 128;
    
    if (super->MajorVersion >= 1) {
        mnt->InodeSize = super->Extended.InodeStructureSize;
    }

    ext2CreateMountedInterface(&mnt->Base);
    ext2GetBlockDescriptor(mnt);
    return &mnt->Base;
}

int ext2FindSuperblock(struct driver_disk_device_interface* dev,
                       lba_t initialLba,
                       struct ext2_superblock* sp)
{
    int sec, error;
    void* rawsp;
    size_t finalSize, offset;

    sec = dev->SectorSize;
    offset = 1024;
    vfsOffsetLba(&offset, &sec);

    finalSize = offset + 1024;
    rawsp = khsAlloc(finalSize);
    if ((error = dev->readSector(dev, sec + initialLba, finalSize, rawsp)) < 0) {
        trmLogfn("extFindSuperblock: failed to read sector %i", sec);
        return error;
    }

    memcpy(sp, rawsp + offset, 1024);
    khsFree(rawsp);
    return 0;
}
