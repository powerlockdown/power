#include "read.h"
#include "block.h"
#include "inode.h"

#include <power/elf.h>
#include "mount.h"
#include "power/filestats.h"

#include <power/memory/heap.h>
#include <power/memory/memset.h>

extern struct mm_cache* blockCache;

void ext2CreateFile(struct ext2_mounted* mt, const char* path,
                    struct fs_file* file)
{
    int error;
    uint32_t no;
    void* buffer = khAlloc(blockCache);
    struct ext2_ino* ino = khsAlloc(sizeof(*ino));
    if (!ino)
      return;

    ino->Inode = khsAlloc(mt->InodeSize);
    ext2FindInode(mt, 2, ino->Inode);

    const char* begin = path;
    const char* ptr = begin;
    while (true) {
        if (*ptr == '/' || !*ptr) {
            error = ext2GetInodeFromDir(mt, ino->Inode, begin, ptr - begin, &no);
	    begin = ptr + 1;
     
            if (error < 0) {
                file->Bad = error;
                goto free;
            }
	    
            ext2FindInode(mt, no, ino->Inode);
            if (*ptr == '\0') {
                ino->Number = no;
                file->Size = ino->Inode->Size;
                file->Reserved = ino;
                break;
            }
        }

        ptr++;
    }

    khFree(blockCache, buffer);
    return;
free:
    khFree(blockCache, buffer);
    khsFree(ino);
}

int ext2CloseFile(struct ext2_mounted* mt, struct fs_file* file)
{
  __unused(mt);
  struct ext2_ino* ino = file->Reserved;

  khsFree(ino->Inode);
  khsFree(ino);
  return 0;
}

int ext2ReadFile(struct ext2_mounted* mt, uint8_t* buffer, size_t size,
                 struct fs_file* file)
{
    int lba;
    struct ext2_ino* inode = file->Reserved;
    struct ext2_inode* ino = inode->Inode;
    
    void* block = khAlloc(blockCache);
    long length = size;

    if (length > ino->Size) {
        length = ino->Size;
    }

    int blocks = (alignUp(length, mt->BlockSize)) / mt->BlockSize;
    int directb = __min(blocks, 12);
    for (int i = 0; i < directb; i++) {
        lba = sbOffsetToLba(mt, ino->DirectBlockPtr[i]);
        sbReadBlock(mt, lba, block);
        memcpy(buffer + (i * mt->BlockSize), block, mt->BlockSize);
    }

    if (blocks > 12) {
        ext2ReadInodeIndirect(mt, ino, buffer + (12 * mt->BlockSize),
                              size - (12 * mt->BlockSize));
    }

    khFree(blockCache, block);
    return blocks * mt->BlockSize;
}

int ext2WriteFile(struct ext2_mounted* mt, const uint8_t* buffer, size_t size,
		  struct fs_file* file)
{
  int gno;
  lba_t lba;
  unsigned long block;
  unsigned long blockend, bdiff;
  unsigned long aligneddiff;

  long length = size;
  
  void* bd;
  uint32_t* cb;

  struct ext2_block_group_desc* bg;
  struct ext2_ino* ino = file->Reserved;

  bd = khAlloc(blockCache);
  
  gno = (ino->Number - 1) / mt->Superblock->BlockGroupInodeCount;
  bg = mt->GroupDescriptors.Data[gno];

  aligneddiff = file->Offset - (file->Offset % mt->BlockSize);
  block = file->Offset / mt->BlockSize;
  bdiff = size / mt->BlockSize;
  
  blockend = block + bdiff;

  size_t le;
  size_t written = 0;
  
  unsigned long i;
  for (i = block; i <= blockend && length > 0; i++) {
    if (i < 12) {
      cb = &ino->Inode->DirectBlockPtr[i];
    }

    if (!*cb) {
      *cb = ext2AllocateBlock(mt, bg);
    }

    lba = sbOffsetToLba(mt, *cb);
    if (i == block) {
      le = min(size, mt->BlockSize - aligneddiff);
      sbReadBlock(mt, lba, bd);
      memcpy(bd + (file->Offset - aligneddiff), buffer, le);

      length -= le;
      written += le;
      sbWriteBlock(mt, lba, bd);
      continue;
    }

    if (i + 1 == blockend) {
      le = min(length, mt->BlockSize);
      memset(bd, 0, mt->BlockSize);
      memcpy(bd, buffer + (size - length), le);
      
      written += le;
      sbWriteBlock(mt, lba, bd);
      break;
    }

    le = mt->BlockSize;
    memcpy(bd, buffer + (size - length), le);

    length -= le;
    written += le;
    sbWriteBlock(mt, lba, bd);
  }

  if (file->Offset + written > ino->Inode->Size) {
    /* Flush the inode and the rewritten blocks. */
    ino->Inode->Size = file->Offset + written;
    ext2FlushInode(mt, ino);
  }
  
  khFree(blockCache, bd);
  return written;
}

int ext2GetFileStatistics(struct ext2_mounted* mt,
			  struct fs_file* file,
			  struct __filestats* stat)
{
  struct ext2_ino* ino = file->Reserved;
  stat->SerialNumber = ino->Number;
  stat->Size = ino->Inode->Size;
  
  stat->StatusTime = 0;
  stat->AccessTime = ino->Inode->AccessTime;
  stat->ModifiedTime = ino->Inode->ModTime;
  stat->LinkCount = ino->Inode->LinkCount;
  stat->UserId = ino->Inode->Uid;
  stat->GroupId = ino->Inode->Gid;

  /* I think this is it??? */
  stat->BlockSize = mt->BlockSize;
  return 0;
}
