#include "interface.h"

#include <power/memory/heap.h>
#include <power/tty.h>
#include <power/vfs.h>

#include <power/driver.h>
#include <power/memory/pfa.h>
#include <power/memory/memset.h>
#include "mount.h"
#include "power/error.h"

/* isofs */

static struct driver_mounted_fs_interface*
mount(struct driver_fs_interface* interface, 
        struct driver_disk_device_interface* dev, 
        uint64_t beginLba, struct fs_mountpoint* mp)
{
    return isofsMount((struct isofs*)interface, dev, beginLba, mp);
}

void registerVfsInterface(struct isofs* iso)
{
    iso->Base.Name = "iso9660";
    iso->Base.mount = mount;
}

/* mounted_isofs */

static void createFile(struct driver_mounted_fs_interface* inter,
                       const char* name, struct fs_file* outFile)
{
    isofsCreateFile((struct mounted_isofs*)inter, name, outFile);
}

static int read(struct driver_mounted_fs_interface* inter, uint8_t* buffer,
                 size_t size, struct fs_file* file)
{
    return isofsReadFile((struct mounted_isofs*)inter, buffer, size, file);
}

static void closeFile(struct driver_mounted_fs_interface* inter, struct fs_file* file)
{
    isofsCloseFile((struct mounted_isofs*)inter, file);
}

static int write(struct driver_mounted_fs_interface* fs,
		 const uint8_t* buffer, size_t length,
		 struct fs_file* file)
{
  __unused(fs);
  __unused(buffer);
  __unused(length);
  __unused(file);
  
  return -ERROR_READ_ONLY_FILESYSTEM;
}

static int getFileStats(struct driver_mounted_fs_interface* fs,
			struct fs_file* file,
			struct __filestats* f)
{
  return isofsGetFileStatistics((struct mounted_isofs*)fs, file, f);
}

struct mounted_isofs* 
createMountedIso(struct isofs* restrict iso, struct driver_disk_device_interface* device,
                 struct fs_mountpoint* mp)
{
    struct mounted_isofs* ret = khsAlloc(sizeof(*ret));
    ret->Base.createFile = createFile;
    ret->Base.read = read;
    ret->Base.closeFile = closeFile;
    ret->Base.write = write;
    ret->Base.stat = getFileStats;
    
    ret->RootDirectory = iso->Volume.RootDirectoryRecord;
    ret->ScratchBuffer = khsAlloc(2048);
    ret->Device = device;

    mp->BlockSize = 2048;
    memcpy(mp->VolumeName, iso->Volume.VolumeIdentifier, FS_VOLUME_NAME_MAX);
    return ret;
}
