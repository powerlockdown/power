#include <power/config.h>
#include <power/driver.h>

#include <power/memory/heap.h>
#include <power/memory/pfa.h>
#include <power/tty.h>

#include "interface.h"

static void driverMain(struct kdriver_manager*);

static struct driver_info info = {
    .Name = "isofs",
    .Role = DRIVER_ROLE_FILESYSTEM,
    .ConditionalLoading = NULL,
    .EntryPoint = driverMain,
    .Interface = NULL
};

struct driver_info* driverQuery()
{
    return &info;
}

void driverMain(struct kdriver_manager* manager)
{
    __unused(manager);
    info.Interface = khsAlloc(sizeof(struct isofs));
    registerVfsInterface(info.Interface);
}


