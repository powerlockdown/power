ISOFS_BUILD=build/drivers/fs/iso
ISOFS_SOURCE=drivers/fs/iso9660
ISOFS_OUTPUT=$(ISOFS_BUILD)/isofs.kd

ISOFS_CFILES=main.c interface.c mount.c susp.c
ISOFS_CSOURCES:=$(call driver_easy_source,ISOFS)
ISOFS_CBUILD:=$(call driver_easy_build,ISOFS)

AUXFS_FILES+=$(ISOFS_OUTPUT)

$(ISOFS_OUTPUT): $(ISOFS_CBUILD) $(KERNEL_STUB)
	$(CC) $(LDFLAGS) $(DRIVER_LDFLAGS) $(ISOFS_CBUILD) -o $@

$(eval $(call driver_easy_c,$(ISOFS_BUILD),$(ISOFS_SOURCE)))

$(eval $(call driver_easy_std_tasks,isofs,ISOFS))
