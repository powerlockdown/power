#include "ahci_state.h"

#include "ahci_device.h"
#include "ahci_interface.h"
#include "ahcidef.h"
#include "power/i386/pgtables.h"
#include "power/memory/hhdm.h"
#include "power/memory/pool.h"
#include <power/i386/idt.h>
#include <power/paging.h>

#include <power/config.h>
#include <power/memory/heap.h>
#include <power/memory/pfa.h>
#include <power/pci.h>

#include <power/scheduler.h>

#include <power/tty.h>
#include <power/vector.h>
#include <stddef.h>

static void ahciInterrupt(struct irq_regs* reg, void* data)
{
  __unused(reg);

  struct ahci* ahc = data;
  volatile struct ahci_abar* br = ahc->Bar;
  
  for (size_t i = 0; i < ahc->Devices.Length; i++) {
    struct ahci_device* dev = ahc->Devices.Data[i];
    if (br->InterruptStatus & BIT(dev->PortIndex)) {
      dev->ErrorInterrupt = false;
      if (dev->Port->InterruptStatus & IS_INT_TFES_BIT)
	dev->ErrorInterrupt = true;
      
      if (dev->Port->InterruptStatus & IS_INT_PCS_BIT)
	dev->Port->SataError = PORT_SERR_DIAG_X_BIT;
      
      if (dev->Port->InterruptStatus & IS_INT_PRCS_BIT)
	dev->Port->SataError = PORT_SERR_DIAG_PHYRDY_CHANGE_BIT;

      dev->Port->InterruptStatus = dev->Port->InterruptStatus;
      br->InterruptStatus = BIT(dev->PortIndex);
      schedEventRaise(dev->Waiter);
    }
  }
}

static inline void generateDevices(struct ahci* ahci,
                                   volatile struct ahci_abar* abar)
{
  uint32_t iplp = abar->PortsImplemented;
  volatile struct ahci_port* prt = &abar->FirstPort;

  for (int i = 0; i < 32; i++) {
    if ((iplp & (1 << i))) {
      volatile struct ahci_port* p = &prt[i];
      if ((p->SataStatus & DEVICE_DETECT_MASK) == DEVICE_DET_PHY_ESTBLSH) {
	struct ahci_device* d = ahciCreateDevice(ahci, i, p);
	if (d && d->EmptyDevice)
	  vectorInsert(&ahci->EmptyDevices, d);
      }
    }
  }
}

static inline
void assertDisabledEngines(volatile struct ahci_abar* ab)
{
  uint32_t iplp = ab->PortsImplemented;
  volatile struct ahci_port* prt = &ab->FirstPort;

  for (int i = 0; i < 32; i++) {
    if ((iplp & (1 << i))) {
      volatile struct ahci_port* p = &prt[i];
      if ((p->SataStatus & DEVICE_DETECT_MASK) == DEVICE_DET_PHY_ESTBLSH) {
	ahciSetCommandEngine(p, false);
      }
    }
  }
}

struct ahci* ahciCreateState(struct pci_device* dev)
{
    uintptr_t bar5;
    struct ahci* ac = khsAlloc(sizeof(*ac));
    if (!ac)
      return NULL;
    
    ac->Devices = vectorCreate(5);
    ac->EmptyDevices = vectorCreate(1);
    ac->DeviceCache = khCreateCache("ahci_device", sizeof(struct ahci_device));

    ac->PrdtPool = poolInit(512, 2);
    ac->FisPool = poolInit(sizeof(struct fis_received), 256);
    ac->CtPool = poolInit(sizeof(struct command_table), 128);
    ac->CtHeaderPool = poolInit(sizeof(struct command_table_header) * AHCI_CMDTBL_LENGTH, 1024);
    
    pciHandleInterrupt(dev, ahciInterrupt, ac);
    bar5 = pciReadDoubleWordFromDevice(dev, 0x24) & ~0xFFF;
    pgMapPage(NULL, bar5, (uintptr_t)mmMakeWritable((void*)bar5),
	      PT_FLAG_WRITE | PT_FLAG_PCD);

    bar5 = (uintptr_t)mmMakeWritable((void*)bar5);
    volatile struct ahci_abar* ab = (void*)bar5;
    ac->Bar = ab;
    
    assertDisabledEngines(ab);
    ab->GlobalHostCtl |= GHC_INT_ENABLE_BIT;
    
    generateDevices(ac, ab);
    ahciRegisterDriverInterface(ac);
    return ac;
}
