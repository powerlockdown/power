#include "ahci_device.h"

#include <power/abstract/timer.h>
#include "ahci_ata.h"
#include "ahci_read.h"
#include "ahci_state.h"

#include "ahcidef.h"
#include <power/memory/heap.h>
#include <power/memory/pfa.h>

#include <power/error.h>
#include <power/memory/memset.h>
#include <power/scheduler.h>
#include "power/ata.h"
#include "power/memory/hhdm.h"
#include "power/memory/pool.h"

#include <power/tty.h>
#include <string.h>

static inline void
assertFisStopped(volatile struct ahci_port* pt)
{
  if (!(pt->CommandStatus & PORT_COMMAND_STATUS_NOT_IDLE_BITS)) {
    return;
  }
  
  struct timer* tm = tmGetDefaultTimer();

  ahciSetCommandEngine(pt, false);
  ahciWrite(&pt->CommandStatus,
	    pt->CommandStatus & ~(PORT_COMMAND_STATUS_FISR_BIT));

  tmSetReloadValue(tm, tm->MillisecondScale * 500);
}

static
struct command_table* createCommandTable(struct ahci* ahci)
{
  struct command_table* tb;
  tb = poolAllocObject(ahci->CtPool);
  memset(tb, 0, sizeof(struct command_table));

  struct command_table* writetb = mmMakeWritable(tb);

  void* buffer;
  struct prdt* pd;

  uintptr_t bufunwr;
  for (int i = 0; i < AHCI_PRDT_BUFFERS_LENGTH; i++) {
    pd = &writetb->PhysicalRegion[i];
    buffer = poolAllocObject(ahci->PrdtPool);

    bufunwr = mmMakeUnwritable(buffer);
    pd->DataBaseAddress = bufunwr & 0xFFFFFFFF;
    pd->UpperDataBaseAddress = bufunwr >> 32;
    pd->ByteCount = 512 - 1;
  }

  __barrier();
  return tb;
}

static void fillCommandTable(struct ahci* ahc, struct command_table_header* ct)
{
  uintptr_t ptr;
  ct->LengthFlags = (AHCI_CMDTBL_LENGTH << COMMANDHTBL_LENGTH_SHIFT);

  struct command_table* cmdt = createCommandTable(ahc);
  
  ptr = mmMakeUnwritable(cmdt);
  ct->CommandTableAddress = ptr & 0xFFFFFFFF;
  ct->UpperCommandTableAddress = (ptr) >> 32;
  
  __barrier();
}

static void createMemoryFields(struct ahci* state, struct ahci_device* dev)
{
    struct fis_received* fis;
    struct command_table_header* cmd;
    volatile struct ahci_port* pt = dev->Port;

    uintptr_t cmdunwr;
    uintptr_t fisunwr;
    
    cmd = poolAllocObject(state->CtHeaderPool);
    if (!cmd)
      return;
    
    memset(cmd, 0, sizeof(*cmd) * AHCI_CMDTBL_LENGTH);
    cmdunwr = mmMakeUnwritable(cmd);
    
    pt->CommandListBase = cmdunwr & 0xFFFFFFFF;
    pt->UpperCommandListBase = cmdunwr >> 32;

    fis = poolAllocObject(state->FisPool);
    if (!fis) {
      poolFreeObject(state->CtHeaderPool, cmd);
      return;
    }

    fisunwr = mmMakeUnwritable(fis);
    memset(fis, 0, sizeof(*fis));
    
    pt->FisBase = fisunwr & 0xFFFFFFFF;
    pt->UpperFisBase = fisunwr >> 32;

    for (int i = 0; i < AHCI_CMDTBL_LENGTH; i++)
      fillCommandTable(state, &cmd[i]);

    pt->CommandStatus = PORT_COMMAND_STATUS_FISR_BIT;
    while (!(pt->CommandStatus & PORT_COMMAND_STATUS_FIS_RUNNING_BIT))
        asm volatile("pause");
}

static void resetPort(volatile struct ahci_port* pt)
{
    struct timer* tm = tmGetDefaultTimer();
    uint64_t ms = tmGetMillisecondUnit(tm);
    uint32_t sctl;
    uint32_t prevCmd = ahciRead(&pt->CommandStatus) & ~PORT_SCTL_IPM_BITS;

    ahciSetCommandEngine(pt, false);
    ahciWrite(&pt->SataCtl, 0);
    tmSetReloadValue(tm, ms);

    sctl = PORT_SCTL_IPM_DISABLED | PORT_SCTL_DET_INIT;
    ahciWrite(&pt->SataCtl, sctl);

    sctl &= ~PORT_SCTL_DET_INIT;
    sctl |= PORT_SCTL_DET_NONE;
    tmSetReloadValue(tm, ms);
    ahciWrite(&pt->SataCtl, sctl);

    while ((pt->SataStatus & DEVICE_DETECT_MASK) != DEVICE_DET_PHY_ESTBLSH) {
        asm ("pause");
        tmSetReloadValue(tm, ms);
    }

    ahciWrite(&pt->SataError, (unsigned int)~0);
    while (ahciRead(&pt->TaskFileData) & PORT_TASK_BSY_BIT) {
        asm ("pause");
        tmSetReloadValue(tm, ms * 2);
    }

    ahciWrite(&pt->CommandStatus, prevCmd);
}

static
void startPort(volatile struct ahci_abar* abar,
	       volatile struct ahci_port* pt);

static struct ata_device_ops adops = {
  .submitCommand = ahciSubmitCommand,
  .atapiCommand = ahatapiSetPacketBuffer,
  .recv = ahataReceive,
  .send = ahataSend
};

struct ahci_device* ahciCreateDevice(struct ahci* ahc, int index,
                                     volatile struct ahci_port* pt)
{
  int error;
  int flags;
  
  struct ahci_device* dev = khAlloc(ahc->DeviceCache);
  if (!dev)
    return NULL;
  
  dev->Port = pt;
  dev->PortIndex = index;
  dev->Waiter = schedCreateEvent();
  dev->CommandSemaphore = schedCreateMutex();

  vectorInsert(&ahc->Devices, dev);

  assertFisStopped(pt);
  createMemoryFields(ahc, dev);
  startPort(ahc->Bar, pt);

  dev->Base.Ops = adops;

  flags = pt->SignatureData == 0;
  if ((error = ataAttachDevice(&dev->Base, AHCI_PRDT_BUFFERS_LENGTH * 512,
			       flags))) {
    schedDestroyEvent(dev->Waiter);
    schedDestroyMutex(dev->CommandSemaphore);
    vectorRemove(&ahc->Devices, dev);
    khFree(ahc->DeviceCache, dev);
    return NULL;
  }
  
  return dev;
}

static inline
int enginePoll(volatile struct ahci_port* pt)
{
  int attempts = 0;  
  static int timeout = 6;

  struct timer* tm;
  unsigned long ms;
  
  tm = tmGetDefaultTimer();
  ms = tm->MillisecondScale;
  
  do {
    if (attempts > timeout) {
      return -ERROR_TIMED_OUT;
    }

    tmSetReloadValue(tm, ms * 20);
    attempts++;
  } while (!(pt->CommandStatus & PORT_COMMAND_STATUS_CR_BIT));

  return attempts - 1;
}

int ahciSetCommandEngine(volatile struct ahci_port* pt, bool enb)
{
  if (enb) {
    ahciWrite(&pt->CommandStatus, pt->CommandStatus
	      | PORT_COMMAND_STATUS_ST_BIT);
    goto ret;
  }

  ahciWrite(&pt->CommandStatus, pt->CommandStatus &
	    ~(PORT_COMMAND_STATUS_ST_BIT));
 ret:
  return enginePoll(pt);
}

void startPort(volatile struct ahci_abar* abar,
	       volatile struct ahci_port* pt)
{
  uint32_t cmd = ahciRead(&pt->CommandStatus) & ~PORT_COMMAND_STATUS_ICC_BITS;
  if (ahciRead(&abar->HostCap) & CAP_SSS_BIT) {
    cmd |= PORT_COMMAND_STATUS_SUD_BIT;
    ahciWrite(&pt->CommandStatus, cmd);
  }

  ahciWrite(&pt->CommandStatus, cmd | PORT_COMMAND_STATUS_ICC_ACTIVE);
  int tt = ahciSetCommandEngine(pt, true);
  if (tt == -ERROR_TIMED_OUT) {
    resetPort(pt);
  }

  ahciWrite(&pt->SataCtl, 0x300);

  ahciWrite(&pt->InterruptEnable, ~0);
  ahciWrite(&pt->InterruptStatus, ~0U);
  ahciWrite(&pt->SataError, ~0U);
}
