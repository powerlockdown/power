#include "ahci_ata.h"
#include "ahci_state.h"
#include "ahcidef.h"

#include <power/memory/hhdm.h>
#include <power/memory/memset.h>

#define AHCI_DIR_RECV  1
#define AHCI_DIR_SEND 2

static inline
int rsend(struct ahci_device* dev, int dir,
	  void* restrict buffer, unsigned int length)
{
  void* src, *dest;
  void* prdbuffer;
  
  volatile struct ahci_port* port;
  
  port = dev->Port;

  struct command_table_header* th;
  struct command_table* cth;
  struct prdt* pd;

  int i, scaledi;

  th = getAddressUpper(port, CommandListBase);
  cth = getAddressUpper(th, CommandTableAddress);
  pd = &cth->PhysicalRegion[0];
  
  i = 0;
  scaledi = 0;
  
  while (length) {
    if (i > AHCI_PRDT_BUFFERS_LENGTH)
      break;

    pd = &cth->PhysicalRegion[i];
    prdbuffer = getAddressUpper(pd, DataBaseAddress);
    
    if (dir == AHCI_DIR_RECV) {
      dest = buffer + scaledi;
      src = prdbuffer;
    } else {
      dest = prdbuffer;
      src = buffer + scaledi;
    }
    
    memcpy(dest, src, min(length, 512)); 

    length -= min(length, 512);
    i++; scaledi = i * 512;
  }

  return scaledi;
}

int ahataReceive(struct ata_device* device, int transfer,
		 void* restrict buffer, unsigned int length)
{
  __unused(transfer);
  struct ahci_device* dev;
  dev = (struct ahci_device*)device;
  
  return rsend(dev, AHCI_DIR_RECV, buffer, length);
}

int ahataSend(struct ata_device* device, int transfer,
	      const void* buffer, unsigned int length)
{
  __unused(transfer);
  struct ahci_device* dev;
  dev = (struct ahci_device*)device;

  return rsend(dev, AHCI_DIR_SEND, (void*)buffer, length);
}
