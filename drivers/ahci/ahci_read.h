/* ahci_read.h
   Purpose: implementation of {read,write}Sector */
#pragma once

#include "ahci_state.h"

#include <power/ata.h>
#include <power/driver.h>

int ahataReadSector(struct ahci_device* device, uint64_t lba,
                    uint8_t* restrict buffer, size_t length);
int ahataWriteSector(struct ahci_device* device, lba_t lba,
                     const uint8_t* buffer, size_t length);

int ahatapiReadSector(struct ahci_device* device, uint64_t lba,
                      uint8_t* restrict buffer, size_t length);
int ahatapiWriteSector(struct ahci_device* device, uint64_t lba,
                       const uint8_t* buffer, size_t length);

int ahciReadManySectors(struct ahci_device* dev, uint64_t lba,
			uint8_t* restrict buffer, size_t length);
int ahatapiSendCommand(struct scsi_device* dev, struct scsi_packet* packet);

int ahciSubmitCommand(struct ata_device* device,
		      struct ata_taskfile* restrict tf);
int ahatapiSetPacketBuffer(struct ata_device* device,
			   const void* buffer, int length);
