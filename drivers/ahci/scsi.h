/* scsi.h 
   Purpose: SCSI (ATAPI) support */
#pragma once

#include "ahci_device.h"
#include "ahci_state.h"

#define AHSC_COMMAND_WRITE_BIT (1 << 1)

#define SCSI_ASC_LOGICAL_UNIT_NOT_READY 0x04

/* Returns the command slot used. */
int ahscSubmitCommand(struct scsi_device* dev, 
                      struct scsi_packet* packet);

/* Test the device */
void ahscMakeReady(struct ahci_device* dev);

int ahscRequestSense(struct ahci_device* dev);

bool ahscTrayEmpty(struct ahci_device* dev);
