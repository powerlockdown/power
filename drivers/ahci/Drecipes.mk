AHCI_BUILD=build/drivers/ahci
AHCI_SOURCE=drivers/ahci
AHCI_OUTPUT=$(AHCI_BUILD)/ahci.kd

AHCI_CFILES=main.c ahci_state.c ahci_device.c ahci_read.c ahci_interface.c ahci_ata.c
AHCI_CSOURCES:=$(call driver_easy_source,AHCI)
AHCI_CBUILD:=$(call driver_easy_build,AHCI)
AHCI_LDFLAGS=$(DRIVER_LDFLAGS)

AUXFS_FILES+=$(AHCI_OUTPUT)

$(AHCI_OUTPUT): $(AHCI_CBUILD) $(KERNEL_STUB) $(SCSI_OUTPUT) $(ATA_OUTPUT)
	mkdir -p $(AHCI_BUILD)
	$(CC) $(DRIVER_LDFLAGS) $(AHCI_CBUILD) -L$(SCSI_BUILD) -lscsi -L$(ATA_BUILD) -lata -o $(AHCI_OUTPUT)

$(eval $(call driver_easy_c,$(AHCI_BUILD),$(AHCI_SOURCE)))

$(eval $(call driver_easy_std_tasks,ahci,AHCI))
