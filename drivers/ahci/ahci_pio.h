/* ahci_pio.h
   Purpose: AHCI PIO reading/writing */
#pragma once

#include <power/ata.h>

int ahciPioRead(struct ata_device* device,
		void* restrict buffer,
		unsigned int length);
