#include "scsi.h"
#include "ahci_device.h"
#include "ahcidef.h"

#include <power/abstract/timer.h>
#include <power/scheduler.h>
#include <power/tty.h>

#include <power/memory/pfa.h>
#include <power/memory/memset.h>
#include <power/error.h>

int ahscSubmitCommand(struct scsi_device* scs, 
                      struct scsi_packet* packet)
{
    struct ahci_device* dev = (struct ahci_device*)scs;
    volatile struct ahci_port* pt = dev->Port;
    struct command_table_header* th;
    struct command_table* cth;

    th = getAddressUpper(pt, CommandListBase);
    cth = getAddressUpper(th, CommandTableAddress);

    memset(cth->CommandFis, 0, sizeof(cth->CommandFis));
    th->LengthFlags = (4 << COMMANDHTBL_LENGTH_SHIFT) 
                    | ((sizeof(struct fis_h2d) / sizeof(uint32_t)) << COMMANDHTBL_FIS_LENGTH_SHIFT)
                    | COMMANDHTBL_PREFETCH_BIT | COMMANDHTBL_ATAPI_BIT;

    if (packet->Flags & SCSI_CMD_WRITE_BIT) {
        th->LengthFlags |= COMMANDHTBL_WRITE_BIT;
    }

    memset(cth->AtapiCommand, 0, sizeof(cth->AtapiCommand));
    memcpy(cth->AtapiCommand, (const uint8_t*)packet->CommandData, packet->CommandLength);

    struct fis_h2d* id = (struct fis_h2d*)cth->CommandFis;
    id->Type = FIS_H2D_TYPE;
    id->Device = 0;
    id->Features = 1;
    id->MultiplierCommand = FIS_H2D_COMMAND_BIT;

    id->Command = 0xA0;

    if (packet->Flags & SCSI_CMD_WRITE_BIT && packet->TransferLength) {
        int i = 0;
        int cl = 0;
        int length = packet->TransferLength;

        void* address;
        struct prdt* prdt;

        while (i < 4 && length) {
            prdt = &cth->PhysicalRegion[i];
            address = getAddressUpper(prdt, DataBaseAddress);
            cl = 512;
            if (length < 512) {
                cl = length;
            }

            memcpy(address, packet->TransferBuffer + (512 * i), cl);
            length -= cl;
            i++;
        }
    }

    schedEventResetFlag(dev->Waiter);

    pt->CommandIssue = 1;
    schedEventPause(dev->Waiter);
    
    if (packet->TransferLength && !(packet->Flags & SCSI_CMD_WRITE_BIT)) {
        int i = 0;
        int cl = 0;
        int length = packet->TransferLength;

        void* address;
        struct prdt* prdt;

        while (i < 4 && length) {
            prdt = &cth->PhysicalRegion[i];
            address = getAddressUpper(prdt, DataBaseAddress);
            cl = 512;
            if (length < 512) {
                cl = length;
            }

            memcpy(packet->TransferBuffer + (512 * i), address, cl);
            length -= cl;
            i++;
        }
    }

    if (dev->ErrorInterrupt) {
        /* Restart command engine in order to clear PxCI. */
        ahciSetCommandEngine(dev->Port, false);
        ahciSetCommandEngine(dev->Port, true);
        return -ERROR_IO;
    }

    return 0;
}
