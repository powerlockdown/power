#include "ahci_read.h"
#include "ahci_device.h"
#include "ahci_state.h"

#include "ahcidef.h"
#include <power/error.h>
#include <power/memory/memset.h>
#include <power/memory/pfa.h>

#include "power/config.h"
#include "power/scsi.h"
#include <power/tty.h>
#include <power/scheduler.h>

#include <power/ata.h>

static inline
int getTransferLength(const struct ata_device*,
		      const struct ata_taskfile*,
		      struct command_table*);

static inline
int isWriteCommand(int command)
{
  switch (command) {
  case 0x30: /* PIO WRITE SECTOR(S) */
  case 0x34: /* PIO WRITE SECTOR(S) EXT */
  case 0x3B: /* PIO WRITE STREAM EXT */
  case 0x39: /* PIO WRITE MULTIPLE EXT */
  case 0xC3: /* PIO WRITE MULTIPLE */
  case 0x3F: /* PIO WRITE LOG EXT */
  case 0xCE: /* PIO WRITE MULTIPLE FUA EXT */ 
    
  case 0xCA: /* WRITE DMA */
  case 0x35: /* WRITE DMA EXT */
  case 0x3D: /* WRITE DMA FUA EXT */
  case 0xCC: /* WRITE DMA QUEUED */
  case 0x36: /* WRITE DMA QUEUED EXT */
  case 0x57: /* WRITE LOG DMA EXT */
    return true;
  }

  return false;
}

static inline
int isAtapiScsiWriteCommand(struct command_table* cth)
{
  unsigned char op = cth->AtapiCommand[0];
  switch (op) {
  case 0x0A: /* WRITE (6) */
  case 0x2A: /* WRITE (10) */
  case 0xAA: /* WRITE (12) */
  case 0x8A: /* WRITE (16) */
  case 0x7F: /* WRITE {AND VERIFY,ATOMIC,SAME,STREAM} (32) */

  case 0x2E: /* WRITE AND VERIFY (10) */
  case 0xAE: /* WRITE AND VERIFY (12) */
  case 0x8E: /* WRITE AND VERIFY (16) */
    
  case 0x9C: /* WRITE ATOMIC (16) */
  case 0x3B: /* WRITE BUFFER */
    
  case 0x3F: /* WRITE LONG (10) */
  case 0x9F: /* WRITE LONG (16) */

  case 0x41: /* WRITE SAME (10) */
  case 0x93: /* WRITE SAME (16) */
  case 0x9A: /* WRITE STREAM (16) */
    return true;
  }

  return false;
}

int ahatapiSetPacketBuffer(struct ata_device* device, const void* buffer,
			   int length)
{
  if (!(device->Flags & ATA_DEVICE_ATAPI_BIT))
    return -ERROR_NOT_SUPPORTED;

  if (length > 16)
    return -ERROR_TOO_BIG;
  
  struct ahci_device* dev = (struct ahci_device*)device;

  volatile struct ahci_port* port;
  struct command_table_header* th;
  struct command_table* cth;

  port = dev->Port;
  th = getAddressUpper(port, CommandListBase);
  cth = getAddressUpper(th, CommandTableAddress);

  memcpy(&cth->AtapiCommand, buffer, length);
  return 0;
}

int ahciSubmitCommand(struct ata_device* device,
		      struct ata_taskfile* restrict tf)
{
  struct ahci_device* dev = (struct ahci_device*)device;
  volatile struct ahci_port* port;

  int length;
  
  port = dev->Port;
  struct fis_received* fis;
  struct command_table_header* th;
  struct command_table* cth;

  th = getAddressUpper(port, CommandListBase);
  cth = getAddressUpper(th, CommandTableAddress);
  fis = getAddressUpper(port, FisBase);
  
  length = getTransferLength(device, tf, cth);
  th->LengthFlags = (length << COMMANDHTBL_LENGTH_SHIFT)
        | COMMANDHTBL_PREFETCH_BIT
        | COMMANDHTBL_CLEAR_BIT
        | ((sizeof(struct fis_h2d) / sizeof(uint32_t))
           << COMMANDHTBL_FIS_LENGTH_SHIFT);
  
  if (isWriteCommand(tf->Command))
    th->LengthFlags |= COMMANDHTBL_WRITE_BIT;

  if (device->Flags & ATA_DEVICE_ATAPI_BIT
      && tf->Command == 0xA0) {
    th->LengthFlags |= COMMANDHTBL_ATAPI_BIT;
    
    if (isAtapiScsiWriteCommand(cth))
      th->LengthFlags |= COMMANDHTBL_WRITE_BIT;
  }

  struct fis_d2h* dh = (struct fis_d2h*)&fis->D2h;
  struct fis_h2d* hd = (struct fis_h2d*)cth->CommandFis;
  memset(hd, 0, sizeof(*hd));
  
  hd->Type = FIS_H2D_TYPE;
  hd->Command = tf->Command & 0xFF;
  hd->Device = tf->DriveHead & ~15;
  
  /* Such confusing shifting is required
     because we mimick IDE's behaviour of
     registers becoming 16-bit in LBA48. */
  
  hd->LbaLow = tf->LbaLow & 0xFF;
  hd->LbaMid = tf->LbaMid & 0xFF;
  hd->LbaHigh = tf->LbaHigh & 0xFF;
  hd->Device |= (tf->LbaHigh >> 12) & 0xF;
  
  if (tf->LbaLow >> 8) {
    hd->LbaMid = tf->LbaLow >> 8;
    hd->LbaHigh = hd->LbaMid;
  }

  if (tf->LbaMid >> 8) {
    hd->Lba3 = hd->LbaHigh;
    hd->LbaHigh = tf->LbaMid >> 8;
    hd->Lba4 = tf->LbaHigh >> 8;
  }

  hd->Count = tf->SectorCount & 0xFF;
  hd->UpperCount = tf->SectorCount >> 8;

  hd->MultiplierCommand = FIS_H2D_COMMAND_BIT; 
  hd->Features = tf->Features & 0xFF;
  hd->UpperFeatures = tf->Features >> 8;
  hd->Control = tf->DriveHead;
  __write_until_here();

  schedEventResetFlag(dev->Waiter);
  port->CommandIssue = 1;

  schedEventPause(dev->Waiter);
  
  if (dev->ErrorInterrupt) {
    /* It is needed to restart the CE
       in order to clear PxCI. */
    ahciSetCommandEngine(dev->Port, false);
    ahciSetCommandEngine(dev->Port, true);			 
  }
  
  tf->Status = dh->Status;
  tf->Error = dh->Error;

  tf->SectorCount = dh->Count;
  
  tf->LbaHigh = dh->LbaHigh;
  tf->LbaMid = dh->LbaMid;
  tf->LbaLow = dh->LbaLow;

  if (dh->Lba3 || dh->Lba5) {
    tf->LbaHigh = ((uint16_t)dh->Lba5 << 8) | dh->Lba4;
    tf->LbaMid = ((uint16_t)dh->Lba3 << 8) | dh->LbaHigh;
    tf->LbaLow = ((uint16_t)dh->LbaMid << 8) | dh->LbaLow;
  }

  return 0;
}

/* IDENTIFY {PACKET} transfer data
   but do not report it explicitly
   using the TF regs. */
static inline
int isImplicitTransferCmd(int op)
{
  return op == 0xEC || op == 0xA1;
}

int getTransferLength(const struct ata_device* dev,
		      const struct ata_taskfile* tf,
		      struct command_table* cth)
{
  int red;
  if (tf->Command != 0xA0) {
    red = (tf->SectorCount * dev->SectorCount) / 512;
    goto ret;
  }

  red = scsiCmdGetTransferLength(cth->AtapiCommand);

 ret:
  if (!red && isImplicitTransferCmd(tf->Command)) {
    return 1;
  }
  
  return (red * dev->SectorSize) / 512;
}
