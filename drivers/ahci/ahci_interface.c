#include "ahci_interface.h"
#include "ahci_read.h"
#include "ahci_state.h"
#include <power/driver.h>
#include "power/device.h"
#include "power/error.h"

static int getDeviceCount(struct driver_disk_interface* di)
{
    struct ahci* ah = (struct ahci*)di;
    return ah->Devices.Length;
}

static struct driver_disk_device_interface*
getDevice(struct driver_disk_interface* di, int index)
{
    struct ahci* ah = (struct ahci*)di;
    if ((size_t)index > ah->Devices.Length) {
        return NULL;
    }

    return ah->Devices.Data[index];
}

void ahciRegisterDriverInterface(struct ahci* ah)
{
    ah->Base.Major = devnRegisterNode("ahci", NULL, DEVN_MANYDEV_BIT);
    ah->Base.getDeviceCount = getDeviceCount;
    ah->Base.getMaxDeviceCount = getDeviceCount; /* For compatibility. */
    ah->Base.getDevice = getDevice;
}
