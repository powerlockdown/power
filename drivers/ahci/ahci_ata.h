/* ahci_ata.h
   Purpose: AHCI send/receive functions */
#pragma once

#include <power/ata.h>

int ahataReceive(struct ata_device*, int transfer, void* restrict buffer,
                 unsigned int length);
int ahataSend(struct ata_device*, int transfer, const void* buffer,
	      unsigned int length);


