#include "state.h"
#include <power/i386/asm.h>
#include <power/memory/pfa.h>
#include <power/tty.h>

void ps2SetDisabled(bool enb)
{
    if (enb) {
        __outb(PS2_PORT_CMD, 0xAD);
        __outb(PS2_PORT_CMD, 0xA7);
        return;
    }

    /* Enable */
    __outb(PS2_PORT_CMD, 0xAE);
    __outb(PS2_PORT_CMD, 0xA8);
}

struct ps2_state* ps2Init()
{
    struct ps2_state* ps = mmAlignedAlloc(sizeof(struct ps2_state), 1);
    uint8_t status;
    ps2SetDisabled(true);
    do {
        status = __inb(PS2_PORT_STATUS);
        if (status & 0x1)
            __inb(PS2_PORT_DATA);
    } while(status & 0x1);

    ps->DualPort = __inb(0x20) & (1 << 5);

    /* ctl self test */
    __outb(PS2_PORT_CMD, 0xAA);
    do {
        status = __inb(PS2_PORT_DATA);
    } while(status != 0x55 && status != 0xFC);

    if (status == 0xFC) {
        trmLogfn("ps2: self-test failed");
        /* TODO add kernel panic */
    }

    __outb(PS2_PORT_CMD, 0xAB);
    do {
        status = __inb(PS2_PORT_DATA);
    } while(status > 0x05);
    if (status)
        trmLogfn("ps2: first port self-test failed");

    if (ps->DualPort) {
        __outb(PS2_PORT_CMD, 0xA9);
        do {
            status = __inb(PS2_PORT_DATA);
        } while(status > 0x05);
        if (status)
            trmLogfn("ps2: second port self-test failed");
    }

    ps2SetDisabled(false);
    ps2SendDeviceCommand(ps, false, 0xFF);
    ps2SendDeviceCommand(ps, true, 0xFF);

    return ps;
}

void ps2EnableConfig(struct ps2_state* ps2, int cfg)
{
    uint8_t status;
    uint8_t cfb;
    if (!ps2->DualPort) {
        cfg &= ~(PS2_CONFIG_P2_INT);
    }

    do {
        status = __inb(PS2_PORT_STATUS);
        __inb(PS2_PORT_DATA);
    } while(status & 1);

    __outb(PS2_PORT_CMD, 0x20);
    cfb = __inb(PS2_PORT_DATA);
    cfb |= cfg;
    cfb &= ~(1 << 6); /* disable translation */
    __outb(PS2_PORT_CMD, 0x60);

    do {
        status = __inb(PS2_PORT_STATUS);
    } while(status & (1 << 1));

    __outb(PS2_PORT_DATA, cfb);
}

void ps2SendDeviceCommand(struct ps2_state* ps2, int secondary, uint8_t command)
{
    if (secondary && !ps2->DualPort) {
        return;
    }

    if (secondary) {
        __outb(PS2_PORT_CMD, 0xD4);
    }

    uint8_t status;
    do {
        status = __inb(PS2_PORT_STATUS);
    } while (status & (1 << 1));
    __outb(PS2_PORT_DATA, command);
}

void ps2SendDeviceCommandEx(struct ps2_state* ps2, int secondary, 
                            uint8_t command, uint8_t data)
{
    if (secondary && !ps2->DualPort) {
        return;
    }

    if (secondary) {
        __outb(PS2_PORT_CMD, 0xD4);
    }

    uint8_t status;
    do {
        status = __inb(PS2_PORT_STATUS);
    } while (status & (1 << 1));
    __outb(PS2_PORT_DATA, command);

    do {
        status = __inb(PS2_PORT_STATUS);
    } while (status & (1 << 1));
    __outb(PS2_PORT_DATA, data);
}
