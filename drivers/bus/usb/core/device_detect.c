/* device_detect.c
   Purpose: Device detection and module loading */
#include "devices.h"
#include <power/driver.h>
#include <power/memory/heap.h>

#include <power/memory/pfa.h>
#include <power/memory/zoned.h>
#include <power/tty.h>

#include <power/list.h>
#include <power/memory/memset.h>

#include <power/string.h>

extern struct vector loadedDrivers;

static inline bool interfaceMatches(struct driver_usb_device_interface* dev,
                                    struct usb_interface* inf);
static int createInterface(struct usb_if_descriptor* desc,
                           struct usb_device* device,
                           struct usb_interface** out);

struct usb_device* usbCreateDevice(struct usb* usb,
                                   const struct usb_device_descriptor* dev)
{
    struct usb_device* d = khsAlloc(sizeof(*d));
    if (dev->bcdUsb >= 0x200) {
        d->Speed = USB_SPEED_HIGH;
    } else {
        d->Speed = USB_SPEED_FULL;
    }

    struct usb_if_descriptor* id;
    struct usb_cfg_descriptor* cd;
    struct usb_address nil = { 0, 0 };
    cd = usbReadConfiguration(usb, &nil, 0);
    id = (struct usb_if_descriptor*)(cd + 1);

    if (!cd->bNumInterfaces) {
        trmLogfn("Device has no endpoints!");
        goto fail;
    }

    d->Class = id->bInterfaceClass;
    d->Subclass = id->bInterfaceSubClass;
    d->Device = dev->idProduct;
    d->Vendor = dev->idVendor;

    unsigned i = 0;
    unsigned length = 0;

    uint64_t address;
    int status = zpgRequestRegion(usb->AddressPool, 1, &address);
    if (status) {
        trmLogfn("Could not allocate address for device.");
        goto fail;
    }

    usbDeviceSetAddress(usb, d, address);
    d->Address.Address = address;

    usbDeviceSetConfiguration(usb, d, cd->bConfigurationValue);
    struct usb_interface* uif;
    while (i < cd->bNumInterfaces) {
        length = createInterface(id, d, &uif);

        if (!d->Interfaces) {
            d->Interfaces = uif;
        } else {
            listInsert(&d->Interfaces->List, &uif->List);
        }

        i++;
        id = PaAdd(id, length);
    }

    listForEach(d->Interfaces, in) {
        uif = (struct usb_interface*)in;
        usbModuleLoad(usb, uif);
    }

    khsFree(cd);
    return d;

fail:
    khsFree(d);
    khsFree(cd);
    return NULL;
}

static int createInterface(struct usb_if_descriptor* desc,
                           struct usb_device* device,
                           struct usb_interface** out)
{
    struct usb_interface* interface;
    interface = khsAlloc(sizeof(*interface));

    interface->Class = desc->bInterfaceClass;
    interface->Subclass = desc->bInterfaceSubClass;
    interface->Protocol = desc->bInterfaceProtocol;
    interface->Device = device;

    int j = 0;
    int total = 0;
    struct usb_endpoint* epobj;
    struct usb_ep_descriptor* ep;

    int extraoffset = 0;
    int extralength = 0;
    void* extra = NULL;
    ep = (struct usb_ep_descriptor*)(desc + 1);
    while (j < desc->bNumEndpoints) {
        if (ep->bDescriptorType != 0x5) {
            /* Class-defined descriptor. */
            extralength += ep->bLength;
            extra = khsRealloc(extra, extralength);
            
            memcpy(extra + extraoffset, ep, ep->bLength);
            extraoffset = extralength;

            total += ep->bLength;
            ep = PaAdd(ep, ep->bLength);
            continue;
        }

        epobj = usbCreateEndpoint(ep);
        if (!interface->Endpoints) {
            interface->Endpoints = epobj;
        } else {
            listInsert(&interface->Endpoints->List, &epobj->List);
        }

        total += ep->bLength;
        j++;
        ep++;
    }

    interface->Extra = extra;
    interface->ExtraLength = extralength;
    (*out) = interface;
    return total;
}

void usbModuleLoad(struct usb* usb, struct usb_interface* inf)
{
    struct driver_info* info;
    struct driver_internal_data* did;
    struct driver_usb_device_interface* udi;
    struct vector* drivers = modGetDrivers();
    for (size_t i = 0; i < drivers->Length; i++) {
        did = drivers->Data[i];
        info = did->Driver->Info;
        if (!strncmp("usbmass", info->Name, 7)) {
            trmLogfn("usbmss role -> %i", info->Role);
        }

        if (info->Role != DRIVER_ROLE_USB_DEVICE) {
            continue;
        }

        udi = info->Interface;
        if (!udi) {
            trmLogfn("Driver %s reported as USB_DEVICE "
                     "but has no interface!",
                     info->Name);
            continue;
        }

        if (!interfaceMatches(udi, inf)) {
            trmLogfn("interf does not match :(");
            continue;
        }

        udi->deviceLoad(&usb->Base, inf);
    }
}

/* Like pciCompare */
#define comparedc(X, y) (((unsigned)(X)) == (unsigned)0xFF || (X) == (y))

static inline bool interfaceMatches(struct driver_usb_device_interface* dev,
                                    struct usb_interface* inf)
{ 
    /* TODO: Add for vendor and product id. */
    return comparedc(dev->Class, inf->Class) && comparedc(dev->Subclass, inf->Subclass)
           && comparedc(dev->Protocol, inf->Protocol);
}
