#include "devices.h"

#include <power/abstract/timer.h>
#include <power/config.h>
#include <power/driver.h>

#include <power/memory/heap.h>
#include "power/usb.h"
#include <power/scheduler.h>

#include "usb.h"
#include <power/list.h>
#include <power/memory/pfa.h>
#include <power/vector.h>

struct usb_connect {
    struct usb* Usb;
    struct driver_usb_port_interface* Port;
};

static void __nil(struct usb_transaction* t, void* d)
{
    __unused(t);
    struct usb* usb = d;
    schedEventRaise(usb->TransactionEvent);
}

static void scheduleConnection(struct usb* usb, 
                               struct driver_usb_port_interface* port);
static void loadDriver(void* ctx);
static void connect(struct driver_usb_port_interface* port, void* data)
{
    struct usb* usb = data;
    if (!usb->SubmitTask) {
        /* We shouldn't submit a connection task
           (Because it is already inside one). */
        schedEventRaise(usb->ResetEvent);
        return;
    }

    scheduleConnection(usb, port);
}

void usbControlRead(struct usb* usb, struct usb_address* address,
                    struct usb_setup* setup, void* restrict outbuffer)
{
    struct driver_usb_ctl_interface* ctl = usb->Controller;

    /* Send the request header */
    struct usb_transaction* tr = khsAlloc(sizeof(*tr));

    tr->Buffer = setup;
    tr->BufferLength = sizeof(*setup);
    tr->TransferType = USB_TRANSFER_DIR_OUT_BIT | USB_TRANSACTION_CONTROL;
    tr->CompleteHandler = __nil;
    tr->HandlerPrivData = usb;
    tr->Address = *address;
    tr->Flags = USB_TRANSACT_SETUP_PID_BIT;

    schedEventResetFlag(usb->TransactionEvent);
    ctl->scheduleTransaction(ctl, tr);
    schedEventPause(usb->TransactionEvent);

    /* Perform the actual transaction */
    struct usb_transaction* data = khsAlloc(sizeof(*data));
    data->Buffer = outbuffer;
    data->BufferLength = setup->wLength;
    data->Address = *address;
    data->TransferType = USB_TRANSFER_DIR_IN_BIT | USB_TRANSACTION_CONTROL;
    data->CompleteHandler = __nil;
    data->HandlerPrivData = usb;

    schedEventResetFlag(usb->TransactionEvent);
    ctl->scheduleTransaction(ctl, data);
    schedEventPause(usb->TransactionEvent);

    /* STATUS part. */
    struct usb_transaction* listener = khsAlloc(sizeof(*listener));
    listener->Buffer = NULL;
    listener->BufferLength = 0;
    listener->Address = *address;
    listener->TransferType = USB_TRANSFER_DIR_OUT_BIT | USB_TRANSACTION_CONTROL;
    listener->CompleteHandler = __nil;
    listener->HandlerPrivData = usb;
    schedEventResetFlag(usb->TransactionEvent);
    ctl->scheduleTransaction(ctl, listener);
    schedEventPause(usb->TransactionEvent);

    khsFree(listener);
    khsFree(tr);
    khsFree(data);
}

void usbControlSend(struct usb* usb, struct usb_device* dev, struct usb_setup* setup)
{
    struct driver_usb_ctl_interface* ctl = usb->Controller;
    struct usb_transaction* tr = khsAlloc(sizeof(*tr));

    tr->Buffer = setup;
    tr->BufferLength = sizeof(*setup);
    tr->TransferType = USB_TRANSFER_DIR_OUT_BIT | USB_TRANSACTION_CONTROL;
    tr->CompleteHandler = __nil;
    tr->HandlerPrivData = usb;
    tr->Address = dev->Address;
    tr->Flags = USB_TRANSACT_SETUP_PID_BIT;

    schedEventResetFlag(usb->TransactionEvent);
    ctl->scheduleTransaction(ctl, tr);
    schedEventPause(usb->TransactionEvent);

    struct usb_transaction* listener = khsAlloc(sizeof(*listener));
    listener->Buffer = NULL;
    listener->BufferLength = 0;
    listener->Address = dev->Address;
    listener->TransferType = USB_TRANSFER_DIR_IN_BIT | USB_TRANSACTION_CONTROL;
    listener->PidCode = USB_PID_IN;
    listener->CompleteHandler = __nil;
    listener->HandlerPrivData = usb;
    
    schedEventResetFlag(usb->TransactionEvent);
    ctl->scheduleTransaction(ctl, listener);
    schedEventPause(usb->TransactionEvent);

    khsFree(listener);
    khsFree(tr);
}

void usbInitializeDevices(struct usb* usb)
{
    struct driver_usb_ctl_interface* ctl = usb->Controller;
    struct driver_usb_port_interface* po;

    for (int i = 0; i < ctl->PortCount; i++) {
        po = ctl->getPort(ctl, i);
        if (po->HasDevice) {
            scheduleConnection(usb, po);
        }
    }

    ctl->ConnectHandler = connect;
}

struct usb_cfg_descriptor*
usbReadConfiguration(struct usb* usb, struct usb_address* addr, int idx)
{
    struct usb_cfg_descriptor* cdesc;
    struct usb_setup* setup = khsAlloc(sizeof(*setup));
    setup->bmRequestType = (1 << 7);
    setup->bRequest = 6;
    setup->wValue = (2 << 8) | (idx & 0xFF);
    setup->wLength = sizeof(*cdesc);
    setup->wIndex = 0;
    
    cdesc = khsAlloc(sizeof(*cdesc));
    usbControlRead(usb, addr, setup, cdesc);

    cdesc = khsRealloc(cdesc, cdesc->wTotalLength);
    setup->wLength = cdesc->wTotalLength;
    usbControlRead(usb, addr, setup, cdesc);
    
    khsFree(setup);
    return cdesc;
}

struct usb_endpoint* usbCreateEndpoint(struct usb_ep_descriptor* ed)
{
    struct usb_endpoint* ep = khsAlloc(sizeof(*ep));
    listInit(&ep->List);
    ep->Address = ed->bEndpointAddress & USB_ENDPOINT_ADDRESS_BITS;
    ep->MaximumPacketSize = ed->wMaxPacketSize & USB_ENDPOINT_MAX_PACKET_BITS;
    ep->Attributes = ed->bmAttributes | (ed->bEndpointAddress & USB_ENDPOINT_DIRECTION_BIT);
    ep->PollingInterval = ed->bInterval;
    ep->ExtraTransactions = (ed->wMaxPacketSize & USB_ENDPOINT_TRANSACT_OPP_BITS) >> 11;

    return ep;
}

void loadDriver(void* ctx)
{
    struct timer* tm = tmGetDefaultTimer();
    struct driver_usb_port_interface* po;
    struct usb_connect* connect = ctx;
    struct usb* usb = connect->Usb;
    po = connect->Port;

    schedSemaphoreAcquire(usb->ResetLock);

    usb->SubmitTask = false;
    schedEventResetFlag(usb->ResetEvent);

    po->reset(po);
    schedEventPause(usb->ResetEvent);
    tmSetReloadValue(tm, tmGetMillisecondUnit(tm) * 50);
    usb->SubmitTask = true;

    struct usb_setup* stp = khsAlloc(sizeof(*stp));
    stp->bmRequestType = (1 << 7);
    stp->bRequest = 6;
    stp->wIndex = 0;
    stp->wLength = sizeof(struct usb_device_descriptor);
    stp->wValue = (1 << 8) | 0;

    struct usb_device_descriptor* dc = khsAlloc(stp->wLength);
    struct usb_address nil = { 0, 0 };
    usbControlRead(usb, &nil, stp, dc);

    struct usb_device* dev = usbCreateDevice(usb, dc);
    vectorInsert(&usb->Devices, dev);
    schedSemaphoreRelease(usb->ResetLock);
    
    khsFree(stp);
    khsFree(connect);
}

void usbDeviceSetAddress(struct usb* usb, struct usb_device* dev, int address)
{
    struct usb_setup* setup = khsAlloc(sizeof(*setup));

    setup->bmRequestType = 0;
    setup->bRequest = 5;
    setup->wValue = address;
    usbControlSend(usb, dev, setup);

    khsFree(setup);
}

void usbDeviceSetConfiguration(struct usb* usb, struct usb_device* dev, int cfg)
{
    struct usb_setup* setup = khsAlloc(sizeof(*setup));

    setup->bmRequestType = 0;
    setup->bRequest = 9;
    setup->wValue = cfg & 0xFF;
    usbControlSend(usb, dev, setup);
    khsFree(setup);
}

void scheduleConnection(struct usb* usb, 
                        struct driver_usb_port_interface* port)
{
    struct usb_connect* connect = khsAlloc(sizeof(*connect));
    connect->Port = port;
    connect->Usb = usb;
    workerSubmitTask(loadDriver, connect);
}
