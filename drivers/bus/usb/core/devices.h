/* device.h
   Purpose: Device lookup, enumeration etc. */
#pragma once

#include <power/usb.h>
#include "usb.h"
#include <power/list.h>

struct usb_device;

struct __packed usb_device_descriptor {
    uint8_t bLength;
    uint8_t bDescriptorType;
    uint16_t bcdUsb;

    uint8_t bDeviceClass;
    uint8_t bDeviceSubClass;
    uint8_t bDeviceProtocol;

    uint8_t bMaxPacketSize0;
    uint16_t idVendor;
    uint16_t idProduct;
    uint16_t bcdDevice;
    uint8_t iManufacturer;
    uint8_t iProduct;
    uint8_t iSerialNumber;
    uint8_t bNumConfigurations;
};

struct __packed usb_cfg_descriptor {
    uint8_t bLength;
    uint8_t bDescriptorType;
    uint16_t wTotalLength;

    uint8_t bNumInterfaces;
    uint8_t bConfigurationValue;
    uint8_t iConfiguration;

    uint8_t bmAttributes;
    uint8_t bMaxPower;
};

struct __packed usb_if_descriptor {
    uint8_t bLength;
    uint8_t bDescriptorType;

    uint8_t bInterfaceNumber;
    uint8_t bAlternateSetting;
    uint8_t bNumEndpoints;

    uint8_t bInterfaceClass;
    uint8_t bInterfaceSubClass;
    uint8_t bInterfaceProtocol;
    uint8_t iInterface;
};

struct __packed usb_ep_descriptor {
    uint8_t bLength;
    uint8_t bDescriptorType;

#define USB_ENDPOINT_DIRECTION_BIT (1 << 7)
#define USB_ENDPOINT_ADDRESS_BITS  (15 << 0)
    uint8_t bEndpointAddress;

    uint8_t bmAttributes;

#define USB_ENDPOINT_MAX_PACKET_BITS (2047 << 0)
#define USB_ENDPOINT_TRANSACT_OPP_BITS (3 << 11)
    uint16_t wMaxPacketSize;

    uint8_t bInterval; /*< Polling interval */
};

void usbInitializeDevices(struct usb*);
struct usb_device* usbCreateDevice(struct usb*,
                                   const struct usb_device_descriptor* dev);
struct usb_cfg_descriptor*
usbReadConfiguration(struct usb*, struct usb_address* addr, int idx);
struct usb_endpoint* usbCreateEndpoint(struct usb_ep_descriptor*);
void usbDeviceSetAddress(struct usb*, struct usb_device* dev, int address);
void usbDeviceSetConfiguration(struct usb*, struct usb_device* dev, int cfg);

void usbControlSend(struct usb*, struct usb_device* dev, struct usb_setup* data);
void usbModuleLoad(struct usb*, struct usb_interface*);

void usbControlRead(struct usb* usb, struct usb_address* address,
                    struct usb_setup* setup, void* restrict outbuffer);
