#include "usb.h"
#include "devices.h"
#include <power/driver.h>
#include <power/memory/heap.h>

#include <power/memory/zoned.h>
#include <power/scheduler.h>
#include <power/tty.h>

#include <power/error.h>

static void connect(struct driver_usb_port_interface* p, void* data)
{
    __unused(p);
    struct usb* usb = data;
    schedEventRaise(usb->ResetEvent);
}

static void triggerEvent(struct usb_transaction* transaction, void* d)
{
}

static int sendSetup(struct usb* usb, struct usb_device* dev,
                     struct usb_setup* data, void* buffer)
{
    if (data->bmRequestType & (1 << 7)) { /* Function-to-host. */
        usbControlRead(usb, &dev->Address, data, buffer);
        return 0;
    }

    if (!buffer) {
        usbControlSend(usb, dev, data);
        return 0;
    }

    /* WRITE to function */
    return -ERROR_INVALID_ARGUMENT;
}

struct usb* usbCreate()
{
    struct usb* usb = khsAlloc(sizeof(*usb));
    usb->ResetEvent = schedCreateEvent();
    usb->TransactionEvent = schedCreateEvent();
    usb->AddressPool = zpgInit(1, 126, 1, 0, NULL);
    usb->ResetLock = schedCreateSemaphore(1);
    usb->Base.sendSetup = sendSetup;

    struct kdriver_manager* man = modGetDriver("ehci");
    if (!man) {
        trmLogfn("No CTL driver!");
        return NULL;
    }

    struct driver_usb_ctl_interface* inf;
    inf = man->Info->Interface;
    usb->Controller = inf;
    inf->ConnectHandler = connect;
    inf->HandlerPrivData = usb;

    return usb;
}
