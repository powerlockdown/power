#include <power/driver.h>
#include "devices.h"
#include "usb.h"

void main();

static struct driver_info dinfo = {
    .Name = "usb",
    .EntryPoint = main,
    .ConditionalLoading = NULL,
    .Role = DRIVER_ROLE_BUS,
};

struct driver_info*
driverQuery()
{
    return &dinfo;
}

void main()
{
    dinfo.Interface = usbCreate();
    usbInitializeDevices(dinfo.Interface);
}
