/* usb.h
   Purpose: USB STRUCT */
#pragma once

#include <power/driver.h>
#include <power/memory/zoned.h>
#include <power/vector.h>
#include <power/usb.h>

struct usb* usbCreate();
