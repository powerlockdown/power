USB_BUILD=build/drivers/bus/usb
USB_SOURCE=drivers/bus/usb/core
USB_OUTPUT=$(USB_BUILD)/usb.kd

USB_CFILES=main.c usb.c devices.c device_detect.c
USB_CSOURCES:=$(call driver_easy_source,USB)
USB_CBUILD:=$(call driver_easy_build,USB)

AUXFS_FILES+=$(USB_OUTPUT)

$(USB_OUTPUT): $(USB_CBUILD) $(KERNEL_STUB)
	$(CC) $(DRIVER_LDFLAGS) $(USB_CBUILD) -o $@

$(eval $(call driver_easy_c,$(USB_BUILD),$(USB_SOURCE)))

$(eval $(call driver_easy_std_tasks,usb,USB))
