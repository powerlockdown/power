/* disk.h
   Purpose: disk interface */
#pragma once

#include "power/scsi.h"
#include <power/usb.h>
#include <power/driver.h>

struct um_device {
    struct scsi_device Base;

    int MaxPacketSize;
    struct usb_interface* Interface;

    struct usb_endpoint* BulkOut;
    struct usb_endpoint* BulkIn;
    void* Reserved; /*< Maybe interrupt endpoint for CBI. */

    struct s_event* InterruptEvent;
    struct driver_usb_ctl_interface* Controller;
    void (*transport)(struct um_device*, const void* data);
};

void massCreateDiskInterface(void);
void massCreateDiskDevice(struct usb_interface* interface,
                         struct driver_usb_ctl_interface* ctl);
