/* probe.h
   Purpose: device probing */
#pragma once

#include <power/driver.h>

int massLoadDevice(struct driver_usb_bus_interface* usb,
                   struct usb_interface* dev);
