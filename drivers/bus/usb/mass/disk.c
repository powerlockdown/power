#include "bbb.h"

#include <power/driver.h>
#include <power/list.h>
#include <power/memory/heap.h>

#include <power/scsi.h>
#include <power/usb.h>
#include <power/vector.h>

#include <power/error.h>
#include <power/memory/memset.h>
#include <power/tty.h>

#include "disk.h"

extern struct driver_info dinfo;

struct um_disk {
    struct driver_disk_interface Base;

    int Max;
    struct vector Devices;
};

static int getDeviceCount(struct driver_disk_interface* i)
{
    struct um_disk* disk = (void*)i;
    return disk->Devices.Length;
}

static int getMaxDeviceCount(struct driver_disk_interface* i)
{
    struct um_disk* disk = (void*)i;
    return disk->Max;
}

static struct driver_disk_device_interface* 
getDevice(struct driver_disk_interface* i, int device)
{
    struct um_disk* disk = (void*)i;
    if ((size_t)device > disk->Devices.Length) {
        return NULL;
    }

    return disk->Devices.Data[device];
}

void massCreateDiskInterface()
{
    struct um_disk* dk = khsAlloc(sizeof(*dk));
    dk->Devices = vectorCreate(3);
    dk->Max = 16;

    dk->Base.getMaxDeviceCount = getMaxDeviceCount;
    dk->Base.getDeviceCount = getDeviceCount;
    dk->Base.getDevice = getDevice;
    dinfo.SupplementaryInterface = dk;
}

void massCreateDiskDevice(struct usb_interface* interface,
                          struct driver_usb_ctl_interface* ctl)
{
    struct um_disk* dk = dinfo.SupplementaryInterface;
    struct um_device* device = khsAlloc(sizeof(*device));
    device->Controller = ctl;

    int direction;
    if (interface->Protocol == USB_MASS_BBB) {
        struct usb_endpoint* ep;
        listForEach(interface->Endpoints, epl) {
            ep = (struct usb_endpoint*)epl;
            direction = (ep->Attributes & (1 << 7)) >> 7;
            if (direction) {
                device->BulkIn = ep;
            } else {
                device->BulkOut = ep;
            }
        }
    }

    device->Base.sendCommand = bbbSendCommand;

    device->MaxPacketSize = 16;
    device->Interface = interface;
    device->InterruptEvent = schedCreateEvent();
    interface->DevPriv = device;
    
    scsiAttachDevice(&device->Base, 0, 0);
    vectorInsert(&dk->Devices, device);
}

void massTriggerEvent(struct usb_transaction* t, void* data)
{
    (void)t;
    struct um_device* dev = data;
    schedEventRaise(dev->InterruptEvent);
}

int bbbSendCommand(struct scsi_device* scs, struct scsi_packet* packet)
{
    int error = 0;

    struct um_device* device = (void*)scs;
    struct bulk_cbw* cbw = khsAlloc(sizeof(*cbw));
    if (packet->CommandLength > 16) {
        error = -ERROR_INVALID_RANGE;
        goto earlyfail;
    }

    /* Create and send the CBW */
    cbw->dCBWTag = 0x55;
    cbw->dCBWSignature = BULK_SIGNATURE;
    cbw->bCBWCBLength = packet->CommandLength;
    cbw->dCBWDataTransferLength = packet->TransferLength;
    if (!(packet->Flags & SCSI_CMD_WRITE_BIT) && packet->TransferLength) {
        cbw->bmCBWFlags = (1 << 7);
    }

    memcpy(cbw->CBWCB, packet->CommandData, packet->CommandLength);

    struct driver_usb_ctl_interface* ctl = device->Controller;
    schedEventResetFlag(device->InterruptEvent);

    struct usb_transaction* t = khsAlloc(sizeof(*t));
    usbCreateAddressInterface(&t->Address, device->Interface, device->BulkOut);
    t->Buffer = cbw;
    t->BufferLength = sizeof(*cbw);
    t->TransferType = USB_TRANSFER_DIR_OUT_BIT | USB_TRANSACTION_BULK;
    t->HandlerPrivData = device;
    t->CompleteHandler = massTriggerEvent;
    
    ctl->scheduleTransaction(ctl, t);
    schedEventPause(device->InterruptEvent);
    if (t->Status) {
        error = t->Status;
        trmLogfn("Error during CBW submission: %y", t->Status);
        goto ef1;
    }

    /* Actually send/receive the data */
    if (packet->TransferLength) {
        schedEventResetFlag(device->InterruptEvent);

        t->Buffer = packet->TransferBuffer;
        t->BufferLength = packet->TransferLength;
        if (!(packet->Flags & SCSI_CMD_WRITE_BIT)) {
            usbCreateAddressInterface(&t->Address, device->Interface, device->BulkIn);
            t->TransferType = USB_TRANSFER_DIR_IN_BIT | USB_TRANSACTION_BULK;
        }

        ctl->scheduleTransaction(ctl, t);
        schedEventPause(device->InterruptEvent);

        if (t->Status) {
            error = t->Status;
            trmLogfn("Error during BBB %s: %y", 
                     (packet->Flags & SCSI_CMD_WRITE_BIT) ? "write" : "read",
                     t->Status);
            goto ef1;
        }
    }

    /* Receive the CSW */
    schedEventResetFlag(device->InterruptEvent);
    struct bulk_csw* csw = khsAlloc(sizeof(*csw));
    usbCreateAddressInterface(&t->Address, device->Interface, device->BulkIn);

    t->Buffer = csw;
    t->BufferLength = sizeof(*csw);
    t->TransferType = USB_TRANSFER_DIR_IN_BIT | USB_TRANSACTION_BULK;
    ctl->scheduleTransaction(ctl, t);
    schedEventPause(device->InterruptEvent);

    if (csw->dCSWStatus) {
        trmLogfn("Error during CSW fetch: %p", csw->dCSWStatus);
    }

    khsFree(csw);

ef1:
    khsFree(t);
earlyfail:
    khsFree(cbw);
    return error;
}
