USBMASS_BUILD=build/drivers/bus/usb/mass
USBMASS_SOURCE=drivers/bus/usb/mass
USBMASS_OUTPUT=$(USBMASS_BUILD)/usbmass.kd

USBMASS_CFILES=main.c disk.c probe.c
USBMASS_CSOURCES:=$(call driver_easy_source,USBMASS)
USBMASS_CBUILD:=$(call driver_easy_build,USBMASS)

AUXFS_FILES+=$(USBMASS_OUTPUT)

$(USBMASS_OUTPUT): $(SCSI_OUTPUT) $(USBMASS_CBUILD) $(KERNEL_STUB)
	$(CC) $(DRIVER_LDFLAGS) -L$(SCSI_BUILD) $(USBMASS_CBUILD) -lscsi -o $@

$(eval $(call driver_easy_c,$(USBMASS_BUILD),$(USBMASS_SOURCE)))

$(eval $(call driver_easy_std_tasks,usbmass,USBMASS))
