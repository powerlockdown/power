#include "disk.h"
#include "power/memory/heap.h"
#include <power/driver.h>

#include "probe.h"

void main(struct kdriver_manager*);

struct driver_info dinfo = {
    .Name = "usbmass",
    .Role = DRIVER_ROLE_USB_DEVICE,
    .Interface = NULL,
    .SupplementaryRole = DRIVER_ROLE_DISK,
    .SupplementaryInterface = NULL,
    .EntryPoint = main
};

struct driver_info* driverQuery()
{
    return &dinfo;
}

void main(struct kdriver_manager* man)
{
    struct driver_usb_device_interface* in = khsAlloc(sizeof((*in)));
    in->Class = 0x8; /*< Mass storage device. */
    in->Subclass = 0xFF;
    in->Protocol = 0xFF;
    in->deviceLoad = massLoadDevice;

    massCreateDiskInterface();
    dinfo.Interface = in;
}
