#include "probe.h"
#include "disk.h"
#include "power/error.h"
#include "power/tty.h"

#include <power/driver.h>
#include <power/usb.h>

int massLoadDevice(struct driver_usb_bus_interface* usb,
                   struct usb_interface* dev)
{  
    if (dev->Subclass != 6 && dev->Protocol != 0x50) {
        trmLogfn("Sorry, not implemented: Non-BBB and non-trans SCSI device detected");
        return -ERROR_NOT_SUPPORTED;
    }

    struct usb* u = (void*)usb;
    massCreateDiskDevice(dev, u->Controller);
    return 0;
}
