/* bbb.h
   Purpose: support for Bulk-only devices */
#pragma once

#include <power/config.h>
#include <power/scsi.h>

#define BULK_SIGNATURE 0x43425355

#define BULK_ERROR_OK    0x0
#define BULK_ERROR_FAILED 0x1
#define BULK_ERROR_PHASE  0x2

struct __packed bulk_cbw {
    uint32_t dCBWSignature;
    uint32_t dCBWTag;
    uint32_t dCBWDataTransferLength;

    uint8_t bmCBWFlags;
    uint8_t bCBWLUN;
    uint8_t bCBWCBLength;

    uint8_t CBWCB[16];
};

struct __packed bulk_csw {
    uint32_t dCSWSignature;
    uint32_t dCSWTag;
    uint32_t dCSWDataResidue;
    uint8_t dCSWStatus;
};

int bbbSendCommand(struct scsi_device* device, struct scsi_packet*);
