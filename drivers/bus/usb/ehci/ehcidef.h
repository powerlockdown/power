/* ehcidef.h
   Purpose: EHCI register map */
#pragma once

#include <power/config.h>
#include <stdint.h>

/**Program the CTRLDSSEGMENT register with 4-Gigabyte segment where all of the
  interface data structures are allocated.

  * Write the appropriate value to the USBINTR register to enable the
  appropriate interrupts.

  * Write the base address of the Periodic Frame List to the
  PERIODICLIST BASE register. If there are no work items in the periodic
  schedule, all elements of the Periodic Frame List should have their T-Bits set
  to a one.

  * Write the USBCMD register to set the desired interrupt threshold, frame list
  size (if applicable) and turn the host controller ON via setting the Run/Stop
  bit.

  * Write a 1 to CONFIGFLAG register to route all ports to the EHCI controller
  (see Section 4.2).
*/

#define EHCI_HCCPARAMS_PROG_FRL_BIT (1 << 1)
#define EHCI_HCSPARAMS_PPC_BIT (1 << 4)

struct __attribute__((packed)) ehci_cap_reg { /*< EHCI capability registers */
    uint8_t Length;
    uint8_t Reserved;
    uint16_t Version;
    uint32_t HcsParams;
    uint32_t HccParams;
    uint64_t CmpPortRoute;
};

#define EHCI_OP_FRL_SIZE_SHIFT   2
#define EHCI_OP_FRL_SIZE_BITS    (0b11 << EHCI_OP_FRL_SIZE_SHIFT)
#define EHCI_OP_FRL_SIZE_512_BIT (0b01 << EHCI_OP_FRL_SIZE_SHIFT)

struct __attribute__((packed)) ehci_op_reg { /*< EHCI operational registers */
    uint32_t Command;
    uint32_t Status;
    uint32_t IntrEnable;
    uint32_t FrameIndex;
    uint32_t CtrlDsSegment;
    uint32_t PeriodicListBase;
    uint32_t AsyncListAddr;
    uint8_t Reserved[36];
    uint32_t ConfigFlag;
    uint32_t PortStsCtl;
};

#define EHCI_USBINTR_ASYNC_ADV_BIT      (1 << 5)
#define EHCI_USBINTR_HOST_ERROR_BIT     (1 << 4)
#define EHCI_USBINTR_FRAME_ROLLOVER_BIT (1 << 3)
#define EHCI_USBINTR_PORT_CHG_BIT       (1 << 2)
#define EHCI_USBINTR_ERROR_BIT          (1 << 1)
#define EHCI_USBINTR_GENERIC_BIT        (1 << 0)

#define EHCI_USBCMD_DOORBELL_BIT (1 << 6)
#define EHCI_USBCMD_ASS_ENB_BIT (1 << 5)
#define EHCI_USBCMD_PSS_ENB_BIT (1 << 4)
#define EHCI_USBCMD_RUN_BIT (1 << 0)

#define EHCI_PORTSC_CONNECTED_BIT (1 << 0)
#define EHCI_PORTSC_RESET_BIT (1 << 8)
#define EHCI_PORTSC_ENABLE_BIT (1 << 2)

#define EHCI_MAKE_DMA(A) ((uint32_t)(uintptr_t)(A))
#define EHCI_DMA(A) EHCI_MAKE_DMA(A) 

/* EHCI Queue Head (EHCI spec, 3.6 "Queue Head"). 
   Must be aligned to a 32-byte boundary. 

   All of these fields encode other bits
   that are not implied by their names. */
struct __packed
__ehci_qhead {
#define EHCI_NEXT_TYPE_BITS    (3 << 1)
#define EHCI_NEXT_TYPE_ITD_BIT (0 << 1)
#define EHCI_NEXT_TYPE_QH_BIT  (1 << 1)
#define EHCI_NEXT_TYPE_SITD_BIT (2 << 1)
#define EHCI_NEXT_TYPE_FSTN_BIT (3 << 1)
    uint32_t HorzNext; 

#define EHCI_QH_INACTIVE_ON_NEXT_BIT (1 << 7)
#define EHCI_QH_ADDRESS_BITS         (0x7F)
    uint8_t Address;

#define EHCI_QH_RECLAIM_BIT (1 << 7)
#define EHCI_QH_EPS_HIGH_BIT (2 << 4)
#define EHCI_QH_EPS_FULL_BIT (0 << 4)
#define EHCI_QH_EPS_LOW_BIT (1 << 4)
#define EHCI_QH_ENDP_BITS   (0xF << 4)
    uint8_t Endpoint;

#define EHCI_QH_CTL_ENDP_BIT (1 << 11)
    uint16_t MaxLength;

    uint8_t UframeSmask;
    uint8_t UframeCmask;
    uint16_t PortHubAddress;

    uint32_t CurrentQueueItem; /*< qTD */
    uint32_t NextQueueItem;    /*< qTD */
    uint32_t AltNextQueueItem; /*< qTD */
    
    uint8_t Status;
    uint8_t PidCode;
    uint16_t ByteCount; /*< Up to 1024. */

    uint32_t BufferPointer[5];
};

/* EHCI spec, 3.5 "Queue Element Transfer Descriptor (qTD)".
   Must be aligned to a 32-byte boundary. */
struct __packed
__ehci_qtd {
#define EHCI_QTD_TERMINATE_BIT (1 << 0)
    uint32_t Next;
    uint32_t AlternateNext;

#define EHCI_QTD_ACTIVE_BIT (1 << 7)
#define EHCI_QTD_HALTED_BIT (1 << 6)
#define EHCI_QTD_BUFFER_ERROR_BIT (1 << 5)
#define EHCI_QTD_BABBLE_BIT       (1 << 4)
#define EHCI_QTD_TRANSACT_ERROR_BIT (1 << 3)
#define EHCI_QTD_MISSED_UFRAME_BIT  (1 << 2)
#define EHCI_QTD_SPLIT_TRANSACT_BIT (1 << 1)
#define EHCI_QTD_PING_BIT           (1 << 0)
    uint8_t Status;

#define EHCI_QTD_CERR_SHIFT 3
#define EHCI_QTD_PID_OUT_BIT   0
#define EHCI_QTD_PID_IN_BIT    1
#define EHCI_QTD_PID_SETUP_BIT 2
#define EHCI_QTD_INTR_BIT (1 << 7)
    uint8_t PidCode;
    uint16_t ByteCount; /* Up to 20480 (= 5 pages) */

    uint32_t BufferPointer[5];
};
