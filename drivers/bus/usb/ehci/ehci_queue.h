/* ehci_queue.h
   Purpose: */
#pragma once

#include "ehci_state.h"
#include "ehcidef.h"
#include "power/config.h"
#include "power/usb.h"
#include <power/list.h>

#define IS_ASYNC_LIST(T)                                                       \
    ((T) == USB_TRANSACTION_CONTROL || (T) == USB_TRANSACTION_BULK)

struct ehci_qtd {
    struct list_item List;

    int PidCode;
    struct page* DmaBuffer;
    struct __ehci_qtd* Repr;
};

struct ehci_qhead {
    struct list_item List;

    int Type;
    int QtdCount;
    int PollingInterval;

    struct __ehci_qhead* Repr;
    struct ehci_qtd* Descriptors;
    struct usb_address Address;
};

__always_inline static bool
ehciQueueAddressCompare(struct usb_transaction* transaction,
                        struct ehci_qhead* qh)
{
    bool address = transaction->Address.Address == qh->Address.Address;
    bool endpoint = transaction->Address.Endpoint == qh->Address.Endpoint;
    return address && endpoint;
}

struct ehci_qhead* ehciCreateQhead(struct ehci*,
                                   const struct usb_address* address, int type);

void ehciAppendQueue(struct ehci_qhead* q1, struct ehci_qhead* q2);

void ehciInsertQueue(struct ehci*, struct ehci_qhead* qh);

int ehciGetQueueTransfer(struct ehci*,
                         const struct usb_transaction* transfer,
                         struct ehci_qhead** qh);

void ehciQueueSetAddress(struct ehci_qhead*, const struct usb_address*);

/* QTD */

struct ehci_qtd* ehciCreateQtd(struct ehci*, int pid);

void ehciInsertQtd(struct ehci_qtd*, struct ehci_qhead* qh);

int ehciQtdSetBuffer(struct ehci_qtd*, const void* data, int length);

void ehciQtdSetActive(struct ehci_qtd* qtd);

struct ehci_qtd*
ehciGetQueueDscTransfer(struct ehci*, struct ehci_qhead* qh,
                        const struct usb_transaction* transfer);

/* Gathers DMA buffers onto `buffer`. */
int ehciQtdGatherDma(struct ehci_qtd*, void* buffer, size_t length);

void ehciQtdSetInterrupt(struct ehci_qtd*, struct ehci*,
                         struct usb_transaction* transaction);
