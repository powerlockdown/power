/* ehci_state.h
   Purpose: EHCI state management */
#pragma once

#include <power/driver.h>
#include "ehci_port.h"
#include "ehcidef.h"

#include <power/pci.h>
#include "power/usb.h"
#include <power/vector.h>

struct ehci {
    struct driver_usb_ctl_interface Base;

    struct vector Ports;
    struct vector Pending; /*< Pending transactions 
                               with CompletionHandlers. */

    struct mm_pool* QhPool;
    struct mm_pool* QtdPool;

    struct s_event* DoorbellEvent;

    struct ehci_qhead* PeriodicQueue;
    struct ehci_qhead* Queue;

    int FrameListSize;
    uint32_t* FrameList;
    
    volatile struct ehci_cap_reg* CapReg;
    volatile struct ehci_op_reg* OpReg;
};

struct ehci* ehciCreateState(struct pci_device* dev);
void ehciSetup(struct ehci* ehci);
void ehciStart(struct ehci* ehci, bool start);

#define EHCI_CONTROL_ENDP_BIT (1 << 1) /*< The endpoint specified \
                                            is a control. */

void ehciScheduleTransfer(struct ehci* ehci, struct usb_transaction* transfer);
void ehciSchedulePeriodic(struct ehci*, struct usb_transaction* transact);

void ehciRingDoorbell(struct ehci* ehci);

__always_inline
static void ehciWrite(volatile uint32_t* addr, uint32_t value)
{
    *addr = value;
    __barrier();
}

/* No ehciRead. */

__always_inline
static void ehciWriteModify(volatile uint32_t* addr, uint32_t clear, uint32_t set)
{
    uint32_t val = *addr;
    val &= ~clear;
    val |= set;
    
    (*addr) = val;
    __barrier();
}
