#include "ehci_queue.h"

#include "ehci_state.h"
#include "ehcidef.h"
#include <power/config.h>

#include <power/memory/heap.h>
#include <power/memory/page.h>
#include <power/memory/pfa.h>

#include "power/usb.h"
#include <power/memory/pool.h>
#include <power/tty.h>

#include <power/elf.h>
#include <power/error.h>
#include <power/list.h>

#include <power/memory/memset.h>

struct ehci_qhead* ehciCreateQhead(struct ehci* ehci,
                                   const struct usb_address* addr, int type)
{
    struct ehci_qhead* head = khsAlloc(sizeof(*head));
    if (!head) {
        return NULL;
    }

    head->Type = type;
    head->QtdCount = 0;
    listInit(&head->List);

    struct __ehci_qhead* repr = poolAllocObject(ehci->QhPool);
    if (!repr) {
        khsFree(head);
        return NULL;
    }

    memset(repr, 0, sizeof(*repr));
    if (type == USB_TRANSACTION_CONTROL) {
        repr->MaxLength |= EHCI_QH_CTL_ENDP_BIT;
    }

    head->Repr = repr;
    if (addr) {
        ehciQueueSetAddress(head, addr);
    }

    repr->NextQueueItem = EHCI_QTD_TERMINATE_BIT;
    return head;
}

void ehciQueueSetAddress(struct ehci_qhead* qhead, const struct usb_address* address)
{
    qhead->Address.Address = address->Address;
    qhead->Address.Endpoint = address->Endpoint;

    qhead->Repr->Address = address->Address & EHCI_QH_ADDRESS_BITS;
    qhead->Repr->Endpoint = (address->Endpoint) & 0xF;
}

void ehciAppendQueue(struct ehci_qhead* q1, struct ehci_qhead* q2)
{
    listEmplace(&q1->List, &q2->List);
    q1->Repr->HorzNext = EHCI_DMA(q2->Repr) | EHCI_NEXT_TYPE_QH_BIT;
    if (q2->List.Next) {
        /* TODO: Fix this when implementing ISO. */
        struct ehci_qhead* qh = (struct ehci_qhead*)q2->List.Next;
        q2->Repr->HorzNext = EHCI_DMA(qh->Repr) | EHCI_NEXT_TYPE_QH_BIT;
        return;
    }

    q2->Repr->HorzNext |= EHCI_QTD_TERMINATE_BIT;
}

/* NOTE: This function only works for ASYNC lists! */
void ehciInsertQueue(struct ehci* ehci, struct ehci_qhead* qh)
{
    if (!ehci->Queue) {
        ehci->Queue = qh;
        qh->Repr->Endpoint |= EHCI_QH_RECLAIM_BIT;
        qh->Repr->HorzNext = EHCI_DMA(qh->Repr) | EHCI_NEXT_TYPE_QH_BIT;
        ehciWrite(&ehci->OpReg->AsyncListAddr, (uint32_t)(uintptr_t)qh->Repr);
        return;
    }

    listInsert(&ehci->Queue->List, &qh->List);

    /* Insert onto repr too. */
    struct ehci_qhead* prev = (struct ehci_qhead*)qh->List.Previous;
    prev->Repr->HorzNext = EHCI_DMA(qh->Repr) | EHCI_NEXT_TYPE_QH_BIT;
    qh->Repr->HorzNext = EHCI_DMA(ehci->Queue->Repr) | EHCI_NEXT_TYPE_QH_BIT;
}

/* Chooses most suitable QH for transfering, given `transfer` restrictions. */
int ehciGetQueueTransfer(struct ehci* ehci,
                         const struct usb_transaction* transfer,
                         struct ehci_qhead** cqh)
{
    int type = transfer->TransferType & 0b11;
    struct ehci_qhead *qh, *chosen = NULL;

    if (!ehci->Queue) {
        goto createq;
    }

    listForEach(&ehci->Queue->List, lqh)
    {
        qh = (struct ehci_qhead*)lqh;
        if (qh->Type == type
            && (qh->Address.Address == transfer->Address.Address)
            && (qh->Address.Endpoint == transfer->Address.Endpoint)) {
            chosen = qh;
            break;
        }
    }

    if (!chosen) {
        goto createq;
    }

    (*cqh) = qh;
    return 0;

createq:
    qh = ehciCreateQhead(ehci, &transfer->Address, type & USB_TRANSACTION_BITS);
    if (!qh) {
        return -ERROR_NO_MEMORY;
    }

    (*cqh) = qh;
    return -ERROR_NOT_FOUND;
}

/* QTD */

static struct page* ehciAllocateBuffer(int length);

struct ehci_qtd* ehciCreateQtd(struct ehci* ehci, int pid)
{
    struct ehci_qtd* qtd = khsAlloc(sizeof(*qtd));
    if (!qtd) {
        return NULL;
    }

    listInit(&qtd->List);

    qtd->PidCode = pid;
    qtd->Repr = poolAllocObject(ehci->QtdPool);
    if (!qtd->Repr) {
        khsFree(qtd);
        return NULL;
    }

    memset(qtd->Repr, 0, sizeof(*qtd->Repr));

    qtd->Repr->PidCode = qtd->PidCode & 0b11;
    qtd->Repr->Next = EHCI_QTD_TERMINATE_BIT;
    qtd->Repr->AlternateNext = EHCI_QTD_TERMINATE_BIT;
    return qtd;
}

void ehciInsertQtd(struct ehci_qtd* qtd, struct ehci_qhead* qh)
{
    qh->QtdCount++;
    if (!qh->Descriptors) {
        qh->Descriptors = qtd;
        qh->Repr->NextQueueItem = EHCI_MAKE_DMA(qtd->Repr);
        return;
    }

    listInsert(&qh->Descriptors->List, &qtd->List);
    struct ehci_qtd* prev = (struct ehci_qtd*)qtd->List.Previous;
    prev->Repr->Next = EHCI_MAKE_DMA(qtd->Repr);
    qtd->Repr->Next = EHCI_QTD_TERMINATE_BIT;

    if (!(qh->Repr->NextQueueItem & ~1)) {
        qh->Repr->NextQueueItem = EHCI_MAKE_DMA(qtd->Repr);
    }
}

int ehciQtdSetBuffer(struct ehci_qtd* qtd, const void* data, int length)
{
    int reallength = length;
    if (!length) {
        /* Might be used for a CONTROL transaction. */
        goto ret;
    }

    if (length > 5 * 4096)
        return -ERROR_TOO_BIG;
    if (length % 4096 != 0)
        length = alignUp(length, 4096);
    if (!qtd->DmaBuffer)
        qtd->DmaBuffer = ehciAllocateBuffer(length);

    int tmplength = length;

    void* write;
    struct page* p;
    struct page* pa = qtd->DmaBuffer;

    int i = 0;
    while (pa != NULL && tmplength > 0) {
        p = (struct page*)pa;
        write = mmMakeWritable(p->Address);

        if (qtd->PidCode != USB_PID_IN)
            memcpy(write, data, __min(tmplength, 4096));

        tmplength -= 4096;
        qtd->Repr->BufferPointer[i] = (uint32_t)(uintptr_t)pa->Address;

        i++;
        pa = (struct page*)p->List.Next;
    }

ret:
    qtd->Repr->ByteCount = reallength;
    return 0;
}

static struct page* ehciAllocateBuffer(int length)
{
    int pages = length / 4096;
    struct page* page = mmAllocPage(pages);
    if (!page) {
        return page;
    }

    return page;
}

void ehciQtdSetActive(struct ehci_qtd* qtd)
{
    qtd->Repr->Status |= EHCI_QTD_ACTIVE_BIT;
}

struct ehci_qtd* ehciGetQueueDscTransfer(struct ehci* ehci,
                                         struct ehci_qhead* qh,
                                         const struct usb_transaction* transfer)
{
    struct ehci_qtd *qtd, *chosen = NULL;

    if (!qh->Descriptors) {
        goto createq;
    }

    listForEach(&qh->Descriptors->List, lqtd)
    {
        qtd = (struct ehci_qtd*)lqtd;
        if (!(qtd->Repr->Status & EHCI_QTD_ACTIVE_BIT)) {
            chosen = qtd;
            break;
        }
    }

    if (!chosen) {
        goto createq;
    }

    chosen->PidCode = transfer->PidCode & 0b11;
    chosen->Repr->ByteCount = transfer->BufferLength;

    return chosen;

createq:
    qtd = ehciCreateQtd(ehci, transfer->PidCode);
    if (!qtd) {
        return NULL;
    }

    ehciInsertQtd(qtd, qh);
    return qtd;
}

int ehciQtdGatherDma(struct ehci_qtd* qtd, void* buffer, size_t length)
{
    void* wrote;

    /* HAS to be signed.
       Or else we will be stuck in a loop! */
    long tmp = length;
    struct page* p = qtd->DmaBuffer;
    while (p != NULL && tmp > 0) {
        wrote = mmMakeWritable(p->Address);
        memcpy(buffer, wrote, __min(length, 4096));

        tmp -= 4096;
        p = (struct page*)p->List.Next;
    }

    return 0;
}

void ehciQtdSetInterrupt(struct ehci_qtd* qtd, struct ehci* ehci,
                         struct usb_transaction* transaction)
{
    if (!transaction->CompleteHandler) {
        return;
    }

    vectorInsert(&ehci->Pending, transaction);
    qtd->Repr->PidCode |= EHCI_QTD_INTR_BIT;
    transaction->HcdReserved = qtd;
}
