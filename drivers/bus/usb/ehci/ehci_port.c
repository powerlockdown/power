#include "ehci_port.h"
#include <power/abstract/timer.h>
#include <power/driver.h>
#include "ehci_state.h"

#include "ehcidef.h"
#include <power/memory/heap.h>

static inline void __reset(struct driver_usb_port_interface* interface)
{
    return ehciPortReset((struct ehci_port*)interface);
}

struct ehci_port* ehciCreatePort(struct ehci* ehci, int index)
{
    volatile uint32_t* ptr = (&ehci->OpReg->PortStsCtl) + index;

    struct ehci_port* port = khsAlloc(sizeof(*port));
    port->Host = ehci;
    port->PortRawInfo = ptr;
    port->Index = index;
    port->Owner = EHCI_PORT_OWNER_SELF;
    port->Base.HasDevice = (*ptr) & EHCI_PORTSC_CONNECTED_BIT;
    port->Base.reset = __reset;
    listInit(&port->List);

    return port;
}

void ehciPortReset(struct ehci_port* port)
{
    port->Base.HasDevice = false;
    struct timer* tm = tmGetDefaultTimer();
    ehciWriteModify(port->PortRawInfo, EHCI_PORTSC_ENABLE_BIT, EHCI_PORTSC_RESET_BIT);
    tmSetReloadValue(tm, tmGetMillisecondUnit(tm) * 10);

    ehciWriteModify(port->PortRawInfo, EHCI_PORTSC_RESET_BIT, 0);
    tmSetReloadValue(tm, tmGetMillisecondUnit(tm) * 2);
}
