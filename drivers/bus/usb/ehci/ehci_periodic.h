/* ehci_periodic.h
   Purpose: support for periodic queues. */
#pragma once

#include "ehci_state.h"
#include "power/usb.h"

void ehciPeriodicFillFrameList(struct ehci*);
int ehciPeriodicSchedule(struct ehci*, struct usb_transaction* transaction);
