#include <power/paging.h>
#include "ehci_state.h"
#include <power/pci.h>
#include <power/tty.h>
#include <power/driver.h>

static struct driver_info dinfo;

static void main(struct kdriver_manager* man)
{
    uint32_t bar0;
    struct pci_device* dev = man->LoadReason;
    bar0 = pciReadDoubleWordFromDevice(dev, 0x10) & ~0xF;
    pgAddGlobalPage(bar0, bar0, PT_FLAG_WRITE | PT_FLAG_PCD);

    struct ehci* ehc = ehciCreateState(dev);
    ehciSetup(ehc);
    dinfo.Interface = ehc;
}

static struct driver_conditional_loading cld = {
    .RelationshipWithPrevious = DRIVER_CLD_RELATIONSHIP_NONE,
    .ConditionalType = DRIVER_CLD_TYPE_PCI,
    .HasNext = 0,
    .Pci = {
        .VendorId = PCI_DONT_CARE,
        .DeviceId = PCI_DONT_CARE,
        .Class = 0x0C,   /*< Serial bus controller */
        .Subclass = 0x03, /*< USB controller */
        .ProgIf = 0x20    /*< EHCI controller */
    }
};

static struct driver_info dinfo = {
    .Name = "ehci",
    .EntryPoint = main,
    .ConditionalLoading = &cld,
    .Role = 0,
};

struct driver_info*
driverQuery()
{
    return &dinfo;
}
