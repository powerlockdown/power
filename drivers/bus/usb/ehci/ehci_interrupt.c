#include "ehci_interrupt.h"

#include "ehci_port.h"
#include "ehci_queue.h"
#include "ehci_state.h"
#include "ehcidef.h"

#include <power/tty.h>
#include <power/error.h>

static int ehciStatusToCode(int status);
static inline void ehciCallConnectInterrupt(struct ehci* ehci)
{
    uint32_t raw;
    struct ehci_port* p;
    for (size_t i = 0; i < ehci->Ports.Length; i++) {
        p = ehci->Ports.Data[i];
        raw = *p->PortRawInfo;

        if (p->Base.HasDevice && !(raw & EHCI_PORTSC_CONNECTED_BIT)) {
            p->Base.HasDevice = false;
            if (!ehci->Base.DisconnectHandler) {
                return;
            }

            ehci->Base.DisconnectHandler(&p->Base, ehci->Base.HandlerPrivData);
            break;
        } else if (!p->Base.HasDevice && (raw & EHCI_PORTSC_CONNECTED_BIT)) {
            p->Base.HasDevice = true;
            if (!ehci->Base.ConnectHandler) {
                return;
            }

            ehci->Base.ConnectHandler(&p->Base, ehci->Base.HandlerPrivData);
            break;
        }
    }
}

static inline void ehciCallTransactionHandlers(struct ehci* ehci)
{
    struct ehci_qtd* qtd;
    struct usb_transaction* ts;
    for (size_t i = 0; i < ehci->Pending.Length; i++) {
        if (!ehci->Pending.Length) {
            break;
        }

        ts = ehci->Pending.Data[i];
        qtd = ts->HcdReserved;
        if (ts->TransferType & USB_TRANSFER_DIR_IN_BIT && ts->Buffer) {
            ehciQtdGatherDma(qtd, ts->Buffer, ts->BufferLength);
        }

        if (!ts->CompleteHandler) {
            goto remove;
        }

        if (!(qtd->Repr->Status & EHCI_QTD_ACTIVE_BIT)
            || ehci->OpReg->Status & EHCI_USBINTR_ERROR_BIT) {
            ts->Status = ehciStatusToCode(qtd->Repr->Status);

            /* Is auto-unhalting a good idea?
               qTD won't even be active anyways */
            qtd->Repr->Status &= ~EHCI_QTD_HALTED_BIT;
            ts->CompleteHandler(ts, ts->HandlerPrivData);
        }
    remove:
        vectorRemove(&ehci->Pending, ts);
        i--;
    }
}

void ehciInterrupt(struct irq_regs* state, void* d)
{
    __unused(state);
    struct ehci* ehci = d;
    uint32_t status = ehci->OpReg->Status;

    if (status & EHCI_USBINTR_PORT_CHG_BIT)
        ehciCallConnectInterrupt(ehci);
    if (status & EHCI_USBINTR_GENERIC_BIT)
        ehciCallTransactionHandlers(ehci);
    if (status & EHCI_USBINTR_ASYNC_ADV_BIT)
        schedEventRaise(ehci->DoorbellEvent);

    if (status & 0x3f)
        ehci->OpReg->Status = status & 0x3f;
}

int ehciStatusToCode(int status)
{
    if (!status) {
        return 0;
    }

    if (status & EHCI_QTD_BABBLE_BIT)
        return -ERROR_INVALID_RANGE;
    else if (status & EHCI_QTD_BUFFER_ERROR_BIT)
        return -ERROR_UNAVAILABLE;
    else if (status & EHCI_QTD_TRANSACT_ERROR_BIT)
        /* TRANSACT_ERROR is for stuff beyond timing out! */
        return -ERROR_TIMED_OUT;
    else if (status & EHCI_QTD_HALTED_BIT)
        return -ERROR_PIPE_BROKEN;

    return -ERROR_FORBIDDEN;
}
