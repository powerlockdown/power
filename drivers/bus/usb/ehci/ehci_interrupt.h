#pragma once

#include <power/interrupt.h>

void ehciInterrupt(struct irq_regs* regs, void* d);
