#include "ehci_periodic.h"
#include "ehci_queue.h"
#include "ehci_state.h"
#include "ehcidef.h"
#include "power/list.h"
#include "power/tty.h"

#include <power/error.h>
#include <power/usb.h>

/* The code is based the following idea:
       +===========================+
       |FRAME 1       ->    QH1    |
       |FRAME 2       ->    QH2    |
       |FRAME 3       ->    QH1    |
       |FRAME 4       ->    QH4    |
       |FRAME 5       ->    QH1    |
       |FRAME 6       ->    QH2    |
       |FRAME 7       ->    QH1    |
       |FRAME 8       ->    QH8    |
       |FRAME 9       ->    QH1    |
       |FRAME 10      ->    QH2    |
       |         .   .   .         |
       +===========================+

   Where each QH(n) is a queue head that gets
   executed every *n* frames.

   A more formal description: For each frame F, the
   QH(n) selected for such F is that, n is the
   biggest number inside QHc which is divisible
   by F; QHc is the collection of all possible QH
   polling intervals.

   If two or more transactions rely on the
   same polling interval, a new QH is created
   following the previous QH(n).
*/

static struct ehci_qhead*
ehciGetQueueHeadPeriodic(struct ehci* ehci, struct usb_transaction* transaction)
{
    int interval = (1 << (transaction->Interval - 1));
    int compare, oldi, i = 0;
    struct ehci_qhead* qhead;
    struct ehci_qhead* next;

    listForEach(&ehci->PeriodicQueue->List, pq) {
        qhead = (struct ehci_qhead*)pq;
        if (interval % qhead->PollingInterval == 0) {
            i = qhead->PollingInterval;
            compare = ehciQueueAddressCompare(transaction, qhead);
            if (qhead->Address.Address == 0xFF) {
                /* QH created from ehciPeriodicFillFrameList that
                   is still free. */
                ehciQueueSetAddress(qhead, &transaction->Address);
                next = qhead;
                goto ret;
            }

            if (compare) {
                next = qhead;
                goto ret;
            }

            oldi = i;
        }

        /* We got at the end of our periodic interval block
           and did not find any QH for us. */
        if (i && i != oldi) {
            goto create;
        }
    }

    return NULL;

create:
    next =
        ehciCreateQhead(ehci, &transaction->Address, USB_TRANSACTION_INTERRUPT);
    ehciAppendQueue(qhead, next);
ret:
    next->Repr->UframeSmask = (1 << transaction->Interval);
    return next;
}

/* Fills the frame list with interrupt QHs.

   Some of this code could be guarded by some
   static bool init variable.
*/
void ehciPeriodicFillFrameList(struct ehci* ehci)
{
    static struct ehci_qhead* qhs[32];

    int idx = 0;
    int begin = 128;

    /* Why use an static array if we could
       simply allocate the first QH and add
       other QHs by ehciAppendQueue?

       TODO: Please fix this. */
    while (begin > 0) {
        qhs[idx] = ehciCreateQhead(ehci, NULL, USB_TRANSACTION_INTERRUPT);
        qhs[idx]->PollingInterval = begin;

        qhs[idx]->Address.Address = 0xFF;
        if (idx > 0) {
            ehciAppendQueue(qhs[idx - 1], qhs[idx]);
        }

        idx++;
        begin /= 2;
    }

    ehci->PeriodicQueue = qhs[0];
    struct ehci_qhead* qh;

    for (int i = 0; i < ehci->FrameListSize; i++) {
        for (int j = idx - 1; j != 0; --j) {
            qh = qhs[j];
            if (i % 2 != 0) {
                /* the last item of qhs is presumed to be
                   QH for every frame (QH1) */
                ehci->FrameList[i] = EHCI_MAKE_DMA(qhs[idx - 1]->Repr) | EHCI_NEXT_TYPE_QH_BIT;
                break;
            }

            if (i % qh->PollingInterval != 0) {
                continue;
            }

            ehci->FrameList[i] = EHCI_MAKE_DMA(qh->Repr) | EHCI_NEXT_TYPE_QH_BIT;
        }
    }
}

int ehciPeriodicSchedule(struct ehci* ehci, struct usb_transaction* transaction)
{
    int type = (transaction->TransferType & USB_TRANSACTION_BITS);
    if (type == USB_TRANSACTION_ISOCHRONOUS) {
        /* TODO: PLEASE implement this in the future. */
        return -ERROR_INVALID_ARGUMENT;
    }

    int oldc;

    /* See ehciScheduleTransfer for comments. */
    struct ehci_qhead* qh = ehciGetQueueHeadPeriodic(ehci, transaction);
    qh->Repr->MaxLength |= __min(transaction->BufferLength, 1024);

    oldc = qh->QtdCount;
    struct ehci_qtd* qtd = ehciGetQueueDscTransfer(ehci, qh, transaction);
    if (oldc == 1 && qh->QtdCount == 1) {
        qh->Repr->NextQueueItem = (uint32_t)(uintptr_t)qtd->Repr;
    }

    qtd->Repr->PidCode = qtd->PidCode & 0b11;

    ehciQtdSetInterrupt(qtd, ehci, transaction);
    ehciQtdSetBuffer(qtd, transaction->Buffer, transaction->BufferLength);
    ehciQtdSetActive(qtd);
    
    if (!(ehci->OpReg->Command & EHCI_USBCMD_PSS_ENB_BIT)) {
        ehciWriteModify(&ehci->OpReg->Command, 0, EHCI_USBCMD_PSS_ENB_BIT);
    }
    return 0;
}
