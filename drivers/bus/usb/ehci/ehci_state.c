#include "ehci_state.h"

#include "ehci_periodic.h"
#include "ehci_port.h"
#include "ehci_queue.h"
#include <power/config.h>

#include "ehcidef.h"
#include <power/memory/heap.h>
#include <power/memory/pfa.h>
#include <power/memory/pool.h>
#include <power/pci.h>

#include <power/list.h>
#include <power/scheduler.h>
#include <power/tty.h>
#include <power/vector.h>

#include <power/usb.h>
#include <power/error.h>

#include "ehci_interrupt.h"

static void setupFrameList(struct ehci* ehci);

static struct driver_usb_port_interface*
ehciGetPort(struct driver_usb_ctl_interface*, int port);

static void ehciScheduleTransaction(struct driver_usb_ctl_interface*,
                                    struct usb_transaction* tract);

struct ehci* ehciCreateState(struct pci_device* dev)
{
    uint32_t bar0 = pciReadDoubleWordFromDevice(dev, 0x10) & ~0xF;
    volatile struct ehci_cap_reg* ehcap = (void*)(uint64_t)bar0;

    struct ehci* ehci = khsAlloc(sizeof(*ehci));
    ehci->QhPool = poolInit(sizeof(struct __ehci_qhead), 32);
    ehci->QtdPool = poolInit(sizeof(struct __ehci_qtd), 32);
    ehci->CapReg = ehcap;
    ehci->OpReg = PaAdd(ehcap, ehcap->Length);
    ehci->DoorbellEvent = schedCreateEvent();

    ehci->Base.PortCount = ehci->CapReg->HcsParams & 0xF;
    ehci->Base.getPort = ehciGetPort;
    ehci->Base.scheduleTransaction = ehciScheduleTransaction;

    ehci->Ports = vectorCreate(ehci->Base.PortCount);
    struct ehci_port* pt;
    for (int i = 0; i < ehci->Base.PortCount; i++) {
        pt = ehciCreatePort(ehci, i);
        vectorInsert(&ehci->Ports, pt);
    }

    pciHandleInterrupt(dev, ehciInterrupt, ehci);
    return ehci;
}

void ehciSetup(struct ehci* ehci)
{
    ehci->OpReg->IntrEnable = EHCI_USBINTR_ERROR_BIT | EHCI_USBINTR_PORT_CHG_BIT
        | EHCI_USBINTR_GENERIC_BIT | EHCI_USBINTR_HOST_ERROR_BIT
        | EHCI_USBINTR_ASYNC_ADV_BIT;

    /* Some places (e.g. QEMU when a MSD device is present) enables
       the PSS before passing onto us. */
    ehciWriteModify(&ehci->OpReg->Command, 
        EHCI_USBCMD_ASS_ENB_BIT | EHCI_USBCMD_PSS_ENB_BIT, EHCI_USBCMD_RUN_BIT);

    setupFrameList(ehci);
    ehciPeriodicFillFrameList(ehci);
    ehciWrite(&ehci->OpReg->ConfigFlag, 1);

    for (int i = 0; i < ehci->Base.PortCount; i++) {
        ehciCreatePort(ehci, i);
    }
}

void setupFrameList(struct ehci* ehci)
{
    void* frame;
    int framelistsz = 1024;
    if (ehci->CapReg->HccParams & EHCI_HCCPARAMS_PROG_FRL_BIT) {
        framelistsz = 1024;
        /* ehciWriteModify(&ehci->OpReg->Command, EHCI_OP_FRL_SIZE_BITS,
                        EHCI_OP_FRL_SIZE__BIT);*/
    }

    frame = mmAlignedPhysicalAlloc(framelistsz * sizeof(uint32_t), 4096);
    ehciWrite(&ehci->OpReg->PeriodicListBase, (uint32_t)((uint64_t)frame));
    ehciWrite(&ehci->OpReg->CtrlDsSegment, (uint32_t)(((uint64_t)frame) >> 32));

    {
        /* SET the T-bit for all frame elements. */
        int total = framelistsz / sizeof(uint32_t);
        uint32_t* write = mmMakeWritable(frame);
        for (int i = 0; i < total; i++) {
            write[i] = 1;
        }
    }

    ehci->FrameList = frame;
    ehci->FrameListSize = framelistsz;
}

void ehciScheduleTransfer(struct ehci* ehci, struct usb_transaction* transfer)
{
    struct ehci_qhead* chqh;
    int found = ehciGetQueueTransfer(ehci, transfer, &chqh);
    chqh->Repr->MaxLength |= __min(transfer->BufferLength, 1024);

    int old = chqh->QtdCount;
    struct ehci_qtd* chqtd = ehciGetQueueDscTransfer(ehci, chqh, transfer);
    ehciQtdSetBuffer(chqtd, transfer->Buffer, transfer->BufferLength);

    chqtd->Repr->PidCode = chqtd->PidCode & 0b11;

    ehciQtdSetInterrupt(chqtd, ehci, transfer);
    ehciQtdSetActive(chqtd);

    /* The controller will keep the stale
       QTD in memory and refuse to reload it
       even if we ring the doorbell because
       it is not advancing. */
    if (old == 1 && chqh->QtdCount == 1) {
        chqh->Repr->NextQueueItem = EHCI_DMA(chqtd->Repr);
        chqtd->Repr->Next = EHCI_DMA(chqtd->Repr);
    }

    if (!(ehci->OpReg->Command & EHCI_USBCMD_ASS_ENB_BIT)) {
        ehciWriteModify(&ehci->OpReg->Command, 0, EHCI_USBCMD_ASS_ENB_BIT);
    }

    /* Do this last or else we will enter
       a race condition with the controller
       and the above commented situation will
       occur AFTER that condition has passed. */
    if (found == -ERROR_NOT_FOUND)
        ehciInsertQueue(ehci, chqh);
}

static struct driver_usb_port_interface*
ehciGetPort(struct driver_usb_ctl_interface* ic, int port)
{
    struct ehci* ehci = (struct ehci*)ic;
    if (port >= ic->PortCount || port < 0) {
        return NULL;
    }

    return ehci->Ports.Data[port];
}

static void ehciScheduleTransaction(struct driver_usb_ctl_interface* ctl,
                                    struct usb_transaction* tract)
{
    struct ehci* ehci = (struct ehci*)ctl;

    tract->PidCode = USB_PID_OUT;
    if (tract->TransferType & USB_TRANSFER_DIR_IN_BIT) {
        tract->PidCode = USB_PID_IN;
    }

    if (tract->Flags & USB_TRANSACT_SETUP_PID_BIT) {
        tract->PidCode = USB_PID_SETUP;
    }

    int type = tract->TransferType & USB_TRANSACTION_BITS;
    switch (type) {
    case USB_TRANSACTION_CONTROL:
    case USB_TRANSACTION_BULK:
        ehciScheduleTransfer(ehci, tract);
        break;
    case USB_TRANSACTION_INTERRUPT:
        ehciPeriodicSchedule(ehci, tract);
        break;
    }
}

void ehciRingDoorbell(struct ehci* ehci)
{
    ehciWriteModify(&ehci->OpReg->Command, 0, EHCI_USBCMD_DOORBELL_BIT);
    schedEventPause(ehci->DoorbellEvent);
}
