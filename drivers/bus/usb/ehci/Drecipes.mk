EHCI_BUILD=build/drivers/bus/usb/ehci
EHCI_SOURCE=drivers/bus/usb/ehci
EHCI_OUTPUT=$(EHCI_BUILD)/usb_ehci.kd

EHCI_CFILES=main.c ehci_state.c ehci_port.c ehci_queue.c ehci_periodic.c \
			ehci_interrupt.c
EHCI_CSOURCES:=$(call driver_easy_source,EHCI)
EHCI_CBUILD:=$(call driver_easy_build,EHCI)

AUXFS_FILES+=$(EHCI_OUTPUT)

$(EHCI_OUTPUT): $(EHCI_CBUILD) $(KERNEL_STUB)
	$(CC) $(DRIVER_LDFLAGS) $(EHCI_CBUILD) -o $@

$(eval $(call driver_easy_c,$(EHCI_BUILD),$(EHCI_SOURCE)))

$(eval $(call driver_easy_std_tasks,ehci,EHCI))
