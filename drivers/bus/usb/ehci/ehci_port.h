/* ehci_port.h
   Purpose: port */
#pragma once

#include <power/config.h>
#include <power/driver.h>
#include <power/memory/pool.h>
#include <power/list.h>

enum {
    EHCI_PORT_OWNER_SELF = 1, /*< WE are the port owner */
    EHCI_PORT_OWNER_COMPANION, /*< They (UHCI or OHCI) are the port owner */
};

struct ehci_port {
    struct driver_usb_port_interface Base;
    struct list_item List;

    bool HasDevice;

    int Owner;
    int Index;

    struct mm_pool* QueuePool;

    struct ehci* Host;
    volatile uint32_t* PortRawInfo;
};

struct ehci_port* ehciCreatePort(struct ehci*, int index);
void ehciPortReset(struct ehci_port* port);

// int ehpPerformTransaction(struct ehci_port*, const void* data, int length, int pool);
