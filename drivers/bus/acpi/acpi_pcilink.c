#include "acpi_pcilink.h"
#include "acexcep.h"
#include "acpi_pci.h"
#include "acpixf.h"
#include "acrestyp.h"
#include "actypes.h"
#include <power/memory/heap.h>
#include <power/tty.h>
#include <power/vector.h>
#include <acpi.h>

static struct vector links; /*< Full of acpi_pci_link. */

static ACPI_STATUS __linkResourceCurrent(ACPI_RESOURCE* res, void* ctx)
{
    int* irqv = ctx;
    switch (res->Type) {
    case ACPI_RESOURCE_TYPE_START_DEPENDENT:
    case ACPI_RESOURCE_TYPE_END_TAG:
        return AE_OK;
    case ACPI_RESOURCE_TYPE_IRQ: {
        struct acpi_resource_irq* irq = &res->Data.Irq;
        (*irqv) = irq->Interrupts[0];
        break;
    }
    case ACPI_RESOURCE_TYPE_EXTENDED_IRQ: {
        struct acpi_resource_extended_irq* irq = &res->Data.ExtendedIrq;
        (*irqv) = irq->Interrupts[0];
        break;
    }
    default: {
        trmLogfn("_CRS is type %p, not IRQ or EXTENDED_IRQ", res->Type);
        return AE_OK;
    }
    }

    return AE_CTRL_TERMINATE;
}

static ACPI_STATUS __linkResourcePossible(ACPI_RESOURCE* res, void* ctx)
{
    struct acpi_pci_link* lnk = ctx;

    switch (res->Type) {
    case ACPI_RESOURCE_TYPE_START_DEPENDENT:
    case ACPI_RESOURCE_TYPE_END_TAG:
        return AE_OK;
    case ACPI_RESOURCE_TYPE_IRQ: {
        struct acpi_resource_irq* irq = &res->Data.Irq;
        for (int i = 0; i < MAX_POSSIBLE_IRQS && i < irq->InterruptCount; i++) {
            lnk->Possible[i] = irq->Interrupts[i];
            lnk->PossibleCount++;
        }

        lnk->Intr.Polarity = irq->Polarity;
        lnk->Intr.Trigger = irq->Triggering;
        break;
    }
    case ACPI_RESOURCE_TYPE_EXTENDED_IRQ: {
        struct acpi_resource_extended_irq* irq = &res->Data.ExtendedIrq;
        for (int i = 0; i < MAX_POSSIBLE_IRQS && i < irq->InterruptCount; i++) {
            lnk->Possible[i] = irq->Interrupts[i];
            lnk->PossibleCount++;
        }

        lnk->Intr.Polarity = irq->Polarity;
        lnk->Intr.Trigger = irq->Triggering;
        break;
    }
    default: {
        trmLogfn("_PRS is type %p, not IRQ or EXTENDED_IRQ", res->Type);
        return AE_OK;
    }
    }

    lnk->Intr.Type = res->Type;
    return AE_CTRL_TERMINATE;
}

static 
UINT32 __findLink(ACPI_HANDLE handle, UINT32 nest, void* ctx, void** ret)
{
    struct acpi_pci_link* lnk = khsAlloc(sizeof(*lnk));
    lnk->Handle = handle;

    acpiLnkGetActive(lnk);
    acpiLnkGetPossible(lnk);
    vectorInsert(&links, lnk);
    return AE_OK;
}

void acpiLnkInit()
{
    links = vectorCreate(4);
    AcpiGetDevices("PNP0C0F", __findLink, NULL, NULL);
}

int acpiLnkGetActive(struct acpi_pci_link* lnk)
{
    int irq, status = 0;
    AcpiWalkResources(lnk->Handle, "_CRS", __linkResourceCurrent, &irq);
    lnk->Active = irq;
    return status;
}

int acpiLnkGetPossible(struct acpi_pci_link* link)
{
    AcpiWalkResources(link->Handle, "_PRS", __linkResourcePossible, link);
    return 0;
}

#pragma GCC diagnostic ignored "-Wgnu-variable-sized-type-not-at-end"
#pragma GCC diagnostic push

struct lnk_resource {
    struct acpi_resource Resource;
    struct acpi_resource End;
};

#pragma GCC diagnostic pop

void acpiLnkSetIrq(struct acpi_pci_link* lnk, int irq)
{
    ACPI_BUFFER buffer;
    struct lnk_resource* res;
    res = khsAlloc(sizeof(*res) + 1);

    buffer.Pointer = res;
    buffer.Length = sizeof(*res) + 1;
    res->Resource.Length = sizeof(ACPI_RESOURCE);
    switch (lnk->Intr.Type) {
    case ACPI_RESOURCE_TYPE_EXTENDED_IRQ:
        res->Resource.Type = lnk->Intr.Type;
        res->Resource.Data.ExtendedIrq.Polarity = lnk->Intr.Polarity;
        res->Resource.Data.ExtendedIrq.Triggering = lnk->Intr.Trigger;
        res->Resource.Data.ExtendedIrq.InterruptCount = 1;
        res->Resource.Data.ExtendedIrq.Interrupts[0] = irq & 0xFF;
        res->Resource.Data.ExtendedIrq.ProducerConsumer = ACPI_CONSUMER;
        if (res->Resource.Data.ExtendedIrq.Triggering == ACPI_LEVEL_SENSITIVE) {
            res->Resource.Data.ExtendedIrq.Shareable = ACPI_SHARED;
        } else {
            res->Resource.Data.ExtendedIrq.Shareable = ACPI_EXCLUSIVE;
        }

        break;
    case ACPI_RESOURCE_TYPE_IRQ:
        res->Resource.Type = lnk->Intr.Type;
        res->Resource.Data.Irq.Polarity = lnk->Intr.Polarity;
        res->Resource.Data.Irq.Triggering = lnk->Intr.Trigger;
        res->Resource.Data.Irq.InterruptCount = 1;
        res->Resource.Data.Irq.Interrupts[0] = irq & 0xFF;
        if (res->Resource.Data.Irq.Triggering == ACPI_LEVEL_SENSITIVE) {
            res->Resource.Data.Irq.Shareable = ACPI_SHARED;
        } else {
            res->Resource.Data.Irq.Shareable = ACPI_EXCLUSIVE;
        }
        break;
    }

    res->End.Type = ACPI_RESOURCE_TYPE_END_TAG;
    res->End.Length = sizeof(struct acpi_resource);

    int status = AcpiSetCurrentResources(lnk->Handle, &buffer);
    if (ACPI_FAILURE(status)) {
        trmLogfn("Error during _SRS method: %p", status);
    }

    khsFree(res);

    ACPI_OBJECT obj;
    ACPI_BUFFER stabuffer = { sizeof(obj), &obj };
    AcpiEvaluateObject(lnk->Handle, "_STA", NULL, &stabuffer);
    acpiLnkGetActive(lnk);

    if (lnk->Active != (uint32_t)irq) {
        trmLogfn("Active IRQ differs from requested %i", irq);
    }
}

void acpiLnkActivateIntrs(struct acpi_pci_link* lnk)
{
    int irq = lnk->Active;
    unsigned i;
    for (i = 0; i < lnk->PossibleCount; i++) {
        if (lnk->Possible[i] == lnk->Active) {
            break;
        }
    }

    if (i == lnk->PossibleCount || !irq) {
        irq = lnk->Possible[lnk->PossibleCount - 1];
    }

    acpiLnkSetIrq(lnk, irq);
}

struct acpi_pci_link* acpiLnkGetLinkHandle(ACPI_HANDLE handle)
{
    struct acpi_pci_link* l = NULL;
    for (size_t i = 0; i < links.Length; i++) {
        l = links.Data[i];
        if (l->Handle == handle) {
            return l;
        }
    }

    return NULL;
}
