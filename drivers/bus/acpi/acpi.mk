# ACPICA source inclusion

UACPI_FILES=$(addprefix acpica/,$(notdir $(wildcard $(ACPI_SOURCE)/acpica/*.c)))
UACPI_FLAGS=-I$(ACPI_SOURCE)/acpica/include -fmax-errors=5
