/* lib_interface.c
   Purpose: Implementation of uACPI
            kernel interface */
#include <power/abstract/timer.h>
#include <power/i386/idt.h>
#include <power/i386/asm.h>

#include <power/memory/heap.h>
#include <power/memory/pfa.h>
#include <power/pci.h>

#include <power/ldr_requests.h>
#include <power/scheduler.h>
#include <power/scheduler.h>

#include <power/tty.h>
#include <stdarg.h>
#include <power/paging.h>

#include <acpi.h>
#include <string.h>

ACPI_STATUS AcpiOsInitialize()
{
    return AE_OK;
}

ACPI_STATUS AcpiOsTerminate()
{
    return AE_OK;
}

ACPI_PHYSICAL_ADDRESS AcpiOsGetRootPointer()
{
    uint64_t hhdm = rqGetHhdmRequest()->offset;
    struct limine_rsdp_response* re = rqGetRsdpResponse();
    return (ACPI_PHYSICAL_ADDRESS)(((void*)re->address) - hhdm);
}

ACPI_STATUS
AcpiOsPredefinedOverride(const ACPI_PREDEFINED_NAMES* PredefinedObject,
                         ACPI_STRING* NewValue)
{
    (*NewValue) = NULL;
    return AE_OK;
}

ACPI_STATUS AcpiOsTableOverride(ACPI_TABLE_HEADER* ExistingTable,
                                ACPI_TABLE_HEADER** NewTable)
{
    (*NewTable) = NULL;
    return AE_OK;
}

void* AcpiOsMapMemory(ACPI_PHYSICAL_ADDRESS PhysicalAddress, ACPI_SIZE Length)
{
    return mmMakeWritable((void*)PhysicalAddress);
}

void AcpiOsUnmapMemory(void* where, ACPI_SIZE length)
{
}

ACPI_STATUS AcpiOsGetPhysicalAddress(void* LogicalAddress,
                                     ACPI_PHYSICAL_ADDRESS* PhysicalAddress)
{
    uint64_t hhdm = rqGetHhdmRequest()->offset;
    if ((ACPI_PHYSICAL_ADDRESS)LogicalAddress >= hhdm) {
        (*PhysicalAddress) = (ACPI_PHYSICAL_ADDRESS)LogicalAddress - hhdm;
    }

    (*PhysicalAddress) = (ACPI_PHYSICAL_ADDRESS)pgGetPhysicalAddress(
        NULL, (ACPI_PHYSICAL_ADDRESS)LogicalAddress);
    return AE_OK;
}

void* AcpiOsAllocate(ACPI_SIZE Size)
{
    return khsAlloc(Size);
}

void AcpiOsFree(void* Memory)
{
    khsFree(Memory);
}

BOOLEAN AcpiOsReadable(void* Memory, ACPI_SIZE Length)
{
    return TRUE;
}

BOOLEAN AcpiOsWritable(void* Memory, ACPI_SIZE Length)
{
    return TRUE;
}

ACPI_THREAD_ID AcpiOsGetThreadId()
{
    return schedGetCurrentThread()->Identifier;
}

void AcpiOsSleep(UINT64 Milliseconds)
{
    schedSleep(Milliseconds);
}

void AcpiOsStall(UINT32 Microseconds)
{
    struct timer* tm = tmGetDefaultTimer();
    tmSetReloadValue(tm, Microseconds);
}

ACPI_STATUS AcpiOsCreateSemaphore(UINT32 MaxUnits, UINT32 InitialUnits,
                                  ACPI_SEMAPHORE* OutHandle)
{
    (*OutHandle) = schedCreateSemaphore(MaxUnits);
    return AE_OK;
}

ACPI_STATUS AcpiOsDeleteSemaphore(ACPI_SEMAPHORE Handle)
{
    if (!Handle)
        return AE_BAD_PARAMETER;

    schedDestroySemaphore(Handle);
    return AE_OK;
}

ACPI_STATUS AcpiOsWaitSemaphore(ACPI_SEMAPHORE Handle, UINT32 Units,
                                UINT16 Timeout)
{
    if (!Handle)
        return AE_BAD_PARAMETER;

    for (unsigned i = 0; i < Units; i++) {
        schedSemaphoreAcquire(Handle);
    }

    return AE_OK;
}

ACPI_STATUS AcpiOsSignalSemaphore(ACPI_SEMAPHORE Handle, UINT32 Units)
{
    if (!Handle)
        return AE_BAD_PARAMETER;

    for (unsigned i = 0; i < Units; i++) {
        schedSemaphoreRelease(Handle);
    }
    return AE_OK;
}

ACPI_STATUS AcpiOsCreateLock(ACPI_SPINLOCK* OutHandle)
{
    (*OutHandle) = schedCreateMutex();
    return AE_OK;
}

void AcpiOsDeleteLock(ACPI_SPINLOCK Handle)
{
    schedDestroyMutex(Handle);
}

ACPI_CPU_FLAGS AcpiOsAcquireLock(ACPI_SPINLOCK Handle)
{
    schedMutexAcquire(Handle);
    return 0;
}

void AcpiOsReleaseLock(ACPI_SPINLOCK Handle, ACPI_CPU_FLAGS Flags)
{
    schedMutexRelease(Handle);
}

ACPI_STATUS AcpiOsInstallInterruptHandler(UINT32 InterruptLevel,
                                          ACPI_OSD_HANDLER Handler,
                                          void* Context)
{
    return AE_ERROR;
}

ACPI_STATUS AcpiOsRemoveInterruptHandler(UINT32 InterruptNumber,
                                         ACPI_OSD_HANDLER Handler)
{
    return AE_ERROR;
}

static void acpiccache(void* object, size_t length)
{
  memset(object, 0, length);
}

static void acpidcache(void* object, size_t length)
{
  memset(object, 0, length);
}

ACPI_STATUS
AcpiOsCreateCache(char* CacheName, UINT16 ObjectSize, UINT16 MaxDepth,
                  ACPI_CACHE_T** ReturnCache)
{
    if (ObjectSize < 16 || !ReturnCache)
        return AE_BAD_PARAMETER;

    (*ReturnCache) = khCreateCacheEx(CacheName, ObjectSize,
				     acpiccache, acpidcache);
    return AE_OK;
}

ACPI_STATUS
AcpiOsDeleteCache(ACPI_CACHE_T* Cache)
{
    if (!Cache)
        return AE_BAD_PARAMETER;

    khFreeCache(Cache);
    return AE_OK;
}

ACPI_STATUS
AcpiOsPurgeCache(ACPI_CACHE_T* Cache)
{
    /* ACPICA prints a lot of "unexpected NULL"
       for some package code during INTx routing. */
    /* khShrinkToFit(Cache); */
    return AE_OK;
}

void* AcpiOsAcquireObject(ACPI_CACHE_T* Cache)
{
    void* c = khAlloc(Cache);
    return c;
}

ACPI_STATUS
AcpiOsReleaseObject(ACPI_CACHE_T* Cache, void* Object)
{
    if (!Cache || !Object)
        return AE_BAD_PARAMETER;

    __khFree(Cache, Object, __builtin_return_address(0));
    return AE_OK;
}


ACPI_STATUS
AcpiOsExecute(ACPI_EXECUTE_TYPE Type, ACPI_OSD_EXEC_CALLBACK Function,
              void* Context)
{
    return AE_ERROR;
}

void AcpiOsWaitEventsComplete(void)
{
}

void ACPI_INTERNAL_VAR_XFACE AcpiOsPrintf(const char* Format, ...)
{
    va_list list;
    va_start(list, Format);
    trmLogfv(Format, list);
    va_end(list);
}

void* AcpiOsAllocateZeroed(ACPI_SIZE Size)
{
    return khsAlloc(Size);
}

void AcpiOsVprintf(const char* Format, va_list Args)
{
    trmLogfv(Format, Args);
}

ACPI_STATUS
AcpiOsEnterSleep(UINT8 SleepState, UINT32 RegaValue, UINT32 RegbValue)
{
    trmLogfn("TODO: Enter SLEEP");
    return AE_OK;
}

ACPI_STATUS
AcpiOsReadMemory(ACPI_PHYSICAL_ADDRESS Address, UINT64* Value, UINT32 Width)
{
    switch (Width) {
    case 1: {
        uint8_t* ptr = mmMakeWritable((void*)Address);
        (*Value) = *ptr;
        return AE_OK;
    }
    case 2: {
        uint16_t* ptr = mmMakeWritable((void*)Address);
        (*Value) = *ptr;
        return AE_OK;
    }
    case 4: {
        uint32_t* ptr = mmMakeWritable((void*)Address);
        (*Value) = *ptr;
        return AE_OK;
    }
    case 8: {
        uint64_t* ptr = mmMakeWritable((void*)Address);
        (*Value) = *ptr;
        return AE_OK;
    }
    default:
        __builtin_unreachable();
    }
}

ACPI_STATUS
AcpiOsWriteMemory(ACPI_PHYSICAL_ADDRESS Address, UINT64 Value, UINT32 Width)
{
    switch (Width) {
    case 1: {
        uint8_t* ptr = mmMakeWritable((void*)Address);
        (*ptr) = Value & (0xFF);
        break;
    }
    case 2: {
        uint16_t* ptr = mmMakeWritable((void*)Address);
        (*ptr) = Value & (0xFFFF);
        break;
    }
    case 4: {
        uint32_t* ptr = mmMakeWritable((void*)Address);
        (*ptr) = Value & (0xFFFFFF);
        break;
    }
    case 8: {
        uint64_t* ptr = mmMakeWritable((void*)Address);
        (*ptr) = Value;
        break;
    }
    default:
        break;
    }

    return AE_OK;
}

ACPI_STATUS
AcpiOsReadPort(ACPI_IO_ADDRESS Address, UINT32* Value, UINT32 Width)
{
    switch (Width) {
    case 1:
        (*Value) = __inb(Address);
        break;
    case 2:
        (*Value) = __inw(Address);
        break;
    case 4:
        (*Value) = __inl(Address);
        break;
    }

    return AE_OK;
}

ACPI_STATUS
AcpiOsWritePort(ACPI_IO_ADDRESS Address, UINT32 Value, UINT32 Width)
{
    switch (Width) {
    case 1:
        __outb(Address, Value & 0xFF);
        break;
    case 2:
        __outw(Address, Value & 0xFFFF);
        break;
    case 4:
        __outl(Address, Value & 0xFFFFFFFF);
        break;
    }

    return AE_OK;
}

ACPI_STATUS
AcpiOsReadPciConfiguration(ACPI_PCI_ID* PciId, UINT32 Reg, UINT64* Value,
                           UINT32 Width)
{
    uint32_t dw;
    dw = pciReadDoubleWord(PciId->Bus, PciId->Device, PciId->Function,
                           Reg & 0xFC);

    switch (Width) {
    case 1:
        (*Value) = dw & 0xFF;
        return AE_OK;
    case 2:
        (*Value) = dw & (0xFFFF);
        return AE_OK;
    case 4:
        (*Value) = dw;
        return AE_OK;
    }

    return AE_OK;
}

ACPI_STATUS
AcpiOsWritePciConfiguration(ACPI_PCI_ID* PciId, UINT32 Reg, UINT64 Value,
                            UINT32 Width)
{
    if (Width == 4) {
        pciWriteDoubleWord(PciId->Bus, PciId->Device, PciId->Function,
                           Reg & 0xFC, Value & 0xFFFFFFFF);
        return AE_OK;
    }

    uint32_t dw;
    dw = pciReadDoubleWord(PciId->Bus, PciId->Device, PciId->Function,
                           Reg & 0xFC);

    switch (Width) {
    case 1:
        dw &= ~(0xFF);
        dw |= Value & 0xFF;
        break;
    case 2:
        dw &= ~(0xFFFF);
        dw |= Value & 0xFFFF;
        break;
    }

    pciWriteDoubleWord(PciId->Bus, PciId->Device, PciId->Function,
                       Reg & 0xFC, dw);
    return AE_OK;
}

UINT64
AcpiOsGetTimer(void)
{
    return __builtin_ia32_rdtsc();
}

ACPI_STATUS
AcpiOsSignal(UINT32 Function, void* Info)
{
    return AE_OK;
}

ACPI_STATUS
AcpiOsPhysicalTableOverride(ACPI_TABLE_HEADER* ExistingTable,
                            ACPI_PHYSICAL_ADDRESS* NewAddress,
                            UINT32* NewTableLength)
{
    (*NewAddress) = 0;
    return AE_OK;
}

// #endif
