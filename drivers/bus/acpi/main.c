#include <power/abstract/intrctl.h>
#include "acexcep.h"
#include "acpi_pci.h"

#include "acpi_pcilink.h"
#include "acpixf.h"
#include <power/ldr_requests.h>

#include "limine.h"
#include <power/memory/heap.h>
#include <power/tty.h>

#include <power/driver.h>
#include <acpi.h>
#include "acpi_pci.h"

static bool shouldLoadAcpi();

void driverMain(struct kdriver_manager* man);
static struct driver_conditional_loading cld;

static struct driver_info info = {
    .Name = "acpi",
    .Interface = NULL,
    .ConditionalLoading = &cld,
    .Role = DRIVER_ROLE_BUS,
    .EntryPoint = driverMain,
};

void driverMain(struct kdriver_manager* man)
{
    ACPI_STATUS status;
    AcpiGbl_EnableInterpreterSlack = TRUE;
    status = AcpiInitializeSubsystem();
    if (ACPI_FAILURE(status)) {
        trmLogfn("error inside AcpiInitializeSubsystem: %p", status);
    }

    status = AcpiInitializeTables(NULL, 16, TRUE);
    if (ACPI_FAILURE(status)) {
        trmLogfn("error inside AcpiInitializeTables: %p", status);
    }

    status = AcpiLoadTables();
    if (ACPI_FAILURE(status)) {
        trmLogfn("error inside AcpiLoadTables: %p", status);
    }

    /* CALL THE _PIC METHOD */
    ACPI_OBJECT pic;
    pic.Type = ACPI_TYPE_INTEGER;
    pic.Integer.Value = intCtlGetControllerType() == INT_CTL_TYPE_APIC;

    ACPI_OBJECT_LIST list;
    list.Count = 1;
    list.Pointer = &pic;
    status = AcpiEvaluateObject(NULL, "\\_PIC", &list, NULL);
    if (ACPI_FAILURE(status)) {
      trmLogfn("error while running _PIC: %i", status);
    }
    
    AcpiEnableSubsystem(ACPI_NO_ACPI_ENABLE);
    AcpiInitializeObjects(ACPI_FULL_INITIALIZATION);

    acpiPciFillRoots();
    acpiLnkInit();

    struct driver_acpi_bus_interface* in = khsAlloc(sizeof(*in));
    info.Interface = in;

    in->Base.BusType = DRIVER_BUS_TYPE_ACPI;
    in->pciEnableInterrupt = acpiPciEnableInterrupts;
}

static struct driver_conditional_loading cld = {
    .RelationshipWithPrevious = DRIVER_CLD_RELATIONSHIP_NONE,
    .ConditionalType = DRIVER_CLD_TYPE_CUSTOM_FUNC,
    .HasNext = 0,
    .shouldLoad = shouldLoadAcpi,
};

bool shouldLoadAcpi()
{
    struct limine_rsdp_response* rsdp = rqGetRsdpResponse();
    return rsdp->address != NULL;
}

struct driver_info* driverQuery()
{
    return &info;
}
