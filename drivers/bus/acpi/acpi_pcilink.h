/* acpi_pcilink.h
   Purpose: ACPI PCI LNK */
#pragma once

#include <acpi.h>
#include <power/config.h>
#include <power/pci.h>

#define MAX_POSSIBLE_IRQS 16

struct acpi_pci_link {
    uint32_t PossibleCount;
    uint8_t Possible[MAX_POSSIBLE_IRQS];
    uint32_t Active;
    int IrqType;
    ACPI_HANDLE Handle;

    struct {
        int Type;
        int Polarity;
        int Trigger;
    } Intr;
};

void acpiLnkInit();
void acpiLnkArbitrateIrq();

int acpiLnkGetPossible(struct acpi_pci_link* link);
int acpiLnkGetActive(struct acpi_pci_link* lnk);

void acpiLnkActivateIntrs(struct acpi_pci_link* lnk);
struct acpi_pci_link* acpiLnkGetLinkHandle(ACPI_HANDLE handle);
struct acpi_pci_link* acpiLnkGetLinkForDevice(struct pci_device* dev);