#include "acpi_pci.h"
#include "acpi_pcilink.h"
#include "acpixf.h"
#include "acrestyp.h"
#include "actypes.h"
#include <power/memory/heap.h>
#include <power/memory/pfa.h>
#include <power/pci.h>
#include <power/tty.h>
#include <power/vector.h>

#include <acpi.h>
#include <power/error.h>

static struct vector pciBridges;

static int getBusNumber(ACPI_HANDLE obj, struct resource_range* resource);
static UINT32 __findBus(ACPI_HANDLE handle, UINT32 nest, void* ctx, void** ret)
{
    struct pci_root* root = khsAlloc(sizeof(*root));
    root->Bridge = handle;
    getBusNumber(handle, &root->BusRange);
    vectorInsert(&pciBridges, root);

    return AE_OK;
}

static ACPI_STATUS __busNumber(ACPI_RESOURCE* res, void* ctx)
{
    ACPI_RESOURCE_ADDRESS64 address;
    struct resource_range* resource = ctx;
    AcpiResourceToAddress64(res, &address);

    if (address.Address.AddressLength > 0
        && address.ResourceType == ACPI_BUS_NUMBER_RANGE) {
        resource->Length = address.Address.AddressLength;
        resource->Begin = address.Address.Minimum;
    }

    return AE_OK;
}

static void getRoutingTableDevice(ACPI_HANDLE handle,
                                  ACPI_PCI_ROUTING_TABLE* table,
                                  struct pci_prt_entry* entry,
                                  struct pci_device* dev)
{
    int function;
    int device;
    int pin = (pciReadDoubleWordFromDevice(dev, 0x3C) & 0xFF00) >> 8;
    while (table->Length) {
        function = table->Address & 0xFFFF;
        device = table->Address >> 16;
        if (((dev->Device != device && device != 0xFFFF) ||
            (dev->Function != function && function != 0xFFFF))
            || (table->Pin + 1 != (unsigned)pin)) {
            table = PaAdd(table, table->Length);
            continue;
        }
        
        entry->Device.Device = device;
        entry->Device.Function = function;
        entry->Device.Bus = dev->Bus;
        entry->Pin = table->Pin + 1;
        entry->Index = table->SourceIndex;

        if (table->Source[0]) {
            AcpiGetHandle(handle, table->Source, &entry->Link);
        }
        break;
    }
}

static struct pci_prt_entry* findRoutingTable(struct pci_root* root,
                                              struct pci_device* dev)
{
    ACPI_BUFFER buffer = { ACPI_ALLOCATE_BUFFER, NULL };
    struct pci_prt_entry* pe = khsAlloc(sizeof(*pe));
    AcpiGetIrqRoutingTable(root->Bridge, &buffer);
    getRoutingTableDevice(root->Bridge, buffer.Pointer, pe, dev);

    khsFree(buffer.Pointer);
    return pe;
}

int getBusNumber(ACPI_HANDLE obj, struct resource_range* resource)
{
    int status;
    status = AcpiWalkResources(obj, "_CRS", __busNumber, resource);
    if (!status) {
        return 0;
    }

    if (status != AE_NOT_FOUND) {
        trmLogfn("_CRS evaluation error: %p", status);
    }

    ACPI_OBJECT bobj;
    ACPI_BUFFER busbf = { sizeof(bobj), &bobj };
    status = AcpiEvaluateObject(obj, "_BBN", NULL, &busbf);

    if (status != 0 && status != AE_NOT_FOUND) {
        trmLogfn("No _BBN inside it!");
        return -ERROR_NOT_FOUND;
    }

    resource->Begin = bobj.Integer.Value;
    resource->Length = 0x100;
    return 0;
}

void acpiPciFillRoots()
{
    pciBridges = vectorCreate(5);
    AcpiGetDevices("PNP0A03", __findBus, NULL, NULL);
}

struct pci_root* acpiPciRootForDevice(struct pci_device* dev)
{
    for (size_t i = 0; i < pciBridges.Length; i++) {
        struct pci_root* root = pciBridges.Data[i];
        if (dev->Bus >= root->BusRange.Begin
            && dev->Bus <= root->BusRange.Begin + root->BusRange.Length) {
            return root;
        }
    }

    return NULL;
}

int acpiPciEnableInterrupts(struct pci_device* dev)
{
    struct pci_root* root = acpiPciRootForDevice(dev);
    if (!root)
        return -ERROR_NO_DEVICE;

    struct acpi_pci_link* link;
    struct pci_prt_entry* entry = findRoutingTable(root, dev);
    int irq = entry->Index;
    
    if (entry->Link) {
        link = acpiLnkGetLinkHandle(entry->Link);
        acpiLnkActivateIntrs(link);
        irq = link->Active;
    }

    return irq;
}
