ACPI_BUILD=build/drivers/acpi
ACPI_SOURCE=drivers/bus/acpi
ACPI_OUTPUT=$(ACPI_BUILD)/acpi.kd

include drivers/bus/acpi/acpi.mk

ACPI_CFILES=$(UACPI_FILES) main.c lib_interface.c acpi_pci.c acpi_pcilink.c
ACPI_CSOURCES:=$(call driver_easy_source,ACPI)
ACPI_CBUILD:=$(call driver_easy_build,ACPI)

__OLD_DRIVER_CFLAGS:=$(DRIVER_CFLAGS)
DRIVER_CFLAGS+=$(UACPI_FLAGS) -Ilimine

AUXFS_FILES+=$(ACPI_OUTPUT)

$(ACPI_OUTPUT): $(ACPI_CBUILD) $(KERNEL_STUB)
	$(CC) $(DRIVER_LDFLAGS) -Wl,--as-needed $(ACPI_CBUILD) -o $@

$(eval $(call driver_easy_c,$(ACPI_BUILD),$(ACPI_SOURCE)))

$(eval $(call driver_easy_std_tasks,acpi,ACPI))

DRIVER_CFLAGS:=$(__OLD_DRIVER_CFLAGS)
