/* acpi_pci.h
   Purpose: PCI */
#pragma once

#include <acpi.h>
#include <power/config.h>
#include <power/pci.h>
#include <power/vector.h>

struct resource_range {
    size_t Begin;
    size_t Length;
};

struct pci_root {
    ACPI_HANDLE Bridge;
    struct vector Links;
    struct resource_range BusRange;
};

struct pci_prt_entry {
    struct {
        int Bus;
        int Device;
        int Function;
    } Device;

    int Pin;
    int Index;
    ACPI_HANDLE Link;
};

void acpiPciFillRoots();
struct pci_root* acpiPciRootForDevice(struct pci_device*);
int acpiPciGetIrqForPin(struct pci_root* bridge, int pin);
int acpiPciEnableInterrupts(struct pci_device* dev);