/* bm.h
   Purpose: Bus mastering DMA */
#pragma once

#include <power/config.h>
#include "ide_state.h"

#define BM_MAKE_DMA(D) ((uint32_t)(uintptr_t)(D))

#define __SECONDARY(V, b) ((b) == IDE_DEV_PRIMARY ? (V) : (V + 0x08))

#define BM_IDE_PORT(Bo) ((Bo)->BusmasterPort)

#define BM_REG_COMMAND(B) __SECONDARY(0x00, B)
#define BM_REG_VENDOR1(B) __SECONDARY(0x01, B)
#define BM_REG_STATUS(B)  __SECONDARY(0x02, B)
#define BM_REG_VENDOR2(B) __SECONDARY(0x03, B)

#define BM_REG_ADDRESS(B) __SECONDARY(0x04, B)
#define BM_REG_ADDRESS_LENGTH 4

#define BM_SECONDARY(R) ((R) + 0x08)

#define BM_CMD_RW_SHIFT 3
#define BM_CMD_RW_BIT (1 << 3)

#define BM_CMD_START_BIT (1 << 0)

#define BM_ST_INTR_BIT (1 << 2)

struct __packed bm_prd {
    uint32_t MemoryAddress;

#define BM_TERMINATE_BIT (1 << 31)
#define BM_BYTE_COUNT_BITS 0xFFFE
    uint32_t ByteCount;
};

int bmCreatePrd(struct ide* ide, int dev);

#define BM_DMA_IN 1 /*< Device -> host */
#define BM_DMA_OUT 0 /*< Host -> device */

void bmStart(struct ide_channel* chan, int direction, int length);
void bmStop(struct ide_channel* chan);
int bmClearIntrs(struct ide_channel* chan);

int bmScatterData(struct ide_channel* chan, const void* data, size_t length);
int bmGatherData(struct ide_channel* chan, void* data, size_t length);
