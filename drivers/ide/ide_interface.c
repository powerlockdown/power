#include "ide_interface.h"
#include "ata.h"
#include "atapi.h"
#include <power/driver.h>
#include "ide_state.h"
#include "power/device.h"

static struct driver_disk_device_interface* 
getDevice(struct driver_disk_interface* di, int device)
{
    int index = device;
    struct ide* ide = (struct ide*)di;
    if (device > 4) {
        return NULL;
    }

    while (index < 4 && !(ide->Devices[index].Flags & IDE_VALID_BIT)) {
        index++;
    }

    return (struct driver_disk_device_interface*)&ide->Devices[index];
}

static int
getDeviceCount(struct driver_disk_interface* di)
{
    return ((struct ide*)di)->DeviceCount;
}

static int
getMaxDeviceCount(struct driver_disk_interface* di)
{
    return 4;
}

void registerIdeInterface(struct ide* ide)
{
    ide->Base.Major = devnRegisterNode("ide", NULL, DEVN_MANYDEV_BIT);
    ide->Base.getDevice = getDevice;
    ide->Base.getDeviceCount = getDeviceCount;
    ide->Base.getMaxDeviceCount = getMaxDeviceCount;
}

static int readSector(struct driver_disk_device_interface* ddi, uint64_t lba, 
                      size_t length, uint8_t* restrict buffer)
{
    struct ide_channel* c = (struct ide_channel*)ddi;
    if (c->Type == IDE_TYPE_ATAPI)
        atapiReadSector((struct ide_channel*)ddi, lba, length, buffer);
    else 
        ataReadSectors(c, lba, length, buffer);
    return 0;
}

static int writeSector(struct driver_disk_device_interface* ddi, uint64_t lba,
                       unsigned long length, const uint8_t* buffer) 
{
    struct ide_channel* c = (struct ide_channel*)ddi;
    if (c->Type == IDE_TYPE_ATA) {
        ataWriteSectors(c, lba, length, buffer);
        return 0;
    }

    return -1;
}

void registerIdeDeviceInterface(struct ide_channel* device)
{
    device->Base.readSector = readSector;
    device->Base.writeSector = writeSector;
}
