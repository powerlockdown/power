IDE_BUILD=build/drivers/ide
IDE_SOURCE=drivers/ide
IDE_OUTPUT=$(IDE_BUILD)/ide.kd

IDE_CFILES=main.c ide_state.c atapi.c ata.c ide_interface.c bm.c
IDE_CSOURCES:=$(call driver_easy_source,IDE)
IDE_CBUILD:=$(call driver_easy_build,IDE)

AUXFS_FILES+=$(IDE_OUTPUT)

$(IDE_OUTPUT): $(IDE_CBUILD) $(KERNEL_STUB)
	$(CC) $(DRIVER_LDFLAGS) $(IDE_CBUILD) -o $@

$(eval $(call driver_easy_c,$(IDE_BUILD),$(IDE_SOURCE)))

$(eval $(call driver_easy_std_tasks,ide,IDE))
