#include "ide_state.h"

#include "atadef.h"
#include <power/abstract/timer.h>
#include <power/paging.h>

#include "ide_interface.h"
#include <power/abstract/intrctl.h>
#include <power/i386/asm.h>

#include <power/memory/heap.h>
#include <power/memory/pfa.h>
#include <power/pci.h>

#include <power/memory/memset.h>
#include <power/tty.h>

#include "bm.h"

static void ideInterrupt(void* d);

static int getSectorSize(struct ide_channel* chan,
                         const uint16_t* restrict idspace)
{
    uint16_t logs = idspace[106 / sizeof(uint16_t)];
    if ((logs & (1 << 14)) && !(logs & (1 << 15)) && (logs & (1 << 12))) {
        uint16_t sizeLow = idspace[117 / sizeof(uint16_t)];
        uint16_t sizeHigh = idspace[118 / sizeof(uint16_t)];
        uint32_t join = (sizeHigh << 16) | sizeLow;
        return join;
    }

    /* Let's just guess */
    return chan->Type == IDE_TYPE_ATA ? 512 : 2048;
}

static void setupPorts(struct ide* ide, struct pci_device* device)
{
    uint16_t irqs[2] = { 14, 15 };
    uint32_t off8 = pciReadDoubleWordFromDevice(device, 0x8);
    uint8_t progif = (off8 >> 8 & 0xFF);
    uint32_t bar[5] = { pciReadDoubleWordFromDevice(device, 0x10),
                        pciReadDoubleWordFromDevice(device, 0x14),
                        pciReadDoubleWordFromDevice(device, 0x18),
                        pciReadDoubleWordFromDevice(device, 0x1C),
                        pciReadDoubleWordFromDevice(device, 0x20) };

    if (((progif & 0b11) == 0b11)) {
        /* Primary device is in native mode and
           native mode can be switched */
        off8 &= ~(1 << 8);
        ide->Ports[IDE_DEV_PRIMARY].Io = bar[0] & ~0xF;
        ide->Ports[IDE_DEV_PRIMARY].Control = bar[1] & ~0xF;
    } else if ((progif & 0b11) == 0b01) {
        /* Primary device is in native mode but
           cannot get out of it */
        trmLogfn("usage of IDE device in native mode is unsupported!");
        ide->Ports[IDE_DEV_PRIMARY].Io = bar[0] & ~0xF;
        ide->Ports[IDE_DEV_PRIMARY].Control = bar[1] & ~0xF;
        irqs[0] = pciReadDoubleWordFromDevice(device, 0x3C) & 0xFF;
    } else {
        /* Primary device is in compat mode */
        ide->Ports[IDE_DEV_PRIMARY].Io = IDE_PRIMARY_PORT;
        ide->Ports[IDE_DEV_PRIMARY].Control = IDE_PRIMARY_CTL_PORT;
    }

    /* The same stuff as above but with the secondary device */
    if (((progif & 0b1100) == 0b1100)) {
        off8 &= ~(1 << 8);
        ide->Ports[IDE_DEV_SECONDARY].Io = bar[2] & ~0xF;
        ide->Ports[IDE_DEV_SECONDARY].Control = bar[3] & ~0xF;
    } else if ((progif & 0b1100) == 0b0100) {
        trmLogfn(
            "usage of IDE device in native mode is unsupported! (secondary)");
        ide->Ports[IDE_DEV_SECONDARY].Io = bar[2] & ~0xF;
        ide->Ports[IDE_DEV_SECONDARY].Control = bar[3] & ~0xF;
        irqs[1] = pciReadDoubleWordFromDevice(device, 0x3C) & 0xFF;
    } else {
        ide->Ports[IDE_DEV_SECONDARY].Io = IDE_SECONDARY_PORT;
        ide->Ports[IDE_DEV_SECONDARY].Control = IDE_SECONDARY_CTL_PORT;
    }

    ide->Devices[0].Ports = &ide->Ports[IDE_DEV_PRIMARY];
    ide->Devices[1].Ports = &ide->Ports[IDE_DEV_PRIMARY];
    ide->Devices[2].Ports = &ide->Ports[IDE_DEV_SECONDARY];
    ide->Devices[3].Ports = &ide->Ports[IDE_DEV_SECONDARY];
    ide->Ports[IDE_DEV_PRIMARY].State = ide;
    ide->Ports[IDE_DEV_SECONDARY].State = ide;

    ide->BusmasterPort = bar[4] & ~0xF;

    intCtlHandleInterrupt(irqs[0], ideInterrupt, &ide->Ports[0]);
    intCtlHandleInterrupt(irqs[1], ideInterrupt, &ide->Ports[1]);
}

struct ide* ideCreateState(struct pci_device* device)
{
    struct ide* state = khsAlloc(sizeof(*state));
    uint16_t* idspace = khsAlloc(256 * sizeof(uint16_t));
    uint16_t bar4 = pciReadDoubleWordFromDevice(device, 0x20) & ~0xF;

    memset(state, 0, sizeof(struct ide));
    setupPorts(state, device);
    registerIdeInterface(state);

    int i = 0;
    for (int d = 0; d < 2; d++) {
        struct ide_ports* port = &state->Ports[d];
        port->Device = d;
        port->State = state;
        port->WriteEvent = schedCreateEvent();
        port->OutputBuffer = khsAlloc(2048);

        ideEnableInterrupts(port, 1);

        for (int c = 0; c < 2; c++) {
            struct ide_channel* dev = &state->Devices[i];
            dev->Flags = IDE_VALID_BIT;
            dev->Channel = c;
            dev->Device = d;
            ideSetCurrentChannel(port, c, true);
            ideEnableInterrupts(port, 0);

            __outb(port->Io + ATA_REG_LBA_LOW, 0);
            __outb(port->Io + ATA_REG_LBA_MID, 0);
            __outb(port->Io + ATA_REG_LBA_HIGH, 0);
            __outb(port->Io + ATA_REG_COMMAND, 0xEC);
            uint8_t status = __inb(port->Io + ATA_REG_STATUS);
            if (!status) {
                dev->Flags &= ~(IDE_VALID_BIT);
                i++;
                continue;
            }

            tmSetReloadValue(tmGetDefaultTimer(), 3000);
            do {
                status = __inb(port->Io + ATA_REG_STATUS);
            } while ((status & 0x80) /* BSY */
                     || !((status & 0x1) /* ERR */
                          || (status & 0x8)) /* DRQ */);

            dev->Type = IDE_TYPE_ATA;
            if (status & 0x1) {
                uint8_t md = __inb(port->Io + ATA_REG_LBA_MID);
                uint8_t hg = __inb(port->Io + ATA_REG_LBA_HIGH);
                if (md == 0x14 && hg == 0xEB) {
                    dev->Type = IDE_TYPE_ATAPI;
                    __outb(port->Io + ATA_REG_COMMAND, 0xA1);
                    do {
                        status = __inb(port->Io + ATA_REG_STATUS);
                    } while (!(status & (1 << 3)) /* DRQ */);
                } else {
                    i++;
                    dev->Flags &= ~(IDE_VALID_BIT);
                    continue;
                }
            }

            __repInl(port->Io + ATA_REG_DATA, (uint32_t*)idspace,
                     512 / sizeof(uint32_t));

            { /* Model name discovery */
                uint8_t* rawids = (uint8_t*)idspace;
                for (int i = 0; i < 40; i += 2) {
                    dev->Model[i] = rawids[ATA_IDENT_MODEL + i + 1];
                    dev->Model[i + 1] = rawids[ATA_IDENT_MODEL + i];
                }
                dev->Model[39] = 0;
            }

            dev->SectorSize = getSectorSize(dev, idspace);
            if (dev->Type == IDE_TYPE_ATA)
                dev->Flags |= ((idspace[83] & (1 << 10))
                               >> (10 - IDE_LBA48_SUPPORTED_SHIFT));
            if (idspace[49] & (1 << 8))
                dev->Flags |= IDE_DMA_SUPPORTED_BIT;
            
            dev->Base.SectorSize = dev->SectorSize;
            
            ideEnableInterrupts(port, 1);
            registerIdeDeviceInterface(dev);
            state->DeviceCount++;
            i++;
        }

        bmCreatePrd(state, d);

        /* RESET EVERYTHING or else we will die at the first read. */
        __outb(port->Control + ATA_CTL_REG_DEVCTL, (1 << 2));
        tmSetReloadValue(tmGetDefaultTimer(), 100);
        __outb(port->Control + ATA_CTL_REG_DEVCTL, 0);
    }

    khsFree(idspace);
    return state;
}

void ideEnableInterrupts(struct ide_ports* device, int enbstate)
{
    uint8_t ctl = __inb(device->Control + ATA_CTL_REG_DEVCTL);
    ctl |= enbstate ? 0 : (1 << 1);
    __outb(device->Control, ctl);
}

void ideSetCurrentChannel(struct ide_ports* ide, int channel, bool force)
{
    if (ide->CurrentChannel == channel && !force)
        return;

    __outb(ide->Io + ATA_REG_DRIVE, 0xA0 | channel << 4);
    for (int i = 0; i < 15; i++) {
        __inb(ide->Io + ATA_REG_STATUS);
    }
    ide->CurrentChannel = channel;
}

void ideInterrupt(void* d)
{
    struct ide* ide;
    struct ide_ports* dev = (struct ide_ports*)d;

    ide = dev->State;
    if (dev->PendingOp) {
        struct ide_channel* chan;
        schedEventRaise(dev->WriteEvent);

        chan = &ide->Devices[dev->Device + dev->CurrentChannel];
        if (bmClearIntrs(chan))
            bmStop(chan);
    }
}

void ideSetLba48(struct ide_ports* port, int lba)
{
    uint8_t ctl = __inb(port->Io + ATA_REG_DRIVE);
    ctl |= lba ? (1 << 6) : 0;
    __outb(port->Io + ATA_REG_DRIVE, ctl);
}
