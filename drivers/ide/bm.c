#include "bm.h"

#include <power/elf.h>
#include <power/memory/pool.h>
#include <power/memory/pfa.h>

#include <power/memory/memset.h>
#include <power/tty.h>
#include <power/error.h>

static struct mm_pool* prdPool;
static struct mm_pool* dmaPool;

/* 8 PRDs * 512 bytes = 4096 bytes. */
#define PRD_COUNT 8
#define PRD_BUFFER_LENGTH 512

int bmCreatePrd(struct ide* ide, int dev)
{
    if (!prdPool) {
        prdPool = poolInit(sizeof(struct bm_prd) * PRD_COUNT, 4);
    }

    if (!dmaPool) {
        dmaPool = poolInit(PRD_BUFFER_LENGTH, 2);
    }

    if (dev >= 2) {
        return -ERROR_INVALID_RANGE;
    }

    void* ad0;
    struct bm_prd* cprd;
    struct bm_prd* prd = poolAllocObject(prdPool);
    for (int i = 0; i < PRD_COUNT; i++) {
        cprd = &prd[i];

        ad0 = poolAllocObject(dmaPool);
        cprd->MemoryAddress = BM_MAKE_DMA(ad0);
        cprd->ByteCount = PRD_BUFFER_LENGTH;

        memset(ad0, 0, PRD_BUFFER_LENGTH);
    }

    ioport bmp = BM_IDE_PORT(ide);
    prd[PRD_COUNT - 1].ByteCount |= BM_TERMINATE_BIT;
    __outl(bmp + BM_REG_ADDRESS(dev), BM_MAKE_DMA(prd));

    ioport port = bmp + BM_REG_STATUS(dev);
    int cmd = __inb(port);
    
    /* Clear interrupt and error bits. */
    __outb(port, cmd & ~(3 << 2));
    ide->DmaDescriptors[dev] = prd;
    return 0;
}

void bmStart(struct ide_channel* chan, int dir, int length)
{
    struct ide* ide = IDE_STATE(chan);
    ioport port = BM_IDE_PORT(ide) + BM_REG_COMMAND(chan->Device);
    uint8_t cmd = __inb(port);

    cmd &= ~BM_CMD_RW_BIT;
    cmd |= ((dir & 1) << BM_CMD_RW_SHIFT);

    if ((length % PRD_BUFFER_LENGTH)) {
        length = alignUp(length, PRD_BUFFER_LENGTH);
    }

    struct bm_prd* p;
    struct bm_prd* prd = ide->DmaDescriptors[chan->Device]; 
    for (int i = 0; i < PRD_COUNT; i++) {
        p = &prd[i];
        p->ByteCount &= ~BM_TERMINATE_BIT;
    }

    prd[(length / PRD_BUFFER_LENGTH) - 1].ByteCount |= BM_TERMINATE_BIT;

    __outb(port, cmd);
    __outb(port, cmd | BM_CMD_START_BIT);
}

void bmStop(struct ide_channel* chan)
{
    struct ide* ide = IDE_STATE(chan);
    ioport port = BM_IDE_PORT(ide) + BM_REG_COMMAND(chan->Device);
    uint8_t cmd = __inb(port);

    __outb(port, cmd & ~BM_CMD_START_BIT);
}

int bmClearIntrs(struct ide_channel* chan)
{
    int prev;

    struct ide* ide = IDE_STATE(chan);
    ioport port = BM_IDE_PORT(ide) + BM_REG_STATUS(chan->Device);
    uint8_t status = __inb(port);

    prev = status & BM_ST_INTR_BIT;
    if (prev) {
        __outb(port, status);
    }

    return prev;
}

static int 
scatterGather(struct ide_channel* chan, void* data, size_t length, int dir)
{
    int count;

    size_t l = length;
    struct ide* ide = IDE_STATE(chan);
    struct bm_prd* prd = ide->DmaDescriptors[chan->Device];

    if (!(length % PRD_BUFFER_LENGTH)) {
        l = alignUp(l, PRD_BUFFER_LENGTH);
    }

    count = l / PRD_BUFFER_LENGTH;
    if (count > PRD_COUNT) {
        return -ERROR_INVALID_RANGE;
    }

    void* ptr;
    struct bm_prd* p;
    
    int copied = 0;
    size_t lencounter = length;
    
    for (int i = 0; i < count; i++) {
        p = &prd[i];
        p->ByteCount &= ~BM_TERMINATE_BIT;

        ptr = mmMakeWritable((void*)(uintptr_t)p->MemoryAddress);

        copied = p->ByteCount & BM_BYTE_COUNT_BITS;
        if (lencounter < PRD_BUFFER_LENGTH) {
            copied = lencounter;
            if (dir == BM_DMA_IN) {
                p->ByteCount = copied;
            }
        }
        
        if (dir == BM_DMA_IN)
            memcpy(ptr, data + (i * PRD_BUFFER_LENGTH), copied);
        else if (dir == BM_DMA_OUT)
            memcpy(data + (i * PRD_BUFFER_LENGTH), ptr, copied);

        lencounter -= copied;
    }

    prd[count - 1].ByteCount |= BM_TERMINATE_BIT;

    return 0;
}

int bmScatterData(struct ide_channel* chan, const void* data, size_t length)
{
    return scatterGather(chan, (void*)data, length, BM_DMA_IN);
}

int bmGatherData(struct ide_channel* chan, void* data, size_t length)
{
    return scatterGather(chan, data, length, BM_DMA_OUT);
}
