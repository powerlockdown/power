#include "atapi.h"
#include <power/i386/asm.h>
#include "atadef.h"
#include "ide_state.h"

#include <power/scheduler.h>
#include <power/memory/pfa.h>
#include <power/tty.h>

#include <power/memory/memset.h>

#include "bm.h"

static void waitStatus(struct ide_ports* pt)
{
    uint8_t status;
    do {
        status = __inb(pt->Io + ATA_REG_STATUS);
    } while (status & 0x80);
}

void atapiReadSector(struct ide_channel* device, uint64_t lba, 
                     size_t length, uint8_t* buffer)
{
    struct ide_ports* ports = device->Ports;
    volatile uint8_t cmd[12] = {
        0xA8, 0,
        ((uint8_t)(lba >> 0x18) & 0xFF),
        ((uint8_t)(lba >> 0x10) & 0xFF),
        ((uint8_t)(lba >> 0x08) & 0xFF),
        ((uint8_t)(lba) & 0xFF),
            0, 0, 0, 1,
        0, 0
    };
    volatile uint16_t* cmd16 = (volatile uint16_t*)cmd;

    schedEventResetFlag(ports->WriteEvent);
    ideSetCurrentChannel(device->Ports, device->Channel, false);
    
    __outb(ports->Io + ATA_REG_FEATURES, (device->Flags & IDE_DMA_SUPPORTED_BIT) >> 1);
    __outb(ports->Io + ATA_REG_LBA_MID, 2048 & 0xFF);
    __outb(ports->Io + ATA_REG_LBA_HIGH, (2048 >> 8) & 0xFF);

    __outb(ports->Io + ATA_REG_COMMAND, 0xA0);
    for (size_t i = 0; i < sizeof(cmd) / 2; i++)
        __outw(ports->Io + ATA_REG_DATA, cmd16[i]);

    ports->PendingOp = true;
    
    if (device->Flags & IDE_DMA_SUPPORTED_BIT) 
        bmStart(device, BM_DMA_IN, 2048);

    schedEventPause(ports->WriteEvent);
    waitStatus(ports);

    if (device->Flags & IDE_DMA_SUPPORTED_BIT)
        bmGatherData(device, buffer, 2048);
    else {
        int readLength = 2048 / sizeof(uint32_t);
        __repInl(ports->Io + ATA_REG_DATA, 
            (uint32_t*)ports->OutputBuffer, readLength);
        memcpy(buffer, ports->OutputBuffer, length);
    }
}
