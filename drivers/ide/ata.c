#include "ata.h"

#include <power/i386/asm.h>
#include "atadef.h"
#include "bm.h"
#include "ide_state.h"

#include <power/scheduler.h>
#include <power/memory/pfa.h>
#include <power/tty.h>

#include <power/memory/memset.h>

/* Put the LBA in its correct registers. */
static void ataSetSectors(struct ide_channel* device,
			  lba_t lba, unsigned long count)
{
  struct ide_ports* ports = device->Ports;
  
  if (lba > 0x0FFFFFFF) {
    if (count >= 0x10000)
      __outw(ports->Io + ATA_REG_SECNT, 0);
    else
      __outw(ports->Io + ATA_REG_SECNT, count);
    
    __outw(ports->Io + ATA_REG_LBA_LOW, (uint16_t)lba);
    __outw(ports->Io + ATA_REG_LBA_MID, (uint16_t)(lba >> 16));
    __outw(ports->Io + ATA_REG_LBA_HIGH, (uint16_t)(lba >> 32));
    return;
  }

  /* LBA28 */
  if (count >= 0x100)
    __outb(ports->Io + ATA_REG_SECNT, 0);
  else
    __outb(ports->Io + ATA_REG_SECNT, count);

  __outb(ports->Io + ATA_REG_LBA_LOW, (uint8_t)lba);
  __outb(ports->Io + ATA_REG_LBA_MID, (uint8_t)(lba >> 8));
  __outb(ports->Io + ATA_REG_LBA_HIGH, (uint8_t)(lba >> 16));
}

/* For compatibility with ATAPI, we can only read a entire sector at once. */
void ataReadSectors(struct ide_channel* device, uint64_t lba, 
                     size_t length, uint8_t* buffer)
{
    struct ide_ports* ports = device->Ports;

    int alg;
    int fet = 0;
    int cmd = 0x20; /* (PIO) READ SECTORS */
    schedEventResetFlag(ports->WriteEvent);
    ideSetCurrentChannel(device->Ports, device->Channel, false);

    if ((alg = length % device->Base.SectorSize) != 0) {
      length += alg;
    }
    
    if (device->Flags & IDE_DMA_SUPPORTED_BIT) {
      fet = 1;
      cmd = 0xC8; /* READ DMA */

      if (lba > 0x0FFFFFFFF)
	cmd = 0x25; /* READ DMA EXT */
    } else if (lba > 0x0FFFFFFF)
      cmd = 0x24; /* (PIO) READ SECTORS EXT */

    unsigned stc = length / device->Base.SectorSize;
    
    ideSetLba48(ports, 1);
    __outb(ports->Io + ATA_REG_FEATURES, fet); /* PIO/DMA read */
    __outb(ports->Io + ATA_REG_SECNT, stc);

    ataSetSectors(device, lba, stc);
    __outb(ports->Io + ATA_REG_COMMAND, cmd); /* READ SECTORS */

    ports->PendingOp = true;
    if (device->Flags & IDE_DMA_SUPPORTED_BIT) {
      bmStart(device, BM_DMA_IN, length);
    }
    
    schedEventPause(ports->WriteEvent);

    if (device->Flags & IDE_DMA_SUPPORTED_BIT)
      bmGatherData(device, buffer, length);
    else {
      __repInl(ports->Io + ATA_REG_DATA, (uint32_t*)ports->OutputBuffer,
	       length / sizeof(uint32_t));
      memcpy(buffer, ports->OutputBuffer, length);
    }
}

void ataWriteSectors(struct ide_channel* device, uint64_t lba, 
		     unsigned long length, const uint8_t* buffer)
{
    struct ide_ports* ports = device->Ports;

    int cmd = 0x30;
    int fet = 0;
    int sct = length;
    int alg;
    
    if ((alg = sct % device->Base.SectorCount) != 0) {
      sct += alg;
    }
    
    if (device->Flags & IDE_DMA_SUPPORTED_BIT) {
      fet = 1;
      cmd = 0xCA; /* WRITE DMA */

      if (lba > 0x0FFFFFFFF)
	cmd = 0x35; /* READ DMA EXT */
    } else if (lba > 0x0FFFFFFF)
      cmd = 0x34; /* (PIO) WRITE SECTORS EXT */

    schedEventResetFlag(ports->WriteEvent);
    ideSetCurrentChannel(ports, device->Channel, false);
    ideSetLba48(ports, 1);

    __outb(ports->Io + ATA_REG_FEATURES, fet); /* PIO read */
    __outb(ports->Io + ATA_REG_SECNT, length / device->Base.SectorSize);

    ataSetSectors(device, lba, length);
    __outb(ports->Io + ATA_REG_COMMAND, cmd);

    ports->PendingOp = true;

    if (device->Flags & IDE_DMA_SUPPORTED_BIT) {
      bmScatterData(device, buffer, length);
      bmStart(device, BM_DMA_OUT, length);
      schedEventPause(ports->WriteEvent);
    } else {
        int c = sct / sizeof(uint16_t);
        uint16_t* bu16 = (uint16_t*)buffer;
        for (int i = 0; i < c; i++) {
            __outw(ports->Io + ATA_REG_DATA, bu16[i]);
        }
    }

    __outb(ports->Io + ATA_REG_COMMAND, 0xE7); /* CACHE FLUSHs */
}
