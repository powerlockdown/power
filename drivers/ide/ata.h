/* ata.h
   Purpose: Routines for non-ATAPI devices */
#pragma once

#include "ide_state.h"

void ataReadSectors(struct ide_channel* state, uint64_t lba, 
                     size_t length, uint8_t* buffer);
void ataWriteSectors(struct ide_channel* state, uint64_t lba, 
                     size_t length, const uint8_t* buffer);
void ataDmaReadSector(struct ide_channel* state, uint64_t lba,
                      size_t length, uint8_t* buffer);
                      
