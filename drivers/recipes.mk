ENABLED_DRIVERS=scsi ata ide fs/iso9660 ps2 \
				ahci fs/ext2 bus/usb/ehci   \
				bus/acpi bus/usb/core bus/usb/mass

DRIVER_LDFLAGS=-ffreestanding -nostdlib -Wl,-e -Wl,driverQuery -L$(KERNEL_BUILD) -l__r-kernelapi \
				-fpic -finline-functions
DRIVER_CFLAGS=-Ikernel/include/user -Ikernel/include -I$(KERNARCHINCDIR) -fpic

# Note to future self: When writing these macros,
# use $$ instead of $ to delay the evalution of the
# expression to when the rule is executed, instead of
# when $(call) is performed.
# 
# This is necessary in all contexts where you
# use an automatic variable.

# Arguments: (1) Variable prefix
define driver_easy_source =
	$(addprefix $($(1)_SOURCE)/,$($(1)_CFILES))
endef

# Arguments: (1) Variable prefix
define driver_easy_build = 
	$(addprefix $($(1)_BUILD)/,$(addsuffix .o,$($(1)_CFILES)))
endef

# Arguments: (1) Build dir, (2) Source dir
define driver_easy_c =

$(1)/%.c.o: $(2)/%.c
	@mkdir -p $(1)/$$(dir $$(subst $(2)/,,$$<))
	@echo CC $$<
	@$(CC) $(CFLAGS) $(DRIVER_CFLAGS) -c $$< -o $$@

endef

# Argument: (1) Task prefix, (2) Variable prefix
define driver_easy_std_tasks =

$(1): $(2)_OUTPUT

$(1)_clean: 
	rm -rf $($(2)_BUILD)
	mkdir -p $($(2)_BUILD)

endef

$(foreach m,$(ENABLED_DRIVERS),$(eval include drivers/$(m)/Drecipes.mk))
