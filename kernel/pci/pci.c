#include <power/pci.h>
#include <power/i386/asm.h>
#include <power/memory/heap.h>

/* NOTICE: This PCI driver impl relies
   that the system is x86_64. Either move it to
   arch/i386 or make it more arch independent */

#define CONFIG_ADDRESS 0xCF8
#define CONFIG_DATA 0xCFC

struct pci_device* devices = NULL;

static void devicesAppend(struct pci_device** head, struct pci_device* dev)
{
    if (!*head) {
        (*head) = dev;
        return;
    }

    struct pci_device* tmp = *head;
    while (tmp->Next)
        tmp = tmp->Next;
    tmp->Next = dev;
}

/* vvvvv ACTUAL PCI vvvvv */

static void checkBus(uint8_t bus);
#define pciCompareGeneric(X, y, t) ((X == (t)PCI_DONT_CARE) || (X == y))
#define pciCompare(X, y) pciCompareGeneric(X, y, uint16_t)
#define pciCompareuint8(X, y) pciCompareGeneric(X, y, uint8_t)

uint32_t pciReadDoubleWord(uint8_t bus, uint8_t device, uint8_t function,
                           uint8_t offset)
{
    uint32_t address = (1 << 31) | ((uint32_t)bus << 16)
        | ((uint32_t)device << 11) | ((uint32_t)function << 8)
        | (offset & 0xFC);
    __outl(CONFIG_ADDRESS, address);
    return __inl(CONFIG_DATA);
}

uint32_t pciReadDoubleWordFromDevice(const struct pci_device* device,
                                     uint8_t offset)
{
    return pciReadDoubleWord(device->Bus, device->Device, device->Function,
                             offset);
}

void pciWriteDoubleWord(uint8_t bus, uint8_t device, uint8_t function,
                        uint8_t offset, uint32_t data)
{
    uint32_t address = (1 << 31) | ((uint32_t)bus << 16)
        | ((uint32_t)device << 11) | ((uint32_t)function << 8)
        | (offset & 0xFC);
    __outl(CONFIG_ADDRESS, address);
    __outl(CONFIG_DATA, data);
}

void pciWriteDoubleWordToDevice(const struct pci_device* dev, uint8_t offset,
                                uint32_t data)
{
    return pciWriteDoubleWord(dev->Bus, dev->Device, dev->Function, offset,
                              data);
}

static void checkFunction(uint8_t bus, uint8_t device, uint8_t function)
{
    uint32_t cls =
        (pciReadDoubleWord(bus, device, function, 8) & 0xFFFF0000) >> 16;
    uint32_t vd = (pciReadDoubleWord(bus, device, function, 0) & 0xFFFF);

    if (vd != 0xFFFF) {
        struct pci_device* dev = khsAlloc(sizeof(*dev));
        dev->Device = device;
        dev->Bus = bus;
        dev->Function = function;
        dev->Next = NULL;
        devicesAppend(&devices, dev);
    }

    if (((cls & 0xFF00)) == 0x600 && (cls & 0x00FF) == 0x4) {
        uint32_t b2 = pciReadDoubleWord(bus, device, function, 0x18) >> 8;
        checkBus(b2 & 0xFF);
    }
}

static void checkDevice(uint8_t bus, uint8_t device)
{
    uint8_t function = 0;

    if ((pciReadDoubleWord(bus, device, function, 0) & 0xFFFF) == 0xFFFF) {
        return; /* Invalid... */
    }

    checkFunction(bus, device, function);
    uint32_t ht = (pciReadDoubleWord(bus, device, function, 0xC) >> 16) & 0xFF;
    if (ht & (1 << 7)) {
        for (function = 1; function < 8; function++) {
            checkFunction(bus, device, function);
        }
    }
}

void checkBus(uint8_t bus)
{
    for (int i = 0; i < 32; i++) {
        checkDevice(bus, i);
    }
}

void pciInitializeRegistry() { checkBus(0); }

struct pci_device* pciSearchDevice(uint16_t vendor, uint16_t device)
{
    struct pci_device* dev = devices;
    uint32_t vddv;
    uint16_t vend;
    uint32_t dend;

    while (dev->Next != NULL) {
        vddv = pciReadDoubleWord(dev->Bus, dev->Device, dev->Function, 0);
        vend = vddv & 0xFFFF;
        dend = vddv >> 16;

        if (vend == vendor && dend == device) {
            return dev;
        }

        dev = dev->Next;
    }

    return NULL;
}

struct pci_device* pciSearchDeviceEx(struct pci_search_query query)
{
    struct pci_device* dev = devices;
    uint32_t vddv, clscl;
    uint16_t vend;
    uint32_t dend;
    uint8_t class, subclass, pif;

    while (dev->Next != NULL) {
        vddv = pciReadDoubleWord(dev->Bus, dev->Device, dev->Function, 0);
        clscl = pciReadDoubleWord(dev->Bus, dev->Device, dev->Function, 8);

        vend = vddv & 0xFFFF;
        dend = vddv >> 16;

        subclass = (clscl >> 16) & 0xFF;
        class = (clscl >> 24) & 0xFF;
        pif = (clscl >> 8) & 0xFF;

        if (pciCompare(query.Vendor, vend) && pciCompare(query.DeviceId, dend)
            && pciCompareuint8(query.Class, class)
            && pciCompareuint8(query.SubClass, subclass)
            && pciCompareuint8(query.ProgIf, pif)) {
            return dev;
        }

        dev = dev->Next;
    }

    return NULL;
}
