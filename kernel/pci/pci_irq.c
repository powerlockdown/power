#include "power/interrupt.h"
#include <power/i386/idt.h>
#include <power/pci.h>

#include <power/memory/heap.h>

#include <power/driver.h>
#include <power/abstract/intrctl.h>
#include <power/tty.h>
#include <power/list.h>
#include <power/vector.h>

#include <power/error.h>

struct pci_irq_handler {
    void* Data;
    interrupt_handler_pfn_t Interrupt;
};

struct pci_handled_irq {
    struct list_item List;
    int Irq;
    int InterruptVector;

    struct vector Handlers;
};

static struct pci_handled_irq* irqs;

static void pciExecuteInterrupt(void*);
int pciHandleMessageInterrupt(struct pci_device* dev, interrupt_handler_pfn_t pfn,
                              void* intData)
{
    uint32_t w = pciReadDoubleWordFromDevice(dev, 0x4) >> 16;
    if (!(w & (1 << 4))) {
        return -ERROR_NOT_SUPPORTED;
    }

    bool found = false;
    uint32_t t;
    uint32_t offset = pciReadDoubleWordFromDevice(dev, 0x34) & 0xFC;
    while (offset) {
        t = pciReadDoubleWordFromDevice(dev, offset);
        if ((t & 0x00FF) == 0x05) {
            found = true;
            break;
        }

        offset = (t & 0xFC00) >> 8;
    }

    if (!found)
        return -ERROR_NOT_SUPPORTED;

    int vector = idtGetFreeVector(3);
    int addend = 0x8;
    uint64_t data;
    uint64_t address = intCtlGetMsiAddress(&data, vector);

    uint16_t msgctl = (t & 0xFFFF0000) >> 16;
    pciWriteDoubleWordToDevice(dev, offset + 0x4, address & 0xFFFFFFFF);
    if (msgctl & (1 << 7)) {
        addend = 0xC;
        pciWriteDoubleWordToDevice(dev, offset + 0x8, address >> 32);
    }

    t &= ~(0xFFFF << 16);
    msgctl |= 1; /* Enable MSI */
    
    pciWriteDoubleWordToDevice(dev, offset, t | (msgctl << 16));
    pciWriteDoubleWordToDevice(dev, offset + addend, data & 0xFFFF);
    idtHandleInterrupt(vector, pfn, intData);
    return 0;
}

static inline int createHandledIrq(struct pci_handled_irq* han, int irq)
{
    int vec = intCtlHandleInterrupt(irq, pciExecuteInterrupt, NULL);
    han->InterruptVector = vec;
    return vec;
}

int pciHandleInterrupt(struct pci_device* dev, interrupt_handler_pfn_t pfn,
                       void* data)
{
    int status;
    status = pciHandleMessageInterrupt(dev, pfn, data);
    if (!status) {
        return 0;
    }

    if (status < 0 && status != -ERROR_NOT_SUPPORTED) {
        trmLogfn("HandleMSI: error: %p", status);
        return status;
    }

    struct driver_acpi_bus_interface* inter;
    inter = modGetDriver("acpi")->Info->Interface;
    if (!inter) {
        return -ERROR_NO_DEVICE;
    }

    int irq = inter->pciEnableInterrupt(dev);
    if (!irqs) {
        irqs = khsAlloc(sizeof(*irqs));
        irqs->Irq = irq;
        createHandledIrq(irqs, irq);
    }

    bool found = false;
    listForEach(&irqs->List, i) {
        struct pci_handled_irq* ir;
        ir = (struct pci_handled_irq*)i;

        if (ir->Irq == irq) {
            found = true;
            break;
        }
    }

    if (!found) {
        struct pci_handled_irq* ir = khsAlloc(sizeof(*ir));
        createHandledIrq(ir, irq);
        listInsert(&irqs->List, &ir->List);
    }

    listForEach(&irqs->List, i) {
        struct pci_handled_irq* ir;
        ir = (struct pci_handled_irq*)i;

        if (ir->Irq == irq) {
            struct pci_irq_handler* han = khsAlloc(sizeof(*han));
            han->Data = data;
            han->Interrupt = pfn;
            vectorInsert(&ir->Handlers, han);
        }
    }

    /* Enabl INTx assertion bit. */
    int command = pciReadDoubleWordFromDevice(dev, 0x4);
	pciWriteDoubleWordToDevice(dev, 0x4, command & ~(1 << 10));
    return 0;
}

void pciExecuteInterrupt(void* ig)
{
    __unused(ig);
    bool found = false;

    int currentInt = intCtlGetCurrentInterrupt();
    struct pci_handled_irq* ir;
    listForEach(irqs, i) {
        ir = (struct pci_handled_irq*)i;
        
        if (ir->InterruptVector == currentInt) {
            found = true;
            break;
        }
    }

    if (!found)
        return;

    struct pci_irq_handler* h;
    for (size_t j = 0; j < ir->Handlers.Length; j++) {
        h = ir->Handlers.Data[j];
        h->Interrupt(NULL, h->Data);
    }
}
