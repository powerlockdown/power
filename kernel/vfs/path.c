#include "power/um.h"
#include <power/error.h>
#include <power/string.h>
#include <power/vfs.h>

#include <power/limits.h>

#include <power/memory/heap.h>
#include <power/memory/memset.h>
#include <power/memory/user_buffer.h>

/* Perform a reverse search for the 
   specified char inside the string. */
static const char* strrevsrch(const char* s1, char c1);

/* Perform a byte-by-byte string search. */
static const char* strsrch(const char* end, char c1);

/* Transform a relative path onto a absolute one,
   given the base (usually the CWD). */
int vfsComputeAbsPath(const char* base, 
                      const char* relp,
                      char* restrict buffer, size_t size)
{
    if (base && !vfsIsAbsolute(base)) {
        return -ERROR_INVALID_ARGUMENT;
    }

    int error = 0;

    size_t len;
    size_t bslen;
    size_t relen;
    size_t scrtchlen;

    char* scratch = NULL;
    const char* resep;
    const char* bslssep; /*< BaSe LaSt SEParator. */
    const char* src;
    const char* relpbegin;

    if (base)
        bslen = strlen(base);
    len = strlen(relp);
    relpbegin = relp;

    if (vfsIsAbsolute(relp) || !base) {
        src = relp;
        goto write;
    }

    scratch = khsAlloc(MAX_PATH);
    size_t bsldistance;

    if (size > MAX_PATH) {
      error = -ERROR_TOO_BIG;
        goto free;
    }

    while (1) {

        /* Has '../'? Go to the previous dir. */
        if (*relp == '.' && *(relp + 1) == '.' && (*(relp + 2) == '/')) {
            /* To accomodate for the two dots and the fslash.
               The fslash gets incremented in the loop end. */
            relp += 2;
            bslssep = strrevsrch(base, '/');
            bsldistance = bslssep - base;

            /* Copy the base to the scratch. */
            memcpy(scratch, base, bsldistance);
            scratch[bsldistance] = '/';

            resep = strsrch(relp + 1, '/');
            relen = resep - relp;
	    
            /* Copy the relative remainder to the scratch.
                `bsldistance + 1' to compensate over the added slash;
                `repl + 1' to compensate over the non-incremented slash \
                    (see above comment). */
            memcpy(scratch + bsldistance + 1, 
                   relp + 1, relen);
            
            /* Compute the length of the scratch buffer.
               Just sum the two length params from the above
               memcpy calls. */
            scrtchlen = relen + bsldistance;

            base = scratch;
            bslen = scrtchlen;
            bslssep = strrevsrch(base, '/');
            scratch[scrtchlen] = 0;
	    relp += relen;
	    if (!*relp)
	      break;
	    
            goto ct;
        }

        /* Has `./'? Ignore dir. */
        if (*relp == '.' && *(relp + 1) == '/') {
            /* Consume the dot. */
            relp++;

            bslen = strlen(base);
            if (bslen > size) {
                goto free;
            }

	    size_t rel = (len - (relpbegin - relp));
	    
            /* Copy the absolute part. */
            memcpy(scratch, base, bslen);
            scratch[bslen] = '/';

            /* Then copy the relative. */
            memcpy(scratch + bslen + 1, 
                   relp + 1, rel);

            /* Nicely compute the new path length. */
            scrtchlen = rel + bslen;
            scratch[scrtchlen] = 0;

            base = scratch;
            bslen = scrtchlen;
	    relp += rel;
            goto ct;
        }

        /* If there is a slash and the above cases
           failed, then we simply copy them to the
           scratch. */
        if (*relp != '.' && (*(relp + 1) == '/' && (*relp + 2) != '\0')) {
	    const char* oldrelp = relp;
	    while (*relp != '/' && relp != relpbegin) {
	      relp--;
	    }

	    if (*relp == '/')
	      relp++;

	    size_t length;
	    length = ((oldrelp + 1) - relp);
	    memcpy(scratch, base, bslen);

	    scratch[bslen] = '/';
	    memcpy(scratch + bslen + 1, relp, length);
	    length += bslen + 1;

	    scratch[length] = 0;
	    base = scratch;
	    bslen = length;
	    relp = oldrelp + 1;
            goto ct;
        }

        /* Copy the last component. */
        if (!(*relp)) {
            resep = strrevsrch(relpbegin, '/');
            
            /* Copy the base. */
            memcpy(scratch, base, bslen);
	    if (resep == relpbegin) {
	      /* No fslash */
	      scratch[bslen] = '/';
	      bslen++;
	    }
	    
            /* And the relative part (the component.) */ 
            relen = (relp - resep) + 1;
            strncat(scratch, resep, relen);
            scrtchlen = bslen + relen;

            scratch[scrtchlen] = 0;
            base = scratch;
            bslen = scrtchlen;
            break;
        }

ct:
        relp++;
    }
    
    src = scratch;
    len = scrtchlen;
write:
    if (len > size) {
        error = -ERROR_TOO_BIG;
        goto free;
    }

    memcpy(buffer, src, len);

free:
    if (scratch)
        khsFree(scratch);
    return error;
}


uint64_t syscGetRealPath(union sysc_regs* regs)
{
    /* GetRealPath(const char* absolute, const char* rel, char* buffer, long size) -> int */

    const char* base = (const char*)regs->Arg1;
    const char* relative = (const char*)regs->Arg2;
    char* buffer = (char*)regs->Arg3;
    long size = regs->Arg4;

    if (size > MAX_PATH) {
        size = MAX_PATH;
    }

    int error;
    if ((error = mmUserBufferCheck(buffer, size, UBUFFER_WRITE_BIT))) {
        return error;
    }

    if ((error = vfsComputeAbsPath(base, relative, buffer, size))) {
        return error;
    }

    return size;
}

const char* strrevsrch(const char* s1, char c1)
{
    size_t st = strlen(s1);
    while (st) {
        if (s1[st] == c1)
            return s1 + st;

        st--;
    }

    return s1 + st;
}

const char* strsrch(const char* end, char c1)
{
    while (*end) {
        if (*end == c1) {
            return end;
        }

        end++;
    }

    return end;
}
