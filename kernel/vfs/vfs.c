#include "power/limits.h"
#include <power/vfs.h>
#include <power/config.h>

#include <power/memory/heap.h>
#include <power/memory/pfa.h>
#include <power/memory/memset.h>

#include <power/driver.h>
#include <power/elf.h>

#include <power/partition.h>
#include <power/um.h>

#include <power/string.h>
#include <power/vector.h>
#include <power/error.h>

#include <power/device.h>
#include <power/memory/user_buffer.h>
#include <power/user_signal.h>

#include <power/filestats.h>

#define VFS_MAX_SCRATCH_SIZE 32768

static struct mm_cache* vfsFileCache;
static struct mm_cache* vfsMountCache;
static const char* letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";
struct vector mountPoints;
extern struct vector loadedDrivers;

void vfsInit() 
{ 
  mountPoints = vectorCreate(2);
}

size_t vfsGetPathComponent(const char* path)
{
    const char* ptr = path;
    const char* lastSlash = ptr;
    while (*ptr) {
        if (*ptr == '/') {
            lastSlash = ptr;
        }

        ptr++;
    }

    return lastSlash - path;
}

struct fs_mountpoint*
vfsCreateFilesystem(const char* filesystem, uint64_t beginLba,
                    struct driver_disk_device_interface* device)
{
    if (!vfsMountCache) {
        vfsMountCache = khCreateCache("vfs_mountpoint", sizeof(struct fs_mountpoint));
    }

    static size_t letterIndex = 0;
    struct fs_mountpoint* mp = khAlloc(vfsMountCache);

    memset(mp, 0, sizeof(*mp));
    mp->Letter[0] = letters[letterIndex];
    letterIndex++;

    struct kdriver_manager* man = modGetFilesystemName(filesystem);
    struct driver_fs_interface* intf = man->Info->Interface;
    mp->Ops = intf->mount(intf, device, beginLba, mp);
    vectorInsert(&mountPoints, mp);

    mp->ScratchBuffer = mmAllocKernel(mp->BlockSize);
    mp->ScratchBufferLength = mp->BlockSize;
    return mp;
}

struct fs_file* vfsOpenFile(const char* at, const char* path, int flags)
{
    if (!vfsFileCache) {
        vfsFileCache = khCreateCache("vfs_file", sizeof(struct fs_file));
    }

    size_t i;
    int error;
    struct fs_mountpoint* mp;

    const char* cln;
    char* fr = khsAlloc(MAX_PATH);

    if (!fr) {
        error = -ERROR_NO_MEMORY;
        goto error;
    }
    
    if ((error = vfsComputeAbsPath(at, path, fr, MAX_PATH))) {
        goto free;
    }

    if (fr[1] == ':') {
        cln = fr + 3;
    } else if (fr[2] == ':') {
        cln = fr + 4;
    }

    struct fs_file* file = khAlloc(vfsFileCache);
    if (!file) {
        error = -ERROR_NO_MEMORY;
        goto free; 
    }

    for (i = 0; i < mountPoints.Length; i++) {
        mp = mountPoints.Data[i];
        if (mp->Letter[0] == fr[0] ||
           (mp->Letter[0] == fr[0] && mp->Letter[1] == fr[1])) {
            break; 
        }

        mp = NULL;
    }

    if (!mp) {
      error = -ERROR_NOT_FOUND;
      khFree(vfsFileCache, file);
      goto free;
    }
    
    file->Mounted = mp;
    file->Flags = flags;
    file->Name = strclone(cln);
    mp->Ops->createFile(mp->Ops, cln, file);

    khsFree(fr);
    return file;
free:
    khsFree(fr);

error:
    return ERROR_INTO_PTR((unsigned long)error);
}

void vfsCloseFile(struct fs_file* file)
{
  if (file->Reserved)
    file->Mounted->Ops->closeFile(file->Mounted->Ops, file);
    
  khsFree(file->Name);
  khFree(vfsFileCache, file);
}

size_t vfsReadFile(struct fs_file* file, uint8_t* buffer, size_t size)
{
    struct fs_mountpoint* mp = file->Mounted;

    if (file->Offset && mp->ScratchBufferLength < size + file->Offset) {
        mmAlignedFree(mp->ScratchBuffer, mp->ScratchBufferLength);
        mp->ScratchBufferLength = size + file->Offset;

        mp->ScratchBufferLength =
            alignUp(mp->ScratchBufferLength, mp->BlockSize);
        mp->ScratchBuffer = mmAllocKernel(mp->ScratchBufferLength);
    }

    size_t sz = mp->Ops->read(mp->Ops, file->Offset ? mp->ScratchBuffer : buffer, size, file);
    if (file->Offset)
        memcpy(buffer, mp->ScratchBuffer + file->Offset, size);

    if (file->Offset && mp->ScratchBufferLength > VFS_MAX_SCRATCH_SIZE) {
        mmAlignedFree(mp->ScratchBuffer, mp->ScratchBufferLength);
        mp->ScratchBufferLength = VFS_MAX_SCRATCH_SIZE;
        mp->ScratchBuffer = mmAllocKernel(mp->ScratchBufferLength);
    }

    return sz;
}

int vfsGetFileStatistics(struct fs_file* file, struct __filestats* fs)
{
  struct fs_mountpoint* mp = file->Mounted;
  return mp->Ops->stat(mp->Ops, file, fs);
}

int vfsWriteFile(struct fs_file* file, const uint8_t* buffer, size_t size)
{
  struct fs_mountpoint* mp = file->Mounted;
  return mp->Ops->write(mp->Ops, buffer, size, file);
}

/* Open a file, device or pipe for reading, writing,
    or other miscellaneous I/O operation: the OpenFile syscall.
    
    Arguments
        file - File/device name to be open;
        flags - Indication of special behaviour 
                during handle opening; 
                currently no flags are defined. 

    Return values
        Returns 0 or bigger for valid handle creation. 
        -1 or smaller for errors.

    Possible errors
        * ERROR_INVALID_ARGUMENT: file is null.
        * ERROR_NO_MEMORY: no memory to create the handle.

        Other errors code may occur, but their interpretation
        is up to the device or filesystem driver.

    Notes
        All devices begin with a "virtual mountpoint" ("\??:/").
            "\??://scsi0p1" is an example of an device.

        Not all filesystems or devices allow you to perform every
            operation. For example, a WriteFile might be rejected
            for a file because its mounted read-only (or the device
            itself is protected against writes); A ReadFile may not 
            occur because you have permissions to know the existence
            of this file, but no perm to read it.

*/
uint64_t syscOpenFile(union sysc_regs* regs)
{
    /* OpenFile( const char* file, int flags ) */
    if (!regs->Arg1) {
        return -ERROR_INVALID_ARGUMENT;
    }

    const char* name = (const char*)regs->Arg1;
    struct process* cp = pcGetCurrentProcess();

    if (!strncmp(name, DEVN_PATH_PREFIX, DEVN_PATH_PREFIX_SIZE)) {
        struct handle* h = devnCreateDeviceHandle(cp, name + DEVN_PATH_PREFIX_SIZE);
        if (!h) {
            /* CreateDeviceHandle returns NULL in 
               two really different scenarios.
               
               1) no device node found (correctly handled);
               2) no memory to allocate the handle (INCORRECTLY HANDLED!)
               
               TODO: make CreateDeviceHandle return error codes */
            return -ERROR_NOT_FOUND;
        }

        return h->Identifier;
    }

    struct fs_file* file = vfsOpenFile(cp->WorkingDirectory, name, regs->Arg2);
    if (ERROR_PTR(file))
      return ERROR_FROM_PTR(file);

    if (file->Bad) {
        khFree(vfsFileCache, file);
        return -file->Bad;
    }

    struct handle* h = pcCreateHandle(cp, HANDLE_TYPE_FILE, file);
    if (ERROR_PTR(h)) {
      vfsCloseFile(file);
      return ERROR_FROM_PTR(h);
    }
    
    h->Resource = file;
    return h->Identifier;
}

/* Read from a file, pipe, or device: the ReadFile system call.

    Arguments
        handle - where to read from that file;
        buffer - where to place the read content;
        length - the maximum amount of content to read.

    Return values
        If the operation was successful, the exact amount
        of bytes written onto the buffer is returned. If
        there was an error, its negative error code is returned.

    Possible errors
        * ERROR_PIPE_BROKEN - (for pipes) The pipe was closed 
            on the other end. Do not try to call this function
            on this handle ever again.
 */
uint64_t syscReadFile(union sysc_regs* regs)
{
    int ret;
    void* buffer = (void*)regs->Arg2;
    if ((ret = mmUserBufferCheck(buffer, regs->Arg3, UBUFFER_WRITE_BIT))) {
        return ret;
    }

    struct process* proc = pcGetCurrentProcess();
    struct handle* handle = pcGetHandleById(proc, regs->Arg1);
    if (!handle) {
        return -ERROR_BAD_HANDLE;
    }

    return handle->Ops.read(handle, buffer, regs->Arg3);
}

uint64_t syscWriteFile(union sysc_regs* regs)
{
    int ret;
    void* buffer = (void*)regs->Arg2;
    if ((ret = mmUserBufferCheck(buffer, regs->Arg3, 0))) {
        return ret;
    }

    struct handle* handle = pcGetHandleById(pcGetCurrentProcess(), regs->Arg1);
    if (!handle) {
        return -ERROR_BAD_HANDLE;
    }

    return handle->Ops.write(handle, buffer, regs->Arg3);
}

uint64_t syscSetFilePointer(union sysc_regs* regs)
{
    /* SetFilePointer( int fh, int rel, size_t offset ) */
    struct handle* buffer = pcGetHandleById(pcGetCurrentProcess(), regs->Arg1);
    if (!buffer->Ops.seek)
      return -ERROR_NOT_SUPPORTED;
      
    return buffer->Ops.seek(buffer, regs->Arg2, regs->Arg3);
}

uint64_t syscGetFileStatistics(union sysc_regs* regs)
{
  /* GetFileStatistics(int fh, struct __fstats*) -> int */
  int error;
  struct process* proc = pcGetCurrentProcess();
  struct handle* han = pcGetHandleById(proc, regs->Arg1);

  if (!han || han->Type != HANDLE_TYPE_FILE)
    return -ERROR_BAD_HANDLE;

  struct __filestats* fs;
  fs = (void*)regs->Arg2;
  
  if ((error = mmUserBufferCheck(fs, sizeof(*fs), UBUFFER_WRITE_BIT)))
    return error;

  return vfsGetFileStatistics(han->Resource, fs);
}

uint64_t syscMountVolume(union sysc_regs* regs)
{
    /* MountVolume( int partition, char* filesystem, void* reserved ) */
    struct kdriver_manager* kd = modGetFilesystemName((const char*)regs->Arg2);
    struct driver_fs_interface* fs = kd->Info->Interface;
    if (!fs)
        return -ERROR_NO_DEVICE;
    
    struct handle* h = pcGetHandleById(pcGetCurrentProcess(), regs->Arg1);
    if (!h || h->Type != HANDLE_TYPE_DEVICE)
        return -ERROR_BAD_HANDLE;

    struct partition* pt = h->Resource;
    vfsCreateFilesystem((const char*)regs->Arg2, pt->BeginLba, pt->Device);    
    return 0;
}
