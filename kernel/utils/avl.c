#include "power/config.h"
#include "power/memory/heap.h"

#include <power/avl.h>
#include <stdint.h>
#include <stddef.h>

#define HEIGHT(X) X ? ((X)->avn_height) : 0
#define IS_LEFT(X) ((X)->avn_parent && (X == (X)->avn_parent->avn_left))

struct avl_node {
    int avn_height;

    uint64_t avn_key;
    void* avn_value;

    struct avl_node* avn_parent;

    struct avl_node* avn_left;
    struct avl_node* avn_right;
};

static struct mm_cache* nodeCache;

static struct avl_node* leftRotate(struct avl_tree* tree, struct avl_node* node);
static struct avl_node* rightRotate(struct avl_tree* tree, struct avl_node* node);

static
struct avl_node* createNode(struct avl_node* parent, uint64_t key, void* value)
{
    if (!nodeCache) {
        nodeCache = khCreateCache("avl_node", sizeof(*parent));
    }

    struct avl_node* node = khAlloc(nodeCache);
    node->avn_parent = parent;
    node->avn_value = value;
    node->avn_key = key;
    return node;
}

static inline
struct avl_node* getSibling(struct avl_node* emplacee)
{
    struct avl_node* sibling;
    if (IS_LEFT(emplacee))
        sibling = emplacee->avn_parent->avn_right;
    else if (!emplacee->avn_parent) /* We are the root etc... */
        sibling = NULL;
    else
        sibling = emplacee->avn_parent->avn_left;
    return sibling;
}

static
struct avl_node* lookupInner(struct avl_tree* tree, uint64_t key)
{
    struct avl_node* node = tree->avt_root;
    while (node) {
        if (node->avn_key > key) {
            node = node->avn_left;
        } else if (node->avn_key < key) {
            node = node->avn_right;
        } else {
            return node;
        }
    }
   
    return NULL;
}

static inline
int getBalanceFactor(struct avl_node* node)
{
    int bf = 0;
    struct avl_node* sibling = getSibling(node);

    if (IS_LEFT(node)) {
        bf = node->avn_height - (sibling ? sibling->avn_height : 0);
    } else {
        bf =  (sibling ? sibling->avn_height : 0) - node->avn_height;
    }

    return bf;
}

static inline
int getChildrenBalanceFactor(struct avl_node* node)
{
    int bf = 0;

    if (node && node->avn_left)
        bf = getBalanceFactor(node->avn_left);
    else if (node && node->avn_right)
        bf = getBalanceFactor(node->avn_right);

    return bf;
}

/* rem - removed node, repl - the node which came to replace it. */
static
void* removeFix(struct avl_tree* tree,
                struct avl_node* rem, 
                struct avl_node* repl)
{
    int bf = 0, sbf = 0;

    struct avl_node* root;
    struct avl_node* node;
    struct avl_node* sibling;

    node = repl;
    if (!node) {
        node = rem->avn_parent;
    }

    while (node) {
        root = node->avn_parent;
        sibling = getSibling(node);
        node->avn_height = 1 + max(HEIGHT(node->avn_left), HEIGHT(node->avn_right));
        bf = getBalanceFactor(node);

        /* LEFT-HEAVY. */
        if (bf > 1) {
            sbf = getChildrenBalanceFactor(node);

            /* PERFORM RIGHT-LEFT ROTATION. */
            if (sbf <= -1) {
                rightRotate(tree, node->avn_right);
                node = leftRotate(tree, node);
                goto c;
            }

            if (node->avn_key > root->avn_key) {
                if (sibling)
                    node = rightRotate(tree, sibling);
            } else if (node->avn_key < root->avn_key) {
                node = rightRotate(tree, node);
            }
        }

        /* RIGHT-HEAVY. */
        if (bf < -1) {
            sbf = getChildrenBalanceFactor(node);

            /* PERFORM LEFT-RIGHT ROTATION. */
            if (sbf >= 1) {
                struct avl_node* l = node->avn_left;
                rightRotate(tree, l);
                if (l->avn_parent)
                    node = leftRotate(tree, l->avn_parent);
                goto c;
            }

            if (root && node->avn_key > root->avn_key) {
                node = leftRotate(tree, root);
            } else if (root && node->avn_key < root->avn_key) {
                if (sibling)
                    node = leftRotate(tree, sibling);
            }
        }

c:
        node = node->avn_parent;
    }

    void* val = rem->avn_value;
    khFree(nodeCache, rem);
    return val;
}

static inline
struct avl_node* getSuccessor(struct avl_node* node)
{
    struct avl_node* n;
    if (node->avn_right) {
        n = node->avn_right;

        while (n->avn_left) {
            n = n->avn_left;
        }
        return n;
    }

    return NULL;
}

void* avlRemove(struct avl_tree* tree, uint64_t key)
{
    struct avl_node* node = lookupInner(tree, key);
    struct avl_node* repl = NULL;

    if (!node) {
        return NULL;
    }

    if (!node->avn_left && !node->avn_right) {
        if (IS_LEFT(node))
            node->avn_parent->avn_left = NULL;
        else if (node->avn_parent /* IS_RIGHT */)
            node->avn_parent->avn_right = NULL;
        else
            tree->avt_root = NULL;
        
        goto free;
    }

    if (node->avn_left && !node->avn_right) {
        if (IS_LEFT(node)) {
            node->avn_parent->avn_left = node->avn_left;
            node->avn_left->avn_parent = node->avn_parent;
        } else if (node->avn_parent /* IS_RIGHT */) {
            node->avn_parent->avn_right = node->avn_left;
            node->avn_left->avn_parent = node->avn_parent;
        } else {
            tree->avt_root = node->avn_left;
            node->avn_left->avn_parent = NULL;
        }
        
        repl = node->avn_left;
        goto free;
    } else if (!node->avn_left && node->avn_right) {
        if (IS_LEFT(node)) {
            node->avn_parent->avn_left = node->avn_right;
            node->avn_right->avn_parent = node->avn_parent;
        } else if (node->avn_parent /* IS_RIGHT */) {
            node->avn_parent->avn_right = node->avn_right;
            node->avn_right->avn_parent = node->avn_parent;
        } else {
            tree->avt_root = node->avn_right;
            node->avn_right->avn_parent = NULL;
        }
        
        repl = node->avn_right;
        goto free;
    }

    struct avl_node* ss = getSuccessor(node);
    if (ss->avn_parent != node) {
        /* SWAP PLACES. */
        struct avl_node* r = ss->avn_parent;
        r->avn_left = ss->avn_right;
        if (ss->avn_right) {
            ss->avn_right->avn_parent = r;
        }

        ss->avn_parent = NULL;
        ss->avn_right = r;

        r->avn_parent->avn_left = NULL;
        r->avn_parent = ss;
    }

    if (IS_LEFT(node)) {
        node->avn_parent->avn_left = ss;
        ss->avn_parent = node->avn_parent;
    } else if (node->avn_parent /* IS_RIGHT */) {
        node->avn_parent->avn_right = ss;
        ss->avn_parent = node->avn_parent;
    } else {
        tree->avt_root = ss;
        ss->avn_parent = NULL;
    }

    repl = ss;
free:
    return removeFix(tree, node, repl);
}

void* avlLookup(struct avl_tree* tree, uint64_t key)
{
    struct avl_node* node = lookupInner(tree, key);
    if (!node) {
        return node;
    }

    return node->avn_value;
}

static 
void insertFix(struct avl_tree* tree, struct avl_node* emplacee)
{
    int grow = 0;
    struct avl_node* sibling = getSibling(emplacee);
    if (!sibling) {
        grow = 1;
    } else {
	    emplacee->avn_height = 1;
    }

    struct avl_node* root;
    struct avl_node* node = emplacee;
    int h = node->avn_height;
    int bf, sbf;

    while (node) {
        /* Has to be updated at every loop
           because it might change. */
        root = node->avn_parent;
        sibling = getSibling(node);
        if (grow) {
            node->avn_height = ++h;
        }

        bf = getBalanceFactor(node);

        /* LEFT-HEAVY. */
        if (bf > 1) {
            sbf = getChildrenBalanceFactor(node);

            /* PERFORM RIGHT-LEFT ROTATION. */
            if (sbf < -1) {
                /*node = leftRotate(tree, node->avn_right);
                node = rightRotate(tree, node);*/
                goto c;
            }

            if (node->avn_key > root->avn_key) {
                if (sibling)
                    node = rightRotate(tree, sibling);
            } else if (node->avn_key < root->avn_key) {
                node = rightRotate(tree, node);
            }
        }

        /* RIGHT-HEAVY. */
        if (bf < -1) {
            sbf = getChildrenBalanceFactor(node);

            /* PERFORM LEFT-RIGHT ROTATION. */
            if (sbf > 1) {
                struct avl_node* l = node->avn_left;
                rightRotate(tree, l);
                if (l->avn_parent)
                    node = leftRotate(tree, l->avn_parent);
                goto c;
            }

            if (root && node->avn_key > root->avn_key) {
                node = leftRotate(tree, root);
            } else if (root && node->avn_key < root->avn_key) {
                if (sibling)
                    node = leftRotate(tree, sibling);
            }
        }

c:
        if (node)
            node = node->avn_parent;
    }
}

struct avl_node* leftRotate(struct avl_tree* tree, struct avl_node* node)
{
    struct avl_node* y = node->avn_right;
    
    node->avn_right = y->avn_left;

    if (y->avn_left) {
        y->avn_left->avn_parent = node;
    }

    y->avn_parent = node->avn_parent;
    if (IS_LEFT(node)) {
        node->avn_parent->avn_left = y;
    } else if (node->avn_parent) {
        node->avn_parent->avn_right = y;
    } else {
        tree->avt_root = y;
    }

    if (y) {
        y->avn_left = node;
        y->avn_height = 1 + max(HEIGHT(y->avn_left), HEIGHT(y->avn_right));
    }

    node->avn_parent = y;
    node->avn_height = 1 + max(HEIGHT(node->avn_left), HEIGHT(node->avn_right));
    return y;
}

struct avl_node* rightRotate(struct avl_tree* tree, struct avl_node* node)
{
    struct avl_node* y = node->avn_parent;
    y->avn_left = node->avn_right;
    if (y->avn_left)
        y->avn_left->avn_parent = y;

    node->avn_parent = y->avn_parent;
    if (IS_LEFT(y)) {
        y->avn_parent->avn_left = node;
    } else if (node->avn_parent) {
        y->avn_parent->avn_right = node;
    } else {
        tree->avt_root = node;
    }

    node->avn_right = y;
    y->avn_parent = node;
    node->avn_height = 1 + max(HEIGHT(node->avn_left), HEIGHT(node->avn_right));
    if (y)
        y->avn_height = 1 + max(HEIGHT(y->avn_left), HEIGHT(y->avn_right));

    return y;
}

void
avlInsert(struct avl_tree* tree, uint64_t key, void* value)
{
    if (!tree->avt_root) {
        tree->avt_root = createNode(NULL, key, value);
        tree->avt_root->avn_height = 1;
        return;
    }

    struct avl_node* par;

    struct avl_node* emplacee = createNode(NULL, key, value);
    emplacee->avn_key = key;

    struct avl_node* node = tree->avt_root;
    while (node) {
        par = node;
        if (node->avn_key > key) {
            node = node->avn_left;
            if (!node) {
                par->avn_left = emplacee;
                emplacee->avn_parent = par;
                insertFix(tree, emplacee);
            }
        } else {
            node = node->avn_right;
            if (!node) {
                par->avn_right = emplacee;
                emplacee->avn_parent = par;
                insertFix(tree, emplacee);
            }
        }
    }
}


struct avl_tree* avlCreateTree()
{
    struct avl_tree* tree = khsAlloc(sizeof(*tree));
    tree->avt_root = NULL;
    return tree;
}

void avlDestroyTree(struct avl_tree* tree)
{
    /* CLEAR ALL NODES. */
    khsFree(tree);
}

void* avlRemoveRoot(struct avl_tree* tree)
{
  if (!tree->avt_root)
    return NULL;
  return avlRemove(tree, tree->avt_root->avn_key);
}

static intptr_t abs(intptr_t x)
{
  return x < 0 ? x * -1 : x;  
}

void* avlLookupClosestNode(struct avl_tree* tree, uintptr_t* key)
{
  struct avl_node* closest = NULL;
  struct avl_node* node = tree->avt_root;

  intptr_t t;
  intptr_t k = (intptr_t)*key;
  intptr_t diff;
  while (node) {
    if (diff > (t = abs(k - (long)node->avn_key))) {
      diff = t;
      closest = node;
    }
    
    if (node->avn_key > *key) {
      node = node->avn_left;
    } else if (node->avn_key < *key) {
      node = node->avn_right;
    } else {
      return node->avn_value;
    } 
  }

  if (!closest)
    return NULL;
  
  *key = closest->avn_key;
  return closest->avn_value;
}
