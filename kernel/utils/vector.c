#include <power/vector.h>
#include <power/config.h>
#include <power/memory/heap.h>
#include <power/memory/pfa.h>

#define GROW_SCALE ((sizeof(void*) * 4))
#define GROW(X)    (X * GROW_SCALE)

static void** resizeBuffer(void** oldBuffer, size_t* length, int flags)
{
    __unused(flags);
    size_t oldLength = *length;
    void** newb = khsRealloc(oldBuffer, GROW(oldLength) + GROW_SCALE);
    (*length) = oldLength + 4;
    return newb;
}

struct vector vectorCreateEx(size_t initialLength, int flags)
{
    struct vector v = {};
    v.Capacity = initialLength;
    v.Length = 0;
    v.Data = NULL;
    v.Flags = flags;

    if (initialLength != 0) {
        v.Data = khsAlloc(sizeof(void*) * initialLength);
    }

    return v;
}

void vectorInsert(struct vector* v, void* data)
{
    if (v->Length >= v->Capacity) {
        v->Data = resizeBuffer(v->Data, &v->Capacity, v->Flags);
    }

    v->Data[v->Length] = data;
    v->Length++;
}

void* vectorRemove(struct vector* v, void* data)
{
    bool seen = false;
    void **d, **result;

    for (size_t i = 0; i < v->Length; i++) {
        d = &v->Data[i];
        result = &v->Data[i + 1];

        if (*d == data) {
            seen = true;
        }

        if (seen && i < v->Length - 1) {
            (*d) = (*result);
        }
    }

    v->Length--;
    return data;
}

void* vectorRemoveIndex(struct vector* v, int index)
{
    return vectorRemove(v, v->Data[index]);
}

void vectorDestroy(struct vector* vector)
{
    khsFree(vector->Data);
}
