#include "power/config.h"
#include "power/tty.h"
#include <power/string.h>

#include <power/memory/pfa.h>
#include <power/memory/heap.h>
#include <power/memory/memset.h>

static int pathmix(struct vector* v, int* off, char* string, char delim);

size_t strlen(const char* str)
{
    const char* s = str;
    if (!str) {
        trmLogfn("str is NULL! callee=%p", __builtin_return_address(0));
    }

    while (*str != 0)
        ++str;
    return ((size_t)(str - s));
}

int strncmp(const char* s1, const char* s2, size_t length)
{
    size_t l = 0;
    while (l < length) {
        if (s1[l] < s2[l])
            return -1;
        if (s1[l] > s2[l])
            return 1;
        l++;
    }

    return 0;
}

int strcmp(const char* s1, const char* s2)
{
    while (*s1 && (*s1 == *s2))
        s1++;
    s2++;
    return *(const unsigned char*)s1 - *(const unsigned char*)s2;
}

char* strsep(char** string, char delim)
{
    char* str = *string;
    char* begin = str;
    if (!*str)
        return NULL;

    while (true) {
        if (*str == delim || *str == '\0') {
            if (*str != '\0')
                (*string) = str + 1;
            else
                (*string) = str;
            (*str) = 0;
            return begin;
        }

        str++;
    }

    return NULL;
}

char* strclone(const char* string)
{
    if (!string) {
        return NULL;
    }

    size_t len = strlen(string);
    char* buffer = khsAlloc(len + 1);
    if (!buffer)
      return NULL;
    
    memcpy(buffer, string, len);
    buffer[len] = 0;

    return buffer;
}

char* strvec(const char* string, char delim, struct vector* vector)
{
    struct vector vec = vectorCreate(5);

    char *token, *begin;
    char* dp = strclone(string);
    begin = dp;
    while ((token = strsep(&dp, delim))) {
        vectorInsert(&vec, token);
    }

    (*vector) = vec;
    return begin;
}

void strvecfree(char* ctx, const char* string, struct vector* vector)
{
    khsFree(ctx);
    while (vector->Length) {
        vectorRemove(vector, vector->Data[0]);
    }
}

char* pathjn(struct vector* wkd, struct vector* rel, char delim)
{
    char* string = khsAlloc(256);
    int offset = 0;
    if (pathmix(wkd, &offset, string, delim)) {
        goto error;
    }

    if (pathmix(rel, &offset, string, delim)) {
        goto error;
    }

    return string;
error:
    khsFree(string);
    return NULL;
}

int pathmix(struct vector* v, int* off, char* string, char delim)
{
    size_t i, l, offset = *off;
    for (i = 0; i < v->Length; i++) {
        if (offset > 256) {
            return 1;
        }

        char* s = v->Data[i];
        l = strlen(s);

        memcpy(string + offset, s, l);
        offset += l;
        string[offset] = delim;
        offset++;
    }

    (*off) = offset;
    return 0;
}

/* From worstc, which in turn is based off Gnulib. */
unsigned long int 
strtoul(const char* _string, char** _endptr, int _bs)
{
    while (*_string == ' ') {
        _string++;
    }

    /* We don't really support negative numbers. */
    if (*_string == '+' || *_string == '-')
        _string++;
    
    if (!_bs) {
        _bs = 10;
        if (*_string == '0') {
            _bs = 8;
            _string++;
            if (*(_string) == 'x'
             || *(_string) == 'X') {
                _bs = 16;
                _string++;
            }
        }
    }

    int index;
    unsigned long output = 0;
    while (*_string) {
        index = (*_string) - '0';        
        if (*_string >= 'a')
            index = (*_string) - 87;
        else if (*_string >= 'A')
            index = (*_string) - 55;

        if (index > _bs) {
            /* TODO set errno */
	  break;
        }

        output *= _bs;
        output += index;        
        _string++;
    }

    if (_endptr)
	    (*_endptr) = (char*)_string;
    return output;
}

char* strunite(const char* p1, size_t p1l, const char* p2, size_t p2l)
{
    if (!p1l) {
        p1l = strlen(p1);
    }

    if (!p2l) {
        p2l = strlen(p2);
    }

    char* str = khsAlloc(p1l + p2l);
    memcpy(str, p1, p1l);
    memcpy(str + p1l, p2, p2l);

    return str;
}

static inline void reverseString(char* str)
{
    int len;
    int index;
    char *start, *end, temp;

    len = strlen(str);

    start = str;
    end = str + len - 1;

    for (index = 0; index < len / 2; index++) {
        temp = *end;
        *end = *start;
        *start = temp;

        start++;
        end--;
    }
}


char* ulltostr(uint64_t number, char* str, int base)
{
    int i = 0;
    bool neg = false;

    if (number == 0) {
        str[i++] = '0';
        str[i] = '\0';
        return str;
    }

    while (number != 0) {
        int rem = number % base;
        str[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
        number = number / base;
    }

    if (neg)
        str[i++] = '-';
    str[i] = '\0';

    reverseString(str);
    return str;
}

char* lltostr(int64_t number, char* str, int base)
{
    int i = 0;
    bool neg = false;

    if (number < 0) {
        str[i++] = '-';
        number *= -1;
    }

    if (number == 0) {
        str[i++] = '0';
        str[i] = '\0';
        return str;
    }

    while (number != 0) {
        int rem = number % base;
        str[i++] = (rem > 9) ? (rem - 10) + 'a' : rem + '0';
        number = number / base;
    }

    if (neg)
        str[i++] = '-';
    str[i] = '\0';
    reverseString(str);
    return str;
}

int strncat(char* restrict dest, const char* src, size_t length)
{
  size_t off = strlen(dest);
  memcpy(dest + off, src, length);
  return off;
}
