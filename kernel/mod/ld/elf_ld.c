#include "power/error.h"
#include "power/system.h"
#include "power/um.h"
#include <power/paging.h>
#include <power/elf.h>
#include <power/memory/hhdm.h>

#include <power/memory/heap.h>
#include <power/memory/memset.h>
#include <power/memory/pfa.h>

#include <power/memory/vm.h>
#include <power/string.h>
#include <power/vfs.h>

#include <power/mmap.h>
#include <power/paging.h>
#include <power/tty.h>

/* A funny fact: No executable knows which modules it loaded.
   (TODO) */

static struct vector loadedLibraries;
static struct mm_cache* libCache;

static void unloadLibraryPages(struct vm* vm,
                               struct loaded_library* lib);
static inline
void loadRecursiveLibraries(struct elf_executable* exe);

static inline
struct vm* getvmtvm();

static struct elf_program_header*
getDynamicProg(const struct elf_executable* exe)
{
    struct elf_program_header* parr =
        PaAdd(exe->FileHeader, exe->FileHeader->ProgramHeaderTablePosition);
    for (int i = 0; i < exe->FileHeader->ProgramHeaderLength; i++) {
        struct elf_program_header* prog = &parr[i];
        if (prog->SegmentType == ELF_PROGTYPE_DYNAMIC)
            return prog;
    }

    return NULL;
}

static bool wasLoaded(struct vm* vm, struct loaded_library* lib)
{
    for (size_t i = 0; i < lib->LoadedAddresses.Length; i++) {
        struct loaded_address_space* d = lib->LoadedAddresses.Data[i];
        if (d->VirtualMemory == vm)
            return true;
    }

    return false;
}

/* Load library *library* onto address space of *exe*. */
static void mapLibrary(const struct elf_executable* exe,
                       struct loaded_library* library)
{
  struct vm* tvm;
  struct vm* vm;

  vm = getvmtvm();
  tvm = vmGetTemporary();
  
  if (!wasLoaded(exe->VirtualMemory, library)) {
    library->ReferenceCount++;
    for (size_t i = 0; i < library->Pages.Length; i++) {
      struct elf_lpage* pg = library->Pages.Data[i];
      vmMapShared(tvm, exe->VirtualMemory,
		  pg->Address, pg->Flags | VMAP_FIXED | VMAP_USER);
    }

    struct loaded_address_space* las;
    if (vm != tvm)
      vmMapShared(tvm, vm, (uintptr_t)library->File, 0);
    las = modReplicatePages(exe->VirtualMemory, library,
			    exe->Flags | ELF_LOAD_SHARED_LIB);
    
    vectorInsert(&library->LoadedAddresses, las);
  }
}

static inline
struct vm* getvmtvm()
{
  struct process* proc = pcGetCurrentProcess();
  if (unlikely(!proc))
    return vmGetTemporary();

  return proc->VirtualMemory;
}

static void createLibrary(struct elf_executable* exe, const char* name)
{
  size_t pathlen;
  size_t fullpathlen;
  
  struct vm* tvm;
  struct vm* ttvm;
  
  struct fs_file* f;
  struct elf_executable lib;
  struct loaded_library* lload;
  
  uint8_t* buffer;
  char* actualName;
  
  if (!libCache) {
    libCache = khCreateCache("library", sizeof(struct loaded_library));
  }
    
  pathlen = strlen("A:/System/Libraries/");
  fullpathlen = strlen(name) + pathlen;
  actualName = khsAlloc(fullpathlen);

  memcpy(actualName, "A:/System/Libraries/", pathlen);
  memcpy(actualName + pathlen, name, strlen(name));
  actualName[fullpathlen] = '\0';
  
  tvm = getvmtvm();
  ttvm = vmGetTemporary();
  
  f = vfsOpenFile(NULL, actualName, 0);
  buffer = vmVirtualFileMap(ttvm, 0, f->Size, f, VPROT_WRITE);

  /* We don't want to override
     the current proc's lib mappings.  */
  lib = modLoadElfEx(buffer, vmGetTemporary(),
		     ELF_LOAD_USER_ADDRESS_SPACE |
		     ELF_LOAD_SHARED_LIB         |
		     ELF_NO_DYN_RESOLVE);

  if (lib.Result == ELF_LOAD_ERROR_NOT_EXECUTABLE) {
    trmLogfn("failed: not an executable");
    vmVirtualFileUnmap(tvm, buffer, f->Size);
    vfsCloseFile(f);
    return;
  }

  lload = khAlloc(libCache);
  lload->Name = khsAlloc(strlen(name));
  lload->Pages = lib.LoadedPages;
  lload->File = lib.FileHeader;
  lload->FileSize = f->Size;

  lload->Base = lib.Base;
  lload->LoadedAddresses = vectorCreate(1);
  lload->ReferenceCount = 1;
  lload->Flags =
    ELF_LOAD_USER_ADDRESS_SPACE | ELF_LOAD_SHARED_LIB | ELF_NO_DYN_RESOLVE;

  vfsCloseFile(f);
  
  mapLibrary(exe, lload);
  modResolveElfDynamic(&lib);
  lload->Dependencies = lib.LoadedLibraries;

  memcpy(lload->Name, name, strlen(name));
  vectorInsert(&loadedLibraries, lload);
  vectorInsert(&exe->LoadedLibraries, lload);
  trmLogfn("loaded library %s at %p", name, lload->Base);

  if (exe->VirtualMemory != vmGetTemporary()) {
    loadRecursiveLibraries(exe);
  }
  
  khsFree(actualName);
}

void loadRecursiveLibraries(struct elf_executable* exe)
{
  unsigned long i;
  struct elf_executable elx;
  struct loaded_library* ll;

  for (i = 0; i < exe->LoadedLibraries.Length; i++) {
    ll = exe->LoadedLibraries.Data[i];
    modLibraryToExecutable(ll, exe->VirtualMemory, &elx);
    elfRelocateRelative(&elx, ".rela.dyn");
	    
    if (exe->Flags & ELF_LOAD_USER_ADDRESS_SPACE) {
      elfRelocateAllLibraries(&elx, ".rela.dyn");
      elfRelocateAllLibraries(&elx, ".rela.plt");
    }
  }
}

static void loadLibrary(struct elf_executable* exe, const char* name)
{
    if (!strncmp(name, "lib__r-kernelapi.so", strlen(name))
        && !(exe->Flags & ELF_LOAD_USER_ADDRESS_SPACE)) {
        return;
    }

    for (size_t i = 0; i < loadedLibraries.Length; i++) {
        struct loaded_library* ll = loadedLibraries.Data[i];
        if (!strncmp(name, ll->Name, strlen(name))) {
            mapLibrary(exe, ll);

            struct elf_executable lex;
            modLibraryToExecutable(ll, exe->VirtualMemory, &lex);
            elfRelocateRelative(&lex, ".rela.dyn");
	    
            if (exe->Flags & ELF_LOAD_USER_ADDRESS_SPACE) {
                elfRelocateAllLibraries(&lex, ".rela.dyn");
                elfRelocateAllLibraries(&lex, ".rela.plt");
            }

            vectorInsert(&exe->LoadedLibraries, ll);
            return;
        }
    }

    createLibrary(exe, name);
}

static void fixSymbols(const struct elf_executable* exec,
                       struct elf_rela_item* item)
{
    if ((exec->Flags & ELF_LOAD_USER_ADDRESS_SPACE))
        return;

    const struct elf_section_header* sec = modGetSection(exec, ".rela.plt");
    const struct elf_section_header* dynstrsec = modGetSection(exec, ".dynstr");
    const struct elf_section_header* dynsymsec = modGetSection(exec, ".dynsym");
    const struct elf_symtab_item* dynsym =
        PaAdd(exec->FileHeader, dynsymsec->FileOffset);
    const char* dynstr = PaAdd(exec->FileHeader, dynstrsec->FileOffset) + 1;

    void* symbol;
    int length = sec->SectionSize / sec->TableEntrySize;

    const struct elf_symtab_item* symt;
    const struct elf_rela_item* ii;

    for (int i = 0; i < length; i++) {
        ii = &item[i];
        uint64_t sym = ELF64_R_SYM(ii->Info);

        symt = &dynsym[sym];
        if (!(exec->Flags & ELF_LOAD_USER_ADDRESS_SPACE)) {
            symbol = modGetSymbolElf(NULL, dynstr + symt->NameOffset - 1);
            void* ptr = pgGetPhysicalAddress(exec->AddressSpace,
                                             exec->Base + ii->Offset);
            ptr = mmMakeWritable(ptr);
            *(uint64_t*)(ptr) = (uint64_t)symbol;
        }
    }
}

const struct elf_section_header* modGetSection(const struct elf_executable* exe,
                                               const char* name)
{
    const struct elf_file_header* header = exe->FileHeader;
    const struct elf_section_header* secs;

    secs = PaAdd(header, header->SectionHeaderTablePosition);
    const struct elf_section_header* str = &secs[header->NamesSectionIndex];
    const char* shstrtab =
        (const char*)(PaAdd(exe->FileHeader, str->FileOffset));

    size_t nl = strlen(name);
    for (int i = 0; i < header->ProgramSectionLength; i++) {
        const struct elf_section_header* s = &secs[i];
        if (!strncmp(shstrtab + s->NameOffset, name, nl)) {
            return s;
        }
    }

    return NULL;
}

static inline
void mapSingleLibraryFile(const struct elf_executable* exe,
			  struct loaded_library* lib)
{
  struct vm* tvm;
  tvm = vmGetTemporary();
  
  vmMapShared(tvm, getvmtvm(),
	      (uintptr_t)lib->File, 0);
}

static
int mapLibraryFile(const struct elf_executable* exe)
{
  char* c;
  const char* strtab;
  
  struct loaded_library* lib;
  struct elf_program_header* dyn;
  struct elf_dynamic_item* darr;
  const struct elf_section_header* dynstr;
  
  dyn = getDynamicProg(exe);
  darr = (struct elf_dynamic_item*)(PaAdd(exe->FileHeader, dyn->DataOffset));
  
  dynstr = modGetSection(exe, ".dynstr");
  if (!dynstr)
    return -ERROR_INVALID_EXECUTABLE;
  
  strtab = (void*)exe->FileHeader + dynstr->FileOffset;
  
  while(darr->Type != 0x0) {
    switch (darr->Type) {
    case 1:
      for (size_t i = 0; i < loadedLibraries.Length; i++) {
	lib = loadedLibraries.Data[i];
	c = (void*)(((uintptr_t)strtab + darr->Value) & 0xFFFFFFFF);

	if (!strncmp(c, lib->Name, strlen(c))) {
	  mapSingleLibraryFile(exe, lib);
	}
      }
      break;
    }

    darr++;
  }

  return 0;
}

void modResolveElfDynamic(const struct elf_executable* exe)
{
    static bool createdVector = false;
    if (!createdVector) {
        loadedLibraries = vectorCreate(2);
        createdVector = true;
    }

    const char* strtab;
    const struct elf_section_header* strt;
    const struct elf_section_header* got;
    const struct elf_section_header* relaplt;
    
    struct elf_program_header* dyn;
    struct elf_dynamic_item* darr;

    int error;
    if ((error = mapLibraryFile(exe)) < 0)
      return;
    
    strt = modGetSection(exe, ".dynstr");
    strtab = (char*)(exe->Base + strt->Address);

    got = modGetSection(exe, ".got.plt");
    if (!got) /* Its a statically linked exec! */
        return;

    dyn = getDynamicProg(exe);
    relaplt = modGetSection(exe, ".rela.plt");
    
    fixSymbols(exe, (struct elf_rela_item*)(exe->Base + relaplt->Address));
    darr = (struct elf_dynamic_item*)(PaAdd(exe->FileHeader, dyn->DataOffset));

    if (exe->Flags & ELF_LOAD_USER_ADDRESS_SPACE)
        strtab = PaAdd(exe->FileHeader, strt->FileOffset);

    while (darr->Type != 0x0) {
        switch (darr->Type) {
        case 0x1: /* NEEDED */
            loadLibrary((struct elf_executable*)exe,
                        strtab + (darr->Value & 0xFFFFFFFF));
            break;
        }
        darr++;
    }

    elfRelocateRelative(exe, ".rela.dyn");
    if (exe->Flags & ELF_LOAD_USER_ADDRESS_SPACE) {
        elfRelocateAllLibraries(exe, ".rela.dyn");
        elfRelocateAllLibraries(exe, ".rela.plt");
    }
}

void modUnloadProgram(const struct elf_executable* exec)
{
    struct elf_lpage* page;
    for (size_t i = 0; i < exec->LoadedPages.Length; i++) {
        page = exec->LoadedPages.Data[i];
	vmVirtualFileUnmap(exec->VirtualMemory,
			   (void*)page->Address, page->Length);
    }

    struct loaded_address_space* space;
    struct loaded_library* lib;
    for (size_t i = 0; i < loadedLibraries.Length; i++) {
        lib = loadedLibraries.Data[i];

        for (size_t w = 0; w < lib->LoadedAddresses.Length; w++) {
            space = lib->LoadedAddresses.Data[w];
            if (vmCompareVm(space->VirtualMemory, exec->VirtualMemory))
                modUnloadLibrary(exec, lib);
        }
    }
}

void modUnloadLibrary(const struct elf_executable* exec,
                      struct loaded_library* lib)
{
  lib->ReferenceCount--;

  struct vm* tvm;
  tvm = vmGetTemporary();
  if (lib->ReferenceCount > 0) {
    vmVirtualFileUnmap(exec->VirtualMemory, lib->File, lib->FileSize);
    unloadLibraryPages(exec->VirtualMemory, lib);
    return;
  }

  khsFree(lib->Name);
  vmVirtualFileUnmap(tvm, lib->File, lib->FileSize);
}

void unloadLibraryPages(struct vm* vm, struct loaded_library* lib)
{
  struct elf_lpage* lp;
  struct loaded_address_space* adrs;
  for (size_t i = 0; i < lib->LoadedAddresses.Length; i++) {
    adrs = lib->LoadedAddresses.Data[i];
    
    if (vmCompareVm(adrs->VirtualMemory, vm)) {
      for (size_t j = 0; j < adrs->LoadedReplicatedPages.Length; j++) {
	lp = adrs->LoadedReplicatedPages.Data[j];
	vmVirtualFileUnmap(vm, (void*)lp->Address, lp->Length);
      }
    }
  }
}

/* I don't know why these functions take an elf_executable,
   just to use the FileHeader. */
void modResolveElfDynamicLibrary(const struct loaded_library* lib)
{
    struct elf_executable exec;
    exec.FileHeader = lib->File;
    exec.Flags = lib->Flags;
    exec.AddressSpace = NULL;
    modResolveElfDynamic(&exec);
}
