#include <power/mmap.h>
#include "power/memory/vm.h"
#include "power/system.h"
#include <power/elf.h>
#include <power/paging.h>
#include <power/memory/vaddress.h>

#include <power/memory/heap.h>
#include <power/memory/pfa.h>
#include <power/memory/memset.h>

#include <power/driver.h>
#include <power/vector.h>

static bool isPic(const struct elf_file_header* he)
{
    bool pic = he->FileType == ELF_FTYPE_SHARED;

    struct elf_program_header* ent;
    struct elf_program_header* ph = PaAdd(he, he->ProgramHeaderTablePosition);
    for (int i = 0; i < he->ProgramHeaderLength; i++) {
        ent = &ph[i];
        if (ent->SegmentType == ELF_PROGTYPE_INTERPRETER) {
            pic = he->FileType == ELF_FTYPE_SHARED;
            break;
        }
    }

    return pic;
}

static uint64_t getExecutableBase(struct address_space_mgr* mgr,
                                  const struct elf_file_header* he, int flags)
{
    if (isPic(he)) {
        int pages = 0;
        struct elf_program_header* arr = (struct elf_program_header*)PaAdd(
            he, he->ProgramHeaderTablePosition);
        for (int i = 0; i < he->ProgramHeaderLength; i++) {
            struct elf_program_header* p = &arr[i];
            if (p->SegmentType == ELF_PROGTYPE_DYNAMIC
                || p->SegmentType == ELF_PROGTYPE_LOAD) {
                uint64_t t = (alignUp(p->MemorySize, 4096)) / 4096;
                pages += t;
            }
        }

        uint64_t base =
            (flags & ELF_LOAD_USER_ADDRESS_SPACE) ? UME_BASE : EXEC_BASE;
        uint64_t pg = asmgrCorrectPage(mgr, base);
        if (pg != base) {
            pg += (10 * 4096);
        }

        while (!asmgrClaimPage(mgr, pg, pages)) {
            pg += (10 * 4096);
        }

        return pg;
    }

    return 0;
}

struct elf_executable modLoadElf(const uint8_t* file, int flags)
{
    return modLoadElfEx(file, NULL, flags);
}

struct elf_executable modLoadElfEx(const uint8_t* file,
                                   struct vm* vm, int flags)
{
    const struct elf_file_header* he = (const struct elf_file_header*)(file);
    struct elf_executable result;

    if (!he || he->Magic[0] != 0x7F || he->Magic[1] != 'E'
        || he->Magic[2] != 'L' || he->Magic[3] != 'F') {
        result.Result = ELF_LOAD_ERROR_NOT_EXECUTABLE;
        return result;
    }

    if (he->Architecture != 2 || he->Endianness != 1
        || he->InstructionSet != 0x3E) {
        result.Result = ELF_LOAD_ERROR_WRONG_ARCHITECTURE;
        return result;
    }

    result.LoadedPages = vectorCreate(he->ProgramHeaderLength);

    struct elf_program_header* ent;
    struct elf_program_header* ph;
    ph = PaAdd(file, he->ProgramHeaderTablePosition);

    void *program;
    uint64_t pflags;
    
    struct address_space_mgr* mgr = addrGetManagerForCr3(vm->AddressSpace);
    uint64_t base = getExecutableBase(mgr, he, flags);

    int minoroffset, pages;
    bool pic = isPic(he);

    if (flags & ELF_LOAD_USER_ADDRESS_SPACE && !(flags & ELF_LOAD_SHARED_LIB)) {
      modMapDrivers(vm);
    }

    uintptr_t vaddr;
    for (int i = 0; i < he->ProgramHeaderLength; i++) {
        ent = &ph[i];

        if (ent->SegmentType != ELF_PROGTYPE_LOAD)
            continue;

        if ((ent->Flags & ELF_PROGFLAGS_WRITE) && (flags & ELF_LOAD_SHARED_LIB))
            continue;

	pflags = VMAP_ANONYMOUS | VMAP_FIXED | VMAP_POPULATE;
        pflags |= (flags & ELF_LOAD_USER_ADDRESS_SPACE) ? VMAP_USER : 0;

	/* W^X is enforced by VirtualFileMap. */
        if (ent->Flags & ELF_PROGFLAGS_EXEC)
            pflags |= VPROT_EXECUTE;
        if (ent->Flags & ELF_PROGFLAGS_WRITE)
            pflags |= VPROT_WRITE;
	
        minoroffset = (ent->VirtualAddress & 0xFFF);
	vaddr = base + ent->VirtualAddress & ~(ent->Alignment - 1);
	program = vmVirtualFileMap(vm, vaddr,
				   ent->MemorySize + minoroffset, NULL, pflags);
        vmWrite(vm, (uintptr_t)program + minoroffset,
		(char*)file + ent->DataOffset, ent->FileSize);

        pages = ent->MemorySize + minoroffset;
        pages += 4096 - pages % 4096;
        pages /= 4096;

        if (!pic)
            asmgrClaimPage(mgr, ent->VirtualAddress, pages);

	struct elf_lpage* lp;

	/* I should add a clause for handling
	   OOM but then I would have to revert everything. */
	lp = khsAlloc(sizeof(*lp));
	lp->Address = (uintptr_t)program;
	lp->Length = ent->MemorySize + minoroffset;
	lp->Flags = pflags;
	vectorInsert(&result.LoadedPages, lp);
    }

    result.FileHeader = (struct elf_file_header*)he;
    result.Base = base;
    result.EntryPoint = base + (void*)he->EntryPointAddress;
    result.AddressSpace = vm->AddressSpace;
    result.VirtualMemory = vm;
    result.Flags = flags;
    result.LoadedLibraries = vectorCreate(2);
    result.Result = ELF_LOAD_ERROR_OK;
    
    if (!(flags & ELF_NO_DYN_RESOLVE))
        modResolveElfDynamic(&result);
    return result;
}

struct loaded_address_space* modReplicatePages(struct vm* vm,
                                               struct loaded_library* exec,
                                               int flags)
{
  if (unlikely(!(flags & ELF_LOAD_SHARED_LIB))) {
    return NULL;
  }

  const struct elf_file_header* he = exec->File;
  struct loaded_address_space* adrspc;
  adrspc = khsAlloc(sizeof(*adrspc));
  if (!adrspc)
    return NULL;
  
  adrspc->VirtualMemory = vm;
  adrspc->LoadedReplicatedPages = vectorCreate(3);
  
  int minoroffset;
  void *program = NULL;
  uint64_t pages, vaddr;

  struct elf_lpage* lp;
  struct elf_program_header *ph, *ent, *nee;
  ph = PaAdd(exec->File, he->ProgramHeaderTablePosition);

  uint64_t base = exec->Base;
  uint64_t pflags = VMAP_ANONYMOUS | VMAP_FIXED | VPROT_WRITE;
  if (flags & ELF_LOAD_USER_ADDRESS_SPACE) {
    pflags |= VMAP_USER;
  }

  size_t next;
  for (int i = 0; i < he->ProgramHeaderLength; i++) {
    ent = &ph[i];
    if (!(ent->Flags & ELF_PROGFLAGS_WRITE) || ent->Alignment != 0x1000) {
      continue;
    }
    
    next = 0;
    nee = &ph[i + 1];
    if (i + 1 < he->ProgramHeaderLength && nee->Alignment != 0x1000) {
      next = nee->MemorySize;
    }

    minoroffset = ent->VirtualAddress & 0xFFF;
    program = vmVirtualFileMap(vm, base + ent->VirtualAddress & ~0xFFF,
			       ent->MemorySize + minoroffset + next,
			       NULL, pflags);

    vaddr = (base + ent->VirtualAddress) & ~0xFFF;
    vmWrite(vm, (uintptr_t)program + minoroffset, (void*)he + ent->DataOffset,
	    ent->FileSize);

    if (next) {
      uint64_t aligned =
	alignUp(((uint64_t)program + minoroffset + ent->MemorySize),
		nee->Alignment);
      vmWrite(vm, aligned, (void*)he + nee->DataOffset, next);
    }
    
    pages = ent->MemorySize + minoroffset;
    pages += 4096 - pages % 4096;
    pages /= 4096;
    
    vaddr = base + ent->VirtualAddress;
      
    lp = khsAlloc(sizeof(*lp));
    lp->Address = vaddr;
    lp->Flags = pflags;
    lp->Length = ent->MemorySize + minoroffset;
      
    vectorInsert(&adrspc->LoadedReplicatedPages, lp);
  }

  return adrspc;
}

void modUnloadElfPages(struct elf_executable* exec)
{
    struct loaded_page* pgo;
    struct vector* pages = &exec->LoadedPages;
    while (pages->Length) {
        pgo = pages->Data[0];

        pgUnmapPage(exec->AddressSpace, pgo->VirtualAddress);
        if (pgo->Length)
            mmAlignedFree((void*)pgo->PhysicalAddress, pgo->Length);

        vectorRemove(pages, pgo);
        khsFree(pgo);
    }
}
