#include "power/memory/vm.h"
#include <power/driver.h>

#include <power/paging.h>
#include <power/ldr_requests.h>
#include <power/memory/heap.h>
#include <power/memory/memset.h>

#include <power/elf.h>
#include <power/ustar.h>

#include <power/pci.h>
#include <power/tty.h>
#include <power/string.h>
#include <power/vector.h>

struct vector loadedDrivers;

static struct pci_device* hasDevicePci(struct driver_cld_pci* conds)
{
    struct pci_search_query query;
    query.Class = conds->Class;
    query.SubClass = conds->Subclass;
    query.DeviceId = conds->DeviceId;
    query.Vendor = conds->VendorId;
    query.ProgIf = conds->ProgIf;

    return pciSearchDeviceEx(query);
}

static struct pci_device*
shouldLoadDriver(struct driver_conditional_loading* cld,
                 struct elf_executable* exe)
{
    if (!cld)
        return (void*)0x1; /* Only an issue if the driver
                             actually tries do deref that */

    int index = 0;
    bool lastiHad = false;
    struct driver_conditional_loading i;
    struct pci_device* dev = NULL;

    do {
        i = cld[index];
        if (i.RelationshipWithPrevious == DRIVER_CLD_RELATIONSHIP_AND
            && !lastiHad) {
            return NULL;
        }

        switch (i.ConditionalType) {
        case DRIVER_CLD_TYPE_PCI:
            lastiHad = (dev = hasDevicePci(&i.Pci)) != NULL;
            break;
        case DRIVER_CLD_TYPE_CUSTOM_FUNC:
            lastiHad = i.shouldLoad();
            dev = (void*)0x1;
            break;
        }
        index++;
    } while (i.HasNext);

    return dev;
}

struct kdriver_manager* modLoadDriver(const char* modName)
{
    return modLoadDriverEx(modName, true);
}

struct kdriver_manager* modLoadDriverEx(const char* modName, bool insideAuxfs)
{
    static bool createdVector = false;
    if (!createdVector) {
        loadedDrivers = vectorCreate(3);
        createdVector = true;
    }

    if (!insideAuxfs) {
        trmLogfn("Loading modules outside auxfs is"
                 "currently not supported. Sorry!");
        return NULL;
    }

    const uint8_t* buffer;
    struct limine_module_response* m = rqGetModuleResponse();
    struct limine_file* f = m->modules[0];

    modLoadUstarFile(f->address, modName, &buffer);
    
    struct vm* tvm = vmGetTemporary();
    struct elf_executable exec = modLoadElfEx(buffer, tvm, ELF_LOAD_DRIVER);
    struct driver_info* info = ((driver_query_pfn_t)(exec.EntryPoint))();
    trmLogfn("[DRIVER] loaded driver %s at %p", modName, exec.Base);

    struct pci_device* dev;
    void* cld = info->ConditionalLoading;
    if (!(dev = shouldLoadDriver(cld, &exec))) {
        trmLogfn("(for driver %s) Conditional loading failed", info->Name);
        goto euload;
    }

    struct kdriver_manager* man = modCreateDriverManager(info, dev);
    if (!info->EntryPoint) {
        trmLogfn("Could not load driver %s: entry point is null");
        goto uload;
    }

    info->EntryPoint(man);

    struct driver_internal_data* id = khsAlloc(sizeof(*id));
    id->Driver = man;
    id->Pages = exec.LoadedPages;
    vectorInsert(&loadedDrivers, id);

    return man;

uload:
    khsFree(man);

euload:
    /* Destroy the loaded ELF because
       CLD failed. We didn't create kdriver so we can
       just clear out the exec. */
    
    modUnloadElfPages(&exec);
    return NULL;
}

void modMapDrivers(struct vm* vm)
{
  struct vm* tvm;
  tvm = vmGetTemporary();
  
  for (size_t i = 0; i < loadedDrivers.Length; i++) {
    struct driver_internal_data* id = loadedDrivers.Data[i];
    for (size_t ix = 0; ix < id->Pages.Length; ix++) {
      struct elf_lpage* lp = id->Pages.Data[ix];
      vmMapShared(tvm, vm, lp->Address, lp->Flags);
    }
  }
}

struct kdriver_manager* modGetDriver(const char* modName)
{
    for (size_t i = 0; i < loadedDrivers.Length; i++) {
        struct driver_internal_data* id = loadedDrivers.Data[i];
        if (!strcmp(modName, id->Driver->Info->Name)) {
            return id->Driver;
        }
    }

    return NULL;
}

struct kdriver_manager* modGetFilesystemName(const char* fsName)
{
    for (size_t i = 0; i < loadedDrivers.Length; i++) {
        struct driver_internal_data* id = loadedDrivers.Data[i];
        if (id->Driver->Info->Role != DRIVER_ROLE_FILESYSTEM)
            continue;

        struct driver_fs_interface* in = id->Driver->Info->Interface;
        if (!strcmp(fsName, in->Name)) {
            return id->Driver;
        }
    }

    return NULL;
}

/* For drivers. */
struct vector* modGetDrivers()
{
    return &loadedDrivers;
}

struct kdriver_manager* modCreateDriverManager(struct driver_info* info, 
                                               struct pci_device* device)
{
    __unused(info);
    
    struct kdriver_manager* man = khsAlloc(sizeof(*man));
    man->LoadReason = device;
    man->Info = info;
    return man;
}

