#include <power/driver.h>
#include <power/ldr_requests.h>
#include <power/memory/heap.h>

#include <power/memory/memset.h>
#include <power/ustar.h>
#include <power/tty.h>

void modAutomaticLoad()
{
    char* temp = khsAlloc(32);
    const uint8_t* buffer;
    struct limine_module_response* m = rqGetModuleResponse();
    struct limine_file* f = m->modules[0];

    modLoadUstarFile(f->address, "driver.autoload", &buffer);
    const char* ptr = (const char*)buffer;
    const char* begin = ptr;
    while(true) {
        if (*ptr == '\n'
         || *ptr == '\0') {
            memset(temp, 0, 32);
            if (ptr - begin > 32) {
                trmLogfn("ignoring big filename inside driver.autoload");
                begin = ptr + 1;
                if (!*ptr)
                    break;
                continue;
            }

            memcpy(temp, begin, ptr - begin);
            if (*temp) 
                modLoadDriver(temp);
            begin = ptr + 1;
            if (!*ptr)
                break;
        }

        ptr++;
    }
    
    khsFree(temp);
}
