#include "power/error.h"
#include <power/config.h>
#include <power/device.h>
#include <power/memory/vm.h>

#include <power/framebuffer.h>
#include <power/um.h>
#include <power/memory/user_buffer.h>
#include <power/mmap.h>

#include <power/ldr_requests.h>

#define FB_ADDRESS 0xF8E7D600000

static int fbmmap(struct handle*, int action, uintptr_t* hint,
                  unsigned long length, int flags);

static unsigned long 
fbioctl(struct handle*, unsigned long command, void* buffer,
        size_t bufferLength);

static struct handle_ops ops = { 
    .mmap = fbmmap,
    .ioctl = fbioctl
};

/* Create initial fbXX devices. */
void fbCreate()
{
    size_t i;
    struct limine_framebuffer* lfb;
    struct limine_framebuffer_response* lfr;
    lfr = rqGetFramebufferRequest();

    major_t* maj = devnRegisterNode("fb", &ops, DEVN_MANYDEV_BIT);
    for (i = 0; i < lfr->framebuffer_count; i++) {
        lfb = lfr->framebuffers[i];
        devAllocateNode(maj, lfb);
    }
}

/* Map the framebuffer onto a process' address space. */
int fbmmap(struct handle* ha, int action, uintptr_t* hint, unsigned long length,
           int flags)
{
    __unused(action);
    __unused(length);
    __unused(flags);
    
    /* Should I use this or create an field in struct handle */
    struct process* proc = pcGetCurrentProcess();
    struct limine_framebuffer* lfb = ha->Resource;

    if (action == MMAP_UNMAP) {
        return -ERROR_INVALID_ARGUMENT;
    }

    int error;
    uintptr_t add = FB_ADDRESS;
    error = vmEstablishMapping(proc->VirtualMemory, &add, lfb->address,
                               lfb->pitch * lfb->height,
                               VMAP_USER | VPROT_WRITE | VPROT_CACHE);
    
    if (error) {
        return error;
    }
    
    (*hint) = add;
    return 0;
}

/* Retrieve information about the framebuffer. */
static void
fbgettopology(struct limine_framebuffer* lb,
              struct video_framebuffer* fb)
{
    fb->BitsPerPixel = lb->bpp;
    fb->Height = lb->height;
    fb->Width = lb->width;
    fb->Pitch = lb->pitch;
}

unsigned long 
fbioctl(struct handle* ha, unsigned long command, void* buffer,
        size_t bufferLength)
{
    switch (command) {
    case FB_GET_TOPOLOGY:
        if (bufferLength != sizeof(struct video_framebuffer)) {
            return -ERROR_INVALID_RANGE;
        }

        if (mmUserBufferCheck(buffer, bufferLength, UBUFFER_WRITE_BIT)) {
            return -ERROR_USER_FAULT;
        }

        fbgettopology(ha->Resource, buffer);
        return 0;
    }

    return -ERROR_NO_SYSCALL;
}
