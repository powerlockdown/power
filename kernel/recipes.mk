KERNEL_OUTPUT=build/kernel/oskrnl.elf
KERNEL_BUILD=build/kernel/

-include kernel/arch/i386/sources.mk

KERNEL_ABSTRACT_CFILES=abstract/interrupt_ctl.c abstract/timer.c

KERNEL_MEMORY_CFILES=memory/physical.c memory/address.c memory/kernel_heap.c \
					 memory/vm.c memory/pool.c memory/zoned.c memory/vm/vm_boguspage.c \
					 memory/vm/vm_estblsh.c memory/vm/vm_mmap.c memory/vm/vm_fault.c memory/vm/vm_file.c memory/vm/vm_tmpvm.c memory/vm/vm_ops.c

KERNEL_PARTITION_CFILES=partition/partition.c partition/msdos.c partition/efi.c

KERNEL_MOD_CFILES=mod/ustar.c mod/elf.c mod/driver/loader.c \
				  mod/ld/elf_ld.c mod/elf_sym.c mod/driver/autoloader.c mod/ld/elf_hashsym.c \
				  mod/ld/elf_reloc.c

KERNEL_SCHEDULER_CFILES=scheduler/scheduler.c scheduler/thread.c scheduler/synchronization.c \
						scheduler/worker.c

KERNEL_TERM_CFILES=term/toshibatxl1.c term/terminal.c term/panic.c term/pty.c term/pty_tty.c
KERNEL_UM_CFILES=um/process.c um/syscall.c um/ioctl.c um/input.c um/handle.c \
				 um/signal.c
KERNEL_UTILS_CFILES=utils/string.c utils/vector.c utils/cmd.c utils/avl.c
KERNEL_PCI_CFILES=pci/pci.c pci/pci_irq.c

KERNEL_CFILES=main.c bootloader_requests.c acpi.c device.c clock.c \
			  fb.c vfs/path.c vfs/vfs.c page_fault.c  \
			$(KERNEL_TERM_CFILES) $(KERNEL_UTILS_CFILES) \
			$(KERNEL_MEMORY_CFILES) $(KERNEL_ARCH_CFILES) $(KERNEL_ABSTRACT_CFILES) \
			$(KERNEL_SCHEDULER_CFILES) $(KERNEL_UM_CFILES) $(KERNEL_MOD_CFILES) \
			$(KERNEL_PARTITION_CFILES) $(KERNEL_PCI_CFILES)

KERNEL_ASFILES=$(KERNEL_ARCH_ASFILES)

KERNEL_ASSOURCES:=$(addprefix $(KERNEL_BUILD)/,$(addsuffix .o,$(KERNEL_ASFILES)))
KERNEL_CSOURCES:=$(addprefix $(KERNEL_BUILD)/,$(addsuffix .o,$(KERNEL_CFILES)))
KERNEL_FILES=$(KERNEL_CFILES) $(KERNEL_ASFILES)
KERNEL_SOURCES=$(addprefix $(KERNEL_BUILD)/,$(addsuffix .o,$(KERNEL_FILES)))
KERNEL_OBJS:=$(addprefix $(KERNEL_BUILD)/,$(addsuffix .o,$(KERNEL_FILES)))

KERNEL_CFLAGS=$(CFLAGS) -Ilimine -Ikernel -mcmodel=kernel -fno-pic -fno-pie \
			  -I$(KERNEL_INCLUDE_DIR) -Ikernel/include -I$(KERNARCHINCDIR)
KERNEL_LDFLAGS=-nostdlib -static -Wl,-m -Wl,elf_x86_64 -Wl,-z -Wl,max-page-size=0x1000 \
			   -Wl,-T -Wl,kernel/linker.ld

KERNEL_DFILES=$(subst .c.o,.c.d,$(KERNEL_CSOURCES))
KERNSRCDIR=kernel

# GMake considers makedep files as
# intermideate files and delete them
# during the end of all tasks. 
.PRECIOUS: $(KERNEL_DFILES)

KERNEL_STUB=$(KERNEL_BUILD)/lib__r-kernelapi.so

$(KERNEL_STUB): $(KERNEL_OUTPUT)
	python tools/stub.py $(KERNEL_OUTPUT) $(KERNEL_BUILD)/

$(KERNEL_OUTPUT): $(KERNEL_OBJS) kernel/linker.ld
	$(CC) $(LDFLAGS) $(KERNEL_LDFLAGS) $(KERNEL_OBJS) -o $@

$(KERNEL_BUILD)/%.c.d: kernel/%.c
	@mkdir -p build/$(dir $(subst source/,,$^))	
	@$(CC) $(KERNEL_CFLAGS)  -MT '$(patsubst $(KERNSRCDIR)/%.c,build/kernel/%.c.o,$<)' $< -MF $@  -MMD -E > /dev/null

$(KERNEL_BUILD)/%.c.o: $(KERNSRCDIR)/%.c build/kernel/%.c.d
	@echo CC $<
	@$(CC) $(KERNEL_CFLAGS) -c $< -o $@

$(KERNEL_BUILD)/%.s.o: $(KERNSRCDIR)/%.s
	echo AS $^
	@mkdir -p build/$(dir $(subst source/,,$^))
	@$(AS) $(ASFLAGS) $^ -o $@

include kernel/uheaders.mk
include $(KERNEL_DFILES)

