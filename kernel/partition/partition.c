#include "power/device.h"
#include "power/error.h"
#include "power/memory/user_buffer.h"
#include "power/string.h"
#include <power/partition.h>

#include <power/driver.h>
#include <power/memory/heap.h>

// static struct vector partitions;
static struct vector disks;

static int partRead(struct handle* handle, void* buffer, int length)
{
    struct partition* pt = handle->Resource;

    int sector = pt->Device->SectorSize;
    int sct, i = 0;
    int actwr = 0, error = 0;

    if (mmUserBufferCheck(buffer, length, UBUFFER_WRITE_BIT)) {
        return -ERROR_USER_FAULT;
    }

    while (length > 0) {
        sct = __min(sector, length);

        if ((error = pt->Device->readSector(pt->Device, pt->BeginLba + i, 
                               sct, buffer + (i * sector))) < 0) {
            if (error == -ERROR_TOO_BIG) {
                break;
            }

            return error;
        }

        length -= sct;
        actwr += sct;
        i++;
    }

    return actwr;
}

static struct handle_ops partops = {
    .read = partRead,
};

void partInit(struct driver_disk_interface* di)
{
    size_t length = 0;
    size_t max = di->getDeviceCount(di);

    while (true) {
        if (length >= max)
            break;

        partInitDevice(di, length);
        length++;
    }
}

void partInitAllDevices()
{
    struct driver_disk_interface* d = NULL;
    struct driver_internal_data* did;
    struct vector* mods = modGetDrivers();

    for (size_t i = 0; i < mods->Length; i++) {
        did = mods->Data[i];
        if (did->Driver->Info->Role == DRIVER_ROLE_DISK) {
            d = did->Driver->Info->Interface;
        } else if (did->Driver->Info->SupplementaryRole == DRIVER_ROLE_DISK) {
            d = did->Driver->Info->SupplementaryInterface;
        }

        if (d)
            partInit(d);
        d = NULL;
    }
}

int partInitDevice(struct driver_disk_interface* interf, int idx)
{
    struct pt_disk* disk;
    major_t* major = interf->Major;

    struct driver_disk_device_interface* din;
    disk = khsAlloc(sizeof(*disk));
    if (!disk) {
        return -ERROR_NO_MEMORY;
    }

    din = interf->getDevice(interf, idx);
    if (!din) {
        goto fail;
    }

    int minor = 0;
    if (major)
        minor = devAllocateNode(major, disk);

    {
        char minstr[11] = {0};
        ulltostr(minor, minstr, 10);

        char* dm = strunite(interf->Major->Name, 0, minstr, 0);
        char* unt = strunite(dm, 0, "p", 1);
        disk->Major = devnRegisterNode(unt, &partops, DEVN_MANYDEV_BIT);
        
        khsFree(dm);
        khsFree(unt);
    }

    disk->Partitions = vectorCreate(4);
    disk->DeviceIndex = idx;
    disk->Ops = din;

    msdosGetPartitions(din, disk);
    vectorInsert(&disks, disk);
    
    return 0;
fail:
    khsFree(disk);
    return -ERROR_NO_DEVICE;
}

struct partition* partCreatePartition(lba_t begin, lba_t end, 
                                      major_t* maj)
{
    struct partition* p = khsAlloc(sizeof(*p));

    p->BeginLba = begin;
    p->EndLba = end;
    devAllocateNode(maj, p);

    return p;
}

struct vector* partGetDisks()
{
    return &disks;
}
