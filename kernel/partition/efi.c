#include <power/partition.h>

#include <power/ioctl.h>
#include <power/driver.h>
#include <power/memory/heap.h>

#include <power/string.h>
#include <power/tty.h>
#include <power/memory/memset.h>

static bool uuidNull(const uuid_t* u)
{
    /* TODO: refactor this */
    return u->Raw[0] == 0 && u->Raw[1] == 0 && u->Raw[2] == 0 && u->Raw[3] == 0
        && u->Raw[4] == 0 && u->Raw[5] == 0 && u->Raw[6] == 0 && u->Raw[7] == 0;
}

bool uuidCompare(union ioctl_uuid* lhs, union ioctl_uuid* rhs)
{
    return lhs->Data1 == rhs->Data1 && lhs->Data2 == rhs->Data2
        && lhs->Data3 == rhs->Data3 && lhs->Data4[0] == rhs->Data4[0]
        && lhs->Data4[1] == rhs->Data4[1] && lhs->Data5[0] == rhs->Data5[0]
        && lhs->Data5[1] == rhs->Data5[1] && lhs->Data5[2] == rhs->Data5[2]
        && lhs->Data5[3] == rhs->Data5[3] && lhs->Data5[4] == rhs->Data5[4]
        && lhs->Data5[5] == rhs->Data5[5];
}

/* convert a 512-byte LBA to a secsz-byte LBA  */
static lba_t get512Lba(unsigned int secsz, uint64_t lba)
{
    return (512 * lba) / secsz;
}

static 
inline lba_t getLba(unsigned int secsz, lba_t lba, int flags)
{
    if (flags & PARTITION_BROKEN_SECTOR_SIZE_BIT) {
        return get512Lba(secsz, lba);
    }

    return lba;
}

void efiGetPartitions(struct driver_disk_device_interface* interf,
                      uint8_t* restrict scratch, struct pt_disk* disk, 
                      int flags)
{
    size_t sector = interf->SectorSize;

    lba_t begin, end;
    lba_t lba = getLba(sector, 1, flags);
    if (interf->readSector(interf, lba, sector, scratch) < 0)
        return;

    int addend = (flags & PARTITION_BROKEN_SECTOR_SIZE_BIT) ? (sector > 512 ? 512 : 0) : 0;
    const struct gpt_header gh = *(struct gpt_header*)(scratch + addend);
    if (strncmp(gh.Signature, "EFI PART", 8)) {
        trmLogfn("handleGptPartition: Not a GPT disk");
        return;
    }

    size_t currentSize = sector, currentIndex = 0;
    lba_t currentLba = getLba(sector, 2, flags);
    addend = (flags & PARTITION_BROKEN_SECTOR_SIZE_BIT) ? (sector > 512 ? 1024 : 0) : 0;

    const struct gpt_partition* pat = (struct gpt_partition*)(scratch + addend);
    if (interf->readSector(interf, currentLba, sector, scratch) < 0)
        return;
    while (currentIndex < gh.PartitionArrayCount) {
        if (!currentSize) {
            /* Read the next LBA
               if there is more
               partitions to be read. */
            if ((currentLba - gh.PartitionArrayBegin > 31)) {
                break;
            }

            currentLba++;
            pat = (struct gpt_partition*)scratch;
            int status =
                interf->readSector(interf, currentLba, sector, scratch);
            if (status < 0)
                return;

            currentSize = sector;
        }

        if (!uuidNull(&pat->Type)) {
            struct partition* p;
	    begin = pat->FirstLba;
	    end = pat->LastLba;

	    if (flags & PARTITION_BROKEN_SECTOR_SIZE_BIT) {
	      begin = (begin * 512) / disk->Ops->SectorSize;
	      end = (end * 512) / disk->Ops->SectorSize;
	    }
	    
            p = partCreatePartition(begin, end, disk->Major);
            memcpy(p->Identifier.Raw, pat->Uid.Raw, sizeof(uuid_t));
            p->Device = interf;
            
            vectorInsert(&disk->Partitions, p);
        }

        currentSize -= gh.PartitionArrayEntrySize;
        currentIndex++;
        pat++;
    }
}
