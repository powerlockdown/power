#include "power/error.h"
#include <power/driver.h>
#include <power/memory/heap.h>
#include <power/memory/pfa.h>
#include <power/partition.h>
#include <power/string.h>

int msdosGetPartitions(struct driver_disk_device_interface* interf,
                       struct pt_disk* disk)
{
    int flags = 0;
    size_t secsz = interf->SectorSize;
    uint8_t* scratch = khsAlloc(secsz);
    if (!scratch)
      return -ERROR_NO_MEMORY;
    
    int error = interf->readSector(interf, 0, secsz, scratch);

    if (error < 0)
        goto retr;

    struct mbr_bootstrp* bs = (struct mbr_bootstrp*)scratch;
    if (!strncmp((char*)&bs->Reserved[3], "LIMINE", 6)) {
        /* Limine's installer has a rather funny way of
           figuring out the sector size:

           Brute-read 512, 2048 or 4096 bytes, in that order.

           This is not an issue when burning real devices; The kernel
           will make reads when their length != sector size fail.

           This though is an issue with ISO burns, because all
           reads will succeed no matter how small or how large.

           That is, in Limine's perspective, all files have an
           512-byte sector size, even when the device will have
           wider sectors. */
        flags |= PARTITION_BROKEN_SECTOR_SIZE_BIT;
    }

    struct mbr_partition* fpart = &bs->Partitions[0];
    if (fpart->PartitionType == 0xEE) {
        efiGetPartitions(interf, scratch, disk, flags);
	
	error = 0;
        goto retr;
    }

    struct partition* p;
    for (int i = 0; i < 4; i++) {
        fpart = &bs->Partitions[i];
        if (fpart->Status != 0x80 && fpart->Status != 0x00) {
            continue;
        }

        if (fpart->LbaStart && fpart->TotalSectors) {
            p = partCreatePartition(fpart->LbaStart,
                                    fpart->LbaStart + fpart->TotalSectors,
                                    disk->Major);
            p->Device = disk->Ops;
            vectorInsert(&disk->Partitions, p);
            continue;
        }
    }

    error = 0;
retr:
    khsFree(scratch);
    return error;
}
