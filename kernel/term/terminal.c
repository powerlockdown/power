#include <power/tty.h>
#include <power/string.h>
#include <power/i386/asm.h>

#include <power/args.h>
#include <power/ldr_requests.h>
#include <power/scheduler.h>

#define SSFN_CONSOLEBITMAP_TRUECOLOR
#include "../ssfn.h"

#define DEFAULT_COLOR 0x0DDB14

extern unsigned char* terminalFont;

static bool useSerial = false;
static bool shouldPrint = true;

static inline void pptWrite(char byte)
{
    __outb(0xE9, byte);
}

static inline bool handleNewLine(char c)
{
    if (c == '\n') {
        ssfn_dst.x = 0;
        ssfn_dst.y += ssfn_src->height;
        if (useSerial)
            pptWrite(c);

        return false;
    }

    return true;
}

static int handleSpecifier(char c, va_list vl)
{
    int status = 0;
    switch (c) {
    case 's': {
        const char* s = va_arg(vl, char*);
        if (!s)
            s = "(null)";
        trmLog(s);
        break;
    }

    case 'u':
    case 'i': {
        char str[20];
        uint64_t i = va_arg(vl, uint64_t);
        ulltostr(i, str, 10);
        trmLog(str);
        break;
    }
 
    case 'd':
    case 'X':
    case 'x':
    case 'p': {
        char str[20];
        uint64_t i = va_arg(vl, uint64_t);
        ulltostr(i, str, 16);

        if (c == 'p')
            trmLog("0x");
        trmLog(str);
        break;
    }

    case 'c': {
        char i = va_arg(vl, int);
        ssfn_putc(i);
        break;
    }

    case '%':
        ssfn_putc('%');
        break;

    case '*':
        va_arg(vl, uint64_t);
        status = 1;
	    break;

    case 'y': {
        char str[20];
        int64_t i = va_arg(vl, int64_t);
        lltostr(i, str, 10);

        trmLog(str);
        break;
    }

    /* ACPICA expects a highly-compliant printf implementation.
       We don't have one. */
    case '.':
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    case '-':
        status = 1;
        break;
    }

    return status;
}

static int escpChangeColor(const char* cform)
{
  int count = 0;
  int colorcount = 0;
  int colors[3];

  uint32_t color;
  
  char type = cform[0];
  if (cform[1] != ';')
    return 1;

  cform += 2;
  count = 2;
  if (!cform[0])
    return 3;

  const char* begin;
  begin = cform;
  
  for (int i = 0; i < 3; i++) {
    colors[i] = strtoul(cform, (char**)&cform, 10);    
    cform++;
    
    count += (cform - begin);
    begin = cform;
  }

  /* So the last digit won't appear
     in the console.*/
  count++;
  
  color = (colors[2] | (colors[1] << 8)
	   | (colors[0] << 16) | (0xFF << 24));

  if (type == 'f')
    ssfn_dst.fg = color;
  else if (type == 'b')
    ssfn_dst.bg = color;
  
  return count;
}

static int handleEscape(const char* format)
{
  if (*format != '\e')
    return 0;

  format += 2;
  
  int count = 0;
  while (format[count] != ':') {
    if (!format[count])
      return count + 2;
    
    count++;
  }

  count++;
  if (!strncmp(format, "cc", 2)) {
    return count + 1 + escpChangeColor(format + count);
  } else if (!strncmp(format, "rc", 2)) {
    ssfn_dst.bg = 0xff000000;
    ssfn_dst.fg = DEFAULT_COLOR;
    return count + 2;
  }

  return count;
}

static void logfv(const char* msg, va_list vl)
{
    if (!shouldPrint)
        return;

    while (*msg != 0) {
        if (!handleNewLine(*msg))
            goto c;

        if (*msg == '%') {
            msg++;

            while (handleSpecifier(*msg, vl))
                msg++;
            msg++;
            continue;
        }

	if (*msg == '\e') {
	  msg += handleEscape(msg);
	  msg--;
	  goto c;
	}

	if (!*msg)
	   break;

        if (useSerial)
            pptWrite(*msg);
        ssfn_putc(*msg);
    c:
        msg++;
    }
}

void trmInit()
{
    useSerial = cmdHasArgument("serial");
    ssfn_src = (ssfn_font_t*)&terminalFont;

    struct limine_framebuffer_response* r = rqGetFramebufferRequest();
    struct limine_framebuffer* fb = r->framebuffers[0];
    ssfn_dst.ptr = fb->address;
    ssfn_dst.p = fb->pitch;
    ssfn_dst.w = fb->width;
    ssfn_dst.h = fb->height;
    ssfn_dst.x = 0;
    ssfn_dst.y = 0;
    ssfn_dst.fg = DEFAULT_COLOR;
}

void trmLog(const char* msg)
{
    while (*msg != 0) {
        if (!handleNewLine(*msg))
            goto c;
        
        if (useSerial)
            pptWrite(*msg);
        ssfn_putc(*msg);
    c:
        ++msg;
    }
}

void trmLogfv(const char* msg, va_list va)
{
    logfv(msg, va);
}

void trmLogf(const char* msg, ...)
{
    va_list vl;
    va_start(vl, msg);
    logfv(msg, vl);
    va_end(vl);
}

void trmLogfn(const char* msg, ...)
{
    va_list vl;
    va_start(vl, msg);
    logfv(msg, vl);
    va_end(vl);

    ssfn_dst.x = 0;
    ssfn_dst.y += ssfn_src->height;
}

struct s_semaphore* trmGetFramebufferLock()
{
    static struct s_semaphore* sem = NULL;
    if (!sem)
        sem = schedCreateSemaphore(1);

    return sem;
}

void trmLogl(const char* msg, size_t length)
{
    size_t i = 0;
    while (i < length) {
        ssfn_putc(msg[i]);
        i++;
    }
}

void trmSetEnable(bool enable)
{
    shouldPrint = enable;
}

void trmGoTo(int column, int row)
{
  ssfn_dst.x = row;
  ssfn_dst.y = column;
}
