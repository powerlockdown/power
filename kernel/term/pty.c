/* pty.c
   Purpose: support for pseudoterminals. */
#include "power/error.h"
#include "power/memory/heap.h"
#include "power/string.h"
#include "power/um.h"
#include <power/device.h>
#include <power/pty.h>
#include <power/tty.h>

static unsigned long muxioctl(struct handle*, unsigned long command,
			      void* buffer, size_t);

static struct handle_ops muxops = {
  .ioctl = muxioctl
};

static struct handle_ops mtyops = {
  .read = ptyReadMaster,
  .write = ptyWriteMaster,
  .ioctl = ptyioctl
};

static struct handle_ops styops = {
  .read = ptyReadSlave,
  .write = ptyWriteSlave,
  .ioctl = ptyioctl
};

static int muxopentty()
{
  major_t* master;
  major_t* slave;
  minor_t masmin;
  
  master = devnRegisterNode("tty", &mtyops, DEVN_MANYDEV_BIT);
  if (!master)
    return -ERROR_NO_MEMORY;
  
  masmin = devAllocateNode(master, NULL);
  if (!masmin)
    return -ERROR_NO_MEMORY;
   
  char* major;
  {
    char slstr[11] = {0};
    ulltostr(masmin, slstr, 10);

    major = strunite("tty", 0, slstr, 0);
    char* majsl = strunite(major, 0, "S", 1);
    slave = devnRegisterNode(majsl, &styops, 0);
    khsFree(majsl);
  }

  minor_t mi;
  struct handle* han;
  struct process* proc;

  proc = pcGetCurrentProcess();
  
  struct tty* tty;
  tty = khsAlloc(sizeof(*tty));
  if (!tty)
    return -ERROR_NO_MEMORY;

  tty->Slave = slave;
  ptyCreateTty(tty);
  
  devAllocateNode(slave, tty);
  han = devnCreateDeviceHandle(proc, major);
  if (ERROR_PTR(han)) {
    khsFree(tty);
    return -ERROR_FROM_PTR(han);
  }

  han->Resource = tty;
  /* We could use devnCreateDeviceHandle
     as any otehr sane person would... */
  return han->Identifier;
}

void ptyInit()
{
  major_t* maj;
  maj = devnRegisterNode("ptymux", &muxops, 0);
  devAllocateNode(maj, NULL);
}

unsigned long muxioctl(struct handle* handle, unsigned long command,
		       void* buffer, size_t length)
{
  __unused(handle);
  __unused(buffer);
  __unused(length);
  
  switch (command) {
  case MUX_OPENTTY:
    return muxopentty();
  }

  return -ERROR_NO_SYSCALL;
}

