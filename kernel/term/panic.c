#include <power/i386/asm.h>
#include <power/interrupt.h>
#include <power/i386/idt.h>

#include <power/tty.h>

static void dumpRegs(const struct irq_regs* regs);

void __panic(const char* message,
	     const char* function,
	     const struct irq_regs* regs)
{
  __irqDisable();
  trmSetEnable(true);
  trmGoTo(0, 0);

  if (!message)
    message = "No message supplied.";
  
  trmLogf("\e[cc:b;0;0;0;\e[cc:f;255;0;0;");
  trmLogfn("KERNEL PANIC\e[cc:f;238;238;238; at %s: %s", function, message);
  if (regs)
    dumpRegs(regs);
  
  asm volatile("cli; hlt");
}

void dumpRegs(const struct irq_regs* regs)
{
  register struct idt_interrupt_frame* ptrs;
  register struct idt_gp_register_state* gp;

  ptrs = regs->PointerRegisters;
  gp = regs->GeneralRegisters;
  
  trmLogfn("Dump of GP register state before panic triggered:");
  trmLogfn("\t RAX=%p RBX=%p RCX=%p RDX=%p", gp->Rax, gp->Rbx, gp->Rcx, gp->Rdx);
  trmLogfn("\t RDI=%p RSI=%p RBP=%p R8=%p", gp->Rdi, gp->Rsi, gp->Rbp, gp->R8);
  trmLogfn("\t R9=%p R10=%p R11=%p R12=%p", gp->R9, gp->R10, gp->R11, gp->R12);
  trmLogfn("\t R13=%p R14=%p R15=%p", gp->R13, gp->R14, gp->R15);

  trmLogfn("Dump of pointer register state before panic triggered:");
  trmLogfn("\t PC(RIP)=%p SP=%p CS=%p%s SS=%p", ptrs->Rip, ptrs->Rsp, ptrs->Cs,
	   ((ptrs->Cs & 0x3) != 0 ? " USER" : ""), ptrs->Ss);
}
