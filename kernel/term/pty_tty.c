#include "power/device.h"
#include "power/error.h"
#include "power/memory/user_buffer.h"
#include "power/scheduler.h"
#include "power/um.h"
#include "power/user_signal.h"
#include <power/tty.h>
#include <power/pty.h>
#include <power/string.h>

#include <power/ring_buffer.h>
#include <sys/types.h>

#define TTY_BUFFER_SIZE 128

static int readslavechar(struct tty*);

void ptyCreateTty(struct tty* tty)
{
  tty->MinLength = 1;
  tty->BufferLock = schedCreateSemaphore(1);
  
  tty->MasterEvent = schedCreateEvent();
  tty->SlaveEvent = schedCreateEvent();
  
  tty->MasterBuffer = ringInit(sizeof(char), TTY_BUFFER_SIZE);
  tty->SlaveBuffer = ringInit(sizeof(char), TTY_BUFFER_SIZE);
}

static int sendIntr(struct process* proc)
{
  struct signal_request* req;
  req = sigCreateRequest(0);
  req->Signal = SIGINT;
  
  sigSubmitSignal(proc, req, NULL);
  return 0;
}

/* Master reads from SlaveBuffer
   and writes onto MasterBuffer. */

int ptyWriteMaster(struct handle* handle,
		   const void* buffer, int length)
{
  int c;
  struct tty* tty = handle->Resource;
  struct process* proc;
  proc = pcLookupProcess(tty->SlaveOwner);
  
  if(!proc)
    return -ERROR_PIPE_BROKEN;
  
  schedSemaphoreAcquire(tty->BufferLock);
  for (int i = 0; i < length; i++) {
    c = ((char*)buffer)[i];
    
    if (c == '\b' && tty->InputFlags & PTY_LISIG) {
      sendIntr(proc);
      continue;
    }

    if (!ringWrite(tty->MasterBuffer, &c)) {
      length = i;      
      goto ret;
    }
  }

ret:
  schedEventRaise(tty->MasterEvent);
  schedEventResetFlag(tty->MasterEvent);
  schedSemaphoreRelease(tty->BufferLock);
  return length;
}

int ptyReadMaster(struct handle* handle,
		  void* buffer, int length)
{
  int i = 0;
  struct tty* tty = handle->Resource;

  schedEventResetFlag(tty->SlaveEvent);
  schedSemaphoreAcquire(tty->BufferLock);
  
  for (; i < length; i++) {
    if(!ringRead(tty->SlaveBuffer, buffer + i)) {
      if ((unsigned int)i >= tty->MinLength)
	return i;

      schedSemaphoreRelease(tty->BufferLock);
      
      schedEventPause(tty->SlaveEvent);
      schedSemaphoreAcquire(tty->BufferLock);
    }
  }
  
  schedSemaphoreRelease(tty->BufferLock);
  return i;
}

/* And the slave reads from MasterBuffer
   and writes onto SlaveBuffer. */

int ptyWriteSlave(struct handle* handle,
		  const void* buffer, int length)
{
  struct tty* tty = handle->Resource;
  tty->SlaveOwner = pcGetCurrentProcess()->Identifier;
  
  schedSemaphoreAcquire(tty->BufferLock);
  for (int i = 0; i < length; i++)
    ringWrite(tty->SlaveBuffer, &buffer[i]);     
  schedSemaphoreRelease(tty->BufferLock);

  schedEventRaise(tty->SlaveEvent);
  return length;
}

int ptyReadSlave(struct handle* handle,
		 void* buffer, int length)
{
  int i = 0;
  int c = 0;
  struct tty* tty = handle->Resource;
  
  schedSemaphoreAcquire(tty->BufferLock);
  for (; i < length; i++) {
    if((c = readslavechar(tty)) == -ERROR_UNAVAILABLE) {
      if ((unsigned int)i >= tty->MinLength)
	return i;

      schedSemaphoreRelease(tty->BufferLock);
      
      schedEventPause(tty->MasterEvent);
      schedSemaphoreAcquire(tty->BufferLock);
      i--;
      
      continue;
    }

    ((char*)buffer)[i] = c;
  }
  schedSemaphoreRelease(tty->BufferLock);
  
  return i;
}

/* Read char from RB and
   perform associated transformations.
   Assumes lock is already held.*/
int readslavechar(struct tty* tty)
{
  char b;
beg:
  if (!ringRead(tty->MasterBuffer, &b)) {
    return -ERROR_UNAVAILABLE;
  }

  if (tty->InputFlags & PTY_ICRTONL
      && b == '\r') {
    b = '\n';
    goto ret;
  }

  if (tty->InputFlags & PTY_INLCR
      && b == '\n') {
    b = '\r';
    goto ret;
  }

  if (tty->InputFlags & PTY_IGNCR
      && b == '\r') {
    goto beg;
  }
  
ret:  
  return b;
}

static int ttygetsln(struct tty* tty, char* buffer, unsigned long length)
{
  const char* slave = tty->Slave->Name;
  const char* united = strunite(DEVN_PATH_PREFIX, DEVN_PATH_PREFIX_SIZE, slave, 0);

  if (!united)
    return -ERROR_NO_MEMORY;
  
  unsigned long slen = strlen(united);
  
  if (slen > length)
    return -ERROR_INVALID_ARGUMENT;

  memcpy(buffer, united, slen);
  return slen;
}

static int ttygetcfg(struct tty* tty, struct _pty* pty)
{
  schedSemaphoreAcquire(tty->BufferLock);
  pty->pty_cflag = tty->ControlFlags;
  pty->pty_iflag = tty->InputFlags;
  pty->pty_lflag = tty->LocalFlags;
  pty->pty_oflag = tty->OutputFlags;
  pty->pty_cc[PTY_VMIN] = tty->MinLength;

  schedSemaphoreRelease(tty->BufferLock);  
  return 0;
}

static int ttysetcfg(struct tty* tty, const struct _pty* pty)
{
  tty->ControlFlags = pty->pty_cflag;
  tty->InputFlags = pty->pty_iflag;
  tty->LocalFlags = pty->pty_lflag;
  tty->OutputFlags = pty->pty_oflag;
  tty->MinLength = pty->pty_cc[PTY_VMIN];

  return 0;
}

unsigned long ptyioctl(struct handle* handle,
		       unsigned long command,
		       void* buffer, size_t length)
{
  int bit;
  int error;
  switch (command) {
  case TTY_GETCFG:
  case TTY_SETCFG:
    bit = command == TTY_GETCFG ? UBUFFER_WRITE_BIT : 0;
    if ((error = mmUserBufferCheck(buffer, sizeof(struct _pty), bit))) {
	return error;
    }

    if (command == TTY_GETCFG)
      return ttygetcfg(handle->Resource, buffer);
    return ttysetcfg(handle->Resource, buffer);
  case TTY_GETSLN:
    if ((error = mmUserBufferCheck(buffer, length, UBUFFER_WRITE_BIT))) {
      return error;
    }

    return ttygetsln(handle->Resource, buffer, length);
  }
  
  return -ERROR_NO_SYSCALL;
}
