#include <power/um.h>
#include <power/input.h>
#include <power/memory/pfa.h>

#include <power/scheduler.h>
#include <power/ring_buffer.h>
#include <power/device.h>
#include <power/tty.h>

#include <power/error.h>

struct {
    struct ring* InputRing;
    major_t* UserDevice;
    minor_t UserMinor;

    struct s_event* ReaderEvent;
    int ReaderCount;
} input;

static struct handle_ops inputhops;

void inpCreateInputRing()
{
    input.UserDevice = devnRegisterNode("input", &inputhops, 0);
    input.UserMinor = devAllocateNode(input.UserDevice, NULL);

    input.InputRing = ringInit(sizeof(struct input_event), 24);
    input.ReaderEvent = schedCreateEvent();
}

bool inpAddReader()
{
    if (input.ReaderCount)
        return false;
    
    input.ReaderCount++;
    return true;
}

void inpRemoveReader()
{
    schedEventResetFlag(input.ReaderEvent);
    input.ReaderCount = 0;
}

int inpReadEvent(int block, struct input_event* event)
{
    bool ev = ringRead(input.InputRing, event);

    if (!ev) {
        if (!block) {
            return -ERROR_WOULD_BLOCK;
        }

        schedEventPause(input.ReaderEvent);
        schedEventResetFlag(input.ReaderEvent);
        ev = ringRead(input.InputRing, event);
    }

    return 0;
}

void inpSendInputEvent(int type, int value, int subFlags)
{
    struct input_event ev;
    ev.Code = value;
    ev.Type = type;
    ev.Flags = subFlags;
    ev.Reserved = 0;

    ringWrite(input.InputRing, &ev);
    schedEventRaise(input.ReaderEvent);
}

static int readWrapper(struct handle* handle, void* buffer, int length)
{
    __unused(handle);
    if ((size_t)length < sizeof(struct input_event) || 
        !buffer)
        return -ERROR_INVALID_ARGUMENT;

    int error = inpReadEvent(!(handle->Flags & HANDLE_NONBLOCK_BIT), buffer);
    if (error)
        return error;
    return sizeof(struct input_event);
}

static struct handle_ops inputhops = {
    .read = readWrapper,

    .close = hndCloseNotSupported,
    .ioctl = hndDefaultIoctl,
    .seek = hndSeekNotSupported,
    .write = hndWriteNotSupported
};
