#include "power/elf.h"
#include <power/mmap.h>
#include "power/memory/vm.h"
#include "power/memory/zoned.h"
#include <power/um.h>
#include <power/memory/vaddress.h>
#include <power/string.h>
#include <power/paging.h>
#include <power/vfs.h>
#include <power/scheduler.h>
#include <power/memory/heap.h>
#include <power/memory/pfa.h>
#include <power/memory/memset.h>
#include <power/ring_buffer.h>

#include <power/system.h>
#include <power/error.h>
#include <power/user_signal.h>

#include <power/limits.h>
#include <power/memory/user_buffer.h>
#include <power/avl.h>

struct process_manager {
  struct process* Processes;
  struct avl_tree* Tree;
  struct s_semaphore* Lock; /*< For both Tree AND Lock! */
  
  struct zoned* PidZone;
};

static struct process_manager pman;
static struct mm_cache* procCache;

static int fillWorkingDir(struct process* proc, const char* name);
static int createStreams(struct process* p, struct _create_process_ex* cpex);

void pcProcessManagerInit() 
{
    pman.Processes = NULL;
    pman.Tree = avlCreateTree();
    pman.PidZone = zpgInit(1, 0xFFFFFFFF, 1, 0, NULL);
    pman.Lock = schedCreateSemaphore(1);
}

struct process* pcCreateProcess(struct process* parent, const char* name,
                                const char* cmdargs,
				struct _create_process_ex* cpex)
{
  long error;
  
  uint8_t* exe;
  struct vm* tvm;
  struct process* p;
  struct fs_file* f;

  struct thread* t;
  struct thread_args args;
  thread_execution_pfn_t ep;
  
  if (unlikely(!procCache)) {
    procCache = khCreateCache("process", sizeof(struct process));
  }

  error = 0;
  p = khAlloc(procCache);
  if (!p)
    return ERROR_INTO_PTR(-ERROR_NO_MEMORY);
  
  schedSemaphoreAcquire(pman.Lock);
  error = zpgRequestRegion(pman.PidZone, 1, &p->Identifier);
  schedSemaphoreRelease(pman.Lock);
  
  if (unlikely(error == -ERROR_UNAVAILABLE)) {
    error = -ERROR_TOO_BIG;
    goto free;
  }
  
  p->HzLock = schedCreateSemaphore(1);
  p->HandleZone = zpgInit(0, 0xFFFFF, 1, 0, NULL);
  p->Children = vectorCreate(1);
  p->Threads = vectorCreate(1);
  p->Handles = vectorCreate(3);

  if (likely(parent)) {
    p->Parent = parent;
    vectorInsert(&parent->Children, p);
  }

  if ((error = fillWorkingDir(p, name)) < 0)
      goto freewd;

  p->Signals = sigCreateSignalReg();
  f = vfsOpenFile(p->WorkingDirectory, name, 0);

  if (f->Bad) {
    error = f->Bad;
    goto freevfs;
  }

  if (unlikely(!parent))
    tvm = vmGetTemporary();
  else
    tvm = parent->VirtualMemory;

  p->AddressSpace = pgCreateAddressSpace();
  p->VirtualMemory = vmInit(p->AddressSpace);
  
  exe = vmVirtualFileMap(tvm, 0, f->Size, f, VPROT_WRITE);  
  p->Executable = modLoadElfEx(exe, p->VirtualMemory,
			       ELF_LOAD_USER_ADDRESS_SPACE);
  
  if (p->Executable.Result != ELF_LOAD_ERROR_OK) {
    error = -ERROR_INVALID_EXECUTABLE;
    goto freeexec;
  }

  p->TerminationEvent = schedCreateEvent();
  p->State = PROCESS_STATE_RUNNING;
  p->CommandLine = strclone(cmdargs);
  
  if (!p->CommandLine && cmdargs) {
    error = -ERROR_NO_MEMORY;
    goto freeexec;
  }

  ep = (void*)p->Executable.EntryPoint;
  args.AddressSpace = p->AddressSpace;
  args.Process = p;
  args.Restorer = 0;
  
  t = schedCreateThreadEx(ep, NULL, &args, THREAD_CREATE_USER_MODE);
  t->Process = p;

  createStreams(p, cpex);
  vectorInsert(&p->Threads, t);

  vmVirtualFileUnmap(tvm, exe, f->Size);
  vfsCloseFile(f);

  schedSemaphoreAcquire(pman.Lock);
  avlInsert(pman.Tree, p->Identifier, p);
  schedSemaphoreRelease(pman.Lock);

  schedAddThread(t);
    
  return p;
freeexec:
    mmAlignedFree(exe, f->Size);
freevfs:
    vfsCloseFile(f);
freewd:
    khsFree(p->WorkingDirectory);
free:
    khFree(procCache, p);
    return ERROR_INTO_PTR(error);
}

struct process* pcGetCurrentProcess()
{
    return schedGetCurrentThread()->Process;
}

int fillWorkingDir(struct process* proc, const char* name)
{
  struct process* parent = proc->Parent;
  
  if (unlikely(!parent)) {
    pman.Processes = proc;
    size_t length = vfsGetPathComponent(name);
    proc->WorkingDirectory = khsAlloc(length + 1);
    if (!proc->WorkingDirectory) {
      return -ERROR_NO_MEMORY;
    }
    
    memset((char*)proc->WorkingDirectory, 0, length + 1);
    memcpy((char*)proc->WorkingDirectory, name, length);
    return 0;
  }
  
  proc->Parent = parent;
  proc->WorkingDirectory = strclone(parent->WorkingDirectory);
  if (!proc->WorkingDirectory && parent->WorkingDirectory) {
    return -ERROR_NO_MEMORY;
  }

  proc->Environment = strclone(parent->Environment);
  if (!proc->Environment && parent->Environment) {
    return -ERROR_NO_MEMORY;
  }

  vectorInsert(&parent->Children, proc);
  return 0;
}

void pcCloseHandle(struct process* p, struct handle* handle)
{
  vectorRemove(&p->Handles, handle);
  if (handle->Ops.close)
    handle->Ops.close(handle);

  schedSemaphoreAcquire(p->HzLock);
  zpgReturnRegion(p->HandleZone, handle->Identifier, 1);
  schedSemaphoreRelease(p->HzLock);
    
  hndDestroyHandle(handle);
}

/* We have to perform this asynchronously
   to avoid crashing when the next inst is fetched. */
static void destroyAddressSpace(address_space_t* address)
{
  pgDestroyAddressSpace(address);
}

void pcTerminateProcess(struct process* p)
{
  size_t i;
  bool state = schedIsEnabled();

  /* All handles. */
  for (i = 0; i < p->Handles.Length; i++) {
    struct handle* h = p->Handles.Data[i];
    pcCloseHandle(p, h);
  }

  /* Detach from its parent. */ 
  if (likely(p->Parent)) {
    vectorRemove(&p->Parent->Children, p);
  } else {
    pman.Processes = NULL;
  }
    
  /* And tell others that we died. */
  schedDestroyEvent(p->TerminationEvent);
  modUnloadProgram(&p->Executable);
  p->State = PROCESS_STATE_DEAD;
  
  khsFree(p->WorkingDirectory);
  if (p->CommandLine)
    khsFree(p->CommandLine);
  if (p->Environment)
    khsFree(p->Environment);
  
  vmDestroy(p->VirtualMemory);
  p->VirtualMemory = NULL;
    
  workerSubmitTask((worker_routine_pfn_t)destroyAddressSpace,
		   p->Executable.AddressSpace);
    
  schedSemaphoreAcquire(pman.Lock);
  avlRemove(pman.Tree, p->Identifier);
  zpgReturnRegion(pman.PidZone, p->Identifier, 1);
  schedSemaphoreRelease(pman.Lock);

  schedSemaphoreAcquire(p->HzLock);
  zpgDestroy(p->HandleZone);
  schedDestroySemaphore(p->HzLock);
    
  schedEnable(false);
  for (i = 0; i < p->Threads.Length; i++) {
    schedRemoveThread(p->Threads.Data[i]);
  }

  schedEnable(state);
  if (!p->Parent)
    khsFree(p);
  
  /* We do not unconditionally free the process
     here because another process might be waiting
     for us to die to dissect our exit code. */
}

struct handle* pcGetHandleById(struct process* process, int handle)
{
    for (size_t i = 0; i < process->Handles.Length; i++) {
        struct handle* ha = process->Handles.Data[i];
        if (ha->Identifier == handle) {
            return ha;
        }
    }

    return NULL;
}

struct handle* pcCreateHandle(struct process* process, int type, void* res)
{
    struct handle* ha = hndCreateHandle(process, type);
    if (ERROR_PTR(ha))
        return ha;

    int error;
    unsigned long id;

    schedSemaphoreAcquire(process->HzLock);
    if ((error = zpgRequestRegion(process->HandleZone, 1, &id))) {
      schedSemaphoreRelease(process->HzLock);
      hndDestroyHandle(ha);
      return ERROR_INTO_PTR((unsigned long)error);
    }
    
    schedSemaphoreRelease(process->HzLock);
    ha->Identifier = id;
    ha->Resource = res;
    return ha;
}

struct process* pcLookupProcess(unsigned int pid)
{
  struct process* p;
  schedSemaphoreAcquire(pman.Lock);
  
  p = avlLookup(pman.Tree, pid);
  schedSemaphoreRelease(pman.Lock);
  return p;
}

uint64_t syscCreateProcess(union sysc_regs* regs)
{
    /* CreateProcess(const char* name, int flags, struct _create_process_ex*) */
    if (!regs->Arg1) {
        return -ERROR_INVALID_ARGUMENT;
    }

    struct process* process;

    const char* cmdline = NULL;
    const char* name = (char*)regs->Arg1;
    struct _create_process_ex* cpex = (void*)regs->Arg3;
    if (regs->Arg3) {
        cmdline = (void*)cpex->CommandLineArgs;
    }

    process = pcCreateProcess(pcGetCurrentProcess(), name, cmdline, cpex);
    if (ERROR_PTR(process)) {
        return ERROR_FROM_PTR(process);
    }

    return process->Identifier;
}

static inline 
int inheritStreams(struct process* p, struct _create_process_ex* cpex)
{
  int dex;
  struct handle* pah;
  struct handle* h;
  
  if (!p->Parent) {
    return -ERROR_INVALID_ARGUMENT;
  }
  
  int handlemap[] = {
    [STREAM_STDERR] = cpex->StderrHandle,
    [STREAM_STDOUT] = cpex->StdoutHandle,
    [STREAM_STDIN] = cpex->StdinHandle
  };

  for (int i = 0; i < 3; i++) {
    dex = i;
    if (handlemap[i] != INVALID_HANDLE) {
      dex = handlemap[i];
    }

    pah = pcGetHandleById(p->Parent, dex);
    h = pcCreateHandle(p, pah->Type, pah->Resource);
      
	  if (h->Type == HANDLE_TYPE_PIPE)
	    ((struct pipe_data*)h->Resource)->ReferenceCount++;
        
    if (h->Type == HANDLE_TYPE_TTY || h->Type == HANDLE_TYPE_DEVICE)
      h->Ops = pah->Ops;
  }
  
  return 0;
}

int createStreams(struct process* p, struct _create_process_ex* cpex)
{
  if (likely(p->Parent)) {
    return inheritStreams(p, cpex);
  }

  for (int i = 0; i < 3; i++) {
    struct handle* h = pcCreateHandle(p, HANDLE_TYPE_PIPE, NULL);
    if (ERROR_PTR(h)) {
      return ERROR_FROM_PTR(h);
    }
    
    struct pipe_data* pd = khsAlloc(sizeof(*pd));
    if (unlikely(!pd)) {
      return -ERROR_NO_MEMORY;
    }
    
    pd->RingBuffer = ringInit(sizeof(void*), 12);
    pd->Flags = PIPE_FLAGS_READ_TO_TTY;
    pd->ReferenceCount = 1;

    h->Resource = pd;
    h->Identifier = i;
    h->Type = HANDLE_TYPE_PIPE;
  }
  
  return 0;
}

uint64_t syscSetWorkingDirectory(union sysc_regs* regs)
{
    /* SetWorkingDirectory(char* newrpath) -> int */
    if (!regs->Arg1) {
        /* Nothing changed. */
        return 0;
    }

    int error;
    struct process* proc;
    const char* path = (const char*)regs->Arg1;

    char cap[MAX_PATH];

    proc = pcGetCurrentProcess();
    if ((error = vfsComputeAbsPath(proc->WorkingDirectory, 
                                   path, cap, MAX_PATH))) {
        return error;
    }

    const char* oldp = proc->WorkingDirectory;
    proc->WorkingDirectory = strclone(cap);
    khsFree(oldp);

    return 0;
}

uint64_t syscGetWorkingDirectory(union sysc_regs* regs)
{
    struct process* p = pcGetCurrentProcess();
    if (!regs->Arg1) {
        return -ERROR_INVALID_ARGUMENT;
    }

    int ret;
    void* buffer = (void*)regs->Arg1;
    size_t length = regs->Arg2;
    if ((ret = mmUserBufferCheck(buffer, length, UBUFFER_WRITE_BIT))) {
        return ret;
    }

    if (!length) {
        return 0;
    }

    if (length > MAX_PATH) {
        return -ERROR_INVALID_RANGE;
    }

    size_t st = strlen(p->WorkingDirectory);
    if (length > st) {
        length = st;
    }

    memcpy(buffer, p->WorkingDirectory, length);
    return length;
}

uint64_t syscWaitForProcess(union sysc_regs* regs)
{
  unsigned int exc;
  unsigned int pid = regs->Arg1;
  
  if (pid == pcGetCurrentProcess()->Identifier)
    return -ERROR_WOULD_DEADLOCK;

  struct process* proc;
  proc = pcLookupProcess(pid);
  if (!proc)
    return -ERROR_NOT_FOUND;
  schedEventPause(proc->TerminationEvent);
  
  /* Does this qualify as use-after-free? */
  exc = proc->ExitCode;
  return exc;
}

uint64_t syscGetCurrentProcessId(union sysc_regs* regs)
{
  (void)regs;
  return pcGetCurrentProcess()->Identifier;
}

uint64_t syscGetEnvironmentString(union sysc_regs* regs)
{
  /* GetEnvironmentString(char* buffer, unsigned int len) -> int  */
  struct process* proc;
  
  char* buffer; 
  unsigned int len;

  proc = pcGetCurrentProcess();

  if (!proc->Environment)
    return 0;

  len = strlen(proc->Environment);
  
  buffer = (void*)regs->Arg1;
  len = min(regs->Arg2, min(MAX_ENV_STR, len));
  
  int error;
  if ((error = mmUserBufferCheck(buffer, len, UBUFFER_WRITE_BIT))) {
    return error;
  }

  memcpy(buffer, proc->Environment, len);
  return len;
}

uint64_t syscSetEnvironmentString(union sysc_regs* regs)
{
  /* SetEnvironmentString(const char* buffer, unsigned int len) -> int  */
  struct process* proc;
  int ret;
  
  char* dest;
  const char* buffer;
  unsigned int len;

  proc = pcGetCurrentProcess();
  buffer = (void*)regs->Arg1;
  len = min(regs->Arg2, MAX_ENV_STR);
  
  if ((ret = mmUserBufferCheck((void*)buffer, len, 0))) {
    return ret;
  }
  
  dest = khsAlloc(len);
  if (!dest)
    return -ERROR_NO_MEMORY;

  memcpy(dest, buffer, len);
  if (proc->Environment)
    khsFree(proc->Environment);
  
  proc->Environment = dest;
  return len;
}
