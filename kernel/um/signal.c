#include "power/signal.h"
#include "power/error.h"
#include "power/memory/user_buffer.h"
#include "power/scheduler.h"
#include "power/tty.h"
#include "power/um.h"
#include <power/user_signal.h>

#include <power/memory/heap.h>
#include <power/memory/memset.h>

#include <power/i386/asm.h>
#include <power/i386/gdt.h>
#include <power/bit.h>

static struct signal_handler* getSignalHandler(struct process*, int signal);
static int createFrame(struct thread* thread, struct signal_frame* frame);
static int performDefaultSignalAction(struct process* proc, int signal);

struct proc_signal* sigCreateSignalReg()
{
    struct proc_signal* ps = khsAlloc(sizeof(*ps));
    if (!ps)
      return NULL;
    
    ps->Handlers = vectorCreate(0);
    ps->Requests = vectorCreate(0);
    ps->RestoreRegisters = khsAlloc(sizeof(*ps->RestoreRegisters));
    ps->WaitSetEvent = schedCreateEvent();
    
    return ps;
}

void sigSetHandler(struct process* proc, int signal,
                   __sig_handler_pfn_t handler)
{
    bool found = false;
    struct signal_handler* sh = NULL;
    struct vector* shv = &proc->Signals->Handlers;

    for (size_t i = 0; i < shv->Length; i++) {
        sh = shv->Data[i];
        if (sh->Signal == signal) {
            found = true;
            break;
        }
    }

    if (!sh) {
        sh = khsAlloc(sizeof(*sh));
    }

    sh->Action = SIGACTION_ROUTINE;
    sh->Routine = handler;
    sh->Signal = signal;

    if (!found) {
        vectorInsert(shv, sh);
    }
}

int sigDispatchSignals(struct thread* thr)
{
    struct process* proc = thr->Process;
    if (!proc) {
        return 0;
    }

    struct vector* shv = &proc->Signals->Requests;
    if (!shv->Length) {
        return 0;
    }

    struct signal_request* sh = shv->Data[0];
    vectorRemove(shv, sh);
   
    if (!shv->Length) {
        proc->Signals->Flags &= ~SIG_PROC_PENDING_BIT;
    }

    /* Masked? simply ignore.
       TODO: we should perform the default action
       if the signal is dangerous enough (SIGKILL | SIGABRT et al)*/
    if (proc->Signals->Mask & (1 << sh->Signal)) {
        goto ret;
    }

    /* Wake up any thread that is sigwaiting us. */
    if (proc->Signals->WaitSet & (1 << sh->Signal)) {
      proc->Signals->WaitSet &= (1 << sh->Signal);
      schedEventRaise(proc->Signals->WaitSetEvent);
    }
    
    struct signal_handler* handler = getSignalHandler(proc, sh->Signal);
    if (!handler) {
      int ret = performDefaultSignalAction(proc, sh->Signal);
      
      khsFree(sh);
      return ret;
    }

    struct thread_registers* regs = proc->Signals->RestoreRegisters;

    memcpy(regs, &thr->Registers, sizeof(thr->Registers));
    memset(&thr->Registers.GeneralRegisters, 0,
           sizeof(thr->Registers.GeneralRegisters));
    memset(&thr->Registers.Pointers, 0, sizeof(thr->Registers.Pointers));

    if (thr->CurrentSyscall != -1) {
        thr->Flags |= THREAD_SIGNAL_CLOBBERED_BIT;
        thr->LastNsRip = thr->SyscallRegs->Rcx;
    }
    
    thr->Registers.Pointers.Rip = (uintptr_t)handler->Routine;
    if (thr->Registers.CodeSegment == USER_CODE_SEGMENT)
        thr->Registers.Pointers.Rsp = regs->Pointers.Rsp - 128;
    else
        thr->Registers.Pointers.Rsp = (uintptr_t)thr->LastUserStack - 128;
        
    thr->Registers.Pointers.Rsp -= sizeof(struct signal_frame);
    void* frame = (void*)(thr->Registers.Pointers.Rsp);
    createFrame(thr, frame);

    thr->Registers.GeneralRegisters.Rdi = sh->Signal;
    thr->Registers.CodeSegment = USER_CODE_SEGMENT;
    thr->Registers.DataSegment = USER_DATA_SEGMENT;

    thr->SignalFrame = frame;
ret:
    khsFree(sh);
    return 0;
}

struct signal_request* sigCreateRequest(int flags)
{
    struct signal_request* sr = khsAlloc(sizeof(*sr));
    if (unlikely(!sr))
      return NULL;
    
    sr->Flags = flags;

    if (flags & SIG_CREATE_INFO_BIT) {
        sr->Info = khsAlloc(sizeof(*sr->Info));
	if (unlikely(!sr->Info))
	  return NULL;
    }

    return sr;
}

void sigSubmitSignal(struct process* proc, struct signal_request* req,
                     struct irq_regs* regs)
{
    struct proc_signal* sgs = proc->Signals;
    vectorInsert(&sgs->Requests, req);

    sgs->Flags |= SIG_PROC_PENDING_BIT;

    if (req->Flags & SIG_WAIT_BIT) {
        if (schedIsEnabled() && regs)
            schedThink(regs);
    }
}

uint64_t syscSigHandler(union sysc_regs* regs)
{
    if (!regs->Arg2) {
        return -ERROR_INVALID_ARGUMENT;
    }

    uintptr_t ptr = regs->Arg2;
    struct process* p = pcGetCurrentProcess();

    sigSetHandler(p, regs->Arg1, (sig_handler_pfn_t)ptr);
    return 0;
}

struct signal_handler* getSignalHandler(struct process* proc, int signal)
{
    struct proc_signal* sgs = proc->Signals;
    struct vector* shv = &sgs->Handlers;

    struct signal_handler* sh;
    for (size_t i = 0; i < shv->Length; i++) {
        sh = shv->Data[i];
        if (sh->Signal == signal) {
            return sh;
        }
    }

    return NULL;
}

/* fill sigcontext from syscall regs */
static void fillCtxFromSregs(struct thread* t,
                             struct sysc_pres_regs* regs, 
                             struct signal_frame* frame)
{
    union sysc_regs* parm = regs->Params;
    uintptr_t stk = ((uintptr_t)regs->Params) + sizeof(*regs->Params);

    frame->Context.sc_r8 = parm->R8;
    frame->Context.sc_r9 = parm->R9;
    frame->Context.sc_r10 = parm->R10;
    frame->Context.sc_r11 = regs->R11;
    frame->Context.sc_r12 = regs->R12;
    frame->Context.sc_r13 = parm->R13;
    frame->Context.sc_r14 = regs->R14;
    frame->Context.sc_r15 = regs->R15;

    frame->Context.sc_rax = parm->Rax;
    frame->Context.sc_rbx = parm->Rbx;
    frame->Context.sc_rcx = regs->Rcx;
    frame->Context.sc_rdx = parm->Rdx;
    frame->Context.sc_rdi = parm->Rdi;
    frame->Context.sc_rsi = regs->Rsi;
    frame->Context.sc_rbp = regs->Rbp;

    frame->Context.sc_rsp = stk;
    frame->Context.sc_rip = t->LastNsRip;
}

static void fillCtxFromTregs(struct thread_registers* regs,
                             struct signal_frame* frame)
{
    frame->Context.sc_r8 = regs->GeneralRegisters.R8;
    frame->Context.sc_r9 = regs->GeneralRegisters.R9;
    frame->Context.sc_r10 = regs->GeneralRegisters.R10;
    frame->Context.sc_r11 = regs->GeneralRegisters.R11;
    frame->Context.sc_r12 = regs->GeneralRegisters.R12;
    frame->Context.sc_r13 = regs->GeneralRegisters.R13;
    frame->Context.sc_r14 = regs->GeneralRegisters.R14;
    frame->Context.sc_r15 = regs->GeneralRegisters.R15;

    frame->Context.sc_rax = regs->GeneralRegisters.Rax;
    frame->Context.sc_rbx = regs->GeneralRegisters.Rbx;
    frame->Context.sc_rcx = regs->GeneralRegisters.Rcx;
    frame->Context.sc_rdx = regs->GeneralRegisters.Rdx;
    frame->Context.sc_rdi = regs->GeneralRegisters.Rdi;
    frame->Context.sc_rsi = regs->GeneralRegisters.Rsi;
    frame->Context.sc_rbp = regs->GeneralRegisters.Rbp;

    frame->Context.sc_rsp = regs->Pointers.Rsp;
    frame->Context.sc_rip = regs->Pointers.Rip;
}

static void fillTregsFromCtx(struct signal_frame* frame, 
                             struct thread_registers* regs)
{
    regs->GeneralRegisters.R8 = frame->Context.sc_r8;
    regs->GeneralRegisters.R9 = frame->Context.sc_r9;
    regs->GeneralRegisters.R10 = frame->Context.sc_r10;
    regs->GeneralRegisters.R11 = frame->Context.sc_r11;
    regs->GeneralRegisters.R12 = frame->Context.sc_r12;
    regs->GeneralRegisters.R13 = frame->Context.sc_r13;
    regs->GeneralRegisters.R14 = frame->Context.sc_r14;
    regs->GeneralRegisters.R15 = frame->Context.sc_r15;

    regs->GeneralRegisters.Rax = frame->Context.sc_rax;
    regs->GeneralRegisters.Rbx = frame->Context.sc_rbx;
    regs->GeneralRegisters.Rcx = frame->Context.sc_rcx;
    regs->GeneralRegisters.Rdx = frame->Context.sc_rdx;
    regs->GeneralRegisters.Rdi = frame->Context.sc_rdi;
    regs->GeneralRegisters.Rsi = frame->Context.sc_rsi;
    regs->GeneralRegisters.Rbp = frame->Context.sc_rbp;

    regs->Pointers.Rsp = frame->Context.sc_rsp;
    regs->Pointers.Rip = frame->Context.sc_rip;
}

int createFrame(struct thread* thread, struct signal_frame* frame)
{
    if (mmUserBufferCheck(frame, sizeof(*frame), 
                           UBUFFER_WRITE_BIT)) {
        return -ERROR_USER_FAULT;
    }

    if (thread->CurrentSyscall != -1) {
        fillCtxFromSregs(thread, thread->SyscallRegs, frame);
    } else {
        fillCtxFromTregs(&thread->Registers, frame);
    }

    return 0;
}

/*  */
int performDefaultSignalAction(struct process* proc, int signal)
{
  switch (signal) {
  /* Terminate the process. */
  case SIGSEGV:
  case SIGABRT:
  case SIGBUS:
  case SIGFPE:
  case SIGILL:
  case SIGKILL:
    pcTerminateProcess(proc);
    break;
  case SIGCHLD:
    return 0;
  }

  return -ERROR_PROCESS_DIED;
}

/* PLEASE test this code later. */
uint64_t syscReturnFromSignal(union sysc_regs* regs)
{
    __unused(regs);

    struct thread* thread = schedGetCurrentThread();
    struct process* process = thread->Process;
    if (!process) {
        return -ERROR_INVALID_ARGUMENT;
    }

    struct proc_signal* signal = process->Signals;
    memcpy(&thread->Registers, signal->RestoreRegisters,
           sizeof(thread->Registers));

    if (thread->Flags & THREAD_SIGNAL_CLOBBERED_BIT) {
        fillTregsFromCtx(thread->SignalFrame, &thread->Registers);
        
        thread->Flags &= ~THREAD_SIGNAL_CLOBBERED_BIT;
        thread->Registers.GeneralRegisters.Rax = -ERROR_INTERRUPTED;
	    thread->Suspended = false;
    }

    schedSkipRegisterSave();
    schedThreadYield();

    __halt();
    return 0;
}

uint64_t syscRaiseSignal(union sysc_regs* regs)
{
    /* RaiseSignal(int) -> void */

    int signal = regs->Arg1;

    struct process* proc = pcGetCurrentProcess();
    struct signal_request* req = sigCreateRequest(SIG_CREATE_INFO_BIT | SIG_WAIT_BIT);
    req->Info->si_signal = signal;
    req->Signal = signal;

    sigSubmitSignal(proc, req, NULL);
    return 0;
}

uint64_t syscWaitForSignal(union sysc_regs* regs)
{
  unsigned long set = regs->Arg1;
  struct process* proc;
  
  proc = pcGetCurrentProcess();
  
  /* No-one to wait! */
  if (!set)
    return 0;

  proc->Signals->WaitSet = set;  
  schedEventResetFlag(proc->Signals->WaitSetEvent);
  schedEventPause(proc->Signals->WaitSetEvent);

  unsigned long ff;
  ff = __ffs(proc->Signals->WaitSet);
  proc->Signals->WaitSet = 0;
  
  return ff;
}

uint64_t syscSetSignalMask(union sysc_regs* regs)
{
  int type = regs->Arg1;
  unsigned long mask = regs->Arg2;

  struct process* proc;
  proc = pcGetCurrentProcess();

  unsigned long oldmask;
  oldmask = proc->Signals->Mask;
  
  switch (type) {
  case SIGMASK_MASK:
    proc->Signals->Mask |= mask;
    break;
  case SIGMASK_UNMASK:
    proc->Signals->Mask &= ~mask;
    break;
  case SIGMASK_SET:
    proc->Signals->Mask = mask;
    break;
  }

  return oldmask;
}

uint64_t syscSendProcessSignal(union sysc_regs* regs)
{
  unsigned pid = regs->Arg1;
  int signal = regs->Arg2;

  struct process* proc;
  proc = pcLookupProcess(pid);
  if (!proc)
    return -ERROR_NOT_FOUND;

  struct signal_request* sre;
  sre = sigCreateRequest(0);
  sre->Signal = signal;
  
  sigSubmitSignal(proc, sre, NULL);
  return 0;
}
