/* syscall.c
   Purpose: syscall handler */

#include "power/limits.h"
#include "power/scheduler.h"
#include <power/time.h>
#include <power/um.h>
#include <power/vfs.h>
#include <power/memory/vm.h>

#include <power/string.h>
#include <power/error.h>
#include <power/memory/memset.h>

#include <power/user_signal.h>
#include <power/system.h>
#include <power/memory/user_buffer.h>

#include <power/i386/fsgs.h>

static uint64_t syscCloseHandle(union sysc_regs* regs) 
{
    struct handle* buffer = pcGetHandleById(pcGetCurrentProcess(), regs->Arg1);
    if (!buffer) {
        return -ERROR_BAD_HANDLE;
    }

    pcCloseHandle(pcGetCurrentProcess(), buffer);
    return 0;
}

static uint64_t syscGetCommandLine(union sysc_regs* regs) 
{
    __unused(regs);
    return -ERROR_NOT_SUPPORTED;
}

static uint64_t syscExitProcess(union sysc_regs* regs)
{
    __unused(regs);
    struct process* p = pcGetCurrentProcess();
    pcTerminateProcess(p);
    
    asm volatile("sti; hlt");
    return 0;
}

static syscall_handler_pfn_t handlers[] = {
    NULL,
    &syscOpenFile,
    &syscWriteFile,
    &syscReadFile,
    &syscCloseHandle,
    &syscGetCommandLine,
    &syscExitProcess,
    &syscVirtualMap,
    &syscVirtualUnmap,
    &syscSetFilePointer,
    &syscIoControl,
    &syscCreateProcess,
    &syscGetWorkingDirectory,
    &syscMountVolume,
    &syscCreatePipe,
    &syscSigHandler,
    &syscReturnFromSignal,
    &syscRaiseSignal,
    NULL,
    &syscCreateThread,
    &syscGetCurrentThreadId,
    &syscKillThread,
    &syscGetTimeOfDay,
    NULL,
    &syscVirtualFileMap,
    &syscGetRealPath,
    &syscSetWorkingDirectory,
    &syscGetFileStatistics,
    &syscFsGsControl,
    &syscSetThreadSuspension,
    &syscWaitForSignal,
    &syscSetSignalMask,
    &syscSendProcessSignal,
    &syscWaitForProcess,
    &syscGetCurrentProcessId,
    &syscGetHandleType,
    &syscSleep,
    &syscDuplicateHandle,
    &syscWaitForThread,
    &syscGetEnvironmentString,
    &syscSetEnvironmentString
};

uint64_t syscGenericHandler(struct sysc_pres_regs* pr) 
{
    union sysc_regs* regs = pr->Params;

    if (unlikely(!regs->Slot)) {
        return -ERROR_NO_SYSCALL;
    }

    size_t tableLength = sizeof(handlers) / sizeof(syscall_handler_pfn_t);
    if (unlikely(regs->Rax > tableLength)) {
        return -ERROR_NO_SYSCALL;
    }

    struct thread* thread = schedGetCurrentThread();
    syscall_handler_pfn_t pf = handlers[regs->Rax];
    if (!pf) {
        return -ERROR_NO_SYSCALL;
    }
    
    thread->SyscallRegs = pr;
    thread->LastNsRip = pr->Rcx;
    thread->CurrentSyscall = regs->Slot;

    uint64_t ret = pf(regs);
    thread->CurrentSyscall = -1;

    return ret;
}
