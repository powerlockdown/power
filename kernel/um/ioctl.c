#include <power/um.h>
#include <power/ioctl.h>
#include <power/partition.h>

#include <power/tty.h>
#include <power/error.h>
#include <power/scheduler.h>

#include <power/memory/heap.h>
#include <power/video/framebuffer.h>
#include <power/ldr_requests.h>

#include <power/paging.h>
#include <power/memory/vaddress.h>

#define FRAMEBUFFER_BEGIN 0xF0F0F000000

static uint64_t ioctlGetDisks(union sysc_regs* regs)
{
    if (regs->Arg4 != sizeof(struct ioctl_get_disk) || !regs->Arg3)
        return -1;
        
    struct vector* dks = partGetDisks();
    struct ioctl_get_disk* gd;
    gd = (struct ioctl_get_disk*)regs->Arg3;
    if (!gd->Data) {
        gd->Length = dks->Length;
        return 0;
    }

    for (size_t i = 0; i < gd->Length; i++) {
        struct pt_disk* p = dks->Data[i];
        gd->Data[i] = p->DeviceIndex;
    }

    return 0;
}

static struct partition* getPartition(struct pt_disk* dk, 
                                      union ioctl_uuid* uuid,
                                      int flags)
{
    for (size_t i = 0; i < dk->Partitions.Length; i++) {
        struct partition* pt = dk->Partitions.Data[i];
        if (flags & SEARCH_PARTITION_BY_INDEX_BIT) {
            if (i == uuid->Data1) {
                return pt;
            }
            
            continue;
        }

        if (uuidCompare(uuid, &pt->Identifier)) {
            return pt;
        }
    }

    return NULL;
}

static uint64_t ioctlSearchPartition(union sysc_regs* regs)
{
    if (regs->Arg4 != sizeof(struct ioctl_search_partition) || !regs->Arg3)
        return -1;

    struct ioctl_search_partition* se;
    se = (struct ioctl_search_partition*)regs->Arg3;

    struct vector* dks = partGetDisks();
    struct pt_disk* d;
    struct partition* pt;
    for (size_t i = 0; i < dks->Length; i++) {
        d = dks->Data[i];
        if (d->DeviceIndex == se->Disk) {
            pt = getPartition(d, &se->Identifier, se->Flags);
            if (!pt)
                return -ERROR_NOT_FOUND;

            struct handle* h = pcCreateHandle(pcGetCurrentProcess(), HANDLE_TYPE_DEVICE, pt);
            return h->Identifier;
        }
    }

    return -ERROR_NO_DEVICE;
}

static uint64_t ioctlGetInputBuffer(union sysc_regs* regs)
{
    __unused(regs);
    if (!inpAddReader()) {
        return -1;
    }

    struct pipe_data* pd = khsAlloc(sizeof(*pd));
    pd->Flags = PIPE_FLAGS_INPUT_BUFFER;
    pd->RingBuffer = NULL;
    pd->ReferenceCount = 1;
    struct handle* h = pcCreateHandle(pcGetCurrentProcess(), HANDLE_TYPE_PIPE, pd);
    return h->Identifier;
}

static uint64_t ioctlGetFramebufferLock(union sysc_regs* regs)
{
    if (!regs->Arg3 || regs->Arg4 != sizeof(struct video_framebuffer)) {
        return -ERROR_INVALID_ARGUMENT;
    }

    struct limine_memmap_entry* fb = NULL;
    struct s_semaphore* sem = trmGetFramebufferLock();
    schedSemaphoreAcquire(sem);

    struct limine_framebuffer_response* fbre = rqGetFramebufferRequest();
    struct limine_memmap_response* re = rqGetMemoryMapRequest();

    for (size_t i = 0; i < re->entry_count; i++) {
        if (re->entries[i]->type == LIMINE_MEMMAP_FRAMEBUFFER) {
            fb = re->entries[i];
        }
    }

    if (!fb)
        return -ERROR_NOT_FOUND;
        
    struct address_space_mgr* mgr = addrGetManagerForCr3(NULL);
    if (!asmgrClaimPage(mgr, FRAMEBUFFER_BEGIN, fb->length / 4096))
        return -1;

    for (size_t i = 0; i < fb->length; i += 2 * MB) {
        pgMapLargePage(NULL, fb->base + i, FRAMEBUFFER_BEGIN + i, 
                  PT_FLAG_WRITE | PT_FLAG_PCD | PT_FLAG_USER);
    }

    pgDumpPageTable(NULL, FRAMEBUFFER_BEGIN);
    struct limine_framebuffer* lfb = fbre->framebuffers[0];
    struct video_framebuffer* vfb = (void*)regs->Arg3;
    vfb->Address = (uint64_t)FRAMEBUFFER_BEGIN;
    vfb->Pitch = lfb->pitch;
    vfb->BitsPerPixel = lfb->bpp;
    vfb->Height = lfb->height;
    vfb->Width = lfb->width;

    return 0;
}

static uint64_t ioctlReleaseFramebufferLock(union sysc_regs* regs)
{
    __unused(regs);
    struct limine_memmap_entry* fb = NULL;
    struct s_semaphore* sem = trmGetFramebufferLock();
    schedSemaphoreRelease(sem);

    struct limine_memmap_response* re = rqGetMemoryMapRequest();

    for (size_t i = 0; i < re->entry_count; i++) {
        if (re->entries[i]->type == LIMINE_MEMMAP_FRAMEBUFFER) {
            fb = re->entries[i];
        }
    }

    struct address_space_mgr* mgr = addrGetManagerForCr3(NULL);
    if (asmgrClaimPage(mgr, FRAMEBUFFER_BEGIN, fb->length / 4096))
        return -1;

    /* TODO: add a call to return vaddrs to asmgr */
    for (size_t i = 0; i < fb->length; i += 2 * MB) {
        pgUnmapPage(NULL, FRAMEBUFFER_BEGIN + i);
    }

    return 0;
}

static uint64_t ioctlStopPrinting(union sysc_regs* regs)
{
    __unused(regs);
    trmSetEnable(false);
    return 0;
}

static uint64_t ioctlSetExitCode(union sysc_regs* regs)
{
    struct process* proc = pcGetCurrentProcess();
    proc->ExitCode = regs->Arg2;
    return 0;
}

static syscall_handler_pfn_t ioctls[] = {
    [IOCTL_GET_DISKS] = ioctlGetDisks,
    [IOCTL_SEARCH_PARTITION] = ioctlSearchPartition,
    [IOCTL_GET_INPUT_BUFFER] = ioctlGetInputBuffer,
    [IOCTL_GET_FRAMEBUFFER_LOCK] = ioctlGetFramebufferLock,
    [IOCTL_RELEASE_FRAMEBUFFER_LOCK] = ioctlReleaseFramebufferLock,
    [IOCTL_ETERM_STOP_PRINTING] = ioctlStopPrinting,
    [IOCTL_SET_EXIT_CODE] = ioctlSetExitCode
};

uint64_t syscIoControl(union sysc_regs* regs)
{
    if (regs->Arg1) {
        struct handle* handle = pcGetHandleById(pcGetCurrentProcess(), regs->Arg1);
        return handle->Ops.ioctl(handle, regs->Arg2, (void*)regs->Arg3, regs->Arg4);
    }

    syscall_handler_pfn_t handler = ioctls[regs->Arg2];
    if (handler) {
        return handler(regs);
    }

    return ERROR_NO_SYSCALL;
}
