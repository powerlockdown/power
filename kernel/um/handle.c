#include "power/handle.h"
#include "power/memory/heap.h"
#include <power/um.h>
#include <power/config.h>
#include <power/vfs.h>

#include <power/error.h>
#include <power/tty.h>
#include <power/ioctl.h>

#include <power/ring_buffer.h>
#include <power/scheduler.h>
#include <power/system.h>
#include <power/limits.h>

static struct mm_cache* handleCache;

/* TTY */
static int uttyWrite(struct handle* h, const void* buffer, int length)
{
    __unused(h);
    trmLogl(buffer, length);
    return length;
}

/* VFS */
static int uvfsRead(struct handle* h, void* buffer, int length);
static int uvfsWrite(struct handle* h, const void* buffer, int length);
static int uvfsSeek(struct handle* h, int relative, size_t length);
static void uvfsClose(struct handle* h);

/* Pipe */
static void pipeBreak(struct pipe_data* pd);
static int upipeRead(struct handle* h, void* buffer, int length);
static int upipeWrite(struct handle* h, const void* buffer, int length);
static void upipeClose(struct handle* h);

/* Process */
static unsigned long uprocIoctl(struct handle* h, unsigned long command,
                                void* buffer, size_t vo);
static void uprocClose(struct handle*);

struct handle* hndCreateHandle(struct process* proc, int type)
{
    if (!handleCache)
        handleCache = khCreateCache("handle", sizeof(struct handle));

    if (proc->Handles.Length >= MAX_HANDLES) {
        return ERROR_INTO_PTR(-ERROR_TOO_MANY_HANDLES);
    }

    struct handle* ha = khAlloc(handleCache);
    if (!ha) {
        return ERROR_INTO_PTR(-ERROR_NO_MEMORY);
    }

    ha->Type = type;

    ha->Ops.read = hndReadNotSupported;
    ha->Ops.write = hndWriteNotSupported;
    ha->Ops.close = hndCloseNotSupported;
    ha->Ops.ioctl = hndDefaultIoctl;
    ha->Ops.seek = hndSeekNotSupported;

    switch (type) {
    case HANDLE_TYPE_FILE:
        ha->Ops.read = uvfsRead;
        ha->Ops.seek = uvfsSeek;
        ha->Ops.close = uvfsClose;
	ha->Ops.write = uvfsWrite;
        break;
    case HANDLE_TYPE_DEVICE:
        break;
    case HANDLE_TYPE_PIPE:
        ha->Ops.read = upipeRead;
        ha->Ops.write = upipeWrite;
        ha->Ops.close = upipeClose;
        break;
    }

    vectorInsert(&proc->Handles, ha);
    return ha;
}

void hndDestroyHandle(struct handle* handle)
{
    khFree(handleCache, handle);
}

/* VFS */
int uvfsRead(struct handle* h, void* buffer, int length)
{
    if (!h->Resource)
        return -1;

    return vfsReadFile(h->Resource, buffer, length);
}

int uvfsSeek(struct handle* h, int relative, size_t offset)
{
    struct fs_file* file = h->Resource;
    switch (relative) {
    case SFILEPTR_REL_BEGIN:
        file->Offset = offset;
        break;
    case SFILEPTR_REL_CURRENT:
        file->Offset += offset;
        break;
    case SFILEPTR_REL_END:
        file->Offset = file->Size + offset;
        break;
    default:
        return -ERROR_INVALID_ARGUMENT;
    }

    return file->Offset;
}

void uvfsClose(struct handle* h)
{
    vfsCloseFile(h->Resource);
}

static int uvfsWrite(struct handle* h, const void* buffer, int length)
{
  return vfsWriteFile(h->Resource, buffer, length);
}

/* Pipe */
static int upipeRead(struct handle* h, void* buffer, int length)
{
    struct pipe_data* pd = h->Resource;
    if (!buffer || !length)
        return -ERROR_INVALID_ARGUMENT;

    if (pd->Broken)
        return -ERROR_PIPE_BROKEN;

    struct pipe_packet pack;
    while (!ringRead(pd->RingBuffer, &pack)) {
        if (h->Flags & HANDLE_NONBLOCK_BIT) {
            return -ERROR_WOULD_BLOCK;
        }

        schedEventResetFlag(pd->Event);
        schedEventPause(pd->Event);
    }

    if (pack.Broken) {
        pd->Broken = true;
        return -ERROR_PIPE_BROKEN;
    }

    if ((size_t)length > pack.Length)
        length = pack.Length;
    memcpy(buffer, pack.Data, length);
    khsFree(pack.Data);

    return length;
}

static int upipeWrite(struct handle* h, const void* buffer, int length)
{
    struct pipe_data* pd = h->Resource;
    if (pd->Broken) {
        return -ERROR_PIPE_BROKEN;
    }

    if (pd->Flags & PIPE_FLAGS_READ_TO_TTY) {
        trmLogl(buffer, length);
        return length;
    }

    if (pd->Flags & PIPE_FLAGS_INPUT_BUFFER) {
        /* It is expected that drivers send to applications.
           Not the other way. */
        return -ERROR_FORBIDDEN;
    }

    if (length > 4096) {
        return -ERROR_TOO_BIG;
    }

    struct pipe_packet pack;
    void* bu = khsAlloc(length);
    memcpy(bu, buffer, length);
    memset(&pack, 0, sizeof(pack));

    pack.Data = bu;
    pack.Length = length;

    schedEventRaise(pd->Event);
    ringWrite(pd->RingBuffer, &pack);
    return length;
}

void upipeClose(struct handle* h)
{
    struct pipe_data* pd = h->Resource;
    pipeBreak(pd);

    if (pd->ReferenceCount) {
        return;
    }

    if (!pd->Flags)
        ringDestroy(pd->RingBuffer);
    khsFree(pd);
}

void pipeBreak(struct pipe_data* pd)
{
    pd->ReferenceCount--;
    if (pd->ReferenceCount > 1 || pd->Flags) {
        return;
    }

    struct pipe_packet pack = {0};
    pack.Broken = 1;
    pack.Data = NULL;
    pack.Length = 0;
    
    ringWrite(pd->RingBuffer, &pack);
    schedEventRaise(pd->Event);
}

uint64_t syscCreatePipe(union sysc_regs* regs)
{
    __unused(regs);
    struct process* proc = pcGetCurrentProcess();

    struct pipe_data* pd = khsAlloc(sizeof(*pd));
    pd->ReferenceCount = 1;
    pd->Event = schedCreateEvent();
    pd->RingBuffer = ringInit(sizeof(struct pipe_packet), 24);

    struct handle* h = hndCreateHandle(proc, HANDLE_TYPE_PIPE);
    h->Resource = pd;
    return h->Identifier;
}

unsigned long hndDefaultIoctl(struct handle* h, unsigned long command, void* buffer,
                              size_t bufferLength)
{
    switch (command) {
    case IOCTL_HANDLE_SET_BLOCKING: {
        if (bufferLength != sizeof(uint8_t)) {
            return -ERROR_INVALID_RANGE;
        }

        uintptr_t value = (uintptr_t)buffer;

        h->Flags &= HANDLE_NONBLOCK_BIT;
        if (value) {
            h->Flags |= HANDLE_NONBLOCK_BIT;
        }

        return 0;
    }
    }

    return -ERROR_NO_SYSCALL;
}

uint64_t syscGetHandleType(union sysc_regs* regs)
{
  struct process* proc;
  struct handle* handle;
  unsigned int fd = regs->Arg1;

  proc = pcGetCurrentProcess();
  handle = pcGetHandleById(proc, fd);
  if (!handle)
    return -ERROR_BAD_HANDLE;

  return handle->Type;
}

uint64_t syscDuplicateHandle(union sysc_regs* regs)
{
  struct process* proc;
  struct handle* handle;
  struct handle* nh;
  
  int fd = regs->Arg1;

  proc = pcGetCurrentProcess();
  handle = pcGetHandleById(proc, fd);
  if (!handle)
    return -ERROR_BAD_HANDLE;

  nh = pcCreateHandle(proc, handle->Type, handle->Resource);
  if (ERROR_PTR(nh))
    return ERROR_FROM_PTR(nh);

  nh->Ops = handle->Ops;
  if (nh->Type == HANDLE_TYPE_PIPE)
    ((struct pipe_data*)nh->Resource)->ReferenceCount++;
  if (nh->Ops.open)
    nh->Ops.open(nh);
  
  return nh->Identifier;
}
