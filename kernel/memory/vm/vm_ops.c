/* vm_ops.c
   Purpose: VM operations (read, write ...) */
#include "power/avl.h"
#include "power/error.h"
#include "power/list.h"
#include "power/memory/hhdm.h"
#include "power/paging.h"
#include "power/scheduler.h"
#include <power/memory/page.h>

#include <power/mmap.h>
#include <power/assert.h>
#include <power/memory/vm.h>
#include <power/memory/memset.h>
#include <stddef.h>

static int write(struct page*, ptrdiff_t offset,
		 void* buffer, unsigned long length);

int vmWrite(struct vm* vm, uintptr_t address, void* buffer,
	    unsigned long length)
{
  uintptr_t old;
  ptrdiff_t offset;
  struct page* first;
  struct vm_region* region;

  old = address;
  schedSemaphoreAcquire(vm->Lock);
  region = avlLookupClosestNode(vm->Tree, &address);
  schedSemaphoreRelease(vm->Lock);

  if (unlikely(!region))
    return -ERROR_NOT_FOUND;
  
  if (address >= old && address + region->Length < old)
    return -ERROR_NOT_FOUND;

  offset = (old - address) / 4096;
  first = (struct page*)listAfter(&region->Page->List, offset);
  PANIC_IF(!first);

  if (likely(vm->AddressSpace == pgGetAddressSpace())
      && region->Protection & VPROT_WRITE) {
    memcpy((void*)old, buffer, length);
    return length;
  }
  
  return write(first, old & 0xFFF, buffer, length);
}

int write(struct page* page, ptrdiff_t offset,
	  void* buffer, unsigned long length)
{
  int distance = 0;
  unsigned long wrote = 0;
  unsigned long rev = length;
  if (rev < (unsigned long)offset) {
    offset = 0;
  }
  
  while (wrote < length) {
    if (unlikely(!page)) {
      break;
    }
    
    distance = min(rev, 4096) - offset;
   
    memcpy(mmMakeWritable(page->Address + offset), buffer, distance);
    
    offset = 0;
    wrote += distance;
    buffer += distance;
    
    rev -= distance;
    page = (struct page*)page->List.Next;
  }

  return wrote;
}
