/* vm_mmap.c
   Purpose: the implementation of VirtualMap. */
#include "power/config.h"
#include "power/error.h"
#include "power/list.h"
#include "power/memory/heap.h"
#include "power/paging.h"
#include "power/system.h"
#include <power/memory/vm.h>
#include <power/memory/user_buffer.h>
#include <power/memory/pfa.h>
#include <power/memory/memset.h>
#include <power/memory/vaddress.h>
#include <power/mmap.h>

#include <power/avl.h>
#include <power/vfs.h>

#include <power/scheduler.h>
#include <stdint.h>

static struct vm_region*
setupRegion(struct vm* vm, uintptr_t hint, int length,
	    int flags, int pflags);

static intptr_t
getHint(struct vm* vm, uintptr_t address);

static 
void anonUnmap(struct vm* vm, struct vm_region* reg, size_t pages)
{
  int delete;
  struct page* pag;

  delete = reg->Page->RefCount <= 0;
  
  pag = reg->Page;
  reg->Length -= pages;
  if (reg->Length && delete) {
    reg->Page = (struct page*)listAfter(&reg->Page->List, pages);
    if (reg->Page && reg->Page->List.Previous) {
      reg->Page->List.Previous->Next = NULL;
      reg->Page->List.Previous = NULL;
    }
  }
  
  schedSemaphoreAcquire(vm->Lock);
  avlRemove(vm->Tree, reg->Page->VirtualAddress);
  schedSemaphoreRelease(vm->Lock);
  
  while (pag->List.Next) {
    if (delete)
      mmFreePage(pag->Address);
    pgUnmapPage(vm->AddressSpace, pag->VirtualAddress);
    pag = (struct page*)pag->List.Next;
  }
}

/* Creates an optionally anonymous mapping and
   return its address. */
void* vmVirtualFileMap(struct vm* vm, uint64_t hint, size_t length, 
                       struct fs_file* file, int flags)
{
  int pages, error;

  intptr_t h = hint;
  uintptr_t pgflags;
  
  struct vm_region* region;
  
  if (!length && file)
    length = file->Size;
  
  if (length % 4096) {
    length = alignUp(length, 4096);
  }

  pgflags = vmFlagsIntoPagingFlags(flags);

  if (!hint)
    hint = __VFM_BEGIN;
  
  if (hint < 0x1000) {
    hint = 0x1000;
  }

  pages = length / 4096;
  if (unlikely((h = getHint(vm, hint)) < 0))
    hint = 0;
  else
    hint = h;
  
  region = setupRegion(vm, hint, pages, flags, pgflags);
  if (ERROR_PTR(region)) {
    return region;
  }

  if (flags & VMAP_ANONYMOUS) {
    region->Flags |= VM_REG_ANON_BIT;
    
    if (flags & VMAP_POPULATE) {
      if ((error = vmPopulateRegion(vm, region)) < 0) {
	return ERROR_INTO_PTR((long)error);
      }
    }

    goto ret;
  }
  
  region->BackingFile = file;
  if (!file)
    return ERROR_INTO_PTR(-ERROR_INVALID_ARGUMENT);

  if ((unsigned long)region->Length * 4096 > file->Size) {
    region->Length = file->Size;
    region->Length += (4096 - (file->Size % 4096));
    region->Length /= 4096;
  }

  vmPopulateFileMapping(vm, region);
 ret:
  return (void*)region->Page->VirtualAddress;
}

/* Removes any established mapping located inside
   address from VM. */
void vmVirtualFileUnmap(struct vm* vm, void* buffer, int length)
{  
  if (length % 4096) {
    length = alignUp(length, 4096);
  }

  struct vm_region* reg;
  int pages = length / 4096;

  schedSemaphoreAcquire(vm->Lock);
  reg = avlLookup(vm->Tree, (uintptr_t)buffer);
  if (!reg)
    goto release;

  reg->Page->RefCount--;  
  anonUnmap(vm, reg, pages);
  
  vm->AllocatedBytes -= pages * 4096;
  vm->AllocatedRegions--;
 release:
  schedSemaphoreRelease(vm->Lock);
}

struct vm_region*
setupRegion(struct vm* vm, uintptr_t hint, int length, int flags, int pflags)
{
  struct vm_region* reg;
  uintptr_t effad, phs;
  
  reg = vmCreateRegion();
  if (!reg)
    return ERROR_INTO_PTR(-ERROR_NO_MEMORY);

  reg->Flags |= VM_REG_ANON_BIT;
  if (flags & VMAP_USER && mmUserPointerCheck(hint))
    hint = 0;

  struct page* first = mmAllocPage(1);
  if (!first)
    return ERROR_INTO_PTR(-ERROR_NO_MEMORY);

  effad = hint;
  phs = (uintptr_t)first->Address;
  if (!effad) {
    phs &= (0xFFFFFFFF << 12);
    effad = __VFM_BEGIN + phs;
  }

  if (effad != hint && flags & VMAP_FIXED) {
    mmFreePage(first->Address);
    return ERROR_INTO_PTR(-ERROR_UNAVAILABLE);
  }

  first->VirtualAddress = effad;
  reg->Page = first;
  reg->Protection = flags & VPROT_MASK;
  reg->CreationFlags = flags;
  
  reg->Advice = 0;
  reg->Length = length;

  pgMapPage(vm->AddressSpace, (uintptr_t)first->Address, effad, pflags);
  first->RefCount++;
  
  for (int i = 1; i < length; i++)
    pgGuardPage(vm->AddressSpace, effad + (i * 4096), 0, pflags);

  schedSemaphoreAcquire(vm->Lock);
  avlInsert(vm->Tree, effad, reg);
  schedSemaphoreRelease(vm->Lock);
  
  vm->AllocatedBytes += 4096;
  vm->AllocatedRegions++;
  return reg;
}

intptr_t getHint(struct vm* vm, uintptr_t address)
{
  intptr_t old;
  struct vm_region* region;

  old = address;
  
  schedSemaphoreAcquire(vm->Lock);
  region = avlLookupClosestNode(vm->Tree, &address);
  schedSemaphoreRelease(vm->Lock);

  if (unlikely(!region))
    return old;

  if (old >= (intptr_t)address &&
      ((intptr_t)address + region->Length * 4096 > old))
    return -ERROR_UNAVAILABLE;

  return old;
}

uintptr_t vmMapShared(struct vm* src, struct vm* dest,
		      uintptr_t address, int flags)
{
  __unused(flags);
  
  intptr_t hint;
  uintptr_t pflags;
  struct page* page;
  struct vm_region* region;

  if (unlikely(vmCompareVm(src, dest))) {
    return address;
  }
  
  schedSemaphoreAcquire(src->Lock);
  region = avlLookup(src->Tree, address);
  schedSemaphoreRelease(src->Lock);

  if (!region)
    return -ERROR_NOT_FOUND;

  hint = getHint(dest, address);
  if (hint < 0)
    /* TODO: Allow mapping to other address if lax. */
    return hint;

  schedSemaphoreAcquire(dest->Lock);
  avlInsert(dest->Tree, address, region);
  schedSemaphoreRelease(dest->Lock);

  pflags = vmFlagsIntoPagingFlags(region->CreationFlags);
  
  listForEach(region->Page, l) {
    page = (struct page*)l;    
    pgMapPage(dest->AddressSpace, (uintptr_t)page->Address,
	      page->VirtualAddress, pflags);
  }
  
  return region->Address;
}

int vmPopulateRegion(struct vm* vm, struct vm_region* region)
{
  struct page* p;

  unsigned long i;

  uintptr_t pgflags;
  uintptr_t address;

  /* Already populated! */
  if (region->Length == 1)
    return 0;
  
  address = region->Page->VirtualAddress;
  pgflags = vmFlagsIntoPagingFlags(region->CreationFlags);  
  
  /* One page is already allocated by setupRegion. */
  struct page* pages = mmAllocPage(region->Length - 1);
  if (unlikely(!pages)) {
    return -ERROR_NO_MEMORY;
  }
  
  listAppend(&region->Page->List, &pages->List);

  i = 1;
  listForEach(pages, l) {
    p = (struct page*)l;
    p->VirtualAddress = address + (4096 * i);

    pgMapPage(vm->AddressSpace, (uintptr_t)p->Address, p->VirtualAddress,
	      pgflags);
    i++;
  }
  
  return 0;
}

