/* vm_tmpvm.c
   Purpose: temporary VM for in-kernel file reading */
#include "power/i386/pgtables.h"
#include <power/memory/vm.h>

static struct vm* rootVm = NULL;
extern address_space_t* rootHeapAddressSpace;

/* The Temporary VM is used to as a scratch buffer for
   reading files and performing other operations without
   fragmenting the whole physical memory! It is only used
   until secondInit calls the user init, right before that
   the TVM is destroyed. */

struct vm* vmGetTemporary()
{
  if (!rootVm)
    rootVm = vmInit(rootHeapAddressSpace);

  return rootVm;
}

void vmDestroyTemporary()
{
  if (!rootVm)
    return;

  vmClearRegions(rootVm);
  vmDestroy(rootVm);
  
  rootVm = NULL;
}
