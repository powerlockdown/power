/* vm_estblsh.c
   Purpose: established mappings. */
#include "power/avl.h"
#include "power/error.h"
#include "power/list.h"
#include "power/memory/hhdm.h"
#include "power/memory/pfa.h"
#include "power/paging.h"
#include "power/scheduler.h"
#include <power/memory/vm.h>

static struct page* getpages(uintptr_t address, int length);
static struct page* createboguspages(uintptr_t address, int length);
static void removeboguspages(struct page*, int length);

/* Forcefully establishes a mapping of a region onto the VM.
   In other words, create a (anonymous) mapping in which
   the physical memory lifetime is not managed by us.
   
   Note that this function does not preinitialize the pointed
   memory to any value. The caller must be sure the memory is to
   be trusted or initialize it itself. */
int vmEstablishMapping(struct vm* vm, uintptr_t* h, void* phys,
                        size_t length, int flags)
{
    if (length % 4096)
        length = alignUp(length, 4096);
    
    uintptr_t hint = *h;
    uint64_t pgflags = vmFlagsIntoPagingFlags(flags);

    if (!hint)
        hint = __VFM_BEGIN;
    
    if (hint < 0x1000) {
        hint = 0x1000;
    }

    int pages = length / 4096;
    struct vm_region* region = vmCreateRegion();
    if (!region) {
        return -ERROR_NO_MEMORY;
    }

    region->Flags = VM_REG_ESTBLSH_BIT;
    region->Length = pages;

    struct page* p, *next;
    if (!(p = mmGetPage(phys))) {
        p = createboguspages((uintptr_t)phys, pages);
        region->Flags |= VM_REG_BOGUS_PAGES_BIT;
    } else {
        next = getpages((uintptr_t)phys + 4096, pages - 1);
        listInsert(&p->List, &next->List);
    }

    region->Page = p;

    int i = 0;
    struct page* w;
    uintptr_t un;
    listForEach(p, v) {
        w = (struct page*)v;

        un = mmMakeUnwritable(w->Address);
        pgMapPage(vm->AddressSpace, un, 
                  hint + (i * 4096), pgflags);
        i++;
    }

    schedSemaphoreAcquire(vm->Lock);

    avlInsert(vm->Tree, hint, region);
    schedSemaphoreRelease(vm->Lock);

    return 0;
}

/* Removes a previously established mapping. */
int vmUnstablishMapping(struct vm* vm, uintptr_t addr)
{
    struct vm_region* r;

    schedSemaphoreAcquire(vm->Lock);
    r = avlRemove(vm->Tree, addr);
    schedSemaphoreRelease(vm->Lock);

    if (!r) {
        return -ERROR_NOT_FOUND;
    }

    int i = 0;
    struct page* p = r->Page;
    listForEach(p, v) {
        pgUnmapPage(vm->AddressSpace, addr + (i * 4096));
        i++;
    }

    if (r->Flags & VM_REG_BOGUS_PAGES_BIT) {
        removeboguspages(p, r->Length);
    }

    return 0;
}

/* Creates and links the bogus pags. */
struct page* createboguspages(uintptr_t address, int length)
{
    struct page* page, *head = NULL;
    for (int i = 0; i < length; i++) {
        page = vmCreatePage(head, 
                            address + (i * 4096));
        if (!head) {
            head = page;
        }
    }

    return head;
}

void removeboguspages(struct page* page, int length)
{
    struct page* p = page;
    struct page* next = NULL;
    for (; next; p = next) {
        next = (struct page*)p->List.Next;
        vmDestroyPage(p);
    }
}

struct page* getpages(uintptr_t address, int length)
{
    struct page* page, *head = NULL;
    for (int i = 0; i < length; i++) {
        page = mmGetPage((void*)address + (i * 4096));
        if (!head) {
            head = page;
        }
    }

    return head;
}
