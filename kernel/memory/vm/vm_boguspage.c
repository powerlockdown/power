/* vm_boguspage.c
   Purpose: Support for bogus pages */

#include "power/list.h"
#include <power/memory/page.h>
#include <power/memory/heap.h>

/* If the caller has to establish a mapping 
   (see vm_estblsh.c) which points to memory not
   marked as free by the firmware/bootloader, we
   have to forge our own struct pages as the
   physical memory manager does not create
   them for us to use. */

static struct mm_cache* bpCache;

/* Allocate and initialize a bogus page. 
   Note the address param is the physical address.*/
struct page* 
vmCreatePage(struct page* list, uintptr_t address)
{
    struct page* pg;
    if (!bpCache) {
        bpCache = khCreateCache("bogus struct page", sizeof(*pg));
    }

    pg = khAlloc(bpCache);
    pg->Address = (void*)address;
    
    if (list)
        listInsert(&list->List, &pg->List);
    return pg;
}

void vmDestroyPage(struct page* page)
{
    listRemove(&page->List);
    khFree(bpCache, page);
}
