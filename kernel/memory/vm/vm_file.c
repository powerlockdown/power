/* vm_file.c
   Purpose: file mapping */
#include "power/list.h"
#include "power/memory/hhdm.h"
#include "power/memory/pfa.h"
#include "power/memory/user_buffer.h"
#include "power/paging.h"
#include "power/tty.h"
#include "power/vfs.h"
#include <power/memory/vm.h>
#include <power/error.h>

static int readFile(struct fs_file* file, void* buffer, unsigned long length);

/* Allocate memory and read the file onto it.
   The region is presumed to be as it was left
   by mmap. */
int vmPopulateFileMapping(struct vm* vm, struct vm_region* region)
{
  int le;
  int i = 0, sads;
  
  struct page* pages;
  struct page* p;

  uintptr_t sz;
  uintptr_t flags;

  sads = pgGetAddressSpace() == vm->AddressSpace;
  flags = vmFlagsIntoPagingFlags(region->CreationFlags);
  flags |= PT_FLAG_WRITE;
  
  le = region->Length - 1;

  /* Same address space? Just populate and
     read normally. */
  if (likely(sads)) {
    vmPopulateRegion(vm, region);
    sz = readFile(region->BackingFile, (void*)region->Page->VirtualAddress,
		  region->Length * 4096);
    if (sz < 0)
      return sz;
    return 0;
  }
  
  pages = mmAllocPage(le);
  listInsert(&region->Page->List, &pages->List);

  vm->AllocatedBytes += le * 4096;
  listForEach(region->Page, l) {
    p = (struct page*)l;

    p->VirtualAddress = region->Page->VirtualAddress + (i * 4096);
    if (likely(p != region->Page)) {
      pgMapPage(vm->AddressSpace, mmMakeUnwritable(p->Address),
		p->VirtualAddress, flags);
    }

    /* If we are not on the same addr space as
       the VM, we have to be inefficient. */
    if (unlikely(!sads)) {
      readFile(region->BackingFile, p->Address, 4096);
    }
    
    i++;
  }

  return 0;
}

/* vfsreadfile but with better error reporting */
int readFile(struct fs_file* file, void* buffer, unsigned long length)
{
  int sz;

  /* Such clamping should be done by FS. */
  if (length > file->Size)
    length = file->Size;
  
  sz = vfsReadFile(file, buffer, length);

  if (sz < 0) {
    trmLogfn("mmap(FILE): failed to read file: %p", sz);
  }

  return sz;
}

