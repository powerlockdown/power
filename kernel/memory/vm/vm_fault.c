/* vm_fault.c
   Purpose: Virtual Memory page fault */
#include "power/avl.h"
#include "power/error.h"
#include "power/list.h"
#include "power/memory/pfa.h"
#include "power/paging.h"
#include "power/scheduler.h"
#include <power/memory/vm.h>

static inline
int getRegionAddress(struct vm*, uintptr_t address,
		     struct vm_region**);
static inline
int getGuardedRegionAddress(struct vm*, uintptr_t address,
			    struct vm_region**);

static int
handleGuardedPage(struct vm*, uintptr_t address);

static inline
int readPageFile(struct vm*, struct vm_region*, uintptr_t);

int vmHandleFault(struct vm* vm, uintptr_t address, int flags)
{
  address &= ~0xFFF;

  int error;
  struct vm_region* region;
  if (likely(flags & VFAULT_GUARDED))
    error = getGuardedRegionAddress(vm, address, &region);
  else
    error = getRegionAddress(vm, address, &region);

  if (error)
    return error;

  if (flags & VFAULT_GUARDED) {
    return handleGuardedPage(vm, address);
  }
  
  return 0;
}

int handleGuardedPage(struct vm* vm, uintptr_t address)
{
  void* allc;
  struct page* page;
  struct vm_region* region;

  int error;
  if ((error = getGuardedRegionAddress(vm, address, &region))) {
    return error;
  }
  
  allc = pgUnguardPage(vm->AddressSpace, address);
  if (ERROR_PTR(allc)) {
    return ERROR_FROM_PTR(allc);
  }

  page = mmGetPage(allc);
  if (!region->Page)
    region->Page = page;
  else
    listInsert(&region->Page->List, &page->List);

  page->VirtualAddress = (address & ~0xFFF);
  vm->AllocatedBytes += 4096;
  return 0;
}


int getGuardedRegionAddress(struct vm* vm, uintptr_t address,
			    struct vm_region** reg)
{
  struct vm_region* region;
  schedSemaphoreAcquire(vm->Lock);
  region = avlLookupClosestNode(vm->Tree, &address);
  schedSemaphoreRelease(vm->Lock);

  if (!region)
    return -ERROR_NOT_FOUND;

  *reg = region;
  return 0;
}

int getRegionAddress(struct vm* vm, uintptr_t address, struct vm_region** reg)
{
  void* p;
  struct page* page;
  struct list_item* list;

  p = pgGetPhysicalAddress(vm->AddressSpace, address);
  if (!p)
    return -ERROR_NOT_FOUND;

  page = mmGetPage(p);
  if (!page)
    return -ERROR_NOT_FOUND;

  list = &page->List;
  while (list->Previous) {
    list = list->Previous;
  }

  page = (struct page*)list;
  schedSemaphoreAcquire(vm->Lock);

  *reg = avlLookup(vm->Tree, (uintptr_t)page->Address);
  schedSemaphoreRelease(vm->Lock);

  if (!*reg)
    return -ERROR_NOT_FOUND;
  
  return 0;
}

