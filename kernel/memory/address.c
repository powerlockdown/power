#include <power/memory/vaddress.h>
#include <power/paging.h>
#include <power/memory/page.h>

#include <power/memory/pfa.h>
#include <power/tty.h>
#include <power/elf.h>

#include <power/vector.h>
#include <power/memory/heap.h>

#define ADDRESS_DATA_OFFSET 0x40000000000

struct allocated_page {
    uint64_t Offset;
    size_t Length;  
};

struct address_space_mgr 
{
    struct vector Pages;
    address_space_t* AddressSpace;
    struct page* Tree;
};

static struct vector cr3vector;

struct address_space_mgr* 
addrGetManagerForCr3(address_space_t* addr)
{
    static bool init = false;
    if (!init) {
        cr3vector = vectorCreate(5);
        init = true;
    }

    if (!addr) addr = pgGetAddressSpace();
    struct address_space_mgr* chosen = NULL;

    for (size_t i = 0; i < cr3vector.Length; i++) {
        struct address_space_mgr* d = cr3vector.Data[i];
        if (d->AddressSpace == addr) {
            chosen = cr3vector.Data[i];
        }
    }

    if (!chosen) {
        chosen = khsAlloc(sizeof(*chosen));
        chosen->AddressSpace = addr;
        chosen->Pages = vectorCreate(3);

        vectorInsert(&cr3vector, chosen);
    }
    
    return chosen;
}

bool asmgrClaimPage(struct address_space_mgr* spa, 
                    uint64_t offset, size_t length)
{
    struct allocated_page* pg = NULL;
    for (size_t i = 0; i < spa->Pages.Length; i++) {
        pg = spa->Pages.Data[i];
        if (offset == pg->Offset) {
            return false;
        }

        if (offset > pg->Offset
         && pg->Offset + (pg->Length * 4096) > offset) {
            return false;
        }
    }

    pg = khsAlloc(sizeof(*pg));
    pg->Length = length;
    pg->Offset = offset;
    
    vectorInsert(&spa->Pages, pg);
    return true;
}

uint64_t asmgrCorrectPage(struct address_space_mgr* spa, 
                          uint64_t offset)
{
    struct allocated_page* pg = NULL;
    for (size_t i = 0; i < spa->Pages.Length; i++) {
        pg = spa->Pages.Data[i];

        if (pg->Offset == offset) {
            offset += pg->Length * 4096;
        } 
    }

    return offset;
}

uint64_t asmgrGetDataPage(struct address_space_mgr* spa, size_t length)
{
    /* length might be in non-page aligned value */
    return asmgrClaimPage(spa, ADDRESS_DATA_OFFSET, length);
}

void asmgrReturnPage(struct address_space_mgr* spa, uint64_t offset)
{
    struct allocated_page* pg = NULL;
    for (size_t i = 0; i < spa->Pages.Length; i++) {
        pg = spa->Pages.Data[i];
        if (offset == pg->Offset) {
            break;
        }

        pg = NULL;
    }
    
    if (!pg)
        return;

    vectorRemove(&spa->Pages, pg);
    khsFree(pg);
}
