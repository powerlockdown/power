#include <power/memory/pool.h>
#include <power/memory/pfa.h>
#include <power/memory/hhdm.h>

#include <power/elf.h>

struct mm_pool {
    int Size;
    int Alignment;
    int Stride;
    struct mm_pool_block* FreeList;
};

struct mm_pool_block {
    struct mm_pool_block* Next;
};

static void* fetchPage(struct mm_pool* pl);
static void* fillFreeList(struct mm_pool* pl, void* begin, long length)
{
    struct mm_pool_block* block, *next, *first;
    void* b = (void*)(alignUp(((uintptr_t)begin), pl->Alignment));
    block = PaAdd(b, pl->Size);
    first = block;

    int stride = pl->Size + sizeof(*block);

    length -= (b - begin);
    next = PaAdd(block, pl->Size + sizeof(*next));
    while (length > 0) {
        if ((((uintptr_t)next) - pl->Size) & (pl->Alignment - 1)) {
            uintptr_t old = (((uintptr_t)next) - pl->Size);
            uintptr_t new = alignUp(old, pl->Alignment);

            next = (void*)new + pl->Size;
        }

        block->Next = next;

        block = next;
        next = PaAdd(next, pl->Size + sizeof(*next));
        length -= stride;
    }

    return first;
}

struct mm_pool* poolInit(size_t objectSize, int align)
{
    void* page = mmAlignedAlloc(4096, 4096);
    struct mm_pool* pool = page;
    pool->Size = objectSize;
    pool->Alignment = align;
    pool->FreeList = fillFreeList(pool, page + sizeof(struct mm_pool),
                                  4096 - sizeof(struct mm_pool));
    return pool;
}

void* poolAllocObject(struct mm_pool* pl)
{
    if (!pl->FreeList && !fetchPage(pl)) {
        return NULL;
    }

    struct mm_pool_block* object = pl->FreeList;
    pl->FreeList = object->Next;
    object->Next = NULL;
    return ((void*)object - pl->Size);
}

void poolFreeObject(struct mm_pool* pl, const void* ptr)
{
    struct mm_pool_block* block = pl->FreeList;
    while (block->Next != NULL) {
        block = block->Next;
    }

    struct mm_pool_block* bl = (struct mm_pool_block*)(ptr + pl->Size);
    block->Next = bl;
}

void* fetchPage(struct mm_pool* pl)
{
    void* page = mmAlignedAlloc(4096, 4096);
    if (!page) {
        return NULL;
    }

    pl->FreeList = fillFreeList(pl, page, 4096);
    return page;
}
