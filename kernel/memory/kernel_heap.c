#include <power/memory/heap.h>
#include <power/memory/vaddress.h>
#include <power/paging.h>

#include <power/memory/pfa.h>
#include <power/memory/zoned.h>
#include <power/tty.h>
#include <power/vector.h>

#include <power/memory/memset.h>

#define BIG_THRESHOLD 1024

#define GET_SLAB(P) (void*)((uint64_t)(P) & ~0xFFF);

#define SLAB_STATE_FREE    1
#define SLAB_STATE_PARTIAL 2
#define SLAB_STATE_FULL    3

#define getFreeArray(B, i) (((unsigned int*)&(B)->FreeItemsIndex) + (i))
#define FREE_END           0U

extern void* rootHeapAddressSpace;

struct mm_cache {
    const char* Name;
    size_t ObjectSize;

    mm_ctor construct;
    mm_ctor destroy;
  
    struct mm_slab* FullSlabs;
    struct mm_slab* PartialSlabs; /*< Or free slabs. */
};

struct mm_sized_cache {
    size_t Size;
    struct mm_cache* Cache;
};

struct mm_slab {
    struct mm_slab* Next;
    struct mm_cache* Cache;

    void* Array;
    void* LastFree;

    unsigned char State;
    unsigned int AllocatedObjects;
    unsigned int FreeItemsIndex; /*< Array */
};

struct mm_buffer {
    uint64_t Reserved;
    struct mm_cache* Cache; /*< This needs to match mm_slab.Cache offset. */
};

bool heapZonesInit = false;
struct zoned heapZones;
static struct mm_sized_cache genericCaches[] = {
    {     32, NULL },
    {     48, NULL },
    {     64, NULL },
    {     96, NULL },
    {    128, NULL },
    {    176, NULL },
    {    240, NULL },
    {    304, NULL },
    {    400, NULL },
    {    512, NULL },
    {    656, NULL },
    {    832, NULL },
    {   1056, NULL },
    {   1328, NULL },
    {   1680, NULL },
    {   2128, NULL },
    {   2688, NULL },
    {   3392, NULL },
    {   4272, NULL },
    {   5376, NULL },
    {   6752, NULL },
    {   8480, NULL },
    {  10656, NULL },
    {  13392, NULL },
    {  16832, NULL },
    {  21152, NULL },
    {  26576, NULL },
    {  33392, NULL },
    {  41952, NULL },
    {  52688, NULL },
    {  66176, NULL },
    {  83104, NULL },
    { 104368, NULL },
    { 131072, NULL },
};

static void* getFreePage(int count);
static void removeFreePage(void* page);
static void slabAppend(struct mm_slab** slab, struct mm_slab* item);
static void slabRemove(struct mm_slab** slab, struct mm_slab* item);

static void* __khAlloc(struct mm_cache* cache);

static
void defaultconstruct(void* object, size_t length)
{
  memset(object, 0, length);
}

static
void defaultdestroy(void* object, size_t length)
{
  memset(object, 0, length);
}

static struct mm_cache cacheCache = {
    .Name = "cache_cache",
    .ObjectSize = sizeof(struct mm_cache),
    .PartialSlabs = NULL,
    .FullSlabs = NULL,
    .construct = defaultconstruct,
    .destroy = defaultdestroy,
};


struct mm_cache* khCreateCache(const char* name, size_t objectSize)
{
  return khCreateCacheEx(name, objectSize, NULL, NULL);
}

struct mm_cache* khCreateCacheEx(const char* name, size_t objectSize,
				 mm_ctor ctor, mm_ctor dtor)
{
    struct mm_cache* cach = khAlloc(&cacheCache);
    memset(cach, 0, sizeof(*cach));

    cach->Name = name;
    cach->ObjectSize = max(objectSize, 16);
    if (!ctor)
      ctor = defaultconstruct;

    if (!dtor)
      dtor = defaultdestroy;
    
    cach->construct = ctor;
    cach->destroy = dtor;
    
    return cach;
}

void khFreeCache(struct mm_cache* cache)
{
    struct mm_slab* slab = cache->FullSlabs;
    while (slab && slab->Next != NULL) {
        removeFreePage(slab);
        slab = slab->Next;
    }

    slab = cache->PartialSlabs;
    while (slab && slab->Next != NULL) {
        removeFreePage(slab);
        slab = slab->Next;
    }

    khFree(&cacheCache, cache);
}

static inline
void becomeFull(struct mm_slab* slab)
{
    struct mm_cache* cache;
    cache = slab->Cache;

    slab->State = SLAB_STATE_FULL;
    slabRemove(&cache->PartialSlabs, slab);
    slabAppend(&cache->FullSlabs, slab);
    slab->Next = NULL;
}

void* __khAlloc(struct mm_cache* cache)
{
    if (!cache->PartialSlabs) {
        /* No free pages left. We must grow. */
        khGrowCache(cache);
    }
    
    struct mm_slab* slab = cache->PartialSlabs;
    if (!slab)
      return NULL;    

    slab->AllocatedObjects++;
    if (slab->FreeItemsIndex == FREE_END) {
        becomeFull(slab);

        if (cache->ObjectSize < 2048) {
            khGrowCache(cache);
            slab = cache->PartialSlabs;
        } else {
            // slab->FreeItemsIndex = FREE_END;
        }
    }

    unsigned clamped = max(1, slab->FreeItemsIndex) - 1;
    unsigned int offset = (unsigned)clamped * (unsigned)cache->ObjectSize;

    void* object = PaAdd(slab->Array, offset);
    unsigned int oldfi = __max(slab->FreeItemsIndex, 1);

    if (oldfi != FREE_END) {
        slab->FreeItemsIndex = *getFreeArray(slab, oldfi);
    }
    
    if (slab->State == SLAB_STATE_FREE) {
        slab->State = SLAB_STATE_PARTIAL;
	if (cache->ObjectSize > 2048) {
	    becomeFull(slab);
	}
    }
    
    return object;
}

void* khAlloc(struct mm_cache* cache)
{
    void* buffer = __khAlloc(cache);
    return buffer;
}

void khFree(struct mm_cache* cache, const void* ptr)
{
    void* callee = __builtin_return_address(0);
    __khFree(cache, ptr, callee);
}

void __khFree(struct mm_cache* cache, const void* ptr, void* callee)
{
    /* All slabs are page-sized and aligned. */
    struct mm_slab* slab = (void*)((uint64_t)ptr & ~0xFFF);
    int index = ((ptr - slab->Array) / cache->ObjectSize) + 1;

    slab->AllocatedObjects--;
    if (cache->ObjectSize > 2048)
      trmLogfn("Huge free! %p block from cache %s (%i) freed.", ptr, cache->Name, cache->ObjectSize);

    if (slab->State == SLAB_STATE_FULL) {
        slab->State = SLAB_STATE_PARTIAL;
        slabRemove(&cache->FullSlabs, slab);
        slabAppend(&cache->PartialSlabs, slab);
        slab->Next = NULL;
    }

    if (!slab->AllocatedObjects) {
        slab->State = SLAB_STATE_FREE;
    }

    unsigned int* array = getFreeArray(slab, index);
    
    (*array) = slab->FreeItemsIndex;
    slab->FreeItemsIndex = index;
    slab->LastFree = callee;

    cache->destroy((void*)ptr, cache->ObjectSize);
}

void* khsRealloc(const void* ptr, size_t nsize)
{
    if (!ptr) {
        return khsAlloc(nsize);
    }

    struct mm_slab* slab = GET_SLAB(ptr);
    if (slab->Cache->ObjectSize >= nsize) {
        return (void*)ptr;
    }

    void* al = khsAlloc(nsize);

    if (ptr) {
        memcpy(al, ptr, slab->Cache->ObjectSize);
        __khFree(slab->Cache, ptr, __builtin_return_address(0));
    }

    return al;
}

void khGrowCache(struct mm_cache* cache)
{
    struct mm_slab* slab;
    int pageCount =
        (cache->ObjectSize >= 4096 ? cache->ObjectSize / 4096 : 0) + 1;
    void* page = getFreePage(pageCount);
    int offset = sizeof(struct mm_slab);

    slab = page;
    int objCount = ((pageCount * 4096) - offset) / cache->ObjectSize;
    int arraySize = ((sizeof(int) * objCount));

    offset += arraySize;
    memset(page, 0, offset);
    objCount = ((pageCount * 4096) - offset) / cache->ObjectSize;

    slab->Array = PaAdd(slab, sizeof(struct mm_slab) + arraySize);
    int i, val;    
    for (i = 0; i < objCount; i++) {
        unsigned int* p = getFreeArray(slab, i);
        val = i + 1;
        (*p) = val;
	
        /* Place the buffer end marker */
        if (i + 1 == objCount) {
            (*p) = FREE_END;
	    continue;
        }

	cache->construct(slab->Array + (i * cache->ObjectSize), cache->ObjectSize);
    }

    slab->AllocatedObjects = 0;
    slab->State = SLAB_STATE_FREE;
    slab->Cache = cache;
    slab->Next = NULL;
    slabAppend(&cache->PartialSlabs, slab);
}

void* getFreePage(int count)
{
    if (!heapZonesInit) {
        zpgInit(KERNEL_HEAP_BEGIN, KERNEL_HEAP_MAX_SIZE, 4096, ZN_USE_POOL_BIT,
                &heapZones);
        heapZonesInit = true;
    }

    uint64_t pg;
    zpgRequestRegion(&heapZones, count * 4096, &pg);
    for (int i = 0; i < count; i++) {
        void* program = mmAlignedPhysicalAlloc(4096, 4096);
        pgMapPage(rootHeapAddressSpace, (uint64_t)program, pg + (i * 4096),
                  PT_FLAG_WRITE | PT_FLAG_NX);
        pgMapPage(NULL, (uint64_t)program, pg + (i * 4096),
                  PT_FLAG_WRITE | PT_FLAG_NX);
    }

    return (void*)pg;
}

static void removeFreePage(void* page)
{
    void* program = pgGetPhysicalAddress(NULL, (uint64_t)page);

    pgUnmapPage(NULL, (uint64_t)page);
    mmAlignedFree(program, 4096);
    zpgReturnRegion(&heapZones, (uintptr_t)page, 0x1000);
}

void slabAppend(struct mm_slab** slab, struct mm_slab* item)
{
    if (!*slab) {
        (*slab) = item;
        return;
    }

    struct mm_slab* tmp = *slab;
    while (tmp->Next != NULL) {
        tmp = tmp->Next;
    }

    tmp->Next = item;
}

void slabRemove(struct mm_slab** slab, struct mm_slab* item)
{
    if (!*slab) {
        return;
    }
    if (*slab == item) {
        *slab = item->Next;
        return;
    }

    struct mm_slab* tmp = *slab;
    while (tmp->Next != NULL && tmp->Next != item) {
        tmp = tmp->Next;
    }

    tmp->Next = item->Next;
    (*slab)->Next = NULL;
}

void* khsAlloc(size_t size)
{
    int i = 0;
    struct mm_sized_cache* chosen = NULL;
    int length = sizeof(genericCaches) / sizeof(struct mm_sized_cache);
    while (i < length) {
        if (genericCaches[i].Size < size) {
            i++;
            continue;
        }

        chosen = &genericCaches[i];
        break;
    }

    if (!chosen) {
        trmLogfn("generic sized object is too big (size: %i)", size);
        return NULL;
    }

    if (!chosen->Cache) {
        chosen->Cache = khCreateCache("sized", chosen->Size);
    }

    void* ptr = khAlloc(chosen->Cache);

    return ptr;
}

void khsFree(const void* ptr)
{
    struct mm_slab* slab = (struct mm_slab*)((uint64_t)ptr & ~0xFFF);
    if (!slab) {
        trmLogfn("slab is NULL; callee=%p", __builtin_return_address(0));
    }

    __khFree(slab->Cache, ptr, __builtin_return_address(0));
}

void khShrinkToFit(struct mm_cache* cache)
{
    struct vector vec = vectorCreate(9);
    struct mm_slab* slab = cache->PartialSlabs;
    while (slab && slab->Next) {
        if (slab->State == SLAB_STATE_FREE) {
            vectorInsert(&vec, slab);
        }

        slab = slab->Next;
    }

    for (size_t i = 0; i < vec.Length; i++) {
        slab = vec.Data[i];
        slabRemove(&cache->PartialSlabs, slab);
        removeFreePage(slab);
    }

    vectorDestroy(&vec);
}

void khRemapHeap(address_space_t* addr)
{
    if (!heapZonesInit) {
        return;
    }

    if (!addr) {
        addr = pgGetAddressSpace();
    }

    int length;
    void* program;
    struct zoned_region* reg = heapZones.RegionList;
    while (reg != NULL) {
        length = reg->Length;
        for (int i = 0; i < length; i += 4096) {
            program =
                pgGetPhysicalAddress(rootHeapAddressSpace, reg->Begin + i);
            pgMapPage(addr, (uint64_t)program, reg->Begin + i,
                      PT_FLAG_WRITE | PT_FLAG_NX);
        }

        reg = reg->Next;
    }
}
