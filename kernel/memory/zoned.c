#include <power/memory/zoned.h>
#include <power/memory/heap.h>
#include <power/memory/pool.h>
#include <power/memory/pfa.h>

#include <power/memory/memset.h>
#include <power/error.h>

static struct mm_cache* regionCache;
static struct mm_pool* pool;

static struct zoned_region* allocZoneRegion(struct zoned* zone);
static void freeZoneRegion(struct zoned* zone, struct zoned_region* reg);
struct zoned* zpgInit(uint64_t begin, size_t length, 
                      int stride, int flags, void* location)
{
    struct zoned* zone;
    if (flags & ZN_USE_POOL_BIT) {
        if (!pool) {
            pool = poolInit(sizeof(struct zoned_region), 1);
        }

        zone = location;
    } else {
        if (!regionCache) {
            regionCache = khCreateCache("zoned_region", sizeof(struct zoned_region));
        }

        zone = khsAlloc(sizeof(*zone));
    }

    zone->Begin = begin;
    zone->Flags = flags;
    zone->Length = length;
    zone->Stride = stride;
    return zone;
}

struct zoned_region* allocZoneRegion(struct zoned* zone)
{
    if (zone->Flags & ZN_USE_POOL_BIT) {
        return poolAllocObject(pool);
    }

    return khAlloc(regionCache);
}

void freeZoneRegion(struct zoned* zone, struct zoned_region* reg)
{
    if (zone->Flags & ZN_USE_POOL_BIT) {
        return poolFreeObject(pool, reg);
    }

    return khFree(regionCache, reg);
}

int zpgRequestRegion(struct zoned* zoned, int length, uint64_t* out)
{
    struct zoned_region* reg, *tmp;
    uint64_t begin = zoned->Begin;

    tmp = zoned->RegionList;
    reg = allocZoneRegion(zoned);
    if (!reg) {
        return -ERROR_NO_MEMORY;
    }

    memset(reg, 0, sizeof(*reg));
    reg->Begin = zoned->Begin;
    reg->Length = length;
    if (!tmp) {
        zoned->RegionList = reg;
        goto out;
    }

    while (tmp != NULL) {
        /* If `reg` conflicts with `tmp` range...  */
        if ((tmp->Begin <= reg->Begin 
           && tmp->Begin + tmp->Length >= begin)) {

            /* Make it go to the next stride boundary. */
            reg->Begin = tmp->Begin + tmp->Length;
        }

        /* If `reg` extrapolates the requested zone allocation... */
        if (reg->Begin >= zoned->Begin + zoned->Length) {
            /* ...return an error. */
            goto fail;
        }

        if (!tmp->Next) {
            tmp->Next = reg;
            goto out;
        }

        tmp = tmp->Next;
    }

    tmp->Next = reg;

out:
    (*out) = reg->Begin;
    return 0;
fail:
    freeZoneRegion(zoned, reg);
    return -ERROR_UNAVAILABLE;
}

void zpgReturnRegion(struct zoned* zoned, uint64_t begin, int length)
{
    struct zoned_region* prev = NULL;
    struct zoned_region* reg = zoned->RegionList;
    if (!reg) {
        return;
    }

    while (reg != NULL) {
        if (reg->Begin == begin
         && reg->Length == length) {
            if (!prev) {
                zoned->RegionList = reg->Next;
                return;
            }

            prev->Next = reg->Next;
            return freeZoneRegion(zoned, reg);
        }

        prev = reg;
        reg = reg->Next;
    }
}

void zpgClearRegions(struct zoned* zoned)
{
  struct zoned_region* nx;
  struct zoned_region* zr = zoned->RegionList;
  while (zr) {
    nx = zr->Next;
    zpgReturnRegion(zoned, zr->Begin, zr->Length);
    zr = nx;
  }
}

void zpgDestroy(struct zoned* zoned)
{
  zpgClearRegions(zoned);
  if (zoned->Flags & ZN_USE_POOL_BIT) {
    return;
  }

  khsFree(zoned);
}
