#include <power/memory/vm.h>
#include <power/paging.h>
#include <power/memory/vaddress.h>

#include <power/avl.h>
#include <power/memory/pfa.h>
#include <power/memory/memset.h>

#include <power/elf.h>
#include "power/memory/heap.h"
#include "power/scheduler.h"
#include "power/um.h"
#include <power/mmap.h>

#include <power/error.h>
#include <power/system.h>
#include <power/vfs.h>

static struct mm_cache* regionCache;

struct vm* vmInit(address_space_t* address)
{
    struct vm* vm = khsAlloc(sizeof(*vm));
    vm->Lock = schedCreateSemaphore(1);
    vm->Tree = avlCreateTree();
    vm->AddressSpace = address;

    if (!vm->AddressSpace) {
        vm->AddressSpace = pgGetAddressSpace();
    }
    return vm;
}

void vmDestroy(struct vm* vm)
{
  vmClearRegions(vm);
  avlDestroyTree(vm->Tree);
  schedDestroySemaphore(vm->Lock);
  khsFree(vm);
}

void vmClearRegions(struct vm* vm)
{
  struct vm_region* region;
  
  schedSemaphoreAcquire(vm->Lock);
  while (vm->Tree->avt_root) {
    region = avlRemoveRoot(vm->Tree);
    if (region->Flags & VM_REG_ESTBLSH_BIT) {
      vmUnstablishMapping(vm, region->Page->VirtualAddress);
      continue;
    }

    vmVirtualFileUnmap(vm, (void*)region->Page->VirtualAddress, region->Length);
  }
  
  schedSemaphoreRelease(vm->Lock);
}

void* vmVirtualHandleMap(struct vm* vm, uint64_t hint, size_t length, 
                         struct handle* handle, int flags)
{
    __unused(vm);
    if (!handle->Ops.mmap) {
        return ERROR_INTO_PTR(-ERROR_NOT_SUPPORTED);
    }

    int error;
    uintptr_t address = hint;
    if ((error = handle->Ops.mmap(handle, MMAP_MAP, &address, length, flags))) {
        return ERROR_INTO_PTR((uintptr_t)error);
    }

    return (void*)address;
}

int vmVirtualHandleUnmap(struct vm* vm, struct handle* handle, 
                         void* address, size_t length)
{
    if (!length) {
        return -ERROR_INVALID_ARGUMENT;
    }

    if (!handle->Ops.mmap) {
        return -ERROR_NOT_SUPPORTED;
    }

    int error;
    uintptr_t h = (uintptr_t)address;
    schedSemaphoreAcquire(vm->Lock);
    if (!avlLookup(vm->Tree, (uintptr_t)address)) {
        goto errnf;
    }

    schedSemaphoreRelease(vm->Lock);
    if ((error = handle->Ops.mmap(handle, MMAP_UNMAP, &h, length, 0))) {
        return error;
    }

    return 0;
errnf:
    schedSemaphoreRelease(vm->Lock);
    return -ERROR_NOT_FOUND;
}

struct vm_region* vmCreateRegion()
{
    if (!regionCache) {
        regionCache = khCreateCache("vm_region", sizeof(struct vm_region));
    }

    struct vm_region* reg = khAlloc(regionCache);
    return reg;
}

/* Convert VMAP_* and VPROT_* into PT_FLAG_*.
   Suitable for calling the paging functions. */
uint64_t vmFlagsIntoPagingFlags(int flags)
{
    uint64_t pgflags = PT_FLAG_NX;
    if (flags & VMAP_USER)
        pgflags |= PT_FLAG_USER;

    if (flags & VPROT_WRITE) {
        pgflags &= ~PT_FLAG_NX;
        pgflags |= PT_FLAG_WRITE;
    }

    if (flags & VPROT_EXECUTE) {
        pgflags &= ~(PT_FLAG_WRITE | PT_FLAG_NX);
    }

    if (flags & VPROT_CACHE) {
        pgflags |= PT_FLAG_PCD;
    }

    return pgflags;
}

uint64_t syscVirtualUnmap(union sysc_regs* regs)
{
    struct process* proc = pcGetCurrentProcess();
    vmVirtualFileUnmap(proc->VirtualMemory, (void*)regs->Arg1, regs->Arg2);
    return 0;
}

uint64_t syscVirtualMap(union sysc_regs* regs)
{
    struct process* proc = pcGetCurrentProcess();
    int flags = regs->Arg3 | VMAP_ANONYMOUS | VMAP_USER;
    return (uintptr_t)vmVirtualFileMap(proc->VirtualMemory, 
                                       regs->Arg1, regs->Arg2, 
                                       NULL, flags);
}

uint64_t syscVirtualFileMap(union sysc_regs* regs)
{
    /* void* VirtualFileMap(int handle, uintptr hint, size_t length, int flags) */

    struct process* proc = pcGetCurrentProcess();
    int flags = regs->Arg4 | VMAP_USER;

    struct handle* han = pcGetHandleById(proc, regs->Arg1);
    if (!han) {
        return -ERROR_BAD_HANDLE;
    }

    return (uintptr_t)vmVirtualHandleMap(proc->VirtualMemory, 
                                         regs->Arg2, regs->Arg3, 
                                         han, flags);
}
