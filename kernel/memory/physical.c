#include "power/config.h"
#include "power/memory/hhdm.h"
#include <power/memory/pfa.h>
#include <power/memory/memset.h>

#include <power/interrupt.h>
#include <power/ldr_requests.h>
#include <power/memory/page.h>

#include <power/tty.h>
#include <power/list.h>
#include <power/error.h>

#define min(X, y)            ((X) > (y) ? y : X)
#define GET_DATA_PTR(B)      (uint64_t)((uint8_t*)B) - (B->Size)
#define GET_HEADER_PTR(B, l) PaAdd(B, l)

typedef struct __attribute__((packed)) mm_block {
  struct mm_block* Next;
  uint16_t Alignment;
  uint8_t Priority; /*< 1 = Created from AlignedFree.
                          0 = from AddUsableBlock */
  unsigned long FullSize; /*< Meaningless if Priority is not 0. */
  unsigned long Size;
  struct page* PageArray; /*< same as FullSize */
} mm_block;

typedef struct __attribute__((packed)) mm_global {
  void* Head;
  struct page* PageArray;
  
  mm_block* FreeList;
  size_t BumpLength;

  long MaxAllocatedMemory;
  long AllocatedMemory;
  long TotalMemory;
} mm_global;

static mm_global* globalMemory = NULL;

static inline void appendLinkedList(mm_block** head, mm_block* block)
{
    if (!*head) {
        (*head) = block;
        return;
    }

    mm_block* tmp = *head;
    while (tmp->Next != NULL) {
        tmp = tmp->Next;
    }

    tmp->Next = block;
    block->Next = NULL;
}

static inline void pushFrontLinkedList(mm_block** head, mm_block* block)
{
    if (!*head) {
        (*head) = block;
        return;
    }

    block->Next = *head;
    (*head) = block;
}

/* This is a workaround a really specific situation
   that will happen sooner or later if we want to
   reuse freed blocks to allocate aligned memory. */
static inline void removeLinkedList2(mm_block** head, mm_block* block, mm_block* next)
{
    if (!head) {
        trmLogfn("removeLinkedList: head is NULL");
        return;
    }

    if (*head == block) {
        (*head) = next;
        return;
    }

    mm_block* tmp = *head;
    while (tmp->Next && tmp->Next != block) {
        if (!tmp) {
            return;
        }
        tmp = tmp->Next;
    }

    tmp->Next = next;
}

static void removeLinkedList(mm_block** head, mm_block* block)
{
    removeLinkedList2(head, block, block->Next);
}

static int pointerToRegion(void* ptr, struct mm_block** block)
{
  ptr = mmMakeWritable(ptr);

  uintptr_t ptg = (uintptr_t)globalMemory;
  uintptr_t pti = (uintptr_t)ptr;
  if (pti > ptg && pti < ptg + globalMemory->BumpLength) {
    (*block) = NULL;
    return 0;
  }

  void* data;
  struct mm_block* tmp = globalMemory->FreeList;
  while (tmp != NULL) {
    if (!tmp->Priority) {
      data = ((void*)tmp) - tmp->FullSize;
      if (ptr > data && ptr < (void*)tmp) {
	(*block) = tmp;
	return 0;
      }
    }
    
    tmp = tmp->Next;
  }

  return -ERROR_NOT_FOUND;
}

static void* bumpStrtg(size_t size, uint16_t alignment)
{
    uint64_t head = (uint64_t)globalMemory->Head;
    uint64_t overhead = alignment - head % alignment;

    if (alignment == 1) {
        overhead = 0;
    }

    head += overhead;
    if (size + overhead > globalMemory->BumpLength) {
        /* No space to fit aligned block in bump. */
        return NULL;
    }

    head += size;
    mm_block* newb = (mm_block*)head;
    newb->Size = size;
    newb->Alignment = alignment;
    newb->Next = NULL;

    globalMemory->Head = (void*)head + sizeof(mm_block);
    return (void*)GET_DATA_PTR(newb);
}

static void* linkedList(size_t size, uint16_t alignment)
{
    uintptr_t head;
    uintptr_t dataptr;
    uintptr_t overhead;
    
    mm_block* tmp = globalMemory->FreeList;
    mm_block* chosen = NULL, *chosenHigh = NULL;
    
    struct mm_block* oldch;
    struct mm_block* prevch;
    
    while (tmp->Next != NULL) {
        head = (uint64_t)GET_DATA_PTR(tmp);
        overhead = alignment - head % alignment;

        if (tmp->Size >= size
            && (!(GET_DATA_PTR(tmp) % alignment)
                || tmp->Size >= overhead + size)) {
            chosen = tmp;
            if (tmp->Priority) {
                chosenHigh = tmp;
            }

            break;
        }

        tmp = tmp->Next;
        tmp = mmMakeWritable(tmp);
    }

    head = (uint64_t)globalMemory->Head;
    overhead = alignment - head % alignment;
    
    if (!chosenHigh && size + overhead < globalMemory->BumpLength) {
        return bumpStrtg(size, alignment);
    }

    if (!chosenHigh) {
        chosenHigh = chosen;
    }

    oldch = chosenHigh;
    prevch = oldch;
    
    dataptr = GET_DATA_PTR(chosenHigh);
    overhead = alignment - ((uintptr_t)dataptr) % alignment;
    if (alignment == 1)
        overhead = 0;

    /* As we reduce usage of mmAllocKernel, most of
       this code renders unneeded. */
    if (chosenHigh->Size > (size + overhead)
	&& chosenHigh->Size - size >= sizeof(mm_block) + 1) {
        void* next = oldch->Next;
        dataptr += overhead;
        chosenHigh = GET_HEADER_PTR(dataptr, size);
        if ((unsigned long)(chosenHigh - oldch) < 8) {
            oldch = chosenHigh;
        }

        chosenHigh->Size = size;
        chosenHigh->Alignment = alignment;
        chosenHigh->Next = NULL;

        if (oldch != chosenHigh) {
            oldch->Size -= chosenHigh->Size + sizeof(mm_block) + overhead;
            if (oldch->Size < 4)
                removeLinkedList(&globalMemory->FreeList, oldch);
        } else {
            removeLinkedList2(&globalMemory->FreeList, prevch, next);
        }
    } else {
        /* Just swallow the overhead */
        chosenHigh->Size = size;
        removeLinkedList(&globalMemory->FreeList, chosenHigh);
    }

    return (void*)GET_DATA_PTR(chosenHigh);
}

static void* insertPageArray(void* base, size_t* length)
{
    uint64_t l = *length;
    if (l <= 4096) {
      return ERROR_INTO_PTR(-ERROR_NO_MEMORY);
    }

    struct page* page = base;
    int items = l / 4096;
    int itemsbytes = items * sizeof(struct page);

    void* rebase = base + itemsbytes;
    for (int i = 0; i < items; i++) {
        page->Address = (void*)(((uintptr_t)rebase + (i * 4096)) & ~0xFFF);
        page->RefCount = 0;
        listInit(&page->List);

        page++;
    }

    (*length) = l - itemsbytes;
    return rebase;
}

void mmInit(void* base, size_t length)
{
    void* oldb = base;
    base = insertPageArray(base, &length);

    globalMemory = base;
    globalMemory->FreeList = NULL;
    globalMemory->PageArray = oldb;
    globalMemory->BumpLength = length - sizeof(mm_global);
    globalMemory->Head = (void*)((uint64_t)base + sizeof(*globalMemory));
}

void mmAddUsablePart(void* begin, size_t length)
{
    if (length == 0)
        return;

    void* page = begin;
    begin = insertPageArray(begin, &length);
    if (ERROR_PTR(begin)) {
        return;
    }
    
    struct mm_block* blk = PaAdd(begin, length - sizeof(mm_block));

    blk->Next = NULL;
    blk->Priority = 0;
    blk->Alignment = 1;
    blk->FullSize = length; 
    blk->Size = length - sizeof(struct mm_block);
    blk->PageArray = page;

    appendLinkedList(&globalMemory->FreeList, blk);
}

void* mmAlignedAlloc(size_t size, uint16_t alignment)
{
    inhDisableInterrupts();
    if (alignment == 0)
        alignment = 1;

    void* ptr = linkedList(size, alignment);
    if (likely(ptr)) {
      globalMemory->AllocatedMemory += size;
    }
    inhEnableInterrupts();

    return ptr;
}

void mmAlignedFree(const void* ptr, size_t size)
{
    inhDisableInterrupts();
    if (ptr == NULL)
        return;
    
    ptr = mmMakeWritable((void*)ptr);
    mm_block* header = GET_HEADER_PTR(ptr, size);
    
    header->Next = NULL;
    header->Priority = 1;
    header->Size = size;

    pushFrontLinkedList(&globalMemory->FreeList, header);

    globalMemory->AllocatedMemory -= size;
    inhEnableInterrupts();
}

void* mmGetGlobal()
{
    return globalMemory;
}

size_t mmGetLength()
{
    return globalMemory->BumpLength;
}

bool mmIsPhysical(void* ptr)
{
    uint64_t cast = (uint64_t)ptr;
    struct limine_memmap_response* mm = rqGetMemoryMapRequest();

    for (uint64_t i = 0; i < mm->entry_count; i++) {
        struct limine_memmap_entry* ent = mm->entries[i];
        if (ent->type == LIMINE_MEMMAP_USABLE
            && (ent->base <= cast && (ent->base + ent->length) >= cast)) {
            return true;
        }
    }

    return false;
}

void* mmAlignedPhysicalAlloc(size_t size, uint16_t alignment)
{
    struct limine_hhdm_response* hhdm = rqGetHhdmRequest();
    void* ptr = mmAlignedAlloc(size, alignment);
    uint64_t offset = 0;
    if ((uintptr_t)ptr > hhdm->offset)
        offset = hhdm->offset;
    return (void*)(((uint64_t)ptr) - offset);
}

void* mmAllocKernel(size_t size)
{
    void* ptr = mmAlignedAlloc(size, 1);
    memset(ptr, 0, size);
    return ptr;
}

uint64_t mmGetHhdmOffset()
{
    return rqGetHhdmRequest()->offset;
}

struct page* mmAllocPage(int count)
{
  count--;

  unsigned rem = 0;
  void* page = mmAlignedPhysicalAlloc(4096, 4096);
  if (!page)
    return NULL;

  struct page* p;
  struct page* apst;
  struct page* pst = mmGetPage(page);
  pst->Address = page;

 iter:
  while (count) {
    if (!(page = mmAlignedPhysicalAlloc(4096 * count, 4096))) {
      count--;
      rem++;
      continue;
    }

    apst = mmGetPage(page);
    apst->Address = page;
      
    listInsert(&pst->List, &apst->List);
    break;
  }
  
  if (unlikely(!page)) {
    mmAlignedFree(page, 4096);
    return NULL;
  }
  
  for (int i = 1; i < count; i++) {
    p = mmGetPage(apst->Address + (i * 4096));
    p->Address = (void*)mmMakeUnwritable(p->Address);
    listInsert(&apst->List, &p->List);
  }

  if (unlikely(rem)) {
    count = rem;
    rem = 0;
    goto iter;
  }

  return pst;
}

struct page* mmGetPage(void* address)
{
    struct mm_block* block;
    int status = pointerToRegion(address, &block);
    if (status < 0) {
        return NULL;
    }

    uint64_t hhdm = mmGetHhdmOffset();
    uintptr_t begin;
    uintptr_t ptr = (uintptr_t)address;
    if (ptr >= hhdm) {
        ptr -= hhdm;
    }

    struct page* pagear;
    if (!block) {
        begin = ((uintptr_t)globalMemory) - hhdm;
        pagear = globalMemory->PageArray;
    } else {
        begin = (uintptr_t)((void*)block) - block->FullSize;
        pagear = block->PageArray;
    }

    ptr -= begin;
    int index = ptr >> 12;
    struct page* page = &pagear[index + 1];
    return mmMakeWritable(page);
}

void mmFreePage(void* address)
{
    struct page* pg = mmGetPage(address);
    mmAlignedFree(pg->Address, 4096);
}
