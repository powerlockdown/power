# uheaders.mk
#   Pupose: Userspace kernel headers
#

DESTKERNINCDIR=$(SYSTEM_ROOT)/System/Headers/power
DESTKERNARCHINCDIR=$(SYSTEM_ROOT)/System/Headers

# The kernel arch specific headers
KERNARCHKINCDIR=kernel/arch/i386/include/

KERNEL_INCLUDE_DIR=kernel/include/user
KERNEL_HEADERS_FILES=system.h ioctl.h input.h input_code.h types.h error.h signal.h video/framebuffer.h time.h limits.h filestats.h pty.h handle.h mmap.h

KERNARCHINCDIR=kernel/arch/i386/include/user
KERNARCHHEADERS:=$(notdir $(wildcard $(KERNARCHINCDIR)/power/*.h))
KERNARCHINSTHEADERS:=$(addprefix $(DESTKERNARCHINCDIR)/power/,$(KERNARCHHEADERS))

KERNEL_HEADERS=$(addprefix $(KERNEL_INCLUDE_DIR)/power/,$(KERNEL_HEADERS_FILES))
KERNEL_INSTALLED_HEADERS=$(addprefix $(DESTKERNINCDIR)/,$(KERNEL_HEADERS_FILES))

kernel_install: $(KERNARCHINSTHEADERS) $(KERNEL_INSTALLED_HEADERS)

# Install usual headers
$(DESTKERNINCDIR)/%.h: $(KERNEL_INCLUDE_DIR)/power/%.h
	install -C -D -t $(DESTKERNINCDIR)/$(dir $(subst $(KERNEL_INCLUDE_DIR)/power/,,$^)) $^

# Install arch specific headers
$(DESTKERNARCHINCDIR)/power/%.h: $(KERNARCHINCDIR)/power/%.h
	install -C -D -t $(DESTKERNARCHINCDIR)/power/$(dir $(subst $(KERNARCHINCDIR)/power/,,$^)) $^



