#include "power/memory/zoned.h"
#include "power/tty.h"
#include "power/vector.h"
#include <power/device.h>
#include <power/list.h>

#include <power/memory/heap.h>
#include <power/error.h>
#include <power/string.h>
#include <power/_acpi.h>

/* I could make a radix tree out of this. */

struct device_nchild {
    struct list_item List;

    minor_t Minor;
    void* DevicePrivData;
};

static struct list_item devices;

major_t* devnRegisterNode(const char* name, const struct handle_ops* handle, int flags)
{
    struct device_ntree* tree = khsAlloc(sizeof(*tree));
    if (!tree) {
        return NULL;
    }

    size_t nl = strlen(name);
    struct device_ntree* nt;
    listForEach(devices.Next, tl) {
        nt = (struct device_ntree*)tl;
        if (!strncmp(name, nt->Name, nl)) {
            return nt;
        }
    }

    listInit(&tree->List);
    listInit(&tree->ChildrenList);

    tree->Name = strclone(name);
    tree->Ops = handle;
    tree->Flags = flags;
    tree->MinorPool = zpgInit(1, UINT32_MAX, 1, 0, NULL);

    listInsert(&devices, &tree->List);
    return tree;
}

minor_t devAllocateNode(major_t* major, void* data)
{
    if (major->ChildrenList.Next && !(major->Flags & DEVN_MANYDEV_BIT)) {
        return -ERROR_INVALID_RANGE;
    }

    int error = 0;

    uint64_t minor;
    struct device_nchild* child = khsAlloc(sizeof(*child));
    if (!child) {
        return -ERROR_NO_MEMORY;
    }

    listInit(&child->List);
    error = zpgRequestRegion(major->MinorPool, 1, &minor);
    if (error) {
        goto error;
    }

    trmLogfn("allocating minor %i for %s", minor, major->Name);
    listInsert(&major->ChildrenList, &child->List);
    child->Minor = minor;
    child->DevicePrivData = data;
    return child->Minor;

error:
    khsFree(child);
    return error;
}

const struct handle_ops* 
devnFindNode(const char* dev, void** outreserved)
{
    size_t length, l2;

    /* find the last non-numeric character in the string */
    length = strlen(dev);
    l2 = length - 1;
    while (isdigit(dev[l2])) {
        l2--;
    }

    l2++;
    struct device_ntree* major;
    listForEach(devices.Next, dl) {
        major = (void*)dl;

        if (!strncmp(dev, major->Name, l2)) {
            break;
        }

	major = NULL;
    }

    if (!major) {
        return NULL;
    }

    /* Now we have to find the minor */
    minor_t minor = 0;
    struct device_nchild* ch = (void*)major->ChildrenList.Next;
    if (l2 != length) {
        /* Majors not declared with MANYDEV cannot accept
           minors in their names. */
        if (!(major->Flags & DEVN_MANYDEV_BIT)) {
            return NULL;
        }

        minor = strtoul(dev + l2, NULL, 10);
    }

    listForEach(major->ChildrenList.Next, c) {
        ch = (void*)c;
        if (ch->Minor == minor) {
            break;
        }
    }

    if (!ch) {
        return NULL;
    }

    (*outreserved) = ch->DevicePrivData;
    return major->Ops;
}

struct handle* devnCreateDeviceHandle(struct process* proc, const char* devName)
{
    void* res;
    const struct handle_ops* ho = devnFindNode(devName, &res);
    if (!ho) {
        return NULL;
    }

    struct handle* handle = pcCreateHandle(proc, HANDLE_TYPE_DEVICE, res);
    if (ERROR_PTR(handle)) {
        return handle;
    }
   
    if (!strncmp("tty", devName, 3)) {
      handle->Type = HANDLE_TYPE_TTY;
    }
    
    handle->Ops = *ho;
    return handle;    
}

major_t* devnFindMajor(const char* name)
{
    struct device_ntree* nt;
    listForEach(devices.Next, tl) {
        nt = (struct device_ntree*)tl;
        if (!strcmp(nt->Name, name)) {
            return nt;
        }
    }

    return NULL;
}

void devnUnregisterNode(major_t* major)
{
    khsFree(major->Name);
}
