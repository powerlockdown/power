#include "power/device.h"
#include "power/framebuffer.h"
#include "power/memory/heap.h"
#include "power/memory/vm.h"
#include "power/panic.h"
#include <power/abstract/intrctl.h>
#include <power/abstract/timer.h>
#include <power/i386/gdt.h>
#include <power/limits.h>

#include <power/i386/idt.h>
#include <power/paging.h>
#include <power/ldr_requests.h>

#include <power/config.h>
#include <power/memory/pfa.h>
#include <power/driver.h>

#include <power/partition.h>
#include <power/pci.h>
#include <power/scheduler.h>

#include <power/um.h>
#include <power/tty.h>
#include <power/args.h>
#include <power/cmos.h>

#include <power/vfs.h>
#include <power/string.h>
#include <limine.h>

void* rootHeapAddressSpace = NULL;
extern void cpuEnableFeatures();

static inline struct limine_memmap_entry* getUsableEntry()
{
    struct limine_memmap_entry* c;
    struct limine_memmap_entry* ret = NULL;
    struct limine_memmap_response* re = rqGetMemoryMapRequest();
    uint64_t max = 0;

    for (uint64_t i = 0; i < re->entry_count; i++) {
        c = re->entries[i];
        if (c->type == LIMINE_MEMMAP_USABLE && c->length > max) {
            max = c->length;
            ret = c;
        }
    }

    return ret;
}

static void
loadInit()
{
  struct process* pc;
  struct partition* pt;
  const char* number;
  const char* fstype;
  
  cmdGetCommandArgument("fstype", &fstype);
  cmdGetCommandArgument("rootpt", &number);

  if (!devnFindNode(number, (void*)&pt)) {
    panic("Root partition not found", NULL);
    return;
  }

  vfsCreateFilesystem(fstype, pt->BeginLba, pt->Device);
  pc = pcCreateProcess(NULL, "A:/System/linit.elf", NULL, NULL);
  
  if (ERROR_PTR(pc)) {
    trmLogfn("pcCreateProcess: %i", ERROR_FROM_PTR(pc));
    panic("Failed to load init", NULL);
  }
}

static void secondInit(void*);

void keMain()
{
    trmInit();
    gdtInit();
    idtInit();
    cpuEnableFeatures();

    struct limine_hhdm_response* hhdm = rqGetHhdmRequest();
    struct limine_memmap_response* re = rqGetMemoryMapRequest();
    struct limine_memmap_entry* ent = getUsableEntry();
    mmInit((void*)(ent->base + hhdm->offset), ent->length);

    struct limine_memmap_entry* c;
    for (uint64_t i = 0; i < re->entry_count; i++) {
        c = re->entries[i];
        if (c->type == LIMINE_MEMMAP_USABLE && c->base != ent->base) {
            mmAddUsablePart((void*)c->base + hhdm->offset, c->length);
      }
    }
    
    // gdtCreateIstStacks();
    rootHeapAddressSpace = pgGetAddressSpace();
    pgSetCurrentAddressSpace(rootHeapAddressSpace);
    intCtlCreateDefault();
    tmCreateTimer();
    tmCreateSecondaryTimer();

    cmosInit();
    schedCreate();
    schedAddThread(schedCreateThread(secondInit, NULL, 0));
    schedEnable(true);
    
    for (;;)
        asm volatile("hlt");
}

void secondInit(void* nothing)
{
    __unused(nothing);
    pciInitializeRegistry();
    workerInit();
    
    pcProcessManagerInit();
    inpCreateInputRing();
    ptyInit();

    asm volatile("sti");
    modAutomaticLoad();

    partInitAllDevices();
    vfsInit();
    fbCreate();
    
    if (cmdHasArgument("noinit")) {
        trmLogfn(
            "Everything done! But the kernel was instructed to not load init");

        for (;;)
            asm volatile("sti; hlt");
    } else {
      loadInit();
    }

    struct thread* t = schedGetCurrentThread();
    schedRemoveThread(t);
    asm volatile("sti; hlt");
}
