#include <power/memory/heap.h>
#include <power/memory/memset.h>
#include <power/scheduler.h>
#include <power/vector.h>
#include <power/panic.h>

#define THREAD_MAX 10

static struct mm_cache* eventCache;
static struct mm_cache* semCache;
static struct mm_cache* mtxCache;

struct s_event {
    int Threads[THREAD_MAX];
    uint8_t ArrayIndex;
    bool Flag;
};

struct s_event* schedCreateEvent()
{
    if (!eventCache)
        eventCache = khCreateCache("sync-event", sizeof(struct s_event));

    struct s_event* event = khAlloc(eventCache);
    return event;
}

void schedDestroyEvent(struct s_event* event)
{
    schedEventRaise(event);
    khsFree(event);
}

void schedEventRaise(struct s_event* event)
{
    if (event->Flag)
        return;

    struct thread* t;
    event->Flag = true;
    for (int i = 0; i < event->ArrayIndex; i++) {
        t = schedGetThreadById(event->Threads[i]);
        if (!t)
            continue;
        t->Suspended = false;
    }
}

void schedEventPause(struct s_event* event)
{
    if (event->Flag)
        return;

    schedEnable(false);
    struct thread* t = schedGetCurrentThread();
    t->Suspended = true;

    event->Threads[event->ArrayIndex] = t->Identifier;
    event->ArrayIndex++;
    schedThreadYield();
    schedEnable(true);

    asm volatile("sti; hlt");
}

bool schedEventFlag(struct s_event* event)
{
    return event->Flag;
}

void schedEventResetFlag(struct s_event* event)
{
    event->ArrayIndex = 0;
    event->Flag = false;
}

/* mutex */
struct s_mutex {
    volatile int Locker; /*< The thread that currently has the lock */
};

struct s_mutex* schedCreateMutex()
{
    if (!mtxCache)
        mtxCache = khCreateCache("sync-mtx", sizeof(struct s_mutex));

    struct s_mutex* mtx = khAlloc(mtxCache);
    mtx->Locker = 0;
    return mtx;
}

void schedDestroyMutex(struct s_mutex* mtx)
{
    mtx->Locker = 0;
    khFree(mtxCache, mtx);
}

void schedMutexAcquire(struct s_mutex* mtx)
{
    struct thread* t = schedGetCurrentThread();
    while (!__sync_bool_compare_and_swap(&mtx->Locker, 0, t->Identifier))
        ;
}

void schedMutexRelease(struct s_mutex* mtx)
{
    mtx->Locker = 0;
}

/* semaphore */
struct s_semaphore {
    int Slots;
    int Owner;
    struct vector Threads;
};

struct s_semaphore* schedCreateSemaphore(int slots)
{
    if (!semCache)
        semCache = khCreateCache("sync-sem", sizeof(struct s_semaphore));
    struct s_semaphore* s = khAlloc(semCache);

    s->Threads = vectorCreate(1);
    s->Slots = slots;
    return s;
}

void schedSemaphoreAcquire(struct s_semaphore* s)
{
    struct thread* thread = schedGetCurrentThread();
    if (s->Owner == thread->Identifier) {
        return;
    }
    
    if (!s->Slots) {
        struct thread* t = schedGetCurrentThread();
        t->Suspended = true;

        vectorInsert(&s->Threads, t);
        __halt();
    }

    s->Slots--;
    s->Owner = thread->Identifier;
}

void schedSemaphoreRelease(struct s_semaphore* s)
{
    s->Slots++;
    s->Owner = 0;
    
    if (s->Threads.Length) {
        struct thread* t = s->Threads.Data[0];
        t->Suspended = false;

        vectorRemove(&s->Threads, t);
    }
}

void schedDestroySemaphore(struct s_semaphore* sem)
{
    while (sem->Threads.Length) {
        schedSemaphoreRelease(sem);
    }

    vectorDestroy(&sem->Threads);
    khFree(semCache, sem);
}
