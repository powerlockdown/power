#include <power/scheduler.h>

#include "power/error.h"
#include "power/memory/hhdm.h"
#include "power/memory/user_buffer.h"

#include <power/mmap.h>
#include "power/memory/vm.h"
#include "power/system.h"

#include "power/um.h"
#include "power/vector.h"
#include <power/i386/asm.h>

#include <power/memory/heap.h>
#include <power/paging.h>

#include <power/limits.h>

#include <power/tty.h>
#include <power/memory/pfa.h>
#include <power/memory/memset.h>
#include <power/i386/gdt.h>
#include <power/memory/vaddress.h>

#define USER_STACK_BEGIN 0x464F000000

#define USER_STACK_SIZE 71680
#define UKERNEL_STACK_SIZE 4096

static struct mm_cache* threadCache;

struct thread* schedCreateThread(thread_execution_pfn_t ep, void* data,
                                 int flags)
{
    return schedCreateThreadEx(ep, data, NULL, flags);
}

struct thread* schedCreateThreadEx(thread_execution_pfn_t ep, void* data,
                                   struct thread_args* args, int flags)
{
    __unused(flags);
    if (!threadCache) {
        threadCache = khCreateCache("thread", sizeof(struct thread));
    }

    struct thread* th = khAlloc(threadCache);
    if (!th) {
        return NULL;
    }

    listInit(&th->List);

    uintptr_t restorer = 0;
    address_space_t* address;
    struct process* process = NULL;
    if (!args || !args->AddressSpace) {
        address = pgGetAddressSpace();
    } else {
        address = args->AddressSpace;
    }

    if (args) {
        restorer = args->Restorer;
        process = args->Process;
    }

    if (process) {
        th->StackTop = threadAllocateStack(process, USER_STACK_SIZE);
        th->KernelStack = threadAllocateStack(process, UKERNEL_STACK_SIZE);
    } else {
        th->StackTop = mmAllocKernel(8192) + 8192;
    }

    th->Process = process;
    th->Registers.Cr3 = (uintptr_t)address;    
    th->Registers.Pointers.Rip = (uintptr_t)ep;
    th->Registers.GeneralRegisters.Rdi = (uint64_t)data;
    th->Registers.CodeSegment = process
        ? USER_CODE_SEGMENT
        : KERNEL_CODE_SEGMENT;
    th->Registers.DataSegment = process
        ? USER_DATA_SEGMENT
        : KERNEL_DATA_SEGMENT;

    if (FP_STATE_BUFFER_SIZE && process) {
        th->Registers.FloatingPointState =
            mmAlignedAlloc(FP_STATE_BUFFER_SIZE, FP_STATE_BUFFER_ALIGNMENT);
        if (!th->Registers.FloatingPointState) {
            return NULL;
        }

        memset(th->Registers.FloatingPointState, 0, FP_STATE_BUFFER_SIZE);
    }

    uint64_t* pstack;
    pstack = pgGetPhysicalAddress(address, (uintptr_t)th->StackTop - 16);
    pstack = mmMakeWritable(pstack);

    th->TerminationEvent = schedCreateEvent();
    (*(uint64_t*)(pstack)) = restorer;
    th->Registers.Pointers.Rsp = (uint64_t)th->StackTop - 16;
    th->Timing.ResetQuantum = 3;
    return th;
}

void schedFreeThread(struct thread* restrict thread)
{
    
    mmAlignedFree(thread->Registers.FloatingPointState, 512);

    struct vm* vm;
    if (thread->Process) {
        vm = thread->Process->VirtualMemory;
	if (!vm)
	  goto ftre;
	
        vmVirtualFileUnmap(vm, thread->StackTop - USER_STACK_SIZE, USER_STACK_SIZE);
        vmVirtualFileUnmap(vm, thread->KernelStack - UKERNEL_STACK_SIZE, 
                           UKERNEL_STACK_SIZE);
        goto ftre;
    }
    
    mmAlignedFree(thread->StackTop - 8192, 8192);
    /* KernelStack is NULL. */
ftre:
    schedDestroyEvent(thread->TerminationEvent);
    khFree(threadCache, thread);
}

void schedThreadYield()
{
    struct thread* thread = schedGetCurrentThread();
    thread->Timing.CurrentQuantum = 1;
    return;
}

void* threadAllocateStack(struct process* proc, size_t size)
{
    struct vm* vm = proc->VirtualMemory;
    void* stack;

    stack = vmVirtualFileMap(vm, USER_STACK_BEGIN, size, 
                             NULL, VMAP_ANONYMOUS | VPROT_WRITE | VMAP_USER);
    return stack + size;
}

uint64_t syscCreateThread(union sysc_regs* regs)
{
    /* CreateThread(void* address, void* parm, size_t stksize, void* restorer) */
    void* address = (void*)regs->Arg1;
    void* param = (void*)regs->Arg2;
    uintptr_t restorer = (uintptr_t)regs->Arg4;

    int error;
    if ((error = mmUserPointerCheck(regs->Arg1))) {
        return error;
    }

    struct process* proc = pcGetCurrentProcess();
    struct thread_args ag = { .AddressSpace = NULL, .Restorer = restorer,
                              .Process = proc };
    struct thread* th = schedCreateThreadEx(address, param, &ag, 
                                            THREAD_CREATE_USER_MODE);
    if (!th) {
        return -ERROR_NO_MEMORY;
    }

    th->Process = proc;
    vectorInsert(&proc->Threads, th);
    
    schedAddThread(th);
    return th->Identifier;
}

uint64_t syscKillThread(union sysc_regs* regs)
{
    /* KillThread(int id) -> int */
    int h = regs->Arg1;
    int wait = 0;

    struct process* proc = pcGetCurrentProcess();
    struct thread* th;

    th = schedGetThreadById(h);    
    if (!th)
      return -ERROR_NOT_FOUND;

    if (th->Process != proc)
      return -ERROR_FORBIDDEN;
    
    vectorRemove(&proc->Threads, th);
    if (th == schedGetCurrentThread()) {
        wait = 1;
    }

    schedRemoveThread(th);
    if (wait) {
        __halt();
    }

    return 0;
}

uint64_t syscGetCurrentThreadId(union sysc_regs* ign)
{
  /* GetCurrentThreadId() -> int */
  __unused(ign);
  return schedGetCurrentThread()->Identifier;
}

static inline
int suspendThread(int id)
{
  struct thread* th;
  struct process* proc;
  
  th = schedGetThreadById(id);
  if (!th)
    return -ERROR_NOT_FOUND;

  proc = pcGetCurrentProcess();
  if (proc != th->Process)
    return -ERROR_FORBIDDEN;

  th->Suspended = true;
  if (th == schedGetCurrentThread())
    __halt();

  return 0;
}

static inline
int unsuspendThread(int id)
{
  struct thread* th;
  struct process* proc;
  
  th = schedGetThreadById(id);
  if (!th)
    return -ERROR_NOT_FOUND;

  proc = pcGetCurrentProcess();
  if (proc != th->Process)
    return -ERROR_FORBIDDEN;

  th->Suspended = false;
  return 0;
}

uint64_t syscSetThreadSuspension(union sysc_regs* regs)
{
  int id = regs->Arg1;

  /* Is a uintptr in order
     to avoid stuff like
     0xf00000000 yielding
     false. */
  uintptr_t ns = regs->Arg2;

  if (ns)
    return suspendThread(id);
  return unsuspendThread(id);
}

uint64_t syscWaitForThread(union sysc_regs* regs)
{
  /* WaitForThread(int) -> 0 (or ERROR_INTERRUPTED) */
  struct process* proc;
  proc = pcGetCurrentProcess();

  int tid = regs->Arg1;
  
  struct thread* thr;
  for (size_t i = 0; i < proc->Threads.Length; i++) {
    thr = proc->Threads.Data[i];
    if (thr->Identifier == tid) {
      break;
    }

    thr = NULL;
  }

  if (unlikely(!thr)) {
    return -ERROR_NOT_FOUND;
  }

  schedEventPause(thr->TerminationEvent); 
  return 0;
}
