#include <power/scheduler.h>
#include <power/memory/heap.h>
#include <power/vector.h>

struct global_worker {
    struct s_semaphore* VectorLock;
    struct s_event* WorkerEvent;
    struct thread* WorkerThread;

    struct vector Workers;
};

struct worker {
    worker_routine_pfn_t Routine;
    void* Data;
};

static struct global_worker* worker = NULL;

static void workerThink(void*);

void workerInit()
{
    if (worker) {
        return;
    }

    worker = khsAlloc(sizeof(*worker));
    worker->WorkerEvent = schedCreateEvent();
    worker->WorkerThread = schedCreateThread(workerThink, NULL, 0);
    worker->VectorLock = schedCreateSemaphore(1);
    
    worker->Workers = vectorCreate(3);
    schedAddThread(worker->WorkerThread);
}

void workerSubmitTask(worker_routine_pfn_t rout, void* ctx)
{
    struct worker* wo = khsAlloc(sizeof(*wo));
    wo->Data = ctx;
    wo->Routine = rout;

    schedSemaphoreAcquire(worker->VectorLock);
    vectorInsert(&worker->Workers, wo);
    schedSemaphoreRelease(worker->VectorLock);
    
    schedEventRaise(worker->WorkerEvent);
    schedEventResetFlag(worker->WorkerEvent);
}

void workerThink(void* ctx)
{
    __unused(ctx);

    struct worker* wo;
    while (true) {
        schedEventPause(worker->WorkerEvent);
        while (worker->Workers.Length) {
            wo = worker->Workers.Data[0];

            wo->Routine(wo->Data);

            schedSemaphoreAcquire(worker->VectorLock);
            vectorRemove(&worker->Workers, wo);
            schedSemaphoreRelease(worker->VectorLock);

            khsFree(wo);
        }
    }
}
