#include "power/error.h"
#include "power/i386/fsgs.h"
#include "power/list.h"
#include "power/memory/hhdm.h"
#include "power/tty.h"
#include <power/scheduler.h>
#include <power/vector.h>

#include <power/i386/gdt.h>
#include <power/i386/idt.h>

#include <power/memory/heap.h>
#include <power/memory/memset.h>

#include <power/abstract/intrctl.h>
#include <power/um.h>
#include <power/user_signal.h>

#include <power/i386/apic.h>

struct thread_list {
    struct thread_list* Previous;
    struct thread_list* Next;

    struct thread* Thread;
};

extern void schedTaskSwitch(struct thread_registers* registers);
extern void schedIntersegmentTaskSwitch(struct thread_registers* registers);

struct scheduler {
    struct list_item ExecList; /*< List of threads currently for scheduling */
    struct thread* CurrentExec;
    struct vector SleepingThreads;

    int ExecCount;
    bool ShouldSaveRegisters;
};

static struct scheduler* defScheduler = NULL;
static bool dsEnabled = false;

static inline uint64_t readTimestamp(void)
{
    uint32_t low, high;
    asm volatile("rdtsc" : "=a"(low), "=d"(high));
    return ((uint64_t)high << 32) | low;
}

static inline bool
updateThreadTimings(struct thread* thread)
{
    uint64_t currentct = readTimestamp();
    thread->Timing.ElapsedCount += currentct - thread->Timing.LastCount;
    thread->Timing.LastCount = currentct;

    thread->Timing.CurrentQuantum--;
    if (!thread->Timing.CurrentQuantum) {
        thread->Timing.CurrentQuantum = thread->Timing.ResetQuantum;
        return true;
    }

    return false;
}

/* Optionally put the sleeping threads to wake */
static inline void unsuspendThreads()
{
    for (size_t i = 0; i < defScheduler->SleepingThreads.Length; i++) {
        struct thread* thr = defScheduler->SleepingThreads.Data[i];
        if (thr->SuspendedTicks != THREAD_NOT_SLEEPING) {
            if (--thr->SuspendedTicks) {
                /* Events may wake the thread up. */
                if (!thr->Suspended)
                    thr->Suspended = true;
                continue;
            }

            thr->SuspendedTicks = THREAD_NOT_SLEEPING;
            thr->Suspended = false;
            vectorRemove(&defScheduler->SleepingThreads, thr);
        }
    }
}

static inline void
nextThread()
{
    struct process* proc;
    
    do {
        defScheduler->CurrentExec = listCast(defScheduler->CurrentExec->List.Next, 
                                             struct thread, List);
        if (!defScheduler->CurrentExec) {
            defScheduler->CurrentExec = (struct thread*)defScheduler->ExecList.Next;
            break;
        }

        proc = defScheduler->CurrentExec->Process;
        if (proc && proc->Signals->Flags & SIG_PROC_PENDING_BIT) {
            return;
        }

    } while (defScheduler->CurrentExec != NULL
             && defScheduler->CurrentExec->Suspended);
    fgsPopSegments(defScheduler->CurrentExec);
}

static void idleTask()
{
    while (true) {
        schedThreadYield();
        asm volatile("sti; hlt");
    }
}

static inline void loadRegisters(const struct irq_regs* state)
{
    if (defScheduler->CurrentExec->Timing.LastCount) {
        defScheduler->CurrentExec->Registers.CodeSegment = state->PointerRegisters->Cs;
        uint64_t cs = defScheduler->CurrentExec->Registers.CodeSegment;
        if (!(cs & 0b11)) {
            defScheduler->CurrentExec->Registers.DataSegment = KERNEL_DATA_SEGMENT;
            // defScheduler->CurrentExec->Thread->Registers.DataSegment &= ~3;
        } else {
            defScheduler->CurrentExec->Registers.DataSegment = USER_DATA_SEGMENT;
            defScheduler->CurrentExec->Registers.DataSegment |= 3;
        }

        void* fps = defScheduler->CurrentExec->Registers.FloatingPointState;
        fps = mmMakeWritable(fps);
        
        if (fps)
            fpsSave(fps);

	fgsPushSegments(defScheduler->CurrentExec);
        memcpy(&defScheduler->CurrentExec->Registers.GeneralRegisters,
               state->GeneralRegisters, sizeof(*state->GeneralRegisters));
        memcpy(&defScheduler->CurrentExec->Registers.Pointers,
               state->PointerRegisters, sizeof(*state->PointerRegisters));
    }
}

struct scheduler* schedCreate()
{
    defScheduler = khsAlloc(sizeof(struct scheduler));
    memset(defScheduler, 0, sizeof(*defScheduler));

    listInit(&defScheduler->ExecList);
    defScheduler->ShouldSaveRegisters = true;
    defScheduler->SleepingThreads = vectorCreate(3);
    
    schedAddThread(schedCreateThread(idleTask, NULL, 0));
    return defScheduler;
}

bool schedIsEnabled()
{
    return dsEnabled;
}

void schedEnable(bool flag)
{
    dsEnabled = flag;
}

void schedAddThread(struct thread* thread)
{
    defScheduler->ExecCount++;
    thread->Identifier = defScheduler->ExecCount;
    listInsert(&defScheduler->ExecList, &thread->List);
}

void schedRemoveThread(struct thread* t)
{
    bool state = dsEnabled;
    dsEnabled = false;
    struct thread* ls = (struct thread*)defScheduler->ExecList.Next;
    while (ls->List.Next != NULL) {
        if (ls == t)
            break;
        ls = (struct thread*)ls->List.Next;
    }

    if (t == defScheduler->CurrentExec) {
        defScheduler->CurrentExec = (struct thread*)defScheduler->CurrentExec->List.Next;
        defScheduler->ShouldSaveRegisters = false;
    }

    listRemove(&t->List);
    schedFreeThread((struct thread*)t);
    dsEnabled = state;
}

void schedThink(const struct irq_regs* state)
{
    int error;
    if (!defScheduler->CurrentExec) {
        defScheduler->CurrentExec = (struct thread*)defScheduler->ExecList.Next;
    }

    /* Redundant check but necessary if ExecList is NULL. */
    if (!defScheduler->ExecCount
        || !defScheduler->CurrentExec)
        return;

    unsuspendThreads();
    if (defScheduler->ShouldSaveRegisters)
        loadRegisters(state);
    defScheduler->ShouldSaveRegisters = true;

    if (updateThreadTimings(defScheduler->CurrentExec)
        || defScheduler->CurrentExec->Suspended) {
        nextThread();
        if (!defScheduler->CurrentExec) {
            return;
        }
    }

    error = sigDispatchSignals(defScheduler->CurrentExec);
    if (error == -ERROR_PROCESS_DIED) {
      defScheduler->CurrentExec = (struct thread*)defScheduler->ExecList.Next;
      nextThread();
    }
    
    if (!defScheduler->CurrentExec->Timing.LastCount) {
        defScheduler->CurrentExec->Timing.LastCount = readTimestamp();
    }
    
    intCtlAckInterrupt();
    schedIntersegmentTaskSwitch(&defScheduler->CurrentExec->Registers);
}

struct thread* schedGetCurrentThread()
{
    if (!defScheduler->CurrentExec)
        return NULL;
    return defScheduler->CurrentExec;
}

struct thread* schedGetThreadById(int id)
{
    struct thread* tmp = (struct thread*)defScheduler->ExecList.Next;
    while (tmp != NULL) {
        if (tmp->Identifier == id)
            return tmp;
        tmp = (struct thread*)tmp->List.Next;
    }

    return NULL;
}

void schedSleep(int ticks)
{
    struct thread* thr = schedGetCurrentThread();
    thr->Suspended = true;
    thr->SuspendedTicks = ticks;
    
    vectorInsert(&defScheduler->SleepingThreads, thr);
    asm volatile("sti; hlt");
}

/* Skip register saving for the next tick.

   Callers usually use this to prevent 
    artificially manipulated register state to be overwritten
    during scheduler thinking */
void schedSkipRegisterSave(void)
{
    defScheduler->ShouldSaveRegisters = false;
}

uint64_t syscSleep(union sysc_regs* regs)
{
  unsigned long secs = regs->Arg1;
  schedSleep((secs * 1000) / APIC_TICKING_PER_MILLIS);
  return 0;
}
