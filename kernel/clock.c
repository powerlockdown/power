#include "power/memory/user_buffer.h"
#include "power/um.h"
#include <power/time.h>

#include <power/error.h>
#include <power/memory/memset.h>

/* Wallclock. */
static wclocktm_t wcaccum = 0;
static struct wdate wallclock;

/* Monotonic. */ 
static wclocktm_t monaccum = 0;
static wclocktm_t monclock = 0;

/* Returns whether such year
   is a leap year. */
static inline
int calisleap(int ye)
{
    return (ye % 400) == 0 || 
           (ye % 4 == 0 && ye % 100 != 0);
}

/* Returns the number of days 
   the month  has. (1 = January) */
static inline
int calgetdaycount(int mo, int ye)
{
    /* Odd months until July have 31 days. */
    if (mo < 8 && mo % 2 != 0) {
        return 31;
    }

    /* Beginning from August, even months
       are the ones to have 31 days. */
    if (mo >= 8 && !(mo % 2)) {
        return 31;
    }
    
    /* February is special, though. */
    if (mo == 2) {
        if (calisleap(ye))
            return 29;
        return 28;
    }

    /* The rest has thirty. */
    return 30;
}

/* Increments a second, 
   which might increment a minute,
   which might increment an hour, 
   which might increment... */
static 
void clkincrement(struct wdate* clock)
{
    if (++clock->Second > 59) {
        clock->Second = 0;
        if (++clock->Minute > 59) {
            clock->Minute = 0;
            if (++clock->Hour > 23) {
                clock->Hour = 0;
                if (++clock->Day > calgetdaycount(clock->Month, clock->Year)) {
                    clock->Day = 1;
                    if (++clock->Month > 12) {
                        clock->Month = 1;
                        clock->Year++;
                        /* I might add a NT timebomb
                           if year >= 2038. */
                    }
                }
            }
        }
    }
}

/* Informs the clock that time
   has passed naturally.
   delta is the number of millis
   since the last call. */
void clkUpdate(int delta)
{
    wcaccum += delta;
    monaccum += delta;

    /* If this condition gets matched by "> 1000",
       some drift may accumulate over time, I think. */
    if (wcaccum >= 1000) {
        clkincrement(&wallclock);
        wcaccum = 0;
    }

    if (monaccum >= 1000) {
        monclock++;
        monaccum = 0;
    }
}


int clkGetTime(struct wdate* time)
{
    if (!time) {
        return -ERROR_INVALID_ARGUMENT;
    }

    memcpy(time, &wallclock, sizeof(wallclock));
    return 0;
}

void clkSetTime(const struct wdate* time)
{
    memcpy(&wallclock, time, sizeof(*time));
    wcaccum = 0;
}

uint64_t syscGetTimeOfDay(union sysc_regs* regs)
{
    /* GetTimeOfDay(struct __wallclock*) -> int */
    struct __wallclock* wc = (void*)regs->Arg1;
    if (mmUserBufferCheck(wc, sizeof(*wc), UBUFFER_WRITE_BIT)) {
        return -ERROR_USER_FAULT;
    }

    wc->wk_day = wallclock.Day;
    wc->wk_hour = wallclock.Hour;
    wc->wk_minute = wallclock.Minute;
    wc->wk_month = wallclock.Month;
    wc->wk_year = wallclock.Year;
    wc->wk_second = wallclock.Second;
    return 0;
}
