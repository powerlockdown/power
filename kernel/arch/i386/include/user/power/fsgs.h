/* fsgs.h
   Purpose: FS/GS read write. */
#pragma once

/* The FsGsControl syscall goes like this.
   void FsGsControl(FSGS_WRITE | ..., unsigned long address); or
   void FsGsControl FSGS_READ | ..., unsigned long* address);

   THIS IS JUST AN SKELETON OF WHAT IT ACCEPTS. I am not going
   to write wrappers for this sysc because of its restricted
   usage.

   The ellipsis indicate one of FSGS_FS/GS macros defined below.
   You can also bypass this call entirely by just using the special
   instructions.
*/

#define FSGS_WRITE (1 << 16)
#define FSGS_READ  0

#define FSGS_FS (1 << 0)
#define FSGS_GS 0
