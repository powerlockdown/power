/* asm.h
   Purpose: generic inline assembly routines */
#pragma once

#include <power/config.h>

#define RED_ZONE_SIZE 128

/* a type which can fit all possible I/O ports. */
typedef unsigned short ioport;

__always_inline
static void __irqDisable()
{
    asm volatile("cli");
}

__always_inline
static void __irqEnable()
{
    asm volatile("sti");
}

__always_inline 
static uint8_t __inb(uint16_t port)
{
    uint8_t ret;
    asm volatile ("inb %1, %0" : "=a"(ret) : "Nd"(port) : "memory");
    return ret;
}

__always_inline 
static uint16_t __inw(uint16_t port)
{
    uint16_t ret;
    asm volatile ("inw %1, %0" : "=a"(ret) : "Nd"(port) : "memory");
    return ret;
}

__always_inline 
static uint32_t __inl(uint16_t port)
{
    uint32_t ret;
    asm volatile ("inl %1, %0" : "=a"(ret) : "Nd"(port) : "memory");
    return ret;
}

__always_inline 
static void __outb(uint16_t port, uint8_t val)
{
    asm volatile ( "outb %0, %1" : : "a"(val), "Nd"(port) : "memory");
}

__always_inline 
static void __outw(uint16_t port, uint16_t val)
{
    asm volatile ( "outw %0, %1" : : "a"(val), "Nd"(port) : "memory");
}

__always_inline 
static void __outl(uint16_t port, uint32_t val)
{
    asm volatile ( "outl %0, %1" : : "a"(val), "Nd"(port) : "memory");
}

__always_inline 
static void __repInw(uint16_t port, uint16_t* buffer, size_t length)
{
    asm volatile("rep insw"
        : "+D" (buffer), "+c" (length), "=m" (*buffer)
        : "d" (port)
        : "memory");
}

__always_inline 
static void __repInl(uint16_t port, uint32_t* buffer, size_t length)
{
    asm volatile("rep insl"
        : "+D" (buffer), "+c" (length), "=m" (*buffer)
        : "d" (port)
        : "memory");
}

__always_inline
static void __halt()
{
    /* IF for reason is always getting unset. */
    asm volatile("sti; hlt");
}

#define FP_STATE_BUFFER_SIZE 512
#define FP_STATE_BUFFER_ALIGNMENT 16

__always_inline
static void fpsSave(char* state)
{
    asm volatile("fxsave %0" : "=m"(*state));
}

__always_inline
static void fpsRestore(void* state)
{
    /* BROKEN. Not really a priority to fix,
       given the only use case for this instruction
       is written in assembly file. */
    asm volatile("fxrstor %0" :: "a"(state));
}
