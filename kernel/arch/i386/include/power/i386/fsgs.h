/* fsgs.h
   Purpose: R/W into FS/GS MSRs */
#pragma once

#include <power/um.h>
#include <power/fsgs.h>
#include <power/scheduler.h>

void fgsPushSegments(struct thread* t);
void fgsPopSegments(struct thread* t);

uint64_t syscFsGsControl(union sysc_regs*);
