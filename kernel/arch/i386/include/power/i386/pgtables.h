/* paging.h
   Purpose: support for virtual memory paging
            in x86 arch. */
#pragma once

#include <power/config.h>

#define PT_PAGING_FLAGS (511 | (1ULL << 63))

#define PT_FLAG_PRESENT  (1UL << 0)
#define PT_FLAG_WRITE    (1UL << 1)
#define PT_FLAG_USER     (1UL << 2)
#define PT_FLAG_PCD      (1UL << 4)
#define PT_FLAG_PS       (1UL << 7)
#define PT_FLAG_LARGE    (1UL << 7)
#define PT_FLAG_GUARD    PGUARD_BIT
#define PT_FLAG_NX       (1UL << 63)

#define PGUARD_BIT         (1 << 2)
#define PGUARD_USER_BIT    (1 << 3)
#define PGUARD_LENGTH_SHIFT 8
#define PGUARD_LENGTH_BITS (255 << 8)

typedef unsigned long address_space_t;

#define PAGE_SIZE 4096
