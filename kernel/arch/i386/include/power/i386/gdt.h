/* gdt.h
   Purpose: Global Descriptor Table loading */
#pragma once

#include <power/config.h>

/* REMEMBED TO EDIT syscall install code (in fet.s) 
   if editing these macros*/
#define KERNEL_CODE_SEGMENT 0x18
#define KERNEL_DATA_SEGMENT 0x20
#define USER_CODE_SEGMENT 0x30
#define USER_DATA_SEGMENT 0x28

struct __packed gdt_pointer
{
    uint16_t Size;
    uint64_t Pointer;
};

struct gdt_entry
{
    uint32_t Base;
    uint16_t Limit;
    uint8_t AccessByte;
    uint8_t Flags;
};

struct __packed gdt_entry_encoded
{
    uint16_t Limit;
    uint16_t BaseLow16;
    uint8_t BaseMid8;
    uint8_t AccessByte;
    uint8_t Flags;
    uint8_t BaseHigh8;
};

struct __packed gdt_tss_entry_encoded {
    struct gdt_entry_encoded Gdt;
    uint32_t BaseHigher32;
    uint32_t Reserved;
} ;

struct gdt_entries
{
    struct gdt_entry_encoded Null;
    struct gdt_entry_encoded Code32Bit;
    struct gdt_entry_encoded Data32Bit;
    struct gdt_entry_encoded KernelCode64Bit;
    struct gdt_entry_encoded KernelData64Bit;
    struct gdt_entry_encoded UserData64Bit;
    struct gdt_entry_encoded UserCode64Bit;
    struct gdt_tss_entry_encoded Tss;
};

struct __packed tss
{
    uint32_t Reserved0;
    uint64_t Rsp[3];
    uint64_t Reserved1;
    uint64_t Ist[7];
    uint64_t Reserved2;
    uint16_t Reserved3;
    uint16_t Iopb;
};

void gdtInit();
void gdtCreateIstStacks();
