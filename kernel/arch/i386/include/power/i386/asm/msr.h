/* msr.h
   Purpose: Model-specific registers */
#pragma once

#include <stdint.h>

#define MSR_FS_BASE 0xC0000100
#define MSR_GS_BASE 0xC0000101

static inline
void __wrmsr(unsigned long msr, unsigned long value)
{
  unsigned int low = value & 0xFFFFFFFF;
  unsigned int high = value >> 32;
  asm volatile ("wrmsr" :: "c"(msr), "a"(low), "d"(high));
}

static inline
unsigned long __rdmsr(unsigned long msr)
{
  unsigned long value;
  asm volatile("rdmsr" : "=A"(value) : "c"(msr));
  return value;
}
