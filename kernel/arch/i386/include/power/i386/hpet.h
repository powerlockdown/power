/* hpet.h
   Purpose: High Precision Event Timer (HPET) */

#include <power/config.h>
#include <power/abstract/timer.h>
#include <power/_acpi.h>

struct __packed hpet_header
{
    struct acpi_system_desc_header Header;
    uint32_t EventBlockTimerId;
    struct acpi_address_structure Address;
    uint8_t TimerNumber;
    uint8_t PageProtectionOem;
};

struct hpet_timer
{
    struct timer Base;
    volatile uint64_t* BaseAddress;
    uint64_t TimerPeriod;
    int ApicLine;
    int TimerNumber;
};

struct hpet_timer* hpetCreateTimer();

/* Used in HPET Legacy Mode */
void pitDisable();
