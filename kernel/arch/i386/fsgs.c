/* fsgs.c
   Purpose: system calls that allow
            R/W onto FS/GS_BASE MSRs. */
#include "power/fsgs.h"

#include "power/i386/asm/msr.h"
#include "power/memory/user_buffer.h"
#include <power/i386/fsgs.h>
#include <power/scheduler.h>

/* Update FS/GS registers for a thread
   that is going away. */
void fgsPushSegments(struct thread* t)
{
  t->Registers.FsSegment = __rdmsr(MSR_FS_BASE);
  t->Registers.GsSegment = __rdmsr(MSR_GS_BASE);
}

/* Update MSRs based on what is
   in thread's registers. */
void fgsPopSegments(struct thread* t)
{
  __wrmsr(MSR_FS_BASE, t->Registers.FsSegment);
  __wrmsr(MSR_GS_BASE, t->Registers.GsSegment);
}

uint64_t syscFsGsControl(union sysc_regs* regs)
{
  /* FsGsControl(int action, unsigned long* value) [IF action is *_READ]
     FsGsControl int action, unsigned long value)  [IF action is *_WRITE] */
  int action = regs->Arg1 & (0xFFFF << 15);
  int regstr = regs->Arg1 & (0xFFFF);

  struct thread* thread;
  thread = schedGetCurrentThread();

  int error;
  uintptr_t* dest;
  switch(action) {
  case FSGS_READ:
    dest = (uintptr_t*)regs->Arg2;
    if ((error = mmUserBufferCheck(dest, sizeof(uintptr_t), UBUFFER_WRITE_BIT))) {
      return error;
    }

    if (regstr == FSGS_FS) {
      (*dest) = __rdmsr(MSR_FS_BASE);
      return 0;
    } else if (regstr == FSGS_GS) {
      (*dest) = __rdmsr(MSR_GS_BASE);
      return 0;
    }
    
    break;
  case FSGS_WRITE:
    if (regstr == FSGS_FS) {
      __wrmsr(MSR_FS_BASE, regs->Arg2);
      thread->Registers.FsSegment = regs->Arg2;
    } else if (regstr == FSGS_GS) {
      __wrmsr(MSR_GS_BASE, regs->Arg2);
      thread->Registers.GsSegment = regs->Arg2;
    }
  }
  return 0;
}
