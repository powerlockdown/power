#include "power/_acpi.h"
#include "power/interrupt.h"
#include "power/time.h"
#include <power/cmos.h>

#include <stdint.h>
#include <power/i386/asm.h>
#include <power/abstract/timer.h>
#include <power/memory/memset.h>

/* Is 20ms too much or too little?
   TODO: PLEASE VERIFY. */
#define CMOS_COOLDOWN 20

#define CMOS_DISABLE_NMI_BIT (1 << 7)

#define CMOS_REGISTER_PORT 0x70
#define CMOS_DATA_PORT     0x71

#define CMOS_SECONDS 0x00
#define CMOS_MINUTES 0x02
#define CMOS_HOURS   0x04
#define CMOS_WEEKDAY 0x06
#define CMOS_DAY     0x07
#define CMOS_MONTH   0x08
#define CMOS_YEAR    0x09
#define CMOS_STATUSA 0x0A
#define CMOS_STATUSB 0x0B

/* RTC in the middle of a write. */
#define CMOS_SA_WRITING_BIT (1 << 7)

struct cmos_clock {
    uint8_t Seconds; /*< 0-59. */
    uint8_t Minutes; /*< 0-59. */
    uint8_t Hours;   /*< 0-23 or 0-12. */

    uint8_t Weekday; /*< 1-7. Sunday = 1. */
    uint8_t Day;     /*< 1 - 31. */
    uint8_t Month;   /*< 1 - 12. */
    uint8_t Year;    /* < 0 - 99. */
    uint8_t Century; /*< 19-20. */
};

static inline uint8_t bcddec(uint8_t bcd);

/* Waits a specified amount of time in millis.
   Used to not ratelimit the battery. */
static inline
void cmdelay(int del)
{
    struct timer* tm = tmGetDefaultTimer();
    tm->setReloadTime(tm, del * tm->MillisecondScale);
}

/* Reads a register from CMOS memory. */
static inline
int cmread(uint8_t reg)
{
    uint8_t acre, data;
    inhDisableInterrupts();
    acre = reg | CMOS_DISABLE_NMI_BIT;
    __outb(CMOS_REGISTER_PORT, acre);

    cmdelay(CMOS_COOLDOWN);
    data = __inb(CMOS_DATA_PORT);
    inhEnableInterrupts();
    return data;
}

/* Writes a value into CMOS memory. */
__used static inline
void cmwrite(uint8_t reg, uint8_t value)
{
    inhDisableInterrupts();
    uint8_t acre = reg | CMOS_DISABLE_NMI_BIT;
    __outb(CMOS_REGISTER_PORT, acre);

    cmdelay(CMOS_COOLDOWN);
    __outb(CMOS_DATA_PORT, value);
    inhEnableInterrupts();
}

/* Attempts to read the century through
   the FADT century field. */
static
int cmgetcentury()
{
    static struct acpi_fadt* fadt = NULL;
    if (!fadt) {
        fadt = (struct acpi_fadt*)acpiFindTable("FACP");
    }

    if (fadt->Century) {
        return cmread(fadt->Century);
    }

    /* The century we are 
       currently in minus one. */
    return 0x20;
}

/* Fills a cmos_clock with its
   appropriate, unprocessed values. */
static
void cmgetclock(struct cmos_clock* clock)
{
    clock->Seconds = bcddec(cmread(CMOS_SECONDS));
    clock->Minutes = bcddec(cmread(CMOS_MINUTES));
    clock->Hours   = bcddec(cmread(CMOS_HOURS));
    clock->Weekday = cmread(CMOS_WEEKDAY);
    clock->Day     = bcddec(cmread(CMOS_DAY));
    clock->Month   = bcddec(cmread(CMOS_MONTH));
    clock->Year    = bcddec(cmread(CMOS_YEAR));
    clock->Century = bcddec(cmgetcentury());
}

/* Compare two clocks and check if
   they are equal. */
static inline
int cmcompareclock(struct cmos_clock* c1, struct cmos_clock* c2)
{
    return (c1->Century == c2->Century) && (c1->Day == c2->Day)
         && (c1->Hours == c2->Hours) && (c1->Minutes == c2->Minutes)
         && (c1->Seconds == c2->Seconds) && (c1->Weekday == c2->Weekday)
         && (c1->Year == c2->Year);
}

/* Like getclock, but waits for CMOS to be
   completely OK for reads. */
static
void cmcomparedgetclock(struct cmos_clock* clock)
{
    while (cmread(CMOS_STATUSA) & CMOS_SA_WRITING_BIT);
    
    cmgetclock(clock);
    struct cmos_clock sec;
    while (1) {
        while (cmread(CMOS_STATUSA) & CMOS_SA_WRITING_BIT);

        cmgetclock(&sec);
        if (cmcompareclock(clock, &sec)) {
            break;
        }

        memcpy(clock, &sec, sizeof(sec));
    }
}

/* converts a cmos_clock to a generic wdate. */
static inline
void cmclktodate(const struct cmos_clock* clock, 
                 struct wdate* restrict wd)
{
    wd->Day = clock->Day;
    wd->Month = clock->Month;
    wd->Year = (clock->Century * 100) + clock->Year;
    wd->Hour = clock->Hours;
    wd->Minute = clock->Minutes;
    wd->Second = clock->Seconds;
}

/* Initially reads the CMOS clock
   and writes it to the system. */
void cmosInit()
{
    struct wdate cvt;
    struct cmos_clock clock;
    cmcomparedgetclock(&clock);
    cmclktodate(&clock, &cvt);
    clkSetTime(&cvt);
}

/* Convert a binary coded decimal
   to a plain binary. */
static inline
uint8_t bcddec(uint8_t bcd)
{
    uint8_t res = 0;
    res += bcd & 0xF;
    res += ((bcd >> 4) & 0xF) * 10;
    return res;
}
