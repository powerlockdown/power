#include "power/i386/apic.h"
#include <power/time.h>
#include <power/panic.h>

#define __INH_DEFINE
#include <power/i386/idt.h>
#include <power/interrupt.h>
#include <power/paging.h>

#include <power/signal.h>

#include <power/config.h>
#include <power/memory/heap.h>
#include <power/memory/pfa.h>

#include <power/scheduler.h>
#include <power/tty.h>
#include <stdbool.h>

#include <power/user_signal.h>

int __inh_count; /*< see interrupt.h. */

extern void* rootHeapAddressSpace;
extern volatile bool hpetSleepFlag;

static void divisionError(struct irq_regs* restrict st)
{
  panic("Division error", st);
}

static void undefinedOpcode(struct irq_regs* restrict st)
{
  panic("Invalid instruction", st);
}

static void generalProtectionFault(int error,
                                   struct irq_regs* restrict st)
{
  trmLogfn("GP error code: %p", error);
  panic("General protection fault", st);
}

static void pageFault(int error, struct irq_regs* restrict st)
{
  uintptr_t cr2;
  inhDisableInterrupts();
  asm volatile("movq %%cr2, %0" : "=r"(cr2));

  pgPageFault(st, cr2, error);
  inhEnableInterrupts();
}

static void apicTimer(struct irq_regs* restrict st)
{
    clkUpdate(APIC_TICKING_PER_MILLIS);
    if (schedIsEnabled())
        schedThink(st);
}

static void hpetTimer(struct irq_regs* restrict st)
{
    __unused(st);
    hpetSleepFlag = true;
}

void excInstallExceptionHandlers(interrupt_handler_pfn_t* restrict handlers)
{
    handlers[0] = (void*)divisionError;
    handlers[6] = (void*)undefinedOpcode;
    handlers[13] = (void*)generalProtectionFault;
    handlers[14] = (void*)pageFault;
    handlers[32] = (void*)apicTimer;
    handlers[33] = (void*)hpetTimer;
}
