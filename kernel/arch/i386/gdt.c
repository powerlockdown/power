#include <power/i386/gdt.h>
#include <power/memory/memset.h>
#include <power/tty.h>

#include <power/memory/pfa.h>

static struct tss tss;
static char _ring0Stack[4096];
static char _interruptStack[4096] __attribute__((aligned(16)));
static char _interrupt32Stack[4096] __attribute__((aligned(16)));

static struct gdt_entries entries = {
    { 0, 0, 0, 0, 0, 0 },
    { 0xffff, 0, 0, 0x9a, 0xcf, 0 },
    { 0xffff, 0, 0, 0x92, 0xcf, 0 },
    { 0xffff, 0, 0, 0x9a, 0xa2, 0 },
    { 0xffff, 0, 0, 0x92, 0xa0, 0 },
    { 0xffff, 0, 0, 0xF2, 0xa2, 0 },
    { 0xffff, 0, 0, 0xFA, 0x20, 0 },
    { { 0, 0, 0, 0, 0, 0 }, 0, 0 }
};

extern void gdtReloadSegments();

void gdtInit()
{
    entries.Tss.Gdt.BaseLow16 = ((uint64_t)(&tss) & 0xFFFF);
    entries.Tss.Gdt.BaseMid8  = (((uint64_t)(&tss) >> 16) & 0xFF);
    entries.Tss.Gdt.BaseHigh8 = (((uint64_t)(&tss) >> 24) & 0xFF);
    entries.Tss.BaseHigher32 = (((uint64_t)(&tss) >> 32) & 0xFFFFFFFF);
    entries.Tss.Gdt.AccessByte = 0x89;
    entries.Tss.Gdt.Limit = sizeof(struct tss);
    entries.Tss.Reserved = 0;

    memset(&tss, 0, sizeof(struct tss));
    tss.Rsp[0] = (uint64_t)_ring0Stack + 4096;
    tss.Ist[0] = (uint64_t)_interruptStack + 4096;
    tss.Ist[1] = (uint64_t)_interrupt32Stack + 4096;
    struct gdt_pointer ptr = { sizeof(struct gdt_entries) - 1, (uint64_t)&entries };

    asm volatile("lgdt %0" ::"m"(ptr));
    gdtReloadSegments();
}

void gdtCreateIstStacks()
{
    for (int i = 0; i < 8; i++) {
        tss.Ist[i] = (uintptr_t)mmAlignedAlloc(4096, 4096) + 4096;
    }
}
