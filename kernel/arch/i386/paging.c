#include "power/error.h"
#include "power/i386/pgtables.h"
#include "power/memory/hhdm.h"
#include <power/i386/idt.h>
#include <power/ldr_requests.h>
#include <power/paging.h>

#include <power/config.h>
#include <power/memory/heap.h>
#include <power/memory/memset.h>
#include <power/memory/page.h>

#include <power/memory/pfa.h>
#include <power/system.h>
#include <power/tty.h>

#define PAGE_CHECK(P, i) PAGE_CHECK_RET_VAL(P, i, )
#define PAGE_CHECK_RET_VAL(P, i, r)                                            \
    do {                                                                       \
        if (!(P[i] & PT_FLAG_PRESENT))                                         \
            return r;                                                          \
    } while (0)

struct global_page {
    uint64_t Physical;
    uint64_t Virtual;
    uint64_t Flags;
};

extern struct idt_encoded_entries entries;
extern uint64_t* rootHeapAddressSpace;

static struct global_page globalPages[10];
static int gpIndex = 0;

static int unrefPageTable(uint64_t* pagetbl);
static uint64_t* getNextLevel(uint64_t* level, size_t offset, int flags);

__always_inline static uint64_t readCr3()
{
    uint64_t cr3;
    asm volatile("movq %%cr3, %0" : "=r"(cr3));
    return cr3;
}

void* pgCreateAddressSpace()
{
    uint64_t hhdm = rqGetHhdmRequest()->offset;
    uint64_t* pml4 = mmAlignedPhysicalAlloc(4096, 4096);
    uint64_t* writable = PaAdd(pml4, hhdm);
    memset(writable, 0, 4096);

    uint64_t* wr = mmMakeWritable(rootHeapAddressSpace);

    /* Copy the higher half entirely from
       what the bootloader gives us. */
    writable[256] = wr[256];
    writable[257] = wr[257];
    writable[511] = wr[511];

    for (int i = 0; i < gpIndex; i++) {
        struct global_page* gp = &globalPages[i];
        pgMapPage(pml4, gp->Physical, gp->Virtual, gp->Flags);
    }

    pgRecycleDirectory(NULL, pml4, KERNEL_HEAP_BEGIN);
    // khRemapHeap(pml4);
    return pml4;
}

void pgMapPage(address_space_t* addrs, uint64_t paddr, uint64_t vaddr,
               uint64_t flags)
{
    if (!addrs)
        addrs = (address_space_t*)readCr3();
    
    flags |= PT_FLAG_PRESENT;

    uint64_t hhdm = rqGetHhdmRequest()->offset;
    size_t index4 = (size_t)(vaddr & (0x1FFUL << 39)) >> 39;
    size_t index3 = (size_t)(vaddr & (0x1FFUL << 30)) >> 30;
    size_t index2 = (size_t)(vaddr & (0x1FFUL << 21)) >> 21;
    size_t index1 = (size_t)(vaddr & (0x1FFUL << 12)) >> 12;

    uint64_t* pdpe = getNextLevel(addrs, index4, flags & ~PT_FLAG_PS);
    uint64_t* pde = getNextLevel(pdpe, index3, flags & ~PT_FLAG_PS);
    uint64_t* pte = getNextLevel(pde, index2, flags);

    uint64_t* writeable = PaAdd(pte, hhdm);
    writeable[index1] = paddr | flags;
    __barrier();
    
    asm volatile("invlpg (%0)" ::"r"(vaddr) : "memory");
}

void pgMapLargePage(address_space_t* addrs, uint64_t paddr, uint64_t vaddr,
                    uint64_t flags)
{
    if (!addrs)
        addrs = (address_space_t*)readCr3();

    uint64_t hhdm = rqGetHhdmRequest()->offset;
    flags |= PT_FLAG_PRESENT | PT_FLAG_PS;

    size_t index4 = (size_t)(vaddr & (0x1FFUL << 39)) >> 39;
    size_t index3 = (size_t)(vaddr & (0x1FFUL << 30)) >> 30;
    size_t index2 = (size_t)(vaddr & (0x1FFUL << 21)) >> 21;

    uint64_t* pdpe = getNextLevel(addrs, index4, flags & ~PT_FLAG_PS);
    uint64_t* pde = getNextLevel(pdpe, index3, flags & ~PT_FLAG_PS);
    uint64_t* writeable = PaAdd(pde, hhdm);
    writeable[index2] = paddr | flags;
    asm volatile("invlpg (%0)" ::"r"(vaddr) : "memory");
}

void pgUnmapPage(address_space_t* addrs, uint64_t vaddr)
{
    if (!addrs)
        addrs = (address_space_t*)readCr3();

    size_t index4 = (size_t)(vaddr & (0x1FFUL << 39)) >> 39;
    size_t index3 = (size_t)(vaddr & (0x1FFUL << 30)) >> 30;
    size_t index2 = (size_t)(vaddr & (0x1FFUL << 21)) >> 21;
    size_t index1 = (size_t)(vaddr & (0x1FFUL << 12)) >> 12;

    addrs = mmMakeWritable(addrs);
    PAGE_CHECK(addrs, index4);
    uint64_t* pdpe =
        mmMakeWritable((uint64_t*)(addrs[index4] & ~PT_PAGING_FLAGS));
    if (unrefPageTable(pdpe)) {
        return;
    }

    PAGE_CHECK(pdpe, index3);
    uint64_t* pde =
        mmMakeWritable((uint64_t*)(pdpe[index3] & ~PT_PAGING_FLAGS));
    if (unrefPageTable(pde)) {
        return;
    }

    PAGE_CHECK(pde, index2);
    uint64_t* pte = mmMakeWritable((uint64_t*)(pde[index2] & ~PT_PAGING_FLAGS));
    if (unrefPageTable(pte)) {
        return;
    }

    PAGE_CHECK(pte, index1);

    pte[index1] = 0;
    asm volatile("invlpg (%0)" ::"r"(vaddr) : "memory");
}

void pgSetCurrentAddressSpace(address_space_t* address)
{
    asm volatile("mov %0, %%cr3" ::"r"(address));
}

uint64_t* getNextLevel(uint64_t* level, size_t offset, int flags)
{
    level = mmMakeWritable(level);

    struct page* pg = NULL;
    if (!(level[offset] & PT_FLAG_PRESENT)) {
        uint64_t* off;
        pg = mmAllocPage(1);

        level[offset] = (uintptr_t)(pg->Address);
        off = mmMakeWritable((void*)level[offset]);
        memset(off, 0, 4096);
    }

    void* clean = (uintptr_t*)((level[offset]) & ~(PT_PAGING_FLAGS | PT_FLAG_NX));
    if (!pg)
        pg = mmGetPage(clean);
    if (pg)
      pg->RefCount++;
    level[offset] |= flags;
    return clean;
}

address_space_t* pgGetAddressSpace()
{
    return (address_space_t*)readCr3();
}

void* pgGetPhysicalAddress(address_space_t* addrs, uint64_t vaddr)
{
    if (!addrs)
        addrs = (address_space_t*)readCr3();

    size_t index4 = (size_t)(vaddr & (0x1FFUL << 39)) >> 39;
    size_t index3 = (size_t)(vaddr & (0x1FFUL << 30)) >> 30;
    size_t index2 = (size_t)(vaddr & (0x1FFUL << 21)) >> 21;
    size_t index1 = (size_t)(vaddr & (0x1FFUL << 12)) >> 12;
    size_t offset = (size_t)(vaddr & (0xFFFUL));

    addrs = mmMakeWritable(addrs);
    uint64_t* pdpe =
        mmMakeWritable((uint64_t*)(addrs[index4] & ~PT_PAGING_FLAGS));
    uint64_t* pde =
        mmMakeWritable((uint64_t*)(pdpe[index3] & ~PT_PAGING_FLAGS));
    PAGE_CHECK_RET_VAL(pde, index2, NULL);

    /* 2-MiB page. */
    if (pde[index2] & PT_FLAG_LARGE) {
        offset = (size_t)(vaddr & (0x1fffff));
        uint64_t* pg = (uint64_t*)(pde[index2] & ~PT_PAGING_FLAGS);
        return PaAdd(pg, offset);
    }

    uint64_t* pte = mmMakeWritable((uint64_t*)(pde[index2] & ~PT_PAGING_FLAGS));
    PAGE_CHECK_RET_VAL(pte, index1, NULL);

    return (void*)((pte[index1] & ~(511 | (1ULL << 63))) + offset);
}

void pgAddGlobalPage(uint64_t paddr, uint64_t vaddr, uint64_t flags)
{
    struct global_page* gp = &globalPages[gpIndex];
    gp->Flags = flags;
    gp->Physical = paddr;
    gp->Virtual = vaddr;

    gpIndex++;
    pgMapPage(NULL, paddr, vaddr, flags);
}

int pgGuardPage(address_space_t* addrs, uint64_t vaddr, size_t length,
                int flags)
{
    __unused(length); /* TODO: remove this param */
    if (!addrs)
        addrs = (address_space_t*)readCr3();

    flags |= PT_FLAG_PRESENT;

    uint64_t oldv = vaddr;
    vaddr = oldv;
    size_t index4 = (size_t)(vaddr & (0x1FFUL << 39)) >> 39;
    size_t index3 = (size_t)(vaddr & (0x1FFUL << 30)) >> 30;
    size_t index2 = (size_t)(vaddr & (0x1FFUL << 21)) >> 21;
    size_t index1 = (size_t)(vaddr & (0x1FFUL << 12)) >> 12;

    uint64_t* pdpe = getNextLevel(addrs, index4, flags);
    uint64_t* pde = getNextLevel(pdpe, index3, flags);
    uint64_t* pte = getNextLevel(pde, index2, flags);
    pte = mmMakeWritable(pte);
    pte[index1] = 0;

    pte[index1] |= PGUARD_BIT;
    if (flags & PT_FLAG_USER) {
        pte[index1] |= PGUARD_USER_BIT;
    }

    return 0;
}

void* pgUnguardPage(address_space_t* addrs, uint64_t vaddr)
{
    if (!addrs)
        addrs = (address_space_t*)readCr3();

    addrs = mmMakeWritable(addrs);
    struct limine_hhdm_response* hhdm = rqGetHhdmRequest();
    size_t index4 = (size_t)(vaddr & (0x1FFUL << 39)) >> 39;
    size_t index3 = (size_t)(vaddr & (0x1FFUL << 30)) >> 30;
    size_t index2 = (size_t)(vaddr & (0x1FFUL << 21)) >> 21;
    size_t index1 = (size_t)(vaddr & (0x1FFUL << 12)) >> 12;

    uint64_t *pdpe, *pde, *pte;

    PAGE_CHECK_RET_VAL(addrs, index4, ERROR_INTO_PTR(-1));
    pdpe = (uint64_t*)(addrs[index4] & ~PT_PAGING_FLAGS);
    pdpe = PaAdd(pdpe, hhdm->offset);

    PAGE_CHECK_RET_VAL(pdpe, index3, ERROR_INTO_PTR(-1));
    pde = (uint64_t*)(pdpe[index3] & ~PT_PAGING_FLAGS);
    pde = PaAdd(pde, hhdm->offset);

    PAGE_CHECK_RET_VAL(pde, index2, ERROR_INTO_PTR(-1));
    pte = (uint64_t*)(pde[index2] & ~PT_PAGING_FLAGS);
    pte = PaAdd(pte, hhdm->offset);
    if (pte[index1] & PT_FLAG_PRESENT || !(pte[index1] & PGUARD_BIT))
      return ERROR_INTO_PTR(-ERROR_INVALID_ARGUMENT);

    bool user = pte[index1] & PGUARD_USER_BIT;
    pte[index1] &= ~(PGUARD_BIT | PGUARD_USER_BIT);

    void* mmap = mmAlignedPhysicalAlloc(4096, 4096);
    if (!mmap)
      return ERROR_INTO_PTR(-ERROR_NO_MEMORY);
    
    pgMapPage(addrs, (uint64_t)mmap, vaddr,
	      (user ? PT_FLAG_USER : 0) | PT_FLAG_WRITE);

    mmap = mmMakeWritable(mmap);
    memset(mmap, 0, 4096);
    return mmap;
}

void pgDumpPageTable(address_space_t* address, uint64_t vaddr)
{
    if (!address)
        address = (address_space_t*)readCr3();
    trmLogfn("Dumping page tables of address %p inside CR3 %p", vaddr, address);
    size_t index4 = (size_t)(vaddr & (0x1FFUL << 39)) >> 39;
    size_t index3 = (size_t)(vaddr & (0x1FFUL << 30)) >> 30;
    size_t index2 = (size_t)(vaddr & (0x1FFUL << 21)) >> 21;
    size_t index1 = (size_t)(vaddr & (0x1FFUL << 12)) >> 12;

    address = mmMakeWritable(address);
    uint64_t* pdpe = mmMakeWritable((uint64_t*)(address[index4] & ~511));
    trmLogfn("PML4E %p offset %i -> PDPE %p (wf %p)", address, index4, pdpe,
             address[index4]);

    uint64_t* pde = mmMakeWritable((uint64_t*)(pdpe[index3] & ~511));
    trmLogfn("PDPE %p offset %i -> PDE %p (wf %p)", pdpe, index3, pde,
             pdpe[index3]);
    uint64_t* pte = mmMakeWritable((uint64_t*)(pde[index2] & ~511));
    trmLogfn("PDE %p offset %i -> PTE %p (wf %p)", pde, index2, pte,
             pde[index2]);
    trmLogfn("PTE %p offset %i -> PADDR %p (wf!)", pte, index1, pte[index1]);
}

int unrefPageTable(uint64_t* pagetbl)
{
    struct page* pg;
    pg = mmGetPage(pagetbl);
    pg->RefCount--;
    if (!pg->RefCount) {
        mmFreePage(pagetbl);
        return 1;
    }

    return 0;
}

/* The entire range of addresses A...A + 0x8000000000 
   of target will behave exactly as source and all changes
   applied to source (mapping, unmapping, reprotecting etc)
   will apply to target. */
void pgRecycleDirectory(address_space_t* source, address_space_t* target,
                        uint64_t vaddr)
{
    if (!source) {
        source = pgGetAddressSpace();
    }

    if (!target) {
        target = pgGetAddressSpace();
    }

    if (source == target) {
        trmLogfn("RecycleDirectory: source == target (%p)", source);
        return;
    }

    address_space_t* tw = mmMakeWritable(target);
    address_space_t* sw = mmMakeWritable(source);

    size_t index4 = (size_t)(vaddr & (0x1FFUL << 39)) >> 39;
    tw[index4] = sw[index4];
}

uint64_t pgGetRawPage(address_space_t* address, uintptr_t vaddr,
                      int* size)
{
    if (!address) {
        address = pgGetAddressSpace();
    }

    size_t index4 = (size_t)(vaddr & (0x1FFUL << 39)) >> 39;
    size_t index3 = (size_t)(vaddr & (0x1FFUL << 30)) >> 30;
    size_t index2 = (size_t)(vaddr & (0x1FFUL << 21)) >> 21;
    size_t index1 = (size_t)(vaddr & (0x1FFUL << 12)) >> 12;

    uintptr_t pdp, pd;

    address = mmMakeWritable(address);
    uint64_t* pdpe = mmMakeWritable((uint64_t*)(address[index4] & ~PT_PAGING_FLAGS));
    PAGE_CHECK_RET_VAL(address, index4, 0);

    pdp = (uintptr_t)address[index4];

    if (pdp & PT_FLAG_PS) {
        /* Maps a 1GB page. */
        if (size)
            (*size) = 1 * GB;
        return pdp;
    }

    uint64_t* pde = mmMakeWritable((uint64_t*)(pdpe[index3] & ~PT_PAGING_FLAGS));
    PAGE_CHECK_RET_VAL(pdpe, index3, 0);

    pd = (uintptr_t)pdpe[index3];

    if (pd & PT_FLAG_PS) {
        /* Maps a 2MB page. */
        if (size)
            (*size) = 2 * MB;
        return pd;
    }

    uint64_t* pte = mmMakeWritable((uint64_t*)(pde[index2] & ~PT_PAGING_FLAGS));
    PAGE_CHECK_RET_VAL(pde, index2, 0);

    if (size)
        (*size) = 4 * KB;
    return (uintptr_t)pte[index1];
}

void pgDestroyAddressSpace(address_space_t* address)
{
  /* Makes to sense to delete
     the current address space. */
  if (!address)
    return;

  mmAlignedFree(address, 4096);
}
