#include <power/i386/apic.h>
#include <power/abstract/intrctl.h>
#include <power/abstract/timer.h>

#include <power/memory/heap.h>
#include <power/tty.h>

static int awaitTimer(volatile uint32_t* lapic)
{
    struct timer* d = tmGetDefaultTimer();

    ApicValue(lapic, 0x3E0) = 0b0011; /* Divide by 16 */
    uint32_t v = ApicValue(lapic, 0x320);

    /* Masking lower byte is necessary because VBox likes
     * to place random vector number that will make it 
     * trigger the wrong IRQ. */
    v &= ~(1 << 16 | 0xFF);
    v |= 32 | (1 << 17);
    ApicValue(lapic, 0x320) = v;

    lapic[LAPIC_REG(0x380)] = 0xFFFFFFFF;
    tmSetReloadValue(d, tmGetMillisecondUnit(d) * APIC_TICKING_PER_MILLIS);

    uint32_t cnt = (0xFFFFFFF - lapic[LAPIC_REG(0x390)]) & 0xFFFF;
    lapic[LAPIC_REG(0x380)] = cnt;
    return cnt;
}

struct apic_timer* apicCreateTimer()
{
    asm volatile("xchg %bx,%bx");
    if (intCtlGetControllerType() != INT_CTL_TYPE_APIC) {
        trmLogfn("apicCreateTimer failed: not using APIC (ctl = %i)", intCtlGetControllerType());
        return NULL;
    }

    struct apic_timer* this = khsAlloc(sizeof(*this));
    volatile uint32_t* lp = apicGetLocalAddress();
    uint32_t ct = awaitTimer(lp);
    ApicValue(lp, 0x380) = ct;
    
    return this;
}
