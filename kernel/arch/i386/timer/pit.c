#include <power/i386/asm.h>
#include <stdint.h>

void pitDisable()
{
    __outb(0x43, (0b11 << 4) | 1 << 1);
    __outb(0x40, 0);
    __outb(0x40, 0);
}
