/* page_fault.c
   Purpose: generic handler for page faults. */
#include "power/i386/idt.h"
#include "power/i386/pgtables.h"
#include "power/paging.h"
#include "power/um.h"
#include "power/user_signal.h"
#include <power/config.h>

#include <power/arch/i386/page_fault.h>
#include <power/memory/vm.h>
#include <power/panic.h>

static inline
int getVmFlags(int pf);

static inline
void sendSegv(struct process* proc,
	      struct irq_regs* irq, uintptr_t address);    

int pgPageFault(struct irq_regs* irq,
		uintptr_t address, int flags)
{
  int vmf;
  uint64_t page;
  
  if (flags & PFAULT_USER_BIT) {
    struct process* proc;
    proc = pcGetCurrentProcess();

    vmf = getVmFlags(flags);
    page = pgGetRawPage(proc->Executable.AddressSpace, address, NULL);
    if (page & PGUARD_USER_BIT)
      vmf |= VFAULT_GUARDED;
    
    if (vmHandleFault(proc->VirtualMemory, address, vmf)) {
      sendSegv(proc, irq, address);
    }

    return 0;
  }

  if (!(flags & PFAULT_USER_BIT)) {
    page = pgGetRawPage(NULL, address, NULL);
    if (page & PT_FLAG_GUARD)
      if (!pgUnguardPage(NULL, address))
	panic("No memory to unguard page", irq);
  }
    
  return 0;
}

void sendSegv(struct process* proc, struct irq_regs* irq, uintptr_t address)
{
  struct signal_request* sr;
  sr = sigCreateRequest(SIG_CREATE_INFO_BIT | SIG_WAIT_BIT);
  if (unlikely(!sr))
    panic("Cannot submit signal", NULL);

  sr->Signal = SIGSEGV;
  sr->Info->si_addr = (void*)address;
  sr->Info->si_pid = proc->Identifier;
  sigSubmitSignal(proc, sr, irq);
}

int getVmFlags(int pf)
{
  int result = 0;
  if (pf & PFAULT_WRITE_BIT)
    result |= VFAULT_WRITE;

  return result;
}
  
