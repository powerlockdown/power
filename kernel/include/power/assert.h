/* assert.h
   Purpose: condition is true else fail */
#pragma once

#include <power/config.h>
#include <power/panic.h>

#define PANIC_IF(E) do { if (unlikely((E))) { panic("Expression '" #E "' asserted to be false.", NULL); }} while (0)
