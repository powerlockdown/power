/* pci.h
   Purpose: PCI support */
#pragma once

#include <power/config.h>
#include <power/interrupt.h>

#define PCI_DONT_CARE -1

#define PCI_ERROR_OK            0
#define PCI_ERROR_NOT_SUPPORTED 1

struct pci_search_query {
    uint16_t Vendor, DeviceId;
    uint8_t Class, SubClass;
    uint8_t ProgIf;

    /* Not really used for SearchDevice,
       but we need for LoadReason */
    uint8_t Bus, Function, Device;
};

struct pci_device {
    struct pci_device* Next;
    uint8_t Bus, Function, Device;
};

typedef void (*pci_interrupt_t)(void* data);

struct pci_device;
struct pci_device* pciSearchDevice(uint16_t vendor, uint16_t device);
struct pci_device* pciSearchDeviceEx(struct pci_search_query);

uint32_t pciReadDoubleWord(uint8_t bus, uint8_t device, uint8_t function,
                           uint8_t offset);
uint32_t pciReadDoubleWordFromDevice(const struct pci_device* device,
                                     uint8_t offset);
void pciWriteDoubleWord(uint8_t bus, uint8_t device, uint8_t function,
                        uint8_t offset, uint32_t data);
void pciWriteDoubleWordToDevice(const struct pci_device*, uint8_t offset,
                                uint32_t data);
void pciInitializeRegistry();

int pciHandleMessageInterrupt(struct pci_device* dev,
                              interrupt_handler_pfn_t pfn, void* data);
int pciHandleInterrupt(struct pci_device* dev, interrupt_handler_pfn_t pfn,
                       void* data);
