/* scsi.h
   Purpose: generic SCSI library */
#pragma once

#include <power/driver.h>

/* A device might be ATTACHED but not INSERTED.
   E.g.: Empty ATAPI disc readers are physically ATTACHED
         but without any CD INSERTED onto it.
         
    INSERTED bit will always be set if the device
    cannot eject its internal memory without deattaching
    itself. (E.g. USB flash drives) */

#define SCSI_DEVICE_ATTACHED_BIT (1 << 0)
#define SCSI_DEVICE_INSERTED_BIT (1 << 1)

/*< If the data direction is to the device (write, 1) or to the host (read, 0). */
#define SCSI_CMD_WRITE_BIT (1 << 0)

/** Error occured during packet execution. This must be set by
    the underlying device if and only if sense data is available
    for us to consume. If an error occurs and no sense key exists
    just return an ERROR_IO. */
#define SCSI_CMD_ERROR_BIT (1 << 1)

struct scsi_packet;

struct scsi_device {
    struct driver_disk_device_interface Base;
    int Flags;
    
    int SectorSize;
    uint64_t SectorCount;

    void* Device;
    int (*sendCommand)(struct scsi_device*, struct scsi_packet*);
};

struct scsi_packet {
    int Flags;
    int TransferLength;
    char* TransferBuffer;

    int CommandLength;
    const char* CommandData;
};

void scsiTestUnit(struct scsi_device* device);
int scsiAttachDevice(struct scsi_device*, int secSize, int flags);

int scsiReadSector(struct driver_disk_device_interface* device,
                   uint64_t lba, size_t length, uint8_t* restrict buffer);
uint32_t scsiCmdGetTransferLength(uint8_t* cdb);
