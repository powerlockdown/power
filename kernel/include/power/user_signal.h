/* um_signal.h
   Purpose: */
#pragma once

#include <power/signal.h>
#include <power/um.h>

#include <power/scheduler.h>
#include <power/interrupt.h>

typedef __sig_handler_pfn_t sig_handler_pfn_t;

#define SIG_PROC_PENDING_BIT (1 << 0) /*< Process has pending signals. */

struct proc_signal {
    uint64_t Mask;
    int Flags;

    struct thread_registers* RestoreRegisters;

    int WaitSet;
    struct s_event* WaitSetEvent;
  
    struct vector Requests;
    struct vector Handlers;
};

struct signal_handler {
    int Signal;
    int Action;

    sig_handler_pfn_t Routine;
};

struct signal_request {
    int Signal;
    int Flags;

    struct __sig_info* Info;
};

struct signal_frame {
    struct __sig_info Info;
    struct __sig_context Context;
};

struct proc_signal* sigCreateSignalReg();
void sigSetHandler(struct process* proc, int signal, sig_handler_pfn_t);
int sigDispatchSignals(struct thread* proc);

/* Also allocate the ctx struct */
#define SIG_CREATE_INFO_BIT (1 << 0)

/* SubmitSignal will block the proc until the signal occurs */
#define SIG_WAIT_BIT (1 << 1)

struct signal_request* sigCreateRequest(int flags);

void sigSubmitSignal(struct process* proc, struct signal_request* req,
                     struct irq_regs* regs);

uint64_t syscSigHandler(union sysc_regs* regs);
uint64_t syscReturnFromSignal(union sysc_regs* regs);
uint64_t syscRaiseSignal(union sysc_regs* regs);
uint64_t syscWaitForSignal(union sysc_regs* regs);
uint64_t syscSetSignalMask(union sysc_regs* regs);
uint64_t syscSendProcessSignal(union sysc_regs* regs);
