/* interrupt_ctl_interface.h
   Purpose: Generic interface for interrupt controllers */
#pragma once

#include <power/config.h>
#include <power/interrupt.h>

struct int_ctl
{
    int CurrentInterrupt; /*< interrupt being currently served.
                              only valid inside handlers. */

    int (*getControllerType)(struct int_ctl*);
    void (*ackInterrupt)(struct int_ctl*);
    int (*handleInterrupt)(struct int_ctl*, int vector, 
                            interrupt_noreg_pfn_t ih, void* data);
    void (*executeInterrupt)(struct int_ctl*, int vector);

    /* Only really used when in PIC. */
    int (*isVectorFree)(struct int_ctl*, int vector);
    
    uint64_t (*getMsiAddress)(struct int_ctl*, uint64_t* data, int vector);
};

#define INT_CTL_TYPE_PIC 0x1
#define INT_CTL_TYPE_APIC 0x2

struct int_ctl;

struct int_ctl* intCtlCreateDefault();
struct int_ctl* intCtlGetDefault();

int intCtlGetControllerType();
int intCtlGetCurrentInterrupt();
void intCtlAckInterrupt();

int intCtlHandleInterrupt(int vector, interrupt_noreg_pfn_t ih, void* data);
void intCtlExecuteInterrupt(int vector);
uint64_t intCtlGetMsiAddress(uint64_t* data, int vector);

