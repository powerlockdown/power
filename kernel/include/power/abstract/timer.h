/* timer.h
   Purpose: timer generic interface */
#pragma once

#include <power/config.h>

#define ARRAY_LENGTH(A) sizeof(*A) / sizeof(A)

#define TIMER_TYPE_HPET 0x1
#define TIMER_TYPE_PIT  0x2

typedef void* (*timer_create_pfn_t)();

struct timer { /*< PRIVATE STRUCT */
    void (*setReloadTime)(struct timer*, size_t units);
    int (*getType)(struct timer*);
    uint64_t (*getMillisecondUnit)(struct timer*);
    void* Reserved[1];

    int EnabledFlag;
    uint64_t MillisecondScale;
};

void tmCreateTimer();
struct timer* tmGetDefaultTimer();

/* The secondary timer inside the kernel
   is used for preempting to the scheduler.

   Unfortunately, the implementation of this
   routine is left as a job to the
   architecture-specific port. */
void tmCreateSecondaryTimer();
struct timer* tmGetSecondaryTimer();

int tmGetType(struct timer*);

/* Returns a number in such that, when passed
   to SetReloadValue, makes the CPU stall for
   1ms. */
int tmGetMillisecondUnit(struct timer*);

void tmSetReloadValue(struct timer*, size_t units);
void tmDisable(struct timer*);
void tmEnable(struct timer*);
