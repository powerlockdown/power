/* bit.h
   Purpose: bit manipulation */
#pragma once

static inline
unsigned long __ffset(unsigned long word)
{
  asm volatile ("bsf %1,%0" : "=r"(word) : "r"(word));
  return word;
}

/* Find First Set bit inside word */
#define __ffs(X) \
  (__builtin_constant_p(X) \
   ? __builtin_ctzl(X) : __ffset(X))

/* Find First Zero bit inside word */
#define __ffz(X) __ffs(~(X))

/* Check if X is aligned to a.
   Given a is a power of two. */
#define __isalgnp2(X, a) (((X) & (a - 1)) == 0)

/* Find First Zero bit inside a Memory buffer. */ 
static inline
unsigned long ffzm(const char* buffer, unsigned long length)
{
  unsigned long i;
  unsigned long rem = 0;

  const unsigned long* reintr;
  reintr = (typeof(reintr))buffer;
  
  if (!__isalgnp2(length, sizeof(long))) {
    rem = length % sizeof(long);
    length = length - rem;
  }

  int bo = (sizeof(long) * 8) + 1;
  
  unsigned long ofs = 0;
  length /= sizeof(long);
  
  for (i = 0; i < length; i++) {
    /* The behaviour of passing
       zero to ffs is undefined. */
    if (reintr[i] == -1ULL) {
      ofs += sizeof(long) * 8;
      continue;
    }
    
    bo = __ffz(reintr[i]);
  }
  
  return bo + ofs;
}

/* pow, when the base is a power of two. */
static inline
long long p2pow(long long base, int exp)
{
  if (!base)
    return 0;
  
  int fs = __ffs(base);
  return fs << (fs * exp);
}
