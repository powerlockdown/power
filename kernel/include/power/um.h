/* um.h
   Purpose: stuff for usermode interaction */
#pragma once

#include <power/config.h>
#include <power/input.h>

#include <power/vector.h>
#include <power/elf.h>
#include <power/error.h>

#include <power/handle.h>
#include <power/system.h>
#include <stdint.h>

#define HANDLE_TYPE_TTY 1
#define HANDLE_TYPE_FILE 2
#define HANDLE_TYPE_DEVICE 3
#define HANDLE_TYPE_PIPE   4

union sysc_regs
{
    struct {
        uint64_t Rax, Rbx, Rdx, R8, R9, R10, R13, Rdi;
    };

    struct {
        uint64_t Slot, Arg1, Arg2, Arg3, 
                 Arg4, Arg5, Arg6, Arg7;
    };
};

struct sysc_pres_regs /*< sysc_regs with other preserved registers. */
{
    uintptr_t Rbp;
    uintptr_t R15;
    uintptr_t R14;
    uintptr_t R12;
    uintptr_t R11;
    uintptr_t Rsi;
    uintptr_t Rcx;
    
    union sysc_regs* Params;
};

typedef uint64_t(*syscall_handler_pfn_t)(union sysc_regs*);

struct handle;

#define MMAP_UNMAP 1
#define MMAP_MAP   2

struct handle_ops {
    int (*read)(struct handle*, void* buffer, int length);
    int (*write)(struct handle*, const void* buffer, int length);
    int (*seek)(struct handle*, int rel, size_t offset);
    unsigned long (*ioctl)(struct handle*, unsigned long command, void* buffer,
                           size_t bufferLength);
    void (*close)(struct handle*);

    int (*mmap)(struct handle*, int action, 
                uintptr_t* hint, unsigned long length,
                int flags);
    int (*open)(struct handle*);
};

/* Performing an operation into this handle that's not
   currently available won't make it be put to sleep. */
#define HANDLE_NONBLOCK_BIT (1 << 0)

struct handle {
    int Type;
    int Identifier;
    int Flags;

    void* Resource;

    struct handle_ops Ops;
};

/* Default ioctl handler */
unsigned long hndDefaultIoctl(struct handle* h, unsigned long command, void* buffer,
                              size_t bufferLength);

__used static int 
hndWriteNotSupported(struct handle* h, const void* buffer, int length) 
{
    __unused(h);
    __unused(buffer);
    __unused(length);

    return -ERROR_NOT_SUPPORTED; 
}

__used static int 
hndReadNotSupported(struct handle* h, void* buffer, int length) 
{
    __unused(h);
    __unused(buffer);
    __unused(length);
    
    return -ERROR_NOT_SUPPORTED; 
}

__used static int 
hndSeekNotSupported(struct handle* h, int rel, size_t offset)
{
    __unused(h);
    __unused(rel);
    __unused(offset);

    return -ERROR_NOT_SUPPORTED;
} 

__used static void 
hndCloseNotSupported(struct handle* h)
{
    __unused(h);
} 

struct process;

struct handle* hndCreateHandle(struct process* proc, int type);
void hndDestroyHandle(struct handle* handle);

uint64_t syscCreatePipe(union sysc_regs* regs);
uint64_t syscIoControl(union sysc_regs* regs);
uint64_t syscGetHandleType(union sysc_regs* regs);
uint64_t syscDuplicateHandle(union sysc_regs* regs);

/* INPUT RING */
void inpCreateInputRing();
bool inpAddReader();
void inpRemoveReader();

/* pop event from ring buffer */
int inpReadEvent(int block, struct input_event* event);

/* subFlags - flags to be sent to user. */
void inpSendInputEvent(int type, int value, int subFlags);

#define PROCESS_STATE_RUNNING 1
#define PROCESS_STATE_DEAD    2

#define PIPE_FLAGS_READ_TO_TTY (1 << 0)
#define PIPE_FLAGS_INPUT_BUFFER (1 << 1)

struct pipe_data {
    struct ring* RingBuffer;
    struct s_event* Event;
    int ReferenceCount;  
    int Flags;
    bool Broken;
};

struct pipe_packet {
    size_t Length;
    const char* Data;
    int Broken;
};

struct process {
  struct process* Parent;
  struct vm* VirtualMemory;

  unsigned long Identifier;
  
  struct vector Children;
  struct vector Threads;
  struct vector Handles;
  
  address_space_t* AddressSpace;
  struct elf_executable Executable;
  const char* CommandLine;
  const char* WorkingDirectory;
  const char* Environment;
  
  int State;
  int ExitCode;

  struct s_event* TerminationEvent;
  struct proc_signal* Signals;

  struct s_semaphore* HzLock;
  struct zoned* HandleZone;
};

void pcProcessManagerInit();
struct process* pcCreateProcess(struct process* parent, const char* name,
                                const char* args, struct _create_process_ex* cpex);
struct handle* pcCreateHandle(struct process*, int type, void* res);
struct handle* pcGetHandleById(struct process*, int handle);
void pcCloseHandle(struct process*, struct handle* handle);

struct process* pcGetCurrentProcess();
struct process* pcLookupProcess(unsigned int pid);

void pcSetExitCode();
void pcTerminateProcess(struct process*);

uint64_t syscCreateProcess(union sysc_regs* reg);
uint64_t syscGetWorkingDirectory(union sysc_regs* regs);
uint64_t syscSetWorkingDirectory(union sysc_regs* regs);
uint64_t syscWaitForProcess(union sysc_regs* regs);
uint64_t syscGetCurrentProcessId(union sysc_regs* regs);
uint64_t syscSetEnvironmentString(union sysc_regs* regs);
uint64_t syscGetEnvironmentString(union sysc_regs* regs);
