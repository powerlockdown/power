/* time.h
   Purpose: Timekeeping */
#pragma once

#include "power/types.h"
#include <stdint.h>

typedef __uint64 wclocktm_t;

struct wdate {
    int Year;
    int Month; /*< 0-11 (= 1-12). */
    int Day; /*< 0-30 (= 1-31). */

    int Hour; /*< 0-23. */
    int Minute; /*< 0-59. */
    int Second; /*< 0-59. */
};

void clkSetTime(const struct wdate* time);
int clkGetTime(struct wdate* time);
void clkUpdate(int delta);

union sysc_regs;
uint64_t syscGetTimeOfDay(union sysc_regs* regs);
