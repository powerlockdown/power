/* scheduler.h
   Purpose: task scheduler */
#pragma once

#include "power/list.h"
#include "power/signal.h"
#include "power/um.h"
#include <power/interrupt.h>
#include <power/paging.h>
#include <power/config.h>
#include <power/i386/idt.h>

#define THREAD_CREATE_USER_MODE 0x00000000001
#define THREAD_NOT_SLEEPING 0xFFFFFFFF /*< Thread suspended but not by sleeping. */

typedef void (*worker_routine_pfn_t)(void* ctx);
typedef void (*thread_execution_pfn_t)(void*);

struct scheduler;

/* Has to be packed so we can easily access
   its fields from ASM. */
struct __packed thread_registers {
    struct idt_interrupt_frame Pointers;
    struct idt_gp_register_state GeneralRegisters;

    uint16_t CodeSegment;
    uint16_t DataSegment;
    uint64_t Cr3;

    void* FloatingPointState;
    uintptr_t FsSegment;
    uintptr_t GsSegment;
};

struct thread_timing {
    uint64_t LastCount;
    uint64_t ElapsedCount;
    unsigned int ResetQuantum;
    unsigned int CurrentQuantum;
};

struct __packed thread {
    struct list_item List;

    void* StackTop;
    void* KernelStack;
    void* LastUserStack; /*< Last SP before the privilege switched */

    uintptr_t LastNsRsp; /*< Last Non-Signal RSP. */
    uintptr_t LastNsRip; /*< Last Non-Signal RIP. */
    /* PLEASE double check the assembly code
       if you modify the layout of any of
       these above fields!! */
   
    struct thread_registers Registers;
    struct thread_timing Timing;
    struct process* Process;
    struct s_event* TerminationEvent;
    
    int Identifier;

    bool Suspended;
    unsigned int SuspendedTicks;

/* Thread was executing a system call when a signal was handled. */
#define THREAD_SIGNAL_CLOBBERED_BIT (1 << 0)
    int Flags;
    int CurrentSyscall;

    struct sysc_pres_regs* SyscallRegs;
    struct signal_frame* SignalFrame;
};

struct thread_args {
    address_space_t* AddressSpace;
    uintptr_t Restorer; /*< Something to put onto the stack
			  to prevent the thread from running
			  onto NULL. */
    struct process* Process;
};

struct thread* schedCreateThread(thread_execution_pfn_t ep, void* data,
                                 int flags);
struct thread* schedCreateThreadEx(thread_execution_pfn_t ep, void* data,
                                   struct thread_args*, int flags);
void schedThreadYield();
void schedFreeThread(struct thread* restrict);
void* threadAllocateStack(struct process*, size_t size);

/* === */

struct scheduler* schedCreate(void);
void schedThink(const struct irq_regs* state);

struct thread* schedGetCurrentThread(void);
struct thread* schedGetThreadById(int id);
void schedSleep(int ticks);

void schedAddThread(struct thread*);
void schedRemoveThread(struct thread*);

void schedEnable(bool flag);
bool schedIsEnabled(void);
void schedSkipRegisterSave(void);

/* === */

struct s_event;
struct s_event* schedCreateEvent();
void schedDestroyEvent(struct s_event*);

void schedEventResetFlag(struct s_event*);
bool schedEventFlag(struct s_event*);

void schedEventRaise(struct s_event*);
void schedEventPause(struct s_event*);


/* NOTE: This is INTERNALLY IMPLEMENTED AS A SPINLOCK. 
    If you want to use mutual exclusion through the
    scheduler, create a semaphore with one slot.*/
struct s_mutex;
struct s_mutex* schedCreateMutex();
void schedDestroyMutex(struct s_mutex*);

void schedMutexAcquire(struct s_mutex*);
void schedMutexRelease(struct s_mutex*);

struct s_semaphore;
struct s_semaphore* schedCreateSemaphore(int slot);
void schedDestroySemaphore(struct s_semaphore* sem);

void schedSemaphoreAcquire(struct s_semaphore*);
void schedSemaphoreRelease(struct s_semaphore*);

/* === */

void workerInit();
void workerSubmitTask(worker_routine_pfn_t rout, void* ctx);

/* === */

uint64_t syscCreateThread(union sysc_regs* regs);
uint64_t syscKillThread(union sysc_regs* regs);
uint64_t syscGetCurrentThreadId(union sysc_regs* regs);
uint64_t syscSetThreadSuspension(union sysc_regs* regs);
uint64_t syscSleep(union sysc_regs*);
uint64_t syscWaitForThread(union sysc_regs*);
