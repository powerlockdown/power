/* endian.h 
   Purpose: endianness byteswapping */
#pragma once

#define END_LITTLE 1
#define END_BIG    2

/* THe kernel currently only target x86 (an LE architecture) */
#define ENDIANNESS END_LITTLE

#if ENDIANNESS == END_LITTLE
#define swapbe16cpu(Be) __builtin_bswap16(Be)
#define swapbe32cpu(Be) __builtin_bswap32(Be)
#define swapbe64cpu(Be) __builtin_bswap64(Be)

#define swaple16cpu(Le) (Le)
#define swaple32cpu(Le) (Le)
#define swaple64cpu(Le) (Le)
#else
#define swapbe16cpu(Be) (Be)
#define swapbe32cpu(Be) (Be)
#define swapbe64cpu(Be) (Be)

#define swaple16cpu(Le) __builtin_bswap16(Le)
#define swaple32cpu(Le) __builtin_bswap32(Le)
#define swaple64cpu(Le) __builtin_bswap64(Le)
#endif
