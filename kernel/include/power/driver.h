/* driver.h
   Purpose: Definitions used by drivers */
#pragma once

#include <power/device.h>
#include <power/config.h>

#include <power/filestats.h>
#include <power/paging.h>
#include <power/vector.h>

#define DRIVER_CLD_RELATIONSHIP_NONE 0
#define DRIVER_CLD_RELATIONSHIP_AND  1
#define DRIVER_CLD_RELATIONSHIP_OR   2

#define DRIVER_CLD_TYPE_CUSTOM_FUNC 1
#define DRIVER_CLD_TYPE_PCI         2

#define DRIVER_DONT_CARE -1

struct kdriver_manager;

typedef struct driver_info* (*driver_query_pfn_t)();
typedef void (*driver_main_pfn_t)(struct kdriver_manager*);
typedef bool (*driver_should_load_pfn_t)();

struct driver_info {
    const char* Name;
    void* Interface;

    driver_main_pfn_t EntryPoint;
    struct driver_conditional_loading* ConditionalLoading;

    int Role;

    /* Imagine the following situation:
     A driver that has to manage 
        USB mass storage devices.

     It has to present itself as a
        DRIVER_ROLE_DISK to the kernel
        and as a USB_DEVICE to the USB driver.
    
     How do we do that, given we only have one Role field? 
        Well, this "Supplementary" thing exists just 
        for that, as a temporary solution, 
        while I think of something better. */
    int SupplementaryRole;
    void* SupplementaryInterface;
};

struct driver_conditional_loading {
    int RelationshipWithPrevious;
    int ConditionalType;
    int HasNext;
    union {
        struct driver_cld_pci {
            uint16_t VendorId;
            uint16_t DeviceId;
            uint8_t Class;
            uint8_t Subclass;
            uint8_t ProgIf;
        } Pci;

        driver_should_load_pfn_t shouldLoad;
    };
};

struct kdriver_manager {
    struct pci_device* LoadReason;
    struct driver_info* Info;
};

struct kdriver_manager* modCreateDriverManager(struct driver_info*,
                                               struct pci_device*);
void modAutomaticLoad();

struct driver_internal_data {
    struct kdriver_manager* Driver;
    struct vector Pages; /*< For mapping onto user address space
                             of created processes*/
};

struct vm;

struct kdriver_manager* modGetDriver(const char* modName);
struct kdriver_manager* modGetFilesystemName(const char* fsName);

struct kdriver_manager* modLoadDriver(const char* modName);
struct kdriver_manager* modLoadDriverEx(const char* modName, bool insideRamfs);

void modMapDrivers(struct vm* address);
struct vector* modGetDrivers();

/* driver disk interface */
#define DRIVER_ROLE_DISK 0x1

struct driver_disk_interface {
    major_t* Major;

    struct driver_disk_device_interface* (*getDevice)(
        struct driver_disk_interface*, int device);
    int (*getDeviceCount)(struct driver_disk_interface*);

    /* This is the maximum number of disks the controller
       can currently accept, NOT the architectural limit
       imposed by the controller spec! */
    int (*getMaxDeviceCount)(struct driver_disk_interface*);
};

struct driver_disk_device_interface {
    int SectorSize;
    int SectorCount;

    int (*readSector)(struct driver_disk_device_interface*, uint64_t lba,
                      size_t length, uint8_t* restrict buffer);
    int (*writeSector)(struct driver_disk_device_interface*, uint64_t lba,
                       unsigned long length, const uint8_t* buffer);
};

/* Filesystem driver */
#define DRIVER_ROLE_FILESYSTEM 0x2

struct fs_file;
struct fs_mountpoint;

typedef uint64_t lba_t;
struct driver_fs_interface {
    const char* Name; /*< File system name (used in mount syscalls) */

    struct driver_mounted_fs_interface* (*mount)(
        struct driver_fs_interface*, struct driver_disk_device_interface* dev,
        lba_t beginLba, struct fs_mountpoint* mp);
};

struct driver_mounted_fs_interface {
    /* Construct a file handle. */
    void (*createFile)(struct driver_mounted_fs_interface*, const char* name,
                       struct fs_file* outFile);
    int (*read)(struct driver_mounted_fs_interface*, uint8_t* buffer,
                size_t size, struct fs_file* file);
    void (*closeFile)(struct driver_mounted_fs_interface*,
                      struct fs_file* file);
    int (*write)(struct driver_mounted_fs_interface*, const uint8_t* buffer,
		 size_t size, struct fs_file* file);
    int (*stat)(struct driver_mounted_fs_interface*,
		struct fs_file*, struct __filestats*);
};

/* Input driver */
#define DRIVER_ROLE_INPUT 0x3

/* USB controller driver */
#define DRIVER_ROLE_USB_CONTROLLER 0x4

struct usb_setup;
struct usb_transaction;
struct driver_usb_port_interface;

typedef void (*driver_usb_connect_change_pfn_t)(
    struct driver_usb_port_interface*, void* data);

struct driver_usb_port_interface {
    bool HasDevice;
    void (*reset)(struct driver_usb_port_interface*);

    void* Reserved[2];
};

struct driver_usb_ctl_interface {
    int PortCount;

    void* HandlerPrivData;
    driver_usb_connect_change_pfn_t ConnectHandler;
    driver_usb_connect_change_pfn_t DisconnectHandler;

    struct driver_usb_port_interface* (*getPort)(
        struct driver_usb_ctl_interface*, int port);

    void (*scheduleTransaction)(struct driver_usb_ctl_interface*,
                                struct usb_transaction* tract);;
};

/* Bus driver */
#define DRIVER_ROLE_BUS 0x5

#define DRIVER_BUS_TYPE_ACPI 1
#define DRIVER_BUS_TYPE_PCI  2
#define DRIVER_BUS_TYPE_USB  3

struct driver_bus_interface {
    int BusType;

    /* The return type and `deviceQuery` arg are
       a struct defined somewhere inside bus drivers. */
    void* (*searchDevice)(struct driver_bus_interface*, void* deviceQuery);

    uint64_t Padding[4];
};

struct driver_acpi_bus_interface { /*< Interface for functions specific to ACPI.
                                    */
    struct driver_bus_interface Base;

    int (*pciEnableInterrupt)(struct pci_device* device);
};

struct usb;
struct usb_device;

struct driver_usb_bus_interface {
    struct driver_bus_interface Base;

    int (*sendSetup)(struct usb*, struct usb_device*, 
                     struct usb_setup* setup,
                     void* buffer);
};

/* USB device driver */
#define DRIVER_ROLE_USB_DEVICE 0x6

struct usb_device;
struct usb_interface;

struct driver_usb_device_interface {
    int Class;
    int Subclass;
    int Protocol;

    int (*deviceLoad)(struct driver_usb_bus_interface* usb,
                      struct usb_interface* dev);
    void (*deviceUnload)(struct driver_usb_bus_interface* usb,
                         struct usb_interface* dev);
};
