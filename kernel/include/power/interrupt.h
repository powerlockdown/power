/* interrupt.h
   Purpose: generic interrupt */
#pragma once

#include <power/i386/asm.h>
#include <power/config.h>

extern int __inh_count;

static __always_inline
void inhDisableInterrupts()
{
    if (!__inh_count) {
        __irqDisable();
    }

    __inh_count++;
}

static __always_inline
void inhEnableInterrupts()
{
    __inh_count--;
    if (!__inh_count) {
        __irqEnable();
    }
}

struct irq_regs;

typedef void (*interrupt_handler_pfn_t)(struct irq_regs*, void* data);
typedef void (*interrupt_noreg_pfn_t)(void* data);
