/* device.h
   Purpose: device operations */
#pragma once

#include <power/um.h>
#include <power/list.h>

#define DEVN_PATH_PREFIX "\\??:/"
#define DEVN_PATH_PREFIX_SIZE 5

#define DEVN_MANYDEV_BIT (1 << 0)

/* A little design explanation:
    * This is a pseudo-filesystem. That is, VFS will simply
      call us whenever the file path begins with DEV_PATH_PREFIX.
    
    * Each device has is composed of two parts: the major, composed
      of only characters and number, and the minor, another alphanumeric
      part after the major.
        Suppose the following device: "scsi0". "scsi" is its
      major and '0' is its minor. A more complicated example now:
      "ahci0p1". "ahci0p" is its major and "1" is its minor.
      
    * Internally, each major is represented as a tree, which is
      severely limited in height: the major cannot have nephews.
        This may introduce some performance problems during lookup 
      in the future but I really don't care now.

    * Let me show you an example of how a driver would call this API.
      Suppose the following function call:

            major_t* maj = devnRegisterNode("ahci", ops, DEVN_MANYDEV_BIT);

        This indicates to us that the driver wants a major named "ahci", that
      allows many devAllocateNode calls to succeed (as indicated 
      by the MANYDEV bit). 
        Assuming favorable conditions (no other major has the same name; there is
      sufficient memory), the call will succeed. You can now create as many
      minors as you want.
        The `ops` symbol refers to a pointer to a struct handle_ops. You need that
      so UMEs can actually use your node for something useful. Unless you consider
      failing all I/O functions with ERROR_NOT_SUPPORTED something to be extracted
      fruitful value of.

       Now, imagine the following call:

            minor_t mi = devAllocateNode(maj, dev);

        Presuming the favourable conditions (enough memory; major is valid etc), and
      that this function returned 0, `??:/ahci0` has been created for UME's use. They
      can now read, write, execute diagnostics and download microcode as much as
      they want.
        The `dev` symbol is a minor-specific data. You should place anything that
      will be needed when e.g. IoControl is called for your node.

    * Be sure to not mix up the functions: RegisterNode creates a major, and
        AllocateNode creates a minor.

    Happy majoring!
 */
 
struct device_ntree;

struct device_ntree {
    struct list_item List;
    int Flags;

    struct list_item ChildrenList;

    const char* Name;
    struct zoned* MinorPool; /*< Pool for minor_t allocation */
    const struct handle_ops* Ops;
};

typedef struct device_ntree major_t; /*< This is meant to be used
                                         as a pointer (i.e. major_t*)*/
typedef unsigned long minor_t;

const struct handle_ops* devnFindNode(const char* dev, void** outreserved);
struct handle* devnCreateDeviceHandle(struct process* proc, const char* devName);

major_t* devnRegisterNode(const char* name, const struct handle_ops* handle, int flags);
void devnUnregisterNode(major_t*);

minor_t devAllocateNode(major_t* major, void* data);
void devFreeNode(major_t* maj, minor_t device);

major_t* devnFindMajor(const char* name);
