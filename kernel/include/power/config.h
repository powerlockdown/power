/* config.h
   Purpose: macros used throught the kernel */
#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#define __barrier __sync_synchronize
#define __write_until_here() asm volatile ("" ::: "memory")

#define __used __attribute__((used))
#define __unused(X) ((void)X)
#define __packed __attribute__((packed))

#define __always_inline inline __attribute__((always_inline))

#define __min(X, y) ((X) > (y)) ? (y) : (X)
#define __max(X, y) ((X) > (y)) ? (X) : (y)

#define likely(E) __builtin_expect(!!(E), 1)
#define unlikely(E) __builtin_expect(!!(E), 0)

#define KB 1024
#define MB KB * 1024
#define GB MB * 1024

static inline long max(long x, long y)
{
  return (x > y) ? x : y;
}

static inline long min(long x, long y)
{
  return (x > y) ? y : x;
}

