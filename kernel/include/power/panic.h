/* panic.h
   Purpose: Crash the kernel */
#pragma once

#include "power/i386/idt.h"

__attribute__((noreturn))
void __panic(const char* message, const char* function,
	     const struct irq_regs* regs);

#define panic(M, r) __panic(M, __func__, r)

