/* args.h
   Purpose: Command-line arguments */
#pragma once

#include <power/config.h>

size_t cmdGetCommandArgument(const char* name, const char** output);
bool cmdHasArgument(const char* name);

