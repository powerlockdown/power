/* usb.h
   Purpose: The definition of stuff used for USB between
            the core and the HCD. */
#pragma once

#include "power/scheduler.h"
#ifndef _POWER_USB_H
#define _POWER_USB_H

#include <power/list.h>
#include <power/driver.h>

struct usb; /*< See the core. */

struct usb_address {
    int Address; /*< Device address */
    int Endpoint;
};

#define USB_TRANSFER_DIR_IN_BIT (1 << 4)
#define USB_TRANSFER_DIR_OUT_BIT (0 << 4)

#define USB_TRANSACTION_BITS 7 /*< Up to 3 bits */
#define USB_TRANSACTION_INTERRUPT   1
#define USB_TRANSACTION_ISOCHRONOUS 2
#define USB_TRANSACTION_CONTROL     3
#define USB_TRANSACTION_BULK        4

#define USB_PID_OUT   0
#define USB_PID_IN    1
#define USB_PID_SETUP 2

/* Use SETUP pid instead of what the bit 4 of
   TransferType reads. */
#define USB_TRANSACT_SETUP_PID_BIT (1 << 1)

struct usb_transaction;
typedef void (*usb_complete_handler_pfn_t)(struct usb_transaction*, void*);

struct usb_transaction {
    int TransferType; /*< Bit 4 denotes the transfer direction. */
    struct usb_address Address;
    
    int Speed; /*< One of USB_SPEED_* */
    int BufferLength;
    void* Buffer;

    int PidCode;
    int Status;
    int Interval;
    int Flags;

    void* HcdReserved;
    void* HandlerPrivData;
    usb_complete_handler_pfn_t CompleteHandler;
};

#define USB_SPEED_LOW   1
#define USB_SPEED_FULL  2
#define USB_SPEED_HIGH  3
#define USB_SPEED_SUPER 4

struct usb_device {
    int Speed;

    uint16_t Vendor;
    uint16_t Device;

    uint16_t Class;
    uint16_t Subclass;
    uint8_t Protocol;

    struct usb_interface* Interfaces;
    struct usb_address Address;
    void* DriverPriv;
};

struct usb_endpoint {
    struct list_item List;

    int Address;
    int ExtraTransactions;
    
#define USB_ENDPOINT_USAGE_BITS (3 << 4)
#define USB_ENDPOINT_SYNC_BITS (3 << 2)
#define USB_ENDPOINT_TRANSFER_BITS (3 << 0)
    int Attributes; /*< Copy of bmAttributes except bit 7
                        reflects the EP direction.*/

    uint16_t MaximumPacketSize;
    uint16_t PollingInterval;
};

struct usb_interface {
    struct list_item List;

    int Class;
    int Subclass;
    int Protocol;
    
    int ExtraLength;
    char* Extra;    /*< Class-specific descriptors */

    struct usb_device* Device;
    struct usb_endpoint* Endpoints;

    void* DevPriv; /*< Reserved for drivers. */
};

static inline void usbCreateAddress(struct usb_address* restrict addr,
                                    const struct usb_device* dev,
                                    const struct usb_endpoint* end)
{
    addr->Address = dev->Address.Address;
    addr->Endpoint = end->Address;
}

static inline void usbCreateAddressInterface(struct usb_address* restrict addr,
                                             const struct usb_interface* ifn,
                                             const struct usb_endpoint* end)
{
    usbCreateAddress(addr, ifn->Device, end);
}

struct usb {
    struct driver_usb_bus_interface Base;
    struct driver_usb_ctl_interface* Controller;

    struct s_semaphore* ResetLock;

    /* THESE ARE PRIVATE! */
    struct s_event* ResetEvent;
    struct s_event* TransactionEvent;
    struct zoned* AddressPool;

    bool SubmitTask;
    struct vector Devices;
};

struct __packed usb_setup {
    uint8_t bmRequestType;
    uint8_t bRequest;
    uint16_t wValue;
    uint16_t wIndex;
    uint16_t wLength;
};

/* Class-specific definitions */

/* Mass storage */
#define USB_MASS_CBI 0x01 /*< Control/Bulk/Interrupt (no interrupts) */
#define USB_MASS_CBI_INTR 0x02 /*< Control/Bulk/Interrupt */
#define USB_MASS_BBB 0x50 /*< Bulk-only MSD */
#define USB_MASS_UAS 0x62 /*< USB-attached SCSI */
#define USB_MASS_VENDOR 0xFF

#endif
