/* string.h
   Purpose: string utilities */
#pragma once

#include <power/config.h>
#include <power/vector.h>

__attribute__((pure)) size_t strlen(const char* s);

static inline int isdigit(int c)
{
    return c >= '0' && c <= '9';
}

char* ulltostr(uint64_t number, char* str, int base);
char* lltostr(int64_t number, char* str, int base);

char* strunite(const char* p1, size_t p1l, const char* p2, size_t p2l);

unsigned long int 
strtoul(const char* _string, char** _endptr, int _bs);

int strcmp(const char* s1, const char* s2);
int strncmp(const char* s1, const char* s2, size_t length);

char* strsep(char** string, char delim);

int strncat(char* restrict dest, const char* src, size_t length);

/* For VFS relative path computation */
char* strvec(const char* string, char delim, struct vector* vector);
void strvecfree(char* ctx, const char* string, struct vector*);
char* strclone(const char* string);

char* pathjn(struct vector* wkd, struct vector* rel, char delim);
