/* memset.h
   Purpose: functions for quickly filling a buffer
            with a certain value. */
#pragma once

#include <power/config.h>

extern void* memset(void* ptr, int value, size_t size);
extern void* _64memset(void* ptr, uint64_t value, size_t qsize);
extern void* memcpy(void* restrict dst, const void* restrict src,
                    size_t length);

/* PLEASE ADD GENERIC VERSIONS OF THESE FUNCTIONS. */

