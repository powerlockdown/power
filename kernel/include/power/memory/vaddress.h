/* vaddress.h 
   Purpose: virtual address allocation*/
#pragma once

#include <power/i386/pgtables.h>
#include <power/config.h>

struct address_space_mgr* addrGetManagerForCr3(address_space_t* addr);
bool asmgrClaimPage(struct address_space_mgr*, uint64_t offset, size_t length);
void asmgrReturnPage(struct address_space_mgr*, uint64_t offset);

uint64_t asmgrCorrectPage(struct address_space_mgr*, uint64_t offset);
uint64_t asmgrGetDataPage(struct address_space_mgr*, size_t length);
