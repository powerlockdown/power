/* pool.h
   Purpose: memory pool */
#pragma once

#include <power/config.h>

struct mm_pool;
struct mm_pool* poolInit(size_t objectSize, int align);

void* poolAllocObject(struct mm_pool*);
void poolFreeObject(struct mm_pool*, const void* ptr);

