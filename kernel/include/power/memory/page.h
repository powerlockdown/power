/* page.h
   Purpose: structure describing a physical memory page. */
#pragma once

#include <power/list.h>

struct page {
    struct list_item List;

    int RefCount;
    void* Address;
    uintptr_t VirtualAddress;

    uint64_t Reserved;
};
