/* zoned.h
   Purpose: zoned allocator */
#pragma once

#include <power/config.h>

#define ZN_USE_POOL_BIT (1 << 1)

struct zoned {
    int Stride;
    int Flags;
    uint64_t Begin;
    size_t Length;

    struct zoned_region* RegionList;
};

struct zoned_region {
    struct zoned_region* Next;

    uint64_t Begin;
    int Length; /*< aligned to the Stride's alignment */
};

struct zoned* zpgInit(uint64_t begin, size_t length, int stride, int flags,
                      void* location);

int zpgRequestRegion(struct zoned*, int length, uint64_t* out);
void zpgReturnRegion(struct zoned*, uint64_t begin, int length);
void zpgDestroy(struct zoned*);
void zpgClearRegions(struct zoned*);
