/* vm.h
   Purpose: virtual memory manager */
#pragma once

#include <power/paging.h>
#include <power/config.h>
#include <power/system.h>

#define VMAP_USER      (1 << 0)
#define VPROT_CACHE    (1 << 15)

#define VFAULT_GUARDED (1 << 0)
#define VFAULT_WRITE   (1 << 1)

struct handle;
struct fs_file;
union sysc_regs;

#define __VFM_BEGIN 0xA1111000

struct vm {
  int AllocatedRegions;
  int AllocatedBytes;
  
  struct avl_tree* Tree;
  struct s_semaphore* Lock; /*< Tree lock. */
  address_space_t* AddressSpace;
};

struct vm_region {
  struct page* Page;

  int Protection;
  int Advice;
  int Length;
  int NominalLength; /*< Reserved length.  */

/* Physical memory is not managed by us. */
#define VM_REG_ESTBLSH_BIT     (1 << 0)

/* The struct pages on this region are bogus. */
#define VM_REG_BOGUS_PAGES_BIT (1 << 1)

/* No backing file. */
#define VM_REG_ANON_BIT (1 << 2)
  int Flags;

  int CreationFlags; /*< Flags used to create the region. */
  
  uintptr_t Address;
  struct fs_file* BackingFile;
};

static inline
bool vmCompareVm(struct vm* v1, struct vm* v2)
{
  return v1->AddressSpace == v2->AddressSpace;    
}

uint64_t vmFlagsIntoPagingFlags(int flags);

struct vm* vmInit(address_space_t* address);
void vmDestroy(struct vm*);

void vmClearRegions(struct vm*);

void* vmVirtualHandleMap(struct vm*, uint64_t hint, size_t length, 
                         struct handle* handle, int flags);
int vmVirtualHandleUnmap(struct vm* vm, struct handle* handle, 
                         void* address, size_t length);

int vmEstablishMapping(struct vm*, uintptr_t* hint, void* phys,
                        size_t length, int flags);
int vmUnstablishMapping(struct vm* vm, uintptr_t addr);

void* vmVirtualFileMap(struct vm*, uint64_t hint, size_t length, 
                       struct fs_file* file, int flags);
void vmVirtualFileUnmap(struct vm*, void* buffer, int length);

struct vm_region* vmCreateRegion();
struct page* vmCreatePage(struct page* list, uintptr_t address);
void vmDestroyPage(struct page* page);
int vmHandleFault(struct vm* vm, uintptr_t address, int flags);

struct vm* vmGetTemporary();
void vmDestroyTemporary();

int vmPopulateFileMapping(struct vm* vm, struct vm_region* region);
uintptr_t vmMapShared(struct vm* src, struct vm* dest, uintptr_t address,
                      int flags);

int vmWrite(struct vm* vm, uintptr_t address, void* buffer,
	    unsigned long length);
int vmPopulateRegion(struct vm* vm, struct vm_region* region);

uint64_t syscVirtualMap(union sysc_regs* regs);
uint64_t syscVirtualUnmap(union sysc_regs* regs);
uint64_t syscVirtualFileMap(union sysc_regs* regs);
