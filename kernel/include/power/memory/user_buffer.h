/* user_buffer.h
   Purpose: checking for user-supplied memory buffers */
#pragma once

#include <power/config.h>
#include <power/memory/heap.h>
#include <power/memory/hhdm.h>

#include <power/paging.h>
#include <power/elf.h>
#include <power/error.h>

#include <power/paging.h>

/* UserBufferCheck should fail 
   if the buffer cannot be written to. */
#define UBUFFER_WRITE_BIT (1 << 0)

static inline int
mmUserBufferCheck(void* buffer, size_t length, int uflags)
{
    uintptr_t hhdm = mmGetHhdmOffset();
    uintptr_t ptr = (uintptr_t)buffer;

    /* 1. Verify if the address is not located inside
          kernel's reserved memory. */
    if ((ptr >= KERNEL_HEAP_BEGIN && ptr < KERNEL_HEAP_END) 
      || (ptr >= EXEC_BASE && ptr < EXEC_END)) {
        return -ERROR_USER_FAULT;
    }

    if (ptr >= hhdm) {
        return -ERROR_USER_FAULT;
    }

    /* 2. Verify if all pages inside the buffer
          and valid and optionally writable */
    uintptr_t pa = ptr & ~0xFFF;
    length += ptr & 0xFFF;

    size_t i;
    int psize;
    uintptr_t flags, total = pa + length;
    for (i = pa; i < total; i += 4096) {
        flags = pgGetRawPage(NULL, i, &psize);
        if (!flags || !(flags & PT_FLAG_USER)) {
            return -ERROR_USER_FAULT;
        }

        if (uflags & UBUFFER_WRITE_BIT && !(flags & PT_FLAG_WRITE)) {
            return -ERROR_USER_FAULT;
        }
        

        /* i will be incremented by 4096 
           every iteration. */
        i += psize - 4096;
    }

    return 0;
}

/* This function only checks whether the supplied
   pointer is valid. */
static inline int mmUserPointerCheck(uintptr_t ptr)
{
    return (ptr >= 0xffffffff8000000 || (ptr >= EXEC_BASE && ptr < EXEC_END));
}
