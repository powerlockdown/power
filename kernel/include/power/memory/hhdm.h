/* hhdm.h
   Purpose: higher half direct map */
#pragma once

#include <power/config.h>

#define PaAdd(B, s) (void*)(((uint64_t)B) + s)

/* Not defined here so drivers 
   won't be having to rely on limine.h
   to make their physical addresses writable */
uintptr_t mmGetHhdmOffset();

/* Makes physical address `unwritable` writable
   by adding the HHDM constant. */
__used __always_inline
static void* mmMakeWritable(void* unwritable)
{
   uintptr_t hhdm = mmGetHhdmOffset();
   if ((uintptr_t)unwritable > hhdm)
      return unwritable;

   return PaAdd(unwritable, hhdm);
}

/* Subtract HHDM from address `writable`. */
__used __always_inline
static uintptr_t mmMakeUnwritable(void* writable)
{
   uintptr_t cast = (uintptr_t)writable;
   uintptr_t hhdm = mmGetHhdmOffset();

   if (cast >= hhdm) {
      return cast - hhdm;
   }

   return cast;
}
