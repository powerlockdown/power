/* heap.h
   Purpose: kernel heap */
#pragma once

#include <power/config.h>
#include <power/i386/pgtables.h>

#define KERNEL_HEAP_BEGIN 0x7FFF0000000
#define KERNEL_HEAP_MAX_SIZE 0xFFFFFFFF
#define KERNEL_HEAP_END (KERNEL_HEAP_BEGIN + KERNEL_HEAP_MAX_SIZE)

struct mm_cache;

typedef void (*mm_ctor)(void*, size_t length);

struct mm_cache* khCreateCache(const char* name, size_t objSize);
struct mm_cache* khCreateCacheEx(const char* name, size_t objSize,
				 mm_ctor ctor, mm_ctor dtor);

void khFreeCache(struct mm_cache* cache);
void khGrowCache(struct mm_cache*);

void* khAlloc(struct mm_cache*);
void khFree(struct mm_cache*, const void* ptr);
void khShrinkToFit(struct mm_cache*);

void __khFree(struct mm_cache* cache, const void* ptr, void* callee);

void* khsAlloc(size_t size);
void* khsRealloc(const void* ptr, size_t nsize);
void khsFree(const void* ptr);

void khRemapHeap(address_space_t* addr);
