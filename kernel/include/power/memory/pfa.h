/* pfa.h
   Purpose: Page frame allocator */
#pragma once

#include <power/config.h>
#include <power/memory/hhdm.h>
#include <power/memory/page.h>

void mmInit(void* base, size_t length);
void mmAddUsablePart(void* begin, size_t length);

/* like AlignedAlloc but subtract from HHDM. */
void* mmAlignedPhysicalAlloc(size_t size, uint16_t alignment);
void* mmAlignedAlloc(size_t size, uint16_t alignment);
void mmAlignedFree(const void* ptr, size_t size);

void* mmGetGlobal();

/* Might remove soon. */
size_t mmGetLength();

bool mmIsPhysical(void* ptr);

struct page* mmAllocPage(int count);
void mmFreePage(void* address);
struct page* mmGetPage(void* address);

/* Might remove soon */
__attribute__((deprecated))
void* mmAllocKernel(size_t size);
