/* ring_buffer.h
   Purpose: ring buffer */
#pragma once

#include <power/config.h>

#include <power/memory/heap.h>
#include <power/memory/memset.h>

struct ring
{
    void* Data; /* Length = ObjectSize * ObjectCount */
    size_t ObjectSize;
    int ObjectCount;

    int WriteEnd;
    int ReadEnd;
};

static __always_inline 
struct ring* ringInit(size_t objSize, int objCount)
{
    struct ring* r = khsAlloc(sizeof(*r));
    memset(r, 0, sizeof(*r));

    r->ObjectSize = objSize;
    r->ObjectCount = objCount;
    r->Data = khsAlloc(objSize * objCount);
    return r;
}

static __always_inline
void ringDestroy(struct ring* ring)
{
    khsFree(ring->Data);
    khsFree(ring);
}

static __always_inline
size_t ringGetTotalCapacity(struct ring* ring)
{
    return ring->ObjectSize * ring->ObjectCount;
}

static inline
bool ringWrite(struct ring* ring, const void* object)
{
    if ((ring->WriteEnd + ring->ObjectSize) % ringGetTotalCapacity(ring) == 0
     && ring->ReadEnd < 0)
        return false;

    memcpy((ring->Data + ring->WriteEnd), object, ring->ObjectSize);
    ring->WriteEnd = (ring->WriteEnd + ring->ObjectSize) % ringGetTotalCapacity(ring);
    return true;
}

static inline
bool ringRead(struct ring* ring, void* outObj)
{
    if (ring->ReadEnd == ring->WriteEnd)
        return false;

    memcpy(outObj, (ring->Data + ring->ReadEnd), ring->ObjectSize);
    ring->ReadEnd = (ring->ReadEnd + ring->ObjectSize) % ringGetTotalCapacity(ring);
    return true;
}

static inline
bool ringWriteBuffer(struct ring* ring,
		     const void* array, unsigned count)
{
  if (count == ring->ObjectSize)
    return ringWrite(ring, array);
  
  if ((ring->WriteEnd + ring->ObjectSize) % ringGetTotalCapacity(ring) == 0
     && ring->ReadEnd < 0)
    return false;

  if (count % ring->ObjectSize != 0)
    count -= count % ring->ObjectSize;

  int idf;
  if (ring->WriteEnd > ring->ReadEnd) {
    idf = ringGetTotalCapacity(ring) - ring->WriteEnd;
    memcpy(ring->Data + ring->WriteEnd, array, min(count, idf));
    count -= idf;

    ring->WriteEnd = idf;
    if (!count)
      return true;

    memcpy(ring->Data, array + idf, min(count, ring->ReadEnd));
  } else {
    idf = min(count, ring->ReadEnd);
    memcpy(ring->Data + ring->WriteEnd, array, idf);
    ring->WriteEnd += idf;
  }
  
  return true;
}

static inline
size_t ringReadBuffer(struct ring* ring,
		      void* array, unsigned count)
{
  if (ring->ReadEnd == ring->WriteEnd)
    return 0;

  int idf = ringGetTotalCapacity(ring) - ring->ReadEnd;

  /* ======================
          W        R   */
  if (ring->WriteEnd < ring->ReadEnd) {
    memcpy(array, ring->Data + ring->ReadEnd, idf);
    count -= idf;

    if (!count)
      return idf;

    memcpy(array, ring->Data, min(count, ring->WriteEnd));
    ring->ReadEnd = count;
    
    count += idf;
  } else {
    idf = min(count, ring->WriteEnd);
    memcpy(array, ring->Data + ring->ReadEnd, idf);
    ring->ReadEnd += idf;
  }

  return count;
}

static __always_inline
size_t ringGetObjectSize(struct ring* ring)
{
    return ring->ObjectSize;
}

