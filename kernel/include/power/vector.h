/* vector.h
   Purpose: implementation of a growable, contiguous,
            collection of data */
#pragma once

#include <power/config.h>
#include <stddef.h>

struct vector {
    void** Data;
    size_t Length;
    size_t Capacity;
    
    int Flags;
};

#define vectorIterator(I, ve, t, va)                                           \
    for (size_t I = 0, t va = (ve).Data[0]; I < (ve).Length; (I)++, va = (ve).Data[I])

struct vector vectorCreateEx(size_t initialLength, int flags);

static __always_inline
struct vector vectorCreate(size_t initialLength)
{
    return vectorCreateEx(initialLength, 0);
}

void vectorDestroy(struct vector* vector);
void vectorInsert(struct vector*, void* data);

void vectorMerge(struct vector* mergee, const struct vector* merged);

void* vectorRemove(struct vector*, void* data);
void* vectorRemoveIndex(struct vector*, int index);
