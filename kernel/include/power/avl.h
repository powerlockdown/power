/* avl.h
   Purpose: generic avl tree */
#pragma once

#include <stdint.h>

struct avl_node;

struct avl_tree {
    struct avl_node* avt_root;
};

struct avl_tree* avlCreateTree();
void avlDestroyTree(struct avl_tree* tree);

void avlInsert(struct avl_tree* tree, uint64_t key, void* value);
void* avlLookup(struct avl_tree* tree, uint64_t key);
void* avlRemove(struct avl_tree* tree, uint64_t key);
void* avlRemoveRoot(struct avl_tree* tree);
void* avlLookupClosestNode(struct avl_tree* tree, uintptr_t* key);
