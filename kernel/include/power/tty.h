/* tty.h
   Purpose: terminal output */
#pragma once

#include "power/scheduler.h"
#include <power/config.h>
#include <power/device.h>
#include <power/ring_buffer.h>

#include <stdarg.h>

struct tty {
  int InputFlags;
  int OutputFlags;
  int ControlFlags;
  int LocalFlags;
  
  unsigned int SlaveOwner; /*< Owner of slave end. */
  unsigned int MinLength;
  
  struct s_semaphore* BufferLock;
  struct s_event* MasterEvent;
  struct s_event* SlaveEvent;
  
  struct ring* MasterBuffer;
  struct ring* SlaveBuffer;
  
  major_t* Slave;
};

int ptyWriteMaster(struct handle* handle, const void* buffer, int length);
int ptyReadMaster(struct handle* handle, void* buffer, int length);
int ptyWriteSlave(struct handle* handle, const void* buffer, int length);
int ptyReadSlave(struct handle* handle, void* buffer, int length);
unsigned long ptyioctl(struct handle* handle, unsigned long command, void* buffer,
		       size_t length);

void ptyCreateTty(struct tty*);
void ptyInit();

void trmInit();
void trmSetEnable(bool enable);
void trmGoTo(int column, int row);

void trmLog(const char* msg);
void trmLogf(const char* msg, ...);
void trmLogfv(const char* msg, va_list va);

void trmLogfn(const char* msg, ...);
void trmLogl(const char* msg, size_t length);

struct s_semaphore* trmGetFramebufferLock();

