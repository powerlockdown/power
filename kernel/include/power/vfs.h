/* vfs.h
   Purpose: virtual file system */
#pragma once

#include <power/config.h>
#include <power/um.h>      /*< sysc_regs */
#include <power/driver.h> /*< driver_mounted_fs_interface */

#define FS_VOLUME_NAME_MAX 32

/* convert byte offset `b` to an LBA address */
__always_inline
static void vfsOffsetLba(uint64_t* offset, int* lps)
{
    if ((uint64_t)*lps > *offset) {
        (*lps) = 0;
        return;
    }

    uint64_t o = *offset;
    int l = *lps;

    *lps = (o / l);
    *offset = o - (*lps * l);
}

__always_inline
static int vfsIsAbsolute(const char* path)
{
    return (path[1] == ':' || path[2] == ':');
}

struct fs_mountpoint
{
    struct fs_mountpoint* Next;

    char Letter[2]; /*< Up to 99 + 26 mount letters possible. */
    char VolumeName[FS_VOLUME_NAME_MAX];
    size_t BlockSize;
    
    struct driver_mounted_fs_interface* Ops;
    size_t ScratchBufferLength;
    void* ScratchBuffer;
};

struct fs_file /*< Represents a open file handle. */
{
    int Flags;
    size_t Size;
    struct fs_mountpoint* Mounted;
    int Bad; /*< If the driver function failed (Bad != FS_ERROR_OK), 
                  this entire struct is invalid. */

    size_t Offset;
    void* Reserved; /*< for FSes */
    const char* Name;
};

int vfsComputeAbsPath(const char* base, 
                      const char* relp,
                      char* restrict buffer, size_t size);

void vfsInit();

struct driver_disk_device_interface;

struct fs_mountpoint* vfsCreateFilesystem(const char* filesystem,
                                          uint64_t beginLba,
                                          struct driver_disk_device_interface* device);

size_t vfsGetPathComponent(const char* path);
struct fs_file* vfsOpenFile(const char* at, 
                            const char* path, int flags);
void vfsCloseFile(struct fs_file* file);
size_t vfsReadFile(struct fs_file* file, uint8_t* buffer, size_t size);
int vfsWriteFile(struct fs_file* file, const uint8_t* buffer, size_t size);

const char* pvfsAnalyzePath(const char* path, const char** premainder,
                            size_t* length, struct fs_mountpoint** mp);

uint64_t syscOpenFile(union sysc_regs* regs);
uint64_t syscReadFile(union sysc_regs* regs);
uint64_t syscWriteFile(union sysc_regs* regs);
uint64_t syscSetFilePointer(union sysc_regs* regs);
uint64_t syscMountVolume(union sysc_regs* regs);
uint64_t syscGetRealPath(union sysc_regs* regs);
uint64_t syscGetFileStatistics(union sysc_regs* regs);
