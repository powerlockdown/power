/* list.h
   Purpose: linked list implementation */
#pragma once

#include <power/config.h>
#include <power/assert.h>

struct list_item {
    struct list_item* Previous;
    struct list_item* Next;
};

#define listForEach(L, v) for (struct list_item* v = (struct list_item*)(L); (v); v = (v)->Next)
#define listCast(L, t, m) (t  *)(((unsigned long)L) - offsetof(t, m))

__always_inline
static void listInit(struct list_item* list)
{
    list->Next = NULL;
    list->Previous = NULL;
}

__always_inline
static inline void listInsert(struct list_item* list, struct list_item* item)
{
    while (list->Next != NULL) {
        list = list->Next;
    }
    
    list->Next = item;
    item->Previous = list;
    item->Next = NULL;
}

static inline void listRemove(struct list_item* item)
{
    struct list_item* p = item->Previous;
    struct list_item* x = item->Next;

    if (x)
        x->Previous = p;
    if (p)
        p->Next = x;
}

static inline void listEmplace(struct list_item* list, struct list_item* item)
{
    struct list_item* oldnext = list->Next;
    list->Next = item;
    item->Previous = list;
    item->Next = oldnext;
}

static inline
struct list_item* listAfter(struct list_item* list, int count)
{
    struct list_item* li = list;
    for (int i = 0; i < count; i++) {
        li = li->Next;
        if (!li)
            return NULL;
    }

    return li;
}

static inline
struct list_item* listAppend(struct list_item* list, struct list_item* item)
{
  struct list_item* tmp;
  tmp = list;
  
  while (tmp->Next != NULL)
    tmp = tmp->Next;

  list->Next = item;
  item->Previous = list;
  return item;
}
