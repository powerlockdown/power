/* paging.h
   Purpose: Memory paging */
#pragma once

#include <power/i386/pgtables.h>
#include <power/interrupt.h>

/* TODO: This is full of deprecated concepts.
   Please remove them. */

int pgPageFault(struct irq_regs* irq,
		uintptr_t address, int flags);
void* pgCreateAddressSpace();
void pgDestroyAddressSpace(address_space_t* address);

void pgSetCurrentAddressSpace(address_space_t* address);
void pgMapPage(address_space_t* addrs, uint64_t paddr, uint64_t vaddr,
               uint64_t flags);
void pgMapLargePage(address_space_t* addrs, uint64_t paddr, uint64_t vaddr,
                    uint64_t flags);
void pgUnmapPage(address_space_t* addrs, uint64_t vaddr);

void pgMapStaticPage(uint64_t paddr, uint64_t vaddr, uint64_t flags);

uint64_t pgMapUser(address_space_t* addrs, uint64_t paddr, size_t pages,
                   int flags);

uint64_t pgGetRawPage(address_space_t* addrs, uintptr_t address, int* psize);

int pgGuardPage(address_space_t*, uint64_t vaddr, size_t length, int flags);
void* pgUnguardPage(address_space_t*, uint64_t vaddr);

void pgFillStaticTables();
void pgAddGlobalPage(uint64_t paddr, uint64_t vaddr, uint64_t flags);
void* pgGetPhysicalAddress(address_space_t* addrs, uint64_t vaddr);
address_space_t* pgGetAddressSpace();

void pgDumpPageTable(address_space_t* address, uint64_t vaddr);
void pgRecycleDirectory(address_space_t* source, address_space_t* target,
                        uint64_t vaddr);
