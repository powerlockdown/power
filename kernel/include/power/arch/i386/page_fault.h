/* page_fault.h */
#pragma once

#define PFAULT_PPRESENT_BIT (1 << 0)
#define PFAULT_WRITE_BIT    (1 << 1)
#define PFAULT_USER_BIT     (1 << 2)
#define PFAULT_INSTRUCTION_BIT (1 << 4)
