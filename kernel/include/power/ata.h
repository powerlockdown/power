/* ata.h
   Purpose: definitions for ATA generic library. */
#pragma once

#include <power/scsi.h>

struct ata_taskfile;
struct ata_device;

/* Maximum LBA represented by LBA28. */
#define ATA_LBA28_MAX 0xFFFFFFF

/* Device supports DMA. */
#define ATA_DEVICE_DMA_BIT (1 << 0)

/* Device does not support LBAs. */
#define ATA_DEVICE_CHS_ONLY_BIT (1 << 1)

/* Device is ATAPI. */
#define ATA_DEVICE_ATAPI_BIT    (1 << 2)

/* ATAPI device requires DMADIR for PACKET commands. */
#define ATA_PACKET_DMADIR_BIT   (1 << 3) 

enum {
    ATA_RESET_STRONGNESS_HARD, /*< Hardware reset */
    ATA_RESET_STRONGNESS_SOFT /*< Software reset (SRST) */
};

enum {
  ATA_TRANSFER_PIO, /*< Transfer through PIO. */
  ATA_TRANSFER_DMA /*< Place onto DMA buffers. */
};

struct ata_device_ops {
  int (*submitCommand)(struct ata_device*,
		       struct ata_taskfile* restrict);
  
  int (*recv)(struct ata_device*, int transfer,
		 void* restrict buffer,
		 unsigned int length);
  int (*send)(struct ata_device*, int transfer,
		 const void* buffer,
		 unsigned int length);
  
  int (*reset)(struct ata_device*, int hard);
  int (*atapiCommand)(struct ata_device* device,
		      const void* buffer, int length);
};

/** Base class for ATA-based devices. */
struct ata_device {
  struct scsi_device Base;

  unsigned int SectorSize;
  
  unsigned int Flags;
  
  uint64_t SectorCount;
  unsigned long DmaLimit;
  char Model[40];

  struct ata_device_ops Ops;
};

/** A type representing an ATA register. */
typedef unsigned short ata_reg_t;

/** State of every register in ATA. */
struct ata_taskfile {
  ata_reg_t LbaLow;
  ata_reg_t LbaMid;
  ata_reg_t LbaHigh;
  
  ata_reg_t Data;
  ata_reg_t Error;
  ata_reg_t Features;
  
  ata_reg_t SectorCount;
  ata_reg_t DriveHead;

  union {
    ata_reg_t Status;
    ata_reg_t Command;
  };
};

/** AttachDevice hint:
    Device is known to be ATAPI;
    do not send commands known to be not implemented. */
#define ATA_ATTACH_HINT_ATAPI 1

/** Attaches (initializes necessary state) for
    a given device. Ops must already be set.
    
    SCSI might also be init if ATAPI.*/
int ataAttachDevice(struct ata_device* dev, int dmabsize,
		    int flags);


