/* elf.h
   Purpose: ELF definitions, program loader
            and dynamic linker. */
#pragma once

#include <power/config.h>
#include <power/vector.h>

#include <power/memory/vm.h>
#include <power/paging.h>

/* EXEC_LIMIT is not really enforced by the kernel!!! */

#define EXEC_BASE 0xDCA0000000
#define EXEC_LIMIT 0xFFFFFFFF
#define EXEC_END (EXEC_BASE + EXEC_LIMIT)

#define UME_BASE  0x54000000000

#define LD_STATE_LOADING 0
#define LD_STATE_LOADED_RELOCATE 1
#define LD_STATE_LOADED 2

#define alignUp(P, l) P + l - P % l

#define ELF_FTYPE_RELOC 1
#define ELF_FTYPE_EXEC 2
#define ELF_FTYPE_SHARED 3
#define ELF_FTYPE_CORE 4

struct __packed elf_file_header {
    uint8_t Magic[4];
    uint8_t Architecture;
    uint8_t Endianness;
    uint8_t HeaderVersion;
    uint8_t Abi;
    uint64_t Padding;

    uint16_t FileType;
    uint16_t InstructionSet;
    uint32_t FileVersion;
    uint64_t EntryPointAddress;
    uint64_t ProgramHeaderTablePosition;
    uint64_t SectionHeaderTablePosition;
    uint32_t Flags;

    uint16_t HeaderSize;
    uint16_t ProgramHeaderSize;
    uint16_t ProgramHeaderLength;
    uint16_t ProgramSectionSize;
    uint16_t ProgramSectionLength;
    uint16_t NamesSectionIndex;
};

#define ELF_PROGTYPE_NULL 0
#define ELF_PROGTYPE_LOAD 1
#define ELF_PROGTYPE_DYNAMIC 2
#define ELF_PROGTYPE_INTERPRETER 3
#define ELF_PROGTYPE_NOTE 4

#define ELF_PROGFLAGS_EXEC 1
#define ELF_PROGFLAGS_WRITE (1 << 1)
#define ELF_PROGFLAGS_READ (1 << 2)

struct __packed elf_program_header {
    uint32_t SegmentType;
    uint32_t Flags;
    uint64_t DataOffset;
    uint64_t VirtualAddress;
    uint64_t Reserved; /*< Some ABIs define this as the
                          physical address. */
    uint64_t FileSize;
    uint64_t MemorySize;
    uint64_t Alignment;
};

#define ELF_SHN_UNDEF 0

struct __packed elf_section_header {
    uint32_t NameOffset;
    uint32_t SectionType;
    uint64_t Flags;
    uint64_t Address;
    uint64_t FileOffset;
    uint64_t SectionSize;
    uint32_t SectionLink;
    uint32_t Info;
    uint64_t AddressAlignment;
    uint64_t TableEntrySize;
};

struct __packed elf_rela_item {
    uint64_t Offset;
    uint64_t Info;
    uint64_t Addend;
};

struct __packed elf_dynamic_item {
    int64_t Type;
    uint64_t Value; /*< or Address. */
};

struct __packed elf_symtab_item 
{
	uint32_t NameOffset;
	uint8_t Info;
	uint8_t Other;
	uint16_t SectionHeaderTableIndex;
	uint64_t Value;
	uint64_t Size;
};

/* Create a new address space for the file */
#define ELF_LOAD_NEW_ADDRESS_SPACE (1 << 2)

/* The created address space is readable by CPL 3. */
#define ELF_LOAD_USER_ADDRESS_SPACE (1 << 4)

/* The loaded executable is a driver. */
#define ELF_LOAD_DRIVER (1 << 5)

/* Load a shared library (aka include the created pages
   inside the returned structure) */
#define ELF_LOAD_SHARED_LIB (1 << 9)

#define ELF_NO_DYN_RESOLVE (1 << 10)

#define ELF_LOAD_ERROR_OK 0 /* File successfully loaded */
#define ELF_LOAD_ERROR_NOT_EXECUTABLE 1 /* Not an executable */
#define ELF_LOAD_ERROR_WRONG_ARCHITECTURE 2 /* Architecture/ISA mismatch */

struct elf_executable {
  struct elf_file_header* FileHeader;
  address_space_t* AddressSpace;
  struct vm* VirtualMemory;
  
  void* EntryPoint;
  uint64_t Base;
  
  int Result;
  int Flags;

  struct vector LoadedPages;
  struct vector LoadedLibraries;
  
};

struct loaded_library;

struct elf_executable modLoadElf(const uint8_t* file, int flags);
struct elf_executable modLoadElfEx(const uint8_t* file,
                                   struct vm* address, int flags);
struct loaded_address_space* modReplicatePages(struct vm* addrs,
                                               struct loaded_library* exec,
                                               int flags);
void modFreeElf(struct elf_executable*);

void modUnloadElfPages(struct elf_executable*);

/* NULL will make the function search in the kernel
   symtab */
void* modGetSymbolElf(struct elf_executable*, const char* symbol);
bool elfSlowCompare(const char* s1, const char* s2);

struct loaded_address_space {
    struct vm* VirtualMemory;
    struct vector LoadedReplicatedPages; /*< of elf_lpage. */
};

struct loaded_library {
    char* Name;
    size_t FileSize;
    struct elf_file_header* File;
    uint64_t Base;
    struct vector Pages;
    struct vector LoadedAddresses; /*< Address spaces this
                                       library was loaded inside.
                                       Vector of loaded_address_space */
    struct vector Dependencies; /*< Libraries that this lib depends on.
                                    Vector of loaded_library. */
    int ReferenceCount; /*< Number of times this library was loaded */
    int Flags;
};

struct loaded_page {
    uint64_t VirtualAddress;
    uint64_t PhysicalAddress;
    uint64_t Flags;
    int Length;
};

struct elf_lpage {
  int Length; /*< In page count. */
  int Flags;
  uintptr_t Address;
};

/* Convert a loaded_library to a elf_executable.
   The loaded_library is still owner of its pointers. */
static inline void modLibraryToExecutable(const struct loaded_library* lib,
					  struct vm* vm, 
                                          struct elf_executable* exec)
{
  exec->VirtualMemory = vm;
  exec->AddressSpace = vm->AddressSpace;
  exec->Base = lib->Base;
  exec->EntryPoint = NULL; /*< Shared libs have no entry point,
			     not here, at least. */
  exec->FileHeader = lib->File;
  exec->LoadedLibraries = lib->Dependencies;
  exec->Flags = lib->Flags;
  exec->LoadedPages = lib->Pages;
}

void modResolveElfDynamicLibrary(const struct loaded_library*);
void modResolveElfDynamic(const struct elf_executable*);
void modUnloadProgram(const struct elf_executable*);
void modUnloadLibrary(const struct elf_executable* exec,
                      struct loaded_library* lib);

const struct elf_section_header* modGetSection(const struct elf_executable* exe,
                                               const char* name);

/* See SysV ABI for relocation types and calcutation formulas and 
   <https://stevens.netmeister.org/631/elf.html> for use cases for them. */
#define ELF64_R_X86_64_COPY 5
#define ELF64_R_X86_64_GLOB_DAT 6
#define ELF64_R_X86_64_JUMP_SLOT 7
#define ELF64_R_X86_64_RELATIVE 8

/* TODO We are missing a few relocations used for TLS and other stuff. */

/* Macros from the ELF-64 Object File Format document.
   <https://uclibc.org/docs/elf-64-gen.pdf> */
#define ELF64_R_SYM(I) ((I) >> 32)
#define ELF64_R_TYPE(I) ((I) & 0xffffffffL)
#define ELF64_R_INFO(S, t) (((S) << 32) + ((t) & 0xffffffffL))


/* Relocate X86_64_JUMP_SLOT and X86_64_GLOB_DAT symbols
   located inside relocation section `section` 
   against library `lib` */
void elfRelocateAbsoluteSymbols(const struct elf_executable* exec, 
                                struct loaded_library* lib, 
                                const char* section);

/* Perform other relocations which are not covered by AbsoluteSymbols. */
void elfRelocateRelative(const struct elf_executable* exec, const char* section);

struct elf_hash
{
    uint32_t BucketCount;
    uint32_t ChainCount;
};

void* modSearchHashSymbol(struct elf_executable* exec, const char* symbolName);
void elfRelocateAllLibraries(const struct elf_executable* exe, const char* section);
