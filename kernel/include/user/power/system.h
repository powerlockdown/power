/* system.h
   Purpose: Kernel API */
#pragma once

#include "types.h"

/* System call slots */
#define SYS_OPENFILE     1
#define SYS_WRITEFILE    2
#define SYS_READFILE     3
#define SYS_CLOSEFILE    4
#define SYS_GETCOMMANDLINE 5
#define SYS_EXITPROCESS    6
#define SYS_MAPPAGES       7
#define SYS_UNMAPPAGES     8
#define SYS_SETFILEPOINTER 9
#define SYS_DEVICEIOCTL    10
#define SYS_CREATEPROCESS  11
#define SYS_GETCURRENTWD   12
#define SYS_MOUNTVOLUME    13
#define SYS_CREATEPIPE     14
#define SYS_SIGHANDLER     15
#define SYS_RETFSIGNAL     16
#define SYS_RAISESIGNAL    17
/* Slot 18 is reserved. */
#define SYS_CREATETHREAD   19
#define SYS_GETCRTHREADID  20
#define SYS_KILLTHREAD     21
#define SYS_GETTIMEOFDAY   22
#define SYS_GETTIMEOFCLOCK 23
#define SYS_VIRTUALFILEMAP 24
#define SYS_GETREALPATH    25
#define SYS_SETWORKINGDIR  26
#define SYS_GETFILESTATS   27
#define SYS_FSGSCTL        28
#define SYS_SETTHRSUSPND   29
#define SYS_WAITFSIGNAL    30
#define SYS_SETSIGNALMASK  31
#define SYS_SENDPRSIGNAL   32
#define SYS_WAITFPROCESS   33
#define SYS_GETCRPROCESSID 34
#define SYS_GETHANDELTYPE  35
#define SYS_SLEEP          36
#define SYS_DUPHANDLE      37
#define SYS_WAITFORTHREAD  38
#define SYS_GETENVSTR      39
#define SYS_SETENVSTR      40


/* Flags for OpenFile */
#define OPENFILE_READ  (1 << 0) /*< The file was open for read */
#define OPENFILE_WRITE (1 << 1) /*< The file was open for write */

#define OPENFILE_TRUNC (1 << 15) /*< Truncate if file open for write
				     already exists. */
#define OPENFILE_CREATE (1 << 16) /*< Create if file open for write
				      does not exists. */

/* Flags for SetFilePointer */
#define SFILEPTR_REL_BEGIN 0
#define SFILEPTR_REL_END   1
#define SFILEPTR_REL_CURRENT 2

#define STREAM_STDOUT 0
#define STREAM_STDIN 2
#define STREAM_STDERR 1
