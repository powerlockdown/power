/* handle.h
   Purpose: definitions used for handle. */
#pragma once

#define HANDLE_TYPE_TTY 1
#define HANDLE_TYPE_FILE 2
#define HANDLE_TYPE_DEVICE 3
#define HANDLE_TYPE_PIPE 4
