/* filestats.h
   Purpose: Query file statistics */
#pragma once

#include "power/types.h"

typedef __uint64 __inode;

struct __filestats {
  __uint32 Mode;
  __uint32 LinkCount;
  __uint32 Blocks;
  
  __uint16 UserId;
  __uint16 GroupId;
  __uint16 BlockSize;
  
  __uint64 AccessTime;
  __uint64 ModifiedTime;
  __uint64 StatusTime;
  
  __uint64 Size;
  __inode SerialNumber;
};

