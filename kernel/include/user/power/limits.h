/* limits.h
   Purpose: maximum values accepted by the kernel. */
#pragma once

/* ifdef _POWER_SOURCE: The kernel uses these macros
   to enforce the limits. This means you can freely
   edit them to make the kernel less limiting for your
   needs. endif. */

/* The maximum number of handle
   a process might have concurrently open
   at any given time. */
#define MAX_HANDLES 64

#define INVALID_HANDLE (MAX_HANDLES+1)
#define THIS_THREAD (INVALID_HANDLE+1)
#define THIS_PROCESS THIS_THREAD

/* The maximum number of characters a path
   might have. */
#define MAX_PATH 320

/* The maximum value for all threads'
   stack size sum for any given process.
   
   The kernel enforces thread limit not
   by number of threads, but by the sum
   of their stack size. Which cannot go
   more than what this macro is   ought
   to be. 
   
   Because all threads have a fixed size
   of 70KiB. A process is currently limi-
   ted to approx. 14 threads.
   
   TODO: Implement this */
#define MAX_THREAD_STK (1 * 1024 * 1024)

#define MAX_ENV_STR 1024
