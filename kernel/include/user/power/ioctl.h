/* ioctl.h
   Purpose: ioctl definitions 
        (especially when DEVICE param is NULL) */
#pragma once

#include "types.h"

#ifndef _POWER_IOCTL_H
#define _POWER_IOCTL_H

/* These structs are deprecated... */

/* Disk IOCTLs */
#define IOCTL_GET_DISKS 1
#define IOCTL_OPEN_DISK 2
#define IOCTL_CLOSE_DISK 3
#define IOCTL_SEARCH_PARTITION 4

struct ioctl_get_disk
{
    __uint64 Length;
    __int32* Data;
};

union ioctl_uuid
{
    struct {
        __uint32 Data1;
        __uint16 Data2;
        __uint16 Data3;
        __uchar Data4[2];
        __uchar Data5[6];
    };
    __uchar Raw[16];
};

#define SEARCH_PARTITION_BY_INDEX_BIT (1 << 0)

struct ioctl_search_partition
{
    int Disk;
    int Flags;
    union ioctl_uuid Identifier;
};

/* Input IOCTLs */
#define IOCTL_GET_INPUT_BUFFER 10

/* Video/Early Terminal IOCTL */
#define IOCTL_GET_FRAMEBUFFER_LOCK 40 /*< Framebuffer address in arg3 */
#define IOCTL_RELEASE_FRAMEBUFFER_LOCK 41
#define IOCTL_ETERM_STOP_PRINTING 42

#define IOCTL_SET_EXIT_CODE 50
#define IOCTL_KERNEL_MISC_CMD_END 120

/* Processes IOCTL */
#define IOCTL_WAIT_FOR_OBJECT 120
#define IOCTL_GET_EXIT_CODE   121

#define IOCTL_HANDLE_SET_BLOCKING 180

#endif
