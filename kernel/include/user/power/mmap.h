/* mmap.h
   Purpose: Definitions for the VirtualMap system call. */
#pragma once

/* Flags for protection are reserved
   for the upper 10 bits of flags param.
   Mapping flags are for the lower 22. */

#define VMAP_ANONYMOUS (1 << 1) /*< Not backed by file  */
#define VMAP_FIXED     (1 << 2) /*< Fail if mapping is already present there. */
#define VMAP_POPULATE  (1 << 3) /*< Preallocate all pages instead of waiting for page faults */

#define VPROT_WRITE    (1 << 31) /*< Capability to write. */ 
#define VPROT_EXECUTE  (1 << 30) /*< Capability to execute. */

/* A funny way I thought to implement denying VPROT_READ
   was simply not mapping the page at all. However this
   would not allow for combinations like write-allowed,
   read-denied.

   As such, I think that POSIX people favoured too much
   hardware abstraction and forgot the practical limitations
   of it. */

#define VMAP_MASK      (0xFFFFFFFF)
#define VPROT_MASK     (0xFFFFFFFF << 15)
