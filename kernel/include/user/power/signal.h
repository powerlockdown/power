/* signal.h
   Purpose: signal handling */
#pragma once

#ifndef _POWER_SIGNAL_H
#define _POWER_SIGNAL_H

#include <power/types.h>

typedef void (*__sig_handler_pfn_t)(int);

enum {
#define SIGABRT SIGABRT
   SIGABRT = 1,
#define SIGSEGV SIGSEGV
   SIGSEGV,
#define SIGBUS SIGBUS
   SIGBUS,
#define SIGCHLD SIGCHLD
   SIGCHLD,
#define SIGCONT SIGCONT
   SIGCONT,
#define SIGFPE SIGFPE
   SIGFPE,
#define SIGHUP SIGHUP
   SIGHUP,
#define SIGILL SIGILL
   SIGILL,
#define SIGUSR1 SIGUSR1
   SIGUSR1,
#define SIGUSR2 SIGUSR2
   SIGUSR2,
#define SIGKILL SIGKILL
   SIGKILL,
#define SIGINT SIGINT
   SIGINT,
#define SIGTERM SIGTERM
   SIGTERM
};

enum {
   SIGACTION_ROUTINE, /*< Use the routine field in the handler. */
   SIGACTION_KILL,
   SIGACTION_IGNORE
};

struct __sig_stack { /*< Structure used for SigSetStack. */
   int ss_flags; /*< Reserved. */

   void* ss_sp;
   __uint32 ss_size; /*< POSIX says this should be a size_t
                          but who is going to put more than 
                          1MB on their signal stack? */
};

struct __sig_info { /*< This struct only provides the fields
                        that cannot be easily fetched by the C library. */
   int si_signal;
   int si_code;
   __uint32 si_pid;

   void* si_addr;
   __uint32 si_uid; /*< Reserved for now. */
};

struct __sig_context {
   long sc_r8;
   long sc_r9;
   long sc_r10;
   long sc_r11;
   long sc_r12;
   long sc_r13;
   long sc_r14;
   long sc_r15;

   long sc_rax;
   long sc_rbx;
   long sc_rcx;
   long sc_rdx;
   long sc_rdi;
   long sc_rsi;
   long sc_rbp;

   long sc_rsp;
   long sc_rip;
};

#define SIGMASK_UNMASK 0 /*< Remove these from mask (but keep the rest). */
#define SIGMASK_MASK   1 /*< Add these to the mask. */
#define SIGMASK_SET    2 /*< Replace the current mask with this one. */

#endif
