/* time.h
   Purpose: keep track of time. */
#pragma once

#include "power/types.h"

#ifdef _POWER_SOURCE
#include_next <power/time.h>
#endif

struct __wallclock {
   int wk_year;
   int wk_month;
   int wk_day;

   int wk_hour;
   int wk_minute;
   int wk_second;
};

typedef __uint64 __monclock;