/* pty.h
   Purpose: support for pseudoterminals. */
#pragma once

#define MUX_OPENTTY 0x2300 /*< Create a new PTY */

#define TTY_SETCFG 0x2400 /*< Set TTY configuration. */
#define TTY_GETCFG 0x2401 /*< Get TTY configuration. */
#define TTY_GETSLN 0x2402 /*< Get slave device name. */

/* TTY local flags. */
#define PTY_LECHO    (1 << 0) /*< Echo terminal input */
#define PTY_LECHOERS (1 << 1) /*< Echo ERASE */
#define PTY_LECHOK   (1 << 2) /*< Echo KILL */
#define PTY_LECHONL  (1 << 3) /*< Echo NL (new line) */
#define PTY_LICANON  (1 << 4) /*< Canonical (line-based) input */
#define PTY_LIEXTEN  (1 << 5) /*< NOP. Enable impl-defined functions. None are defined currently */
#define PTY_LISIG    (1 << 6) /*< Enable signals. */
#define PTY_NOFLSH   (1 << 7) /*< Disable flush after intr. */
#define PTY_TOSTOP   (1 << 8) /*< Send SIGTTOU for background output. */

/* TTY input flags */
#define PTY_ICRTONL (1 << 1)
#define PTY_IGNBRK  (1 << 2)
#define PTY_IGNCR   (1 << 3)
#define PTY_INLCR   (1 << 4)
#define PTY_INPCK   (1 << 5)
#define PTY_ISTRIP  (1 << 6)
#define PTY_IXANY   (1 << 7)
#define PTY_IXOFF   (1 << 8)
#define PTY_IXON    (1 << 9)
#define PTY_BRKINT  (1 << 10)
#define PTY_PARMRK  (1 << 11)
#define PTY_IGNPAR  (1 << 12)

/* TTY output flags */
#define PTY_OPOST (1 << 0)
#define PTY_ONLCR (1 << 1)
#define PTY_OCRNL (1 << 2)
#define PTY_ONOCR (1 << 3)
#define PTY_ONLRET (1 << 4)
#define PTY_OFDEL  (1 << 5)
#define PTY_OFILL  (1 << 6)

#define PTY_NLDLY  (3 << 7)
#define PTY_NL0    (1 << 7)
#define PTY_NL1    (1 << 8)

#define PTY_CRDLY (15 << 9)
#define PTY_CR0   (1 << 9)
#define PTY_CR1   (1 << 10)
#define PTY_CR2   (1 << 11)
#define PTY_CR3   (1 << 12)

#define PTY_TABDLY (15 << 13)
#define PTY_TAB0   (1 << 13)
#define PTY_TAB1   (1 << 14)
#define PTY_TAB2   (1 << 15)
#define PTY_TAB3   (1 << 16)

#define PTY_BSDLY (3 << 16)
#define PTY_BS0   (1 << 16)
#define PTY_BS1   (1 << 17)

#define PTY_VTDLY (3 << 18)
#define PTY_VT0   (1 << 18)
#define PTY_VT1   (1 << 19)

#define PTY_FFDLY (3 << 20)
#define PTY_FF0   (1 << 20)
#define PTY_FF1   (1 << 21)

#define PTY_CC_LENGTH 9

/* cc subscripts */
#define PTY_VMIN 0
#define PTY_VTIME 1
#define PTY_VERASE 2
#define PTY_VINTR  3
#define PTY_VKILL  4
#define PTY_VQUIT  5
#define PTY_VSTART  6
#define PTY_VSTOP   7
#define PTY_VSUSP   8

/* PTY control flags */
#define PTY_CSIZE (15)
#define PTY_CS5   (1)
#define PTY_CS6   (1 << 1)
#define PTY_CS7   (1 << 2)
#define PTY_CS8   (1 << 3)

#define PTY_CSTOPB (1 << 4)
#define PTY_CREAD  (1 << 5)
#define PTY_PARENB (1 << 6)
#define PTY_PARODD (1 << 7)
#define PTY_HUPCL  (1 << 8)
#define PTY_CLOCAL (1 << 9)

struct _pty {
  int pty_lflag;
  int pty_oflag;
  int pty_iflag;
  int pty_cflag;

  int pty_cc[PTY_CC_LENGTH];
};
