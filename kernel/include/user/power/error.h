/* error.h
   Purpose: Definitions of error codes for syscalls */
#pragma once

#define ERROR_OK         0 /*< Everything went OK. */
#define ERROR_NOT_FOUND  1 /*< Resource not found. */
#define ERROR_NO_SYSCALL 2 /*< Unknown syscall */
#define ERROR_NO_DEVICE  3 /*< The device is empty. */
#define ERROR_INVALID_RANGE 4 /*< The value does not fall within the expected range.*/
#define ERROR_FORBIDDEN     5 /*< No permission to access resource. */
#define ERROR_INVALID_ARGUMENT 6 /*< An argument was invalid. */
#define ERROR_TIMED_OUT        7 /*< Timed out while waiting for resource. */
#define ERROR_IO               8 /*< An physical I/O error has occured. */
#define ERROR_NOT_SUPPORTED    9 /*< File/device does not support this operation. */
#define ERROR_PIPE_BROKEN      10 /*< Broken pipe. */
#define ERROR_BAD_HANDLE       11 /*< Bad file handle. */
#define ERROR_ALREADY_EXISTS   12 /*< Resource already exists. */
#define ERROR_NO_MEMORY        13 /*< Not enough memory to fulfill the operation. */
#define ERROR_UNAVAILABLE      14 /*< Resource unavailable. Try again later. */
#define ERROR_WOULD_BLOCK      15 /*< Resource set to non-blocking would block the flow. */
#define ERROR_USER_FAULT       16 /*< Invalid buffer supplied */
#define ERROR_TOO_MANY_HANDLES 17 /*< Too many handles. */
#define ERROR_INVALID_EXECUTABLE 18 /*< Attempt to execute invalid file. */
#define ERROR_INTERRUPTED        19 /*< System call interrupted. */
#define ERROR_READ_ONLY_FILESYSTEM 20 /*< Filesystem was not mounted with write support. */
#define ERROR_PROCESS_DIED         21 /*< Resource killed during operation. */
#define ERROR_WOULD_DEADLOCK       22 /*< Deadlock prevented. */

#define ERROR_TOO_BIG ERROR_INVALID_RANGE /*< Old name for this error. */
#define MAX_ERRORS 1023

#ifdef _POWER_SOURCE

#define ERROR_INTO_PTR(P) (void*)(P)
#define ERROR_FROM_PTR(P) (uintptr_t)(P)
#define ERROR_PTR(P) ((intptr_t)(P) < 0 && (intptr_t)(P) >= -MAX_ERRORS)

#endif
