/* types.h
   Purpose: Type definitions used inside 
            user kernel headers */
#pragma once

#ifndef _POWER_TYPES_H
#define _POWER_TYPES_H

typedef signed char __char;
typedef unsigned char __uchar;

typedef short __int16;
typedef unsigned short __uint16;

typedef int __int32;
typedef unsigned int __uint32;

typedef long __int64;
typedef unsigned long __uint64;

typedef __int32 __handle;

struct _create_process_ex {
  __uint64 CommandLineArgs;
  __uint64 Environment;
  
  __handle StdoutHandle;
  __handle StdinHandle;
  __handle StderrHandle;

  __uint64 Reserved;
};

#endif

