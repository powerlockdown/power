/* framebuffer.h
   Purpose: Framebuffer video mode */
#pragma once

#ifndef _POWER_VIDEO_FRAMEBUFFER_H
#define _POWER_VIDEO_FRAMEBUFFER_H 1

#include <power/types.h>

/* IOCTLs. */
#define FB_GET_TOPOLOGY 0x1000

struct video_framebuffer {
    __uint64 Address;

    __uint32 Pitch;
    __uint32 Height;
    __uint32 Width;
    __uint16 BitsPerPixel;
};

#endif
